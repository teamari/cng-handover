-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: mlink_ari_sys
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `class` varchar(30) DEFAULT NULL,
  `parentid` int(11) DEFAULT '0',
  `menuorder` int(11) DEFAULT '0',
  `haschildren` smallint(6) DEFAULT '0',
  `adminallowed` smallint(6) DEFAULT '0',
  `normalallowed` smallint(6) DEFAULT '0',
  `reconcilerallowed` smallint(6) DEFAULT '0',
  `candisplay` smallint(6) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (11,'M-pesa Interface',NULL,NULL,'header',0,0,0,1,1,1,1),(12,'Dashboard','dashboard','fa fa-dashboard','dashboard',0,1,0,1,1,0,1),(23,'Account Details','audit',NULL,'header',0,12,0,0,0,0,1),(33,'System Users','users','fa fa-users',NULL,0,7,0,1,0,0,1),(41,'System Reports',NULL,NULL,'header',0,13,0,0,0,0,1),(42,'Audit Report','audit','fa fa-file-pdf-o',NULL,0,4,0,1,0,0,1),(49,'Change Password','changepassword','fa fa-exchange',NULL,0,11,0,1,1,1,1),(53,'Customers','customers','fa fa-user',NULL,0,2,0,1,1,0,1),(54,'Transactions','transactions','fa fa-money','header',0,3,1,1,1,1,1),(55,'Account Details',NULL,NULL,'header',0,6,0,1,1,1,1),(56,'customer_history','customer_history',NULL,NULL,NULL,2,0,1,NULL,0,1),(57,'All Transactions','transactions','','',54,0,0,1,1,0,1),(58,'Pending Allocations','pending_allocations','','',54,0,0,1,1,0,1),(59,'Allocated','allocated','','',54,0,0,1,1,0,1),(60,'Todays Transactions','todaystransactions','','',0,0,0,1,1,0,0),(61,'Pending Allocations','todays_pending_allocations','','',0,0,0,1,1,0,0),(62,'Todays\' Allocations','todays_allocations','','',0,0,0,1,1,0,0),(64,'Reconcile Here','reconciliation','fa fa-file-pdf-o','dashboard',0,0,0,0,0,1,1),(65,'Reconciled','reconciled','','',54,0,0,1,1,1,1),(66,'Monthly Charts','monthlysummary','fa fa-bar-chart','',0,5,0,1,1,0,1),(67,'Report Recipients','reportrecipient','fa fa-envelope',NULL,0,10,0,1,0,0,1),(68,'Simulate','simulate',NULL,NULL,0,0,0,0,0,0,1),(69,'Grant Portal','grantusers','',NULL,71,0,0,1,0,0,1),(70,'Grant Reports','grantreports','',NULL,71,0,0,1,0,0,1),(71,'Grant','grant','fa fa-key','header',0,9,1,1,0,0,1);
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-07 11:21:19
