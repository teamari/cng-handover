-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: mlink_ari_sys
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `tid` bigint(20) NOT NULL AUTO_INCREMENT,
  `recordstate` smallint(6) NOT NULL,
  `CreationDate` datetime DEFAULT NULL,
  `CreationUser` varchar(50) DEFAULT NULL,
  `ApproveDate` datetime DEFAULT NULL,
  `ApproveComment` varchar(200) DEFAULT NULL,
  `DeleteDate` datetime DEFAULT NULL,
  `DeleteUser` varchar(50) DEFAULT NULL,
  `DeleteReason` varchar(200) DEFAULT NULL,
  `UserCode` varchar(50) NOT NULL,
  `Surname` varchar(49) DEFAULT NULL,
  `Firstname` varchar(50) DEFAULT NULL,
  `Secondname` varchar(49) DEFAULT NULL,
  `email` varchar(49) DEFAULT NULL,
  `phone` varchar(49) DEFAULT NULL,
  `LastLoginTime` datetime DEFAULT NULL,
  `UserEnabled` smallint(6) DEFAULT NULL,
  `Password` varchar(150) NOT NULL,
  `BusinessID` varchar(80) DEFAULT NULL,
  `Usertype` varchar(80) DEFAULT NULL,
  `resettime` datetime DEFAULT NULL,
  `isreset` tinyint(4) DEFAULT '0',
  `resetpass` varchar(255) DEFAULT '0',
  `region` varchar(100) DEFAULT NULL,
  `route` varchar(100) DEFAULT NULL,
  `CUST_CD` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`tid`),
  UNIQUE KEY `tid` (`tid`,`UserCode`,`recordstate`)
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,0,'2018-05-05 18:51:26','0','2018-05-05 18:51:26','New System User',NULL,'','','vbnetter@gmail.com','mburu','jonathan','mburu','vbnetter@gmail.com','','2018-05-05 18:51:26',1,'$2y$10$GI6/iAeo2vKpyYvMy80q0eminme/G328AwvI5cicj8xairyOpAage','1','Admin','2019-03-21 12:30:24',0,'$2y$10$GI6/iAeo2vKpyYvMy80q0eminme/G328AwvI5cicj8xairyOpAage',NULL,NULL,NULL),(110,0,'2019-09-19 11:23:11','vlangat@ari.co.ke','2019-09-28 00:09:33','New System User',NULL,'','','nkinyagia@ari.co.ke',NULL,'Ndiang\'ui','Kinyagia','nkinyagia@ari.co.ke','0725992355','2019-09-28 00:09:33',1,'$2y$10$cnwf.FDD3QQBD42SSrxaTuW2sVUdsBUhBKbL2PgMSpQ/bI.VLWPre','1','Admin','2020-03-30 11:10:42',0,'$2y$10$cnwf.FDD3QQBD42SSrxaTuW2sVUdsBUhBKbL2PgMSpQ/bI.VLWPre',NULL,NULL,NULL),(114,0,'2019-10-03 12:43:56','vlangat@ari.co.ke','2019-10-03 12:43:56','New System User',NULL,'','','vlangat@ari.co.ke',NULL,'Victor','Lang\'at','vlangat@ari.co.ke','0717269895','2019-10-03 12:43:56',1,'$2y$10$DhqmTypfVEZ.PPnqLnTJMejuYB8SpwDDyOKzCHnQQuQFR9cOW4zZS','1','Admin','2020-10-05 16:32:23',0,'$2y$10$DhqmTypfVEZ.PPnqLnTJMejuYB8SpwDDyOKzCHnQQuQFR9cOW4zZS',NULL,NULL,NULL),(118,0,'2019-12-19 13:34:07','vlangat@ari.co.ke','2019-12-19 13:34:07','New System User',NULL,'','','viclan29@gmail.com',NULL,'Kipngetich','Lang\'at','viclan29@gmail.com','0717269899','2019-12-19 13:34:07',1,'$2y$10$gj.ke5JF3DfIDdySqP1xVOaNay1kPHpJJ7PmNFYr1xQRUgP.fLAQW','1','Normal',NULL,0,'0',NULL,NULL,NULL),(119,0,'2019-12-19 14:33:53','vlangat@ari.co.ke','2019-12-19 14:33:53','New System User',NULL,'','','titus.murage@cargen.com',NULL,'Titus',' Murage','titus.murage@cargen.com','0716552733','2019-12-19 14:33:53',1,'$2y$10$KJ9AR6gYzXesH5TSVOyNiuSXVl2L5ZE1jKQYidaZj77pXdodVzW2y','1','Normal',NULL,0,'0',NULL,NULL,NULL),(120,0,'2019-12-19 14:34:52','vlangat@ari.co.ke','2019-12-19 14:34:52','New System User',NULL,'','','gilbert.mutai@cargen.com',NULL,'Gilbert','Mutai','gilbert.mutai@cargen.com','0711241716','2019-12-19 14:34:52',1,'$2y$10$XtQhrdRYjMm8DgwGcl78QetqxilP3MGk8EixOuJHiamwKmCsZ6NXO','1','Normal','2020-04-29 10:32:43',0,'$2y$10$XtQhrdRYjMm8DgwGcl78QetqxilP3MGk8EixOuJHiamwKmCsZ6NXO',NULL,NULL,NULL),(121,0,'2019-12-19 14:36:00','vlangat@ari.co.ke','2019-12-19 14:36:00','New System User',NULL,'','','Joel@cargen.com',NULL,'Joel','Muya','Joel@cargen.com','0722285349','2019-12-19 14:36:00',1,'$2y$10$j7C/yu1m5xu6jZg4eF2gQOMDRykw.JG7YWKtR3Q7emOVNqNqTcc4m','1','Normal',NULL,0,'0',NULL,NULL,NULL),(122,0,'2019-12-19 14:39:46','vlangat@ari.co.ke','2019-12-19 14:39:46','New System User',NULL,'','','gkahindi@cumminscargen.com',NULL,'Goeorge','Goeorge','gkahindi@cumminscargen.com','0700000000','2019-12-19 14:39:46',1,'$2y$10$n7FEaotCM966AUKCSNyu3edeFJuTi1VLK4hyF2ZEZri9TPcVH2h1a','1','Normal',NULL,0,'0',NULL,NULL,NULL),(123,0,'2019-12-19 14:41:23','vlangat@ari.co.ke','2019-12-19 14:41:23','New System User',NULL,'','','maryanne.njeri@cumminscargen.com',NULL,'Maryanne','Njeri','maryanne.njeri@cumminscargen.com','0700000001','2019-12-19 14:41:23',1,'$2y$10$6bCShRWPoOeZ7SoamfPUZu8TYRZ2XBKCTovi1QqjKkdftqBHMpgaW','1','Normal',NULL,0,'0',NULL,NULL,NULL),(124,900,'2020-01-14 10:59:17','vlangat@ari.co.ke','2020-01-14 10:59:17','New System User',NULL,'','','rwaiharo@ari.co.ke',NULL,'Ruth','Waiharo','rwaiharo@ari.co.ke','0700000000','2020-01-14 10:59:17',0,'$2y$10$J6Noj1hFPhJycVL7OFg2sumo86eqCPD68VAk6vxvDreC0yKwJoiUS','1','Admin',NULL,0,'0',NULL,NULL,NULL),(125,900,'2020-02-06 17:59:31','vlangat@ari.co.ke','2020-02-06 17:59:31','New System User',NULL,'','','godfrey.langat@cargen.com',NULL,'Godfrey','Langat','godfrey.langat@cargen.com','0723159595','2020-02-06 17:59:31',0,'$2y$10$2pW6RznH3PlX8XUtZwdvie6rK.mFYr8Gn3yCdHTz5o6deDlY4MXWe','1','Normal',NULL,0,'0',NULL,NULL,NULL),(126,0,'2020-10-06 10:19:16','nkinyagia@ari.co.ke','2020-10-06 10:19:16','New System User',NULL,'','','laura.irungu@cargen.com',NULL,'Laura','Irungu','laura.irungu@cargen.com','0700747849','2020-10-06 10:19:16',1,'$2y$10$9wfsWOV1/4LA6nmAp0PZQeMFNDYDY8XqDxomHno3m80Esn87QVzXm','1','Normal',NULL,0,'0',NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-07 11:21:06
