-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: mlink_ari_sys
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tblreportgrant`
--

DROP TABLE IF EXISTS `tblreportgrant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tblreportgrant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `creadential_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `creadential_id` (`creadential_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `tblReportGrant_ibfk_3` FOREIGN KEY (`creadential_id`) REFERENCES `tblcredentials` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `tblReportGrant_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `tblreportemails` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblreportgrant`
--

LOCK TABLES `tblreportgrant` WRITE;
/*!40000 ALTER TABLE `tblreportgrant` DISABLE KEYS */;
INSERT INTO `tblreportgrant` VALUES (25,5,1),(30,6,1),(32,7,1),(34,9,1),(39,7,2),(40,8,1),(41,10,2),(42,11,2),(43,14,2),(44,15,2),(58,7,20),(59,7,18),(60,7,28),(61,7,21),(62,7,17),(63,7,19),(64,7,29),(65,7,23),(66,7,15),(67,7,22),(68,7,27),(69,7,24),(71,7,30),(72,5,2);
/*!40000 ALTER TABLE `tblreportgrant` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-07 11:21:16
