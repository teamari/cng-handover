-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: mlink_ari_sys
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tblcredentials`
--

DROP TABLE IF EXISTS `tblcredentials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tblcredentials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shortCode` text NOT NULL,
  `branch` varchar(100) NOT NULL,
  `type` int(11) NOT NULL,
  `apiKey` varchar(100) NOT NULL,
  `apiSecret` varchar(100) NOT NULL,
  `enabled` int(11) NOT NULL DEFAULT '0',
  `deleted` int(11) NOT NULL DEFAULT '0',
  `added_by` int(11) NOT NULL,
  `registered` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblcredentials`
--

LOCK TABLES `tblcredentials` WRITE;
/*!40000 ALTER TABLE `tblcredentials` DISABLE KEYS */;
INSERT INTO `tblcredentials` VALUES (1,'554554','Nairobi HQ',0,'4azONG7qq6AJkQPDYp9mkunsMr5hr9WT','K5s7nuWTF0eiEQuN',1,0,1,1),(2,'664988','CUMMINS',0,'S4QaGCaBAeSHcLrEx7O5tMiLYMRN07wd','eYKGQoAaolO35QFD',1,0,1,1),(15,'889384','Mombasa Branch',1,'x8cRpAAtuFgcO5PW1fx4i8jzn0Eykj62','ECQuAUHCDAbosVGD',1,0,1,1),(17,'889388','Kisumu Branch',1,'5dQocsIUKsZr8rsoEf2eVodoMGlsP6yP','3khzUOFtUr9QiwQW',1,0,1,1),(18,'889389','Eldoret Branch',1,'iVPmAwrVxqeoSPe44FkjdRQxxerARaAk','PYXtVikqAvdUtASM',1,0,1,1),(19,'889391','Kitale Branch',1,'fzQ5xOIqFRVGEMg4tFRFUTe144DkpOGl','jGmjZVxBzhcn3Dnh',1,0,1,1),(20,'889394','Bungoma Branch',1,'TAX8gsBh4wTdVZQAqMFYymz4CsZeSRhE','s6MQPxk0ksNm4utI',1,0,1,1),(21,'889393','Kisii Branch',1,'0OhObWonaf28UOmfJnvsLGFk3OpCKoTj','Hkgg5dZ7NNIUiwR2',1,0,1,1),(22,'889385','Nakuru Branch',1,'rm8BAsCUV9SGCHMekAeJtUUbuAFn5M3n','gkvC5VGU2CbkPCGF',1,0,110,1),(23,'889387','Malindi Branch',1,'o9VcymZ6EJKEtyfu5EgpViNegoJT9nru','igQzWSJsU2nOt8uL',1,0,110,1),(24,'889386','Voi Branch',1,'6yV9Mh4Xko44OHRXIk35jXklSqMN6Vti','DDR13L4vhRbcUXwZ',1,0,110,1),(27,'180514','Nanyuki Branch',1,'2waVpG6yxLpdVCkHzY3n85pmA8gLeJgT','At4IKrxx1bwARJCE',1,0,1,1),(28,'583050','Kericho Branch',1,'ME5fagUITuzsIQHhu3nGWxPbmeq6opdN','3GCje4bQBBRvGKlA',1,0,1,1),(29,'583030','Kitengela Branch',1,'587HbIme4mPLdF06EDpCz88ibA6Su5RK','2GqeeLtZVZeDxWMv',1,0,1,1),(30,'562691','NIIT Kenya',1,'Nl2YGBgYR2fOg77h5Ukdf6NtZRuXU9P8','vsAmHQqT5p1mSzGq',1,0,1,1);
/*!40000 ALTER TABLE `tblcredentials` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-07 11:21:17
