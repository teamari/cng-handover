-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: mlink_ari_sys
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tblportalgrants`
--

DROP TABLE IF EXISTS `tblportalgrants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tblportalgrants` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `creadential_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `creadential_id` (`creadential_id`),
  CONSTRAINT `tblPortalGrants_ibfk_4` FOREIGN KEY (`creadential_id`) REFERENCES `tblcredentials` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblportalgrants`
--

LOCK TABLES `tblportalgrants` WRITE;
/*!40000 ALTER TABLE `tblportalgrants` DISABLE KEYS */;
INSERT INTO `tblportalgrants` VALUES (12,110,2),(13,110,1),(14,113,2),(15,113,1),(16,115,2),(17,115,1),(18,118,2),(19,118,1),(20,117,2),(21,117,1),(22,119,2),(23,119,1),(24,120,2),(25,120,1),(26,121,2),(27,121,1),(28,122,2),(29,123,2),(30,124,2),(31,124,1),(34,114,2),(35,114,1),(36,125,1),(60,114,15),(61,110,17),(62,110,15),(63,114,17),(64,110,18),(65,114,18),(66,110,19),(67,114,19),(68,110,20),(69,114,20),(70,110,21),(71,114,21),(72,114,23),(73,114,22),(74,114,24),(75,114,27),(76,110,23),(77,110,22),(78,110,27),(79,110,24),(80,114,28),(81,110,28),(82,114,30),(83,114,29),(84,110,29),(85,110,30);
/*!40000 ALTER TABLE `tblportalgrants` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-07 11:21:08
