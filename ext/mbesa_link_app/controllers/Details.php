<?php

class Details extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $userid = $this->session->userdata('userid');
        $this->load->library('form_validation');
        $this->load->model('loginaudits');
        $this->load->model('credentials');
        $this->load->model('tills');
        $this->load->model('user');
    }

    //Add User in the  system
    public function adduser()
    {
      $userid = $this->session->userdata('userid');
      if (empty($userid) || ($userid == null)) {
        $this->session->set_flashdata('tempdata', "Sorry Please log in to continue.");
        redirect('admin/');
    }
    $this->form_validation->set_rules('fullname', 'Full Name', 'trim|required|xss_clean');
    $this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|xss_clean');
    $this->form_validation->set_rules('useremail', 'E-mail', 'trim|xss_clean|required|valid_email');

    if ($this->form_validation->run() == false) {
        $this->session->set_flashdata('tempdata', "Sorry there is an error on your data. Please fix the issues addressed and try again.<br>". validation_errors());
        redirect('dashboard/users');
    } else {
        $fullname = $this->input->post('fullname');
        $phone = $this->input->post('phone');
        $useremail = $this->input->post('useremail');

        $addedby = $this->session->userdata('userid');

        $data = array('phone'=>$phone);

        $result = $this->user->confirmifexist($data);

        if ($result->num_rows() > 0) {
            $this->session->set_flashdata('tempdata', "Sorry, the user already exist in the system.<br>");
            redirect('dashboard/users');
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];

            $auditdata = array('registered_by_id' => $addedby, 'phone_registered' => $phone, 'status' => 'success', 'description' => 'Admin user added successfully', 'ip_address' => $ip, 'headers' => json_encode($this->input->request_headers()));

            $billdata = array('full_name' => $fullname, 'email' => $useremail, 'phone' => $phone, 'user_added_by' => $addedby);
            $result = $this->user->insert($billdata, $auditdata);

            if ($result > 0) {
                $this->session->set_flashdata('tempdata', 'You have successfully added the user');
                redirect('dashboard/users');
            } else {
                $this->session->set_flashdata('tempdata', "Sorry, there was some problem trying to add the user, try again.<br>");
                redirect('dashboard/users');
            }
        }
    }
}

function disableThis(){
    $tid = $this->input->get('usercode');
    $tid = base64_decode(urldecode($tid));
    $usercode = $this->user->updatereportusers(array('active'=>0), array('id'=>$tid));
    
    if($this->db->affected_rows()>0) {
        $flashdata ='Selected user disabled from receiving emails.';
        $this->session->set_flashdata('tempdata',$flashdata);
        redirect(APP_BASE ."reportrecipient");
    }
    else{
      $flashdata ='Selected user disabled from receiving emails.';
      $this->session->set_flashdata('tempdata',$flashdata);
      redirect(APP_BASE ."reportrecipient");
  }
  
}

//Add Report Recipient User in the  system
public function addrecipientuser()
{
    $this->form_validation->set_rules('firstname', 'Recipient Name', 'trim|required|xss_clean');
    $this->form_validation->set_rules('email', 'E-mail', 'trim|xss_clean|required|valid_email');

    if ($this->form_validation->run() == false) {
        $this->session->set_flashdata('tempdata', "Sorry there is an error on your data. Please fix the issues addressed and try again.<br>". validation_errors());
        redirect(APP_BASE ."reportrecipient");
    } else {
        $name = $this->input->post('firstname');
        $useremail = $this->input->post('email');

        $data = array('email'=>$useremail);
        $result = $this->user->confirmifexistreportusers($data);

        if ($result->num_rows() > 0) {
            $flashdata ='Sorry, the user already exist in the system.<br>';
            $this->session->set_flashdata('tempdata',$flashdata);
            redirect(APP_BASE ."reportrecipient");
        } else {
            $userdata = array('name' => $name, 'email' => $useremail, 'active' => 1);
            $result = $this->user->insertreportusers($userdata);
            if ($result > 0) {
               $flashdata ='You have successfully added the user.<br>';
               $this->session->set_flashdata('tempdata',$flashdata);
               redirect(APP_BASE ."reportrecipient");
           } else {
             $flashdata ='Sorry, there was some problem adding the user.<br>';
             $this->session->set_flashdata('tempdata',$flashdata);
             redirect(APP_BASE ."reportrecipient");
         }
     }
 }
}


    //Edit User in the database
public function edituser()
{
  $userid = $this->session->userdata('userid');
  if (empty($userid) || ($userid == null)) {
    $this->session->set_flashdata('tempdata', "Sorry Please log in to continue.");
    redirect('admin/');
}

$this->form_validation->set_rules('fullname', 'Full Name', 'trim|required|xss_clean');
$this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|xss_clean');
$this->form_validation->set_rules('useremail', 'E-mail', 'trim|xss_clean|required|valid_email');

$orig_bills = $this->input->post('orig_bills');
$orig_bills_encrypted = $this->input->post('orig_bills');
$orig_bills = base64_decode(strtr($orig_bills, '._-', '+/='));

if ($this->form_validation->run() == false) {
    $this->session->set_flashdata('tempdata', "Sorry there is an error on your data. Please fix the issues addressed and try again.<br>". validation_errors());
    redirect('dashboard/user/'.$orig_bills_encrypted);
} else {
    $ip = $_SERVER['REMOTE_ADDR'];

    $fullname = $this->input->post('fullname');
    $phone = $this->input->post('phone');
    $useremail = $this->input->post('useremail');

    $addedby = $this->session->userdata('userid');

    $condition = array('id' => $orig_bills);
    $userdata = array('full_name' => $fullname, 'email' => $useremail, 'phone' => $phone, 'last_edited_by' => $addedby);

    $auditdata = array('item_id' => $orig_bills, 'edited_by' => $addedby, 'status' => 'success', 'description' => 'User Updated', 'ip_address' => $ip, 'headers' => json_encode($this->input->request_headers()));

    $result = $this->user->updateuser($userdata, $condition, $auditdata);


    if ($result > 0) {
        $this->session->set_flashdata('tempdata', 'You have successfully updated the user');
        redirect('dashboard/user/'.$orig_bills_encrypted);
    } else {
        $this->session->set_flashdata('tempdata', "Sorry, there was some problem trying to update the user, try again.<br>");
        redirect('dashboard/user/'.$orig_bills_encrypted);
    }
}
}

    //Edit my profile
public function editmyprofile()
{
  $userid = $this->session->userdata('userid');
  if (empty($userid) || ($userid == null)) {
    $this->session->set_flashdata('tempdata', "Sorry Please log in to continue.");
    redirect('admin/');
}

$this->form_validation->set_rules('fullname', 'Full Name', 'trim|required|xss_clean');
$this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|xss_clean');
$this->form_validation->set_rules('useremail', 'E-mail', 'trim|xss_clean|required|valid_email');

$this->form_validation->set_rules('address', 'Full Name', 'trim|xss_clean');
$this->form_validation->set_rules('zip', 'Zip', 'trim|xss_clean');
$this->form_validation->set_rules('town', 'Town', 'trim|xss_clean');
$this->form_validation->set_rules('country', 'Country', 'trim|xss_clean');

$orig_bills = $this->input->post('orig_bills');
$orig_bills_encrypted = $this->input->post('orig_bills');
$orig_bills = base64_decode(strtr($orig_bills, '._-', '+/='));

if ($this->form_validation->run() == false) {
    $this->session->set_flashdata('tempdata', validation_errors());
    redirect('dashboard/profile');
} else {

  $ip = $_SERVER['REMOTE_ADDR'];

  $fullname = $this->input->post('fullname');
  $phone = $this->input->post('phone');
  $useremail = $this->input->post('useremail');
  $address = $this->input->post('address');
  $zip = $this->input->post('zip');
  $town = $this->input->post('town');
  $country = $this->input->post('country');

  $addedby = $this->session->userdata('userid');

  $condition = array('id' => $orig_bills_encrypted);
  $userdata = array('full_name' => $fullname, 'email' => $useremail, 'phone' => $phone, 'last_edited_by' => $addedby, 'address' => $address, 'zip' => $zip, 'town' => $town, 'country' => $country);

  $auditdata = array('item_id' => $orig_bills_encrypted, 'edited_by' => $addedby, 'status' => 'success', 'description' => 'Profile Updated', 'ip_address' => $ip, 'headers' => json_encode($this->input->request_headers()));

  $result = $this->user->updateuser($userdata, $condition, $auditdata);

  if ($result > 0) {
    $data = array('tblUser.id' => $orig_bills_encrypted);

    $resultuser = $this->user->confirmifexist($data);

    if ($resultuser->num_rows() > 0) {
        $row = $resultuser->row();
        $this->session->set_userdata('fullname', $row->full_name);
        $this->session->set_flashdata('tempdata', 'You have successfully updated your profile');
        redirect('dashboard/profile');
    }
} else {
    $this->session->set_flashdata('tempdata', "Sorry, there was some problem trying to update your profile, try again later.<br>");
    redirect('dashboard/profile');
}
}
}

public function do_upload()
{
    $userid = $this->session->userdata('userid');
    if (empty($userid) || ($userid == null)) {
        $this->session->set_flashdata('tempdata', "Sorry Please log in to continue.");
        redirect('admin/');
    }
    

    $editedby = $this->session->userdata('userid');
    $config = array(
        'upload_path' => './uploads/',
        'allowed_types' => 'gif|jpg|png',
        'overwrite' => true,
        'file_name' => 'profile'.$userid,
        'max_size' => '2048000', // Can be set to particular file size , here it is 2 MB(2048 Kb)
    );
    $this->load->library('upload', $config);

    if ($this->upload->do_upload()) {
        $filename = $this->upload->data('file_name');

        $condition = array('id' => $userid);
        $userdata = array('photo' => $filename);


        $ip = $_SERVER['REMOTE_ADDR'];
        $auditdata = array('item_id' => $userid, 'edited_by' => $userid, 'status' => 'success', 'description' => 'Photo Updated', 'ip_address' => $ip, 'headers' => json_encode($this->input->request_headers()));


        $result = $this->user->updateuser($userdata, $condition, $auditdata);

        if ($result > 0) {
            $data = array('tblUser.id' => $userid);

            $resultuser = $this->user->confirmifexist($data);

            if ($resultuser->num_rows() > 0) {
                $row = $resultuser->row();
                $this->session->set_userdata('photo', $row->photo);
                $this->session->set_flashdata('tempdata', 'You have successfully updated your photo');
                redirect('dashboard/profile');
            }
        } else {
            $this->session->set_flashdata('tempdata', "Sorry, there was some problem trying to update your profile, try again later.<br>");
            redirect('dashboard/profile');
        }
    } else {
          /*echo $this->upload->display_errors();
          */
          $this->session->set_flashdata('tempdata', "Sorry, there was some problem trying to update your profile, try again later.<br>");
          redirect('dashboard/profile');
      }
  }



    //Add Till number to the database
  public function addtillno()
  {
     $userid = $this->session->userdata('userid');
     if (empty($userid) || ($userid == null)) {
        $this->session->set_flashdata('tempdata', "Sorry Please log in to continue.");
        redirect('admin/');
    }

    $this->form_validation->set_rules('shortcode', 'Pay Bill', 'trim|required|xss_clean');
    $this->form_validation->set_rules('branchname', 'Branch Name', 'trim|required|xss_clean');
    $this->form_validation->set_rules('apikey', 'API Key ', 'trim|xss_clean|required');
    $this->form_validation->set_rules('apisecret', 'API Secret', 'trim|xss_clean|required');

    if ($this->form_validation->run() == false) {
        $this->session->set_flashdata('tempdata', "Sorry there is an error on your data. Please fix the issues addressed and try again.<br>". validation_errors());
        redirect('dashboard/tills');
    } else {
        $shortcode = $this->input->post('shortcode');
        $branchname = $this->input->post('branchname');
        $apikey = $this->input->post('apikey');
        $apisecret = $this->input->post('apisecret');
        $addedby = $this->session->userdata('userid');

        $data = array('shortCode' => $shortcode);

        $result = $this->tills->get($data);

        if ($result->num_rows() > 0) {
            $this->session->set_flashdata('tempdata', "Sorry, the PayBill number already exist in the system.<br>");
            redirect('dashboard/tills');
        } else {
            $billdata = array('shortCode' => $shortcode, 'branch' => $branchname, 'apiKey' => $apikey, 'apiSecret' => $apisecret, 'added_by' => $addedby);
            $result = $this->tills->insert($billdata);

            if ($result > 0) {
                $this->session->set_flashdata('tempdata', 'You have successfully added the short code');
                redirect('dashboard/tills');
            } else {
                $this->session->set_flashdata('tempdata', "Sorry, there was some problem trying to add the short code, try again.<br>");
                redirect('dashboard/tills');
            }
        }
    }
}

    //Edit Activity to the database
public function edittill()
{
    $userid = $this->session->userdata('userid');
    if (empty($userid) || ($userid == null)) {
        $this->session->set_flashdata('tempdata', "Sorry Please log in to continue.");
        redirect('admin/');
    }
    
    $this->form_validation->set_rules('shortcode', 'Pay Bill', 'trim|required|xss_clean');
    $this->form_validation->set_rules('branchname', 'Branch Name', 'trim|required|xss_clean');
    $this->form_validation->set_rules('apikey', 'API Key ', 'trim|xss_clean|required');
    $this->form_validation->set_rules('apisecret', 'API Secret', 'trim|xss_clean|required');

    $orig_bills = $this->input->post('orig_bills');
    $orig_bills_encrypted = $this->input->post('orig_bills');
    $orig_bills = base64_decode(strtr($orig_bills, '._-', '+/='));

    if ($this->form_validation->run() == false) {
        $this->session->set_flashdata('tempdata', "Sorry there is an error on your data. Please fix the issues addressed and try again.<br>". validation_errors());
        redirect('dashboard/till/'.$orig_bills_encrypted);
    } else {
        $shortcode = $this->input->post('shortcode');
        $branchname = $this->input->post('branchname');
        $apikey = $this->input->post('apikey');
        $apisecret = $this->input->post('apisecret');
        $addedby = $this->session->userdata('userid');

        $condition = array('id' => $orig_bills);
        $tilldata = array('shortCode' => $shortcode, 'branch' => $branchname, 'apiKey' => $apikey, 'apiSecret' => $apisecret, 'added_by' => $addedby);

        $ip = $_SERVER['REMOTE_ADDR'];
        $auditdata = array('item_id' => $orig_bills, 'edited_by' => $addedby, 'status' => 'success', 'description' => 'Till Updated', 'ip_address' => $ip, 'headers' => json_encode($this->input->request_headers()));


        $result = $this->tills->update($tilldata, $condition, $auditdata);

        if ($result > 0) {
            $this->session->set_flashdata('tempdata', 'You have successfully edited the short code');
            redirect('dashboard/till/'.$orig_bills_encrypted);
        } else {
            $this->session->set_flashdata('tempdata', "Sorry, there was some problem trying to edit the short code, try again.<br>");
            redirect('dashboard/till/'.$orig_bills_encrypted);
        }
    }
}

    //Add paybill to the database
public function addpaybill()
{
    $userid = $this->session->userdata('userid');
    if (empty($userid) || ($userid == null)) {
        $this->session->set_flashdata('tempdata', "Sorry Please log in to continue.");
        redirect('admin/');
    }
    $this->form_validation->set_rules('shortcode', 'Pay Bill', 'trim|required|xss_clean');
    $this->form_validation->set_rules('branchname', 'Branch Name', 'trim|required|xss_clean');
    $this->form_validation->set_rules('type', 'Type', 'trim|required|xss_clean');
    $this->form_validation->set_rules('apikey', 'API Key ', 'trim|xss_clean|required');
    $this->form_validation->set_rules('apisecret', 'API Secret', 'trim|xss_clean|required');

    if ($this->form_validation->run() == false) {
        $this->session->set_flashdata('tempdata', "Sorry there is an error on your data. Please fix the issues addressed and try again.<br>". validation_errors());
        redirect('dashboard/pay_bill');
    } else {
        $shortcode = $this->input->post('shortcode');
        $branchname = $this->input->post('branchname');
        $apikey = $this->input->post('apikey');
        $apisecret = $this->input->post('apisecret');
        $type = $this->input->post('type');
        $addedby = $this->session->userdata('userid');

        $data = array('shortCode' => $shortcode);

        $result = $this->credentials->get($data);

        if ($result->num_rows() > 0) {
            $this->session->set_flashdata('tempdata', "Sorry, the PayBill number already exist in the system.<br>");
            redirect('dashboard/pay_bill');
        } else {
            $billdata = array('shortCode' => $shortcode, 'branch' => $branchname, 'apiKey' => $apikey, 'apiSecret' => $apisecret, 'added_by' => $addedby, 'type' => $type);
            $result = $this->credentials->insert($billdata);

            if ($result > 0) {
                $this->session->set_flashdata('tempdata', 'You have successfully added the short code');
                redirect('dashboard/pay_bill');
            } else {
                $this->session->set_flashdata('tempdata', "Sorry, there was some problem trying to add the short code, try again.<br>");
                redirect('dashboard/pay_bill');
            }
        }
    }
}

    //Edit Activity to the database
public function editpaybill()
{
    $userid = $this->session->userdata('userid');
    if (empty($userid) || ($userid == null)) {
        $this->session->set_flashdata('tempdata', "Sorry Please log in to continue.");
        redirect('admin/');
    }
    
    $this->form_validation->set_rules('shortcode', 'Pay Bill', 'trim|required|xss_clean');
    $this->form_validation->set_rules('branchname', 'Branch Name', 'trim|required|xss_clean');
    $this->form_validation->set_rules('apikey', 'API Key ', 'trim|xss_clean|required');
    $this->form_validation->set_rules('apisecret', 'API Secret', 'trim|xss_clean|required');

    $orig_bills = $this->input->post('orig_bills');
    $orig_bills_encrypted = $this->input->post('orig_bills');
    $orig_bills = base64_decode(strtr($orig_bills, '._-', '+/='));

    if ($this->form_validation->run() == false) {
        $this->session->set_flashdata('tempdata', "Sorry there is an error on your data. Please fix the issues addressed and try again.<br>". validation_errors());
        redirect('dashboard/paybill/'.$orig_bills_encrypted);
    } else {
        $shortcode = $this->input->post('shortcode');
        $branchname = $this->input->post('branchname');
        $apikey = $this->input->post('apikey');
        $apisecret = $this->input->post('apisecret');
        $addedby = $this->session->userdata('userid');

        $condition = array('id' => $orig_bills);

        $billdata = array('shortCode' => $shortcode, 'branch' => $branchname, 'apiKey' => $apikey, 'apiSecret' => $apisecret, 'added_by' => $addedby);

        $ip = $_SERVER['REMOTE_ADDR'];
        $auditdata = array('item_id' => $orig_bills, 'edited_by' => $addedby, 'status' => 'success', 'description' => 'PayBill Updated', 'ip_address' => $ip, 'headers' => json_encode($this->input->request_headers()));



        $result = $this->credentials->update($billdata, $condition, $auditdata);

        if ($result > 0) {
            $this->session->set_flashdata('tempdata', 'You have successfully added the short code');
            redirect('dashboard/paybill/'.$orig_bills_encrypted);
        } else {
            $this->session->set_flashdata('tempdata', "Sorry, there was some problem trying to add the short code, try again.<br>");
            redirect('dashboard/paybill/'.$orig_bills_encrypted);
        }
    }
}

public function currentPayBill()
{
  $id = $this->uri->segment(3);
  $id = base64_decode(strtr($id, '._-', '+/='));

  $shortcode = $this->uri->segment(4);
  $shortcode = base64_decode(strtr($shortcode, '._-', '+/='));

  $branch = $this->uri->segment(5);
  $branch = base64_decode(strtr($branch, '._-', '+/='));
  
  $this->session->set_userdata('selectdid', $id);
  $this->session->set_userdata('selectdcode', $shortcode);
  $this->session->set_userdata('selectdbranch', $branch);
  
  redirect($_SERVER['HTTP_REFERER']);

}

public function test()
{
    echo $this->session->userdata('selectdid').'<br>';
    echo $this->session->userdata('selectdcode').'<br>';
    echo $this->session->userdata('selectdbranch').'<br>';

}
}