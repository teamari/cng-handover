<?php
class Report extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $userid = $this->session->userdata('userid');
        if (empty($userid) || ($userid == null)) {
            $this->session->set_flashdata('tempdata', "Sorry Please log in to continue.");
            redirect('admin/');
        }
        $this->load->library('Datatables');
    }


    //Load users
    public function users()
    {
        $this->load->view("adminincludes/header");
        $this->load->view("adminincludes/side");
        $this->load->view("adminincludes/topnav");
        $this->load->view("adminreports/users");
        $this->load->view("adminincludes/footer");
    }

    //Load paybills
    public function bills()
    {
        $this->load->view("adminincludes/header");
        $this->load->view("adminincludes/side");
        $this->load->view("adminincludes/topnav");
        $this->load->view("adminreports/paybills");
        $this->load->view("adminincludes/footer");
    }

    //Load tills
    public function tills()
    {
        $this->load->view("adminincludes/header");
        $this->load->view("adminincludes/side");
        $this->load->view("adminincludes/topnav");
        $this->load->view("adminreports/tills");
        $this->load->view("adminincludes/footer");
    }

    //Load ergistration logs
    public function register()
    {
        $this->load->view("adminincludes/header");
        $this->load->view("adminincludes/side");
        $this->load->view("adminincludes/topnav");
        $this->load->view("adminreports/registrationlogs");
        $this->load->view("adminincludes/footer");
    }
}
