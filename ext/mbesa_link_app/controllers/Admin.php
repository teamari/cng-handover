<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $userid = $this->session->userdata('userid');
        if (!empty($userid) && ($userid != null)) {
            redirect('dashboard/dashboard');
        }
    }

    //The default function to load for the admin
    public function index()
    {
        $this->load->view("adminwebsite/header");
        $this->load->view('adminwebsite/index');
        $this->load->view("adminwebsite/footer");
    }

    //Load to enter password
    public function pass()
    {
        $this->load->view("adminwebsite/header");
        $this->load->view('adminwebsite/pass');
        $this->load->view("adminwebsite/footer");
    }

    //Load to enter code
    public function entercode()
    {
        $this->load->view("adminwebsite/header");
        $this->load->view('adminwebsite/entercode');
        $this->load->view("adminwebsite/footer");
    }
}
