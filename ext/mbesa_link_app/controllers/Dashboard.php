<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $userid = $this->session->userdata('userid');
        if (empty($userid) || ($userid == null)) {
            $this->session->set_flashdata('tempdata', "Sorry Please log in to continue.");
            redirect('admin/');
        }

        $this->load->model('tills');
        $this->load->model('credentials');
        $this->load->model('loginaudits');
        $this->load->model('user');
    }

    //Load my profile
    public function profile()
    {
        $userid = $this->session->userdata('userid');
        $data = array('tblUser.id' => $userid);

        $users = $this->user->getuserlist($data);
        $data = array(
            'users_data' => $users,
            'orig_bill' => $userid,
        );

        $this->load->view("adminincludes/header");
        $this->load->view("adminincludes/side");
        $this->load->view("adminincludes/topnav");
        $this->load->view("dashboard/profile", $data);
        $this->load->view("adminincludes/footer");
    }


    //Load the default dashboard for an admin
    public function dashboard()
    {
        $this->load->view("adminincludes/header");
        $this->load->view("adminincludes/side");
        $this->load->view("adminincludes/topnav");
        $this->load->view("dashboard/dashboard");
        $this->load->view("adminincludes/footer");
    }

    //Load the paybill numbers
    public function pay_bill($id = '')
    {
        if ($id != null) {
			$id = base64_decode(strtr($id, '._-', '+/='));

            $data = array('tblCredentials.type' => $id);

            $bills = $this->credentials->getcredlist($data);
            $data = array(
                'bills_data' => $bills,
            );

            $this->load->view("adminincludes/header");
            $this->load->view("adminincludes/side");
            $this->load->view("adminincludes/topnav");
            $this->load->view("dashboard/paybill", $data);
            $this->load->view("adminincludes/footer");
        } else {
            $bills = $this->credentials->getcredlist();
            $data = array(
                'bills_data' => $bills,
            );

            $this->load->view("adminincludes/header");
            $this->load->view("adminincludes/side");
            $this->load->view("adminincludes/topnav");
            $this->load->view("dashboard/paybill", $data);
            $this->load->view("adminincludes/footer");
        }
    }

    //Load one paybill number
    public function paybill()
    {
        $original_bill_number = $this->uri->segment(3);
		$bill_number = base64_decode(strtr($original_bill_number, '._-', '+/='));


        $data = array('tblCredentials.id' => $bill_number);
        $bills = $this->credentials->getlist($data);
        $data = array(
            'bills_data' => $bills,
            'orig_bill' => $original_bill_number,
        );

        $this->load->view("adminincludes/header");
        $this->load->view("adminincludes/side");
        $this->load->view("adminincludes/topnav");
        $this->load->view("dashboard/bill", $data);
        $this->load->view("adminincludes/footer");
    }

    //Enable / Disable paybills
    public function enable()
    {
        $original_bill_number = $this->uri->segment(3);
		$original_bill_number = base64_decode(strtr($original_bill_number, '._-', '+/='));

        $original_val = $this->uri->segment(4);
		$original_val = base64_decode(strtr($original_val, '._-', '+/='));



        $condition = array('tblCredentials.id' => $original_bill_number);
        $data = array('tblCredentials.enabled' => $original_val);

        if ($original_val == 1) {
            $message = "Enabling successful";
            $audit = "The account is enabled successful.";
        } else {
            $message = "Disabling successful.";
            $audit = "The account is disabled successful.";
        }

        $update = $this->credentials->update($data, $condition);
        $user_id = $this->session->userdata('userid');

        if ($update > 0) {
            $this->session->set_flashdata('tempdata', $message.".<br>");
            $this->loginaudits->insert($user_id, $audit);
            redirect('dashboard/pay_bill');
        } else {
            $this->session->set_flashdata('tempdata', "Sorry, Something went wrong, kindly try again.<br>");
            redirect('dashboard/pay_bill');
        }
    }

    //Delete / Undelete paybills
    public function delete()
    {
        $original_bill_number = $this->uri->segment(3);
		$bill_number = base64_decode(strtr($original_bill_number, '._-', '+/='));
		

        $original_val = $this->uri->segment(4);
		$val_number = base64_decode(strtr($original_val, '._-', '+/='));


        $condition = array('tblCredentials.id' => $bill_number);
        $data = array('tblCredentials.deleted' => $val_number);

        if ($val_number == 1) {
            $message = "Deleted successfully";
            $audit = "The account is deleted successful.";
        } else {
            $message = "Undeleting successful.";
            $audit = "The account is undeleted successful.";
        }

        $update = $this->credentials->update($data, $condition);
        $user_id = $this->session->userdata('userid');

        if ($update > 0) {
            $this->session->set_flashdata('tempdata', $message . ".<br>");
            $this->loginaudits->insert($user_id, $audit);
            redirect('dashboard/pay_bill');
        } else {
            $this->session->set_flashdata('tempdata', "Sorry, Something went wrong, kindly try again.<br>");
            redirect('dashboard/pay_bill');
        }
    }

    //Enable / Disable till
    public function enabletill()
    {
        $original_bill_number = $this->uri->segment(3);
		$bill_number = base64_decode(strtr($original_bill_number, '._-', '+/='));

        $original_val = $this->uri->segment(4);
		$val_number = base64_decode(strtr($original_val, '._-', '+/='));


        $condition = array('tblTills.id' => $bill_number);
        $data = array('tblTills.enabled' => $val_number);

        if ($val_number == 1) {
            $message = "Enabling successful";
            $audit = "The account is enabled successful.";
        } else {
            $message = "Disabling successful.";
            $audit = "The account is disabled successful.";
        }

        $user_id = $this->session->userdata('userid');

        $update = $this->tills->update($data, $condition);

        if ($update > 0) {
            $this->session->set_flashdata('tempdata', $message . ".<br>");
            $this->loginaudits->insert($user_id, $audit);
            redirect('dashboard/pay_bill/');
        } else {
            $this->session->set_flashdata('tempdata', "Sorry, Something went wrong, kindly try again.<br>");
            redirect('dashboard/pay_bill/');
        }
    }

    //Delete / Undelete till
    public function deletetill()
    {
        $original_bill_number = $this->uri->segment(3);
		$bill_number = base64_decode(strtr($original_bill_number, '._-', '+/='));

        $original_val = $this->uri->segment(4);
		$val_number = base64_decode(strtr($original_val, '._-', '+/='));


        $condition = array('tblTills.id' => $bill_number);
        $data = array('tblTills.deleted' => $val_number);

        if ($val_number == 1) {
            $message = "Deleted successfully";
            $audit = "The account is deleted successful.";
        } else {
            $message = "Undeleting successful.";
            $audit = "The account is undeleted successful.";
        }

        $update = $this->tills->update($data, $condition);
        $user_id = $this->session->userdata('userid');

        if ($update > 0) {
            $this->session->set_flashdata('tempdata', $message . ".<br>");
            $this->loginaudits->insert($user_id, $audit);
            redirect('dashboard/pay_bill/');
        } else {
            $this->session->set_flashdata('tempdata', "Sorry, Something went wrong, kindly try again.<br>");
            redirect('dashboard/pay_bill/');
        }
    }

    //Load one paybill number
    public function till()
    {
        $original_till_number = $this->uri->segment(3);
		$bill_number = base64_decode(strtr($original_till_number, '._-', '+/='));

        $data = array('tblTills.id' => $bill_number);
        $bills = $this->tills->getlist();
        $data = array(
            'tills_data' => $bills,
            'orig_bill' => $original_till_number,
        );
        $this->load->view("adminincludes/header");
        $this->load->view("adminincludes/side");
        $this->load->view("adminincludes/topnav");
        $this->load->view("dashboard/till", $data);
        $this->load->view("adminincludes/footer");
    }

    //Load the users from the system
    public function users()
    {
        $users = $this->user->getuserlist();
        $data = array(
            'users_data' => $users,
        );

        $this->load->view("adminincludes/header");
        $this->load->view("adminincludes/side");
        $this->load->view("adminincludes/topnav");
        $this->load->view("dashboard/users", $data);
        $this->load->view("adminincludes/footer");
    }

    //Load one user
    public function user()
    {
        $original_till_number = $this->uri->segment(3);
		$bill_number = base64_decode(strtr($original_till_number, '._-', '+/='));

        $data = array('tblUser.id' => $bill_number);
        $users = $this->user->getuserlist($data);
        $data = array(
            'users_data' => $users,
            'orig_bill' => $original_till_number,
        );
        $this->load->view("adminincludes/header");
        $this->load->view("adminincludes/side");
        $this->load->view("adminincludes/topnav");
        $this->load->view("dashboard/user", $data);
        $this->load->view("adminincludes/footer");
    }

    //Enable / Disable user
    public function enableuser()
    {
        $original_bill_number = $this->uri->segment(3);
		$bill_number = base64_decode(strtr($original_bill_number, '._-', '+/='));

        $original_val = $this->uri->segment(4);
		$val_number = base64_decode(strtr($original_val, '._-', '+/='));


        $condition = array('tblLogin.user_id' => $bill_number);
        $data = array('tblLogin.enabled' => $val_number);

        if ($val_number == 1) {
            $message = "Enabling successful";
            $audit = "The user is enabled successful.";
        } else {
            $message = "Disabling successful.";
            $audit = "The user is disabled successful.";
        }

        $user_id = $this->session->userdata('userid');

        $update = $this->user->update($data, $condition);

        if ($update > 0) {
            $this->session->set_flashdata('tempdata', $message . ".<br>");
            $this->loginaudits->insert($user_id, $audit);
            redirect('dashboard/users');
        } else {
            $this->session->set_flashdata('tempdata', "Sorry, Something went wrong, kindly try again.<br>");
            redirect('dashboard/users');
        }
    }

    //Delete / Undelete user
    public function deleteuser()
    {
        $original_bill_number = $this->uri->segment(3);
		$bill_number = base64_decode(strtr($original_bill_number, '._-', '+/='));

        $original_val = $this->uri->segment(4);
		$val_number = base64_decode(strtr($original_val, '._-', '+/='));


        $condition = array('tblLogin.user_id' => $bill_number);
        $data = array('tblLogin.deleted' => $val_number);

        if ($val_number == 1) {
            $message = "User Deleted successfully";
            $audit = "The user is deleted successful.";
        } else {
            $message = "Undeleting user successful.";
            $audit = "The user is undeleted successful.";
        }

        $update = $this->user->update($data, $condition);
        $user_id = $this->session->userdata('userid');

        if ($update > 0) {
            $this->session->set_flashdata('tempdata', $message . ".<br>");
            $this->loginaudits->insert($user_id, $audit);
            redirect('dashboard/users');
        } else {
            $this->session->set_flashdata('tempdata', "Sorry, Something went wrong, kindly try again.<br>");
            redirect('dashboard/users');
        }
    }
}
