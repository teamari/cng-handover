<?php
class Users extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('usersmodel');
		$this->load->model('tokenmodel');
		$this->load->model('auditmodel');
		$this->load->model('captchamodel');
		$this->load->model('msgmodel');
		$this->load->model('user');
		$this->load->library('form_validation');
		$this->load->model('Import_model', 'import');
		
		$this->load->library('email');
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html'; 
		$config['newline'] ='\r\n';
		$config['clrf'] ='\r\n';
		$config['protocol'] = 'smtp';
		
		$config['smtp_host'] = SMTP_HOST;
		$config['smtp_port'] = SMTP_PORT;
		$config['smtp_user'] = SMTP_USER;
		$config['smtp_pass'] = SMTP_PASS;

		$this->email->initialize($config);
	}

	public function index(){
		show_404();
	}

	function checkUser(){
		$userCode = $this->input->get('email');
		
		$users = $this->usersmodel->get(array('UserCode'=>$userCode,'recordstate'=>0));

		if($users->num_rows()>0 )
			echo json_encode(array('result'=>'FAIL', 'message'=>'Username Already exists in the system'));
		else
			echo json_encode(array('result'=>'OK', 'message'=>''));
	}

	function checkPhone(){
		$userCode = $this->input->get('phone');
		$users = $this->usersmodel->get(array('phone'=>$userCode,'recordstate'=>0));
		if($users->num_rows()>0 )
			echo json_encode(array('result'=>'FAIL', 'message'=>'Username Already exists in the system'));
		else
			echo json_encode(array('result'=>'OK', 'message'=>''));
	}
	
	function disableThis(){
		$tid = $this->input->get('usercode');
		$tid = base64_decode(urldecode($tid));
		$usercode = $this->usersmodel->update(array('recordstate'=>900, 'UserEnabled'=>0), array('tid'=>$tid));
		
		if($this->db->affected_rows()>0) 
			echo "Selected user disabled from system access.";
		else
			echo "Sorry failed to disable user.Please try again.";
		
	}

	private function generatePass(){
		$word="";
		$pick = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$length = 6;
		while( strlen($word) < $length ) {
			$word .= substr($pick, mt_rand() % (strlen($pick)), 1);
		}
		return $word;
	}

	private function generatePin(){
		$word="";
		$pick = '0123456789';
		$length = 4;
		while( strlen($word) < $length ) {
			$word .= substr($pick, mt_rand() % (strlen($pick)), 1);
		}
		return $word;
	}

	public function addUser(){
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('phoneno', 'Phone Number', 'required');
		$this->form_validation->set_rules('firstname', 'First name', 'required');
		$this->form_validation->set_rules('secondname', 'Second name', 'required');

		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('tempdata',"Sorry there is an error on your data. Please fix the issues addressed and try again.<br>". validation_errors());
			redirect(APP_BASE ."users");
		} else {
      //validation OK
			$username = $this->input->post('email');
			$phoneno = $this->input->post('phoneno');
			$email = $this->input->post('email');
			$firstname = $this->input->post('firstname');
			$secondname = $this->input->post('secondname');
			
			$username = trim($username);
			$usersexist = $this->usersmodel->get(array('UserCode'=> $username,'recordstate'=>0));
			if($usersexist->num_rows()>0){
				$flashdata ='Sorry the username already exists in the system';
			}else{
				$password = $this->generatePass();
				$merchantId = $this->session->userdata('merchantid');
				$result = $this->usersmodel->insert($password,$merchantId);
				if($result>0){
					$msg = '<p>Your account has been created. Use the following credentials to access your account.</p>
					<table border="0" cellpadding="0" cellspacing="0">
					<tbody>
					<tr><td>Username: '.$username.'</td></tr>
					<tr><td>Password: '.$password.'</td></tr>
					</tbody>
					</table>
					<br>
					<table border="0" cellpadding="0" cellspacing="0"  class="btn btn-primary">
					<tbody>
					<tr><td> <a href="'. base_url() .'" target="_blank">Log in now</a> </td> </tr>
					</tbody>
					</table>';

					$sms = $firstname .", your account has been created on ". APP_NAME .". Visit ".  base_url() ." and log in with Username: ". $username ." Password ". $password;
					
					if(!empty($email)){
						$this->msgmodel->insert_comm($email, "email", $msg, "Account Created",$firstname );
						$this->sendemail($email,  "Account Created", $msg);
						$flashdata ='User details added in the system and an email sent out with the credentials.';
						$this->session->set_flashdata('tempdata',$flashdata);
						redirect(APP_BASE ."users");
					}

					if(!empty($phoneno)){
						$this->msgmodel->insert_comm($phoneno, "sms", $sms, "Account Created",$firstname );
						$flashdata ='User details added in the system and an email sent out with the credentials.';
						$this->session->set_flashdata('tempdata',$flashdata);
						redirect(APP_BASE ."users");
					}
				} else 
				$flashdata ='Sorry please supply the information required and follow the set registration rules.';
				$this->auditmodel->insert("Added local user details: Email: ". $email ." name : ". $firstname. $secondname,$result,"",json_encode($this->input->post()));
				$this->session->set_flashdata('tempdata',$flashdata);
				redirect(APP_BASE ."users");
			}
		}
	}

	function login(){

		$this->form_validation->set_rules('username', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('userCaptcha', 'User Captcha', 'required');
		
		$username =$this->input->post('username');
		$password = $this->input->post('password');
		$captcha=$this->input->post("userCaptcha");

		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('tempdata',"Sorry there is an error on your data. Please fix the issues addressed and try again.<br>". validation_errors());
			redirect(base_url());
		} else {
      //validation OK
			$systemCaptcha = $this->captchamodel->get(array('word'=>$captcha,'origin'=>'login','deleted'=>0));
			if($systemCaptcha->num_rows()> 0 && $captcha == $systemCaptcha->row()->word ){
        //captcha ok
				$captcharow = $systemCaptcha->row();
				$this->captchamodel->update($captcharow->captcha_id);
        //check logins
				$data=array('UserCode'=>$username,'UserEnabled'=>1,'recordstate'=>0);
				$result = $this->usersmodel->get($data);
				if($result->num_rows()>0){
					$row=$result->row();
					$dbPassword = $row->Password;
					if(password_verify($password, $dbPassword)){
						$token = do_hash(uniqid($row->UserCode.'$$@#FRW$%YHRTEW748@fh!()_+ETERT^%$^&*0dhaa!`',true));
						$this->session->set_userdata('username', $row->UserCode);
						$this->session->set_userdata('role',$row->Usertype);
						$this->session->set_userdata('userid',$row->tid);
						$this->session->set_userdata('merchantid',$row->BusinessID);
						$this->session->set_userdata('names', $row->Firstname);
						$this->session->set_userdata('userphone', $row->phone);
						$this->session->set_userdata('token', $token);
						$this->tokenmodel->insert($token,$row->UserCode);
						$this->auditmodel->insert("Logged into the sytem ",1,$row->UserCode);
						if($row->Usertype == "Reconciler"){
							redirect(APP_BASE ."reconciliation");
						}else{
							redirect(APP_BASE ."dashboard");
						}
					} else {
						$this->auditmodel->insert("Tried to Log in the sytem, invalid username / password combination",0,$username);
						$this->session->set_flashdata('tempdata','Sorry please supply the correct username - password pair combination');
						redirect(base_url());
					}
				}else{
					$this->auditmodel->insert("Tried to Log in the sytem,  account is does not exist",0,$username);
					$this->session->set_flashdata('tempdata','Sorry your account does not exist. Contact IT for further details.');
					redirect(base_url());
				}
			} else { 
        //invalid captcha
				$this->session->set_flashdata('tempdata','Sorry could not log you in, please enter the correct captcha code and try again.');
				$this->auditmodel->insert("Tried to log in, Invalid captcha was entered.",-1,$username);
				redirect(base_url());
			}
		}
	}
	

	public function admin_reset(){
		$tid = $this->input->get('usercode');
		$tid = base64_decode(urldecode($tid));
		$mtype = $this->input->get('mtype');

		$muser = $this->usersmodel->get(array('tid'=>$tid,'recordstate'=>0));
		if($muser->num_rows()>0){
			$user = $muser->row();
			if(!empty($mtype)){
				$pin =  $this->generatePass();

				$msg = '<p>Your password has been reset. Use the new password to log in to the system.</p>
				<table border="0" cellpadding="0" cellspacing="0">
				<tbody><tr><td>Password: '.$pin.'</td></tr></tbody></table>
				<br>
				<table border="0" cellpadding="0" cellspacing="0"  class="btn btn-primary">
				<tbody><tr><td> <a href="'. base_url() .'" target="_blank">Log in now</a> </td> </tr>
				</tbody></table>';

				$sms = "Your password has been changed. New password: ". $pin;

			} else {
				$pin = $this->generatePin();

				$msg = '<p>Your PIN has been changed. Use the new PIN log in on the mobile App.</p>
				<table border="0" cellpadding="0" cellspacing="0">
				<tbody><tr><td>PIN: '.$pin.'</td></tr></tbody></table>';
				$sms = "Your Pin has been changed. New PIN: ". $pin;
			}

			$upassword =  password_hash($pin,PASSWORD_BCRYPT);

			$this->usersmodel->update(array('password'=>$upassword), array('tid'=>$tid));

			if(!empty($user->email))
				$this->msgmodel->insert_comm($user->email, "email", $msg, "PIN Reset", $user->Firstname );
			$this->sendemail($user->email,  "PIN Reset", $msg);

			if(!empty($user->phone))
				$this->msgmodel->insert_comm($user->phone, "sms", $sms, "PIN Reset", $user->Firstname );

			echo "PIN has been reset and communication sent out.";
		} else
		echo "Could not change PIN. Please try again.";
	}


	public function reset(){
		$this->form_validation->set_rules('password', 'Current Password', 'required');
		$this->form_validation->set_rules('newpassword', 'New Password', 'required');
		$this->form_validation->set_rules('cpassword', 'Confirmation password', 'required');

		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('tempdata',"Sorry there is an error on your data. Please fix the issues addressed and try again.<br>". validation_errors());
			redirect(APP_BASE ."changepassword");
		} else {
			$currentPassword = $this->usersmodel->get(array('UserCode'=>$this->session->userdata('username'),'recordstate'=>0))->row();

			$password = $this->input->post('password');
			$new = $this->input->post('newpassword');
			$retype = $this->input->post('cpassword');
			
			if( password_verify($password, $currentPassword->Password)  && $new==$retype){
				$mpass = password_hash($new,PASSWORD_BCRYPT);
				$this->usersmodel->update(array('Password'=>$mpass),array('tid'=>$currentPassword->tid));
				$this->session->set_flashdata('tempdata',"Password successfully changed. During the next login use your new password.");
			} else
			$this->session->set_flashdata('tempdata',"Sorry ensure your current password is correct and that the new password matches the retype password.");
			redirect(APP_BASE .'changepassword');
		}
	}

	public function confirm_password($token){

		$username = base64_decode(urldecode($token));
		$username = trim($username);

		$musers = $this->usersmodel->get( array('UserCode'=>$username,'recordstate'=>0));

		if($musers->num_rows()>0){
			$user = $this->usersmodel->get(array('UserCode'=>$username))->row();
			$datetime1 =strtotime(date('Y-m-d H:i:s'));
			$datetime2 =strtotime($user->resettime);

			$interval =  round(abs($datetime1 - $datetime2) / 60,0);

			if($interval > 15 || $user->isreset==0)
				$flashdata ='Sorry the confirmation link has expired! Reset password again.';
			else{
				$update = array(
					'Password' => $user->resetpass,
					'isreset'=>'0',
					'resettime' => date('Y-m-d H:i:s')
				);
				$status  = $this->usersmodel->update($update,array('UserCode'=>$username));
				if($status>0)
					$flashdata ='Password reset done. Log in with your new password specified during reset request.';
				else
					$flashdata ='Password reset failed. Please try again.';
				$this->auditmodel->insert("Confirmed password reset for : usercode ". $username,$status,$username);
			}
		}else{
			$flashdata ='Sorry no account exists with the provided email address.';
		}
		$this->session->set_flashdata('tempdata',$flashdata);
		redirect(base_url());
	}


	public function forgot_password(){
		$this->form_validation->set_rules('email', 'Eamail', 'required');

		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('tempdata',"Sorry there is an error on your data. Please fix the issues addressed and try again.<br>". validation_errors());
			redirect('home/forgot_password');
		} else {
			$username = $this->input->post('email');

			$username = trim($username);
			$users = $this->usersmodel->get( array('UserCode'=>$username));

			if($users->num_rows()>0){
				$row=$users->row();
				$id = $row->tid;
				$phone = $row->phone;
				
				$word="";
				$pick = '0123456789';
				$length = 4;
				while( strlen($word) < $length ) {
					$word .= substr($pick, mt_rand() % (strlen($pick)), 1);
				}

				$curl_post_data = array(
					'apiKey' => SMSKEY,
					'shortCode' => SMSCODE,
					'message' =>  'Password reset code: '.$word,
					'recipient' => $phone,
					'callbackURL' => 'https://774d6fd7.ngrok.io',
					'enqueue' => 0
				);
				$data_string = json_encode($curl_post_data);
				$url = 'https://api.vaspro.co.ke/v3/BulkSMS/api/create';
				$curl = curl_init();
				curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json')); //setting a custom header
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);


    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    
    if (!$err) {
    	$data = array('code' => $word, 'user_id' => $id);
    	$this->import->insertcode($data);
    	$flashdata ='Reset code sent to you';
    }
    
}else{
	$flashdata ='Sorry no account exists with the provided details.';
	$this->session->set_flashdata('tempdata',$flashdata);
	redirect('home/forgot_password');
}
}

$this->session->set_flashdata('tempdata',$flashdata);
redirect(base_url().'/home/reset_password');
}

public function reset_password(){$username = $this->input->post('email');
$username = trim($username);

$reset = $this->input->post('reset');
$reset= trim($reset);

$data=array('UserCode'=>$username);
$result = $this->usersmodel->get($data);
if($result->num_rows()>0){
	$row=$result->row();
	$id = $row->tid;
	$data=array('code'=>$reset,'user_id'=>$id);
	$result = $this->import->getcode($data);
	if($result->num_rows()>0){
		$users = $this->usersmodel->get( array('UserCode'=>$username,'recordstate'=>0));

		if($users->num_rows()>0){
			$mpassword = $this->input->post('password');
			$cpassword = password_hash($mpassword,PASSWORD_BCRYPT);
			$update = array(
				'resetpass' => $cpassword ,
				'isreset'=>'1',
				'resettime' => date('Y-m-d H:i:s')
			);

			$status  = $this->usersmodel->update($update,array('UserCode'=>$username));
			$this->auditmodel->insert("Initiated password reset for : usercode ". $username,$status,$username);
			$url = base_url() .'users/confirm_password/'. urlencode(base64_encode($username));
			if($status>0){
				
				$this->confirm_password(urlencode(base64_encode($username)));
				
				$data=array('user_id'=>$id);
				$result = $this->import->rowdelete($data);
				
			} else
			$flashdata ='Sorry the reset account does not exist, ensure the details are correct and try again.';
		}else{
			$flashdata ='Sorry no account exists with the provided email address.';
		}
	}else{
		$flashdata ='Sorry the reset code provided is wrong.';
	}
}else{
	$flashdata ='Sorry the reset account does not exist, ensure the details are correct and try again.';
}


$this->session->set_flashdata('tempdata',$flashdata);
redirect($status>0?base_url():'/home/reset_password');
}

function timeout(){
	$this->session->set_flashdata('tempdata', "Sorry your previous session has expired. Please log in to continue.");
	redirect(base_url());
}

function terminateaccess(){
	$this->session->sess_destroy();
	redirect(base_url());
}

function sendemail($to, $subject, $message){
	$this->email->to($to);
	$this->email->subject($subject);
	$this->email->from(MAIL_FROM);
	$this->email->message( $message );
	$this->email->send();
}


public function addGrant(){
	$creds = $this->input->post('cred');
	$userid = $this->input->post('userid');
	
	foreach ($creds as $index => $cred) {
		$this->form_validation->set_rules('userid', 'User', 'required');
		$this->form_validation->set_rules('cred[]', 'Paybill', 'required');
		
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('tempdata',"Sorry ". validation_errors());
			redirect(APP_BASE ."grantusers");
		}else{
			$grantdata = array('user_id' => $userid, 'creadential_id' => $cred);
			$usersexist = $this->user->getgrants($grantdata);
			
			if($usersexist->num_rows()<1){
				$grantdata = array('user_id' => $userid, 'creadential_id' => $cred);
				$result = $this->user->insertgrants($grantdata);
			}
		}
	}
	$this->session->set_flashdata('tempdata', "Grant successfully");
	redirect(APP_BASE ."grantusers");
}

public function addReportReceipient(){
	$creds = $this->input->post('cred');
	$userid = $this->input->post('userid');
	
	foreach ($creds as $index => $cred) {
		$this->form_validation->set_rules('userid', 'User', 'required');
		$this->form_validation->set_rules('cred[]', 'Paybill', 'required');
		
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('tempdata',"Sorry ". validation_errors());
			redirect(APP_BASE ."grantreports");
		}else{
			$grantdata = array('user_id' => $userid, 'creadential_id' => $cred);
			$usersexist = $this->user->getreportreceipients($grantdata);
			
			if($usersexist->num_rows()<1){
				$grantdata = array('user_id' => $userid, 'creadential_id' => $cred);
				$result = $this->user->insertreportreceipients($grantdata);
			}
		}
	}
	$this->session->set_flashdata('tempdata', "Grant successfully");
	redirect(APP_BASE ."grantreports");
}

public function getReportReceipientslisting()
{
	if (!empty($this->input->post('userid'))) {
		$cat = $this->input->post('userid');
		$data = array('tblReportGrant.user_id' => $cat);
		$receipients = $this->user->getreceipientslisting($data);
	} else {
		$receipients = $this->user->getreceipientslisting();
	}
	
	echo json_encode($receipients);
}

public function getportalgrantslisting()
{
	if (!empty($this->input->post('userid'))) {
		$cat = $this->input->post('userid');
		$data = array('tblportalgrants.user_id' => $cat);
		$receipients = $this->user->getportalslisting($data);
	} else {
		$receipients = $this->user->getportalslisting();
	}
	
	echo json_encode($receipients);
}

//remove a user from accessing the reports 
public function deletereceipientsemail()
{
	$id = $this->input->post('id');
	$data = array('id' => $id);
	$result = $this->user->deletereportreceipients($data);
	
	if($result >0) 
		echo "Selected user deleted from receiving emails. Refresh the page to confirm";
	else
		echo "Sorry failed to delete the  user.Please try again.";
}

//remove the user from accessing the portal
public function deleteportalaccess()
{
	$id = $this->input->post('id');
	$data = array('id' => $id);
	$result = $this->user->deleteportalaccess($data);
	
	if($result >0) 
		echo "User deleted from the selected protal. Refresh the page to confirm";
	else
		echo "Sorry failed to delete the user.Please try again.";
}
}
?>