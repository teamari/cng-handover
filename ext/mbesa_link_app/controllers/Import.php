<?php
class Import extends CI_Controller{

  public function __construct() {
    parent::__construct();
    $this->load->model('Import_model', 'import');
    $this->load->library('excel');
  }
  
  public function generateotp() {
    $word="";
    $pick = '0123456789';
    $length = 4;
    while( strlen($word) < $length ) {
      $word .= substr($pick, mt_rand() % (strlen($pick)), 1);
    }
    
    
    $curl_post_data = array(
      'apiKey' => SMSKEY,
      'shortCode' => SMSCODE,
      'message' =>  $word,
      'recipient' => $this->session->userdata('userphone'),
      'callbackURL' => 'https://774d6fd7.ngrok.io',
      'enqueue' => 0
    );
    $data_string = json_encode($curl_post_data);
    $url = 'https://api.vaspro.co.ke/v3/BulkSMS/api/create';
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json')); //setting a custom header
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);


    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    
    if (!$err) {
     $userid = $this->session->userdata('userid');
     $data = array('code' => $word, 'user_id' => $userid);
     $this->import->insertcode($data);
   }
   
 }

    // import excel data
 public function save() {
  if ($this->input->post('importfile')) {
    
    $this->form_validation->set_rules('otp', 'OTP', 'required');
    $otp =$this->input->post('otp');
    
    if ($this->form_validation->run() == FALSE){
      $this->session->set_flashdata('tempdata',"Sorry there is an error on your data. Please fix the issues addressed and try again.<br>". validation_errors());
      redirect(APP_BASE ."reconciliation");
    } else {
      $userid = $this->session->userdata('userid');
      $data=array('code'=>$otp,'user_id'=>$userid);
      $result = $this->import->getcode($data);
      if($result->num_rows()>0){
        $path = './uploads/';

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'xlsx|xls|jpg|png';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('userfile')) {
          $this->session->set_flashdata('tempdata',"Sorry ". $this->upload->display_errors());
          redirect(APP_BASE ."reconciliation");
        } else {
          $data = array('upload_data' => $this->upload->data());
        }
        
        if (!empty($data['upload_data']['file_name'])) {
          $import_xls_file = $data['upload_data']['file_name'];
        } else {
			$this->session->set_flashdata('tempdata',"Sorry check on the encountered problem. ". $this->upload->display_errors());
			redirect(APP_BASE ."reconciliation");
          //$import_xls_file = 0;
        }
        $inputFileName = $path . $import_xls_file;
        try {
          $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
          $objReader = PHPExcel_IOFactory::createReader($inputFileType);
          $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
          $this->session->set_flashdata('tempdata',"Sorry Error loading file ". pathinfo($inputFileName, PATHINFO_BASENAME));
          redirect(APP_BASE ."reconciliation");
        }
        
        $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

        $arrayCount = count($allDataInSheet);
        $flag = 0;
        $createArray = array('Receipt No.', 'Completion Time', 'Paid In', 'Other Party Info');
        $makeArray = array('Receipt No.' => 'Receipt No.', 'Completion Time' => 'Completion Time', 'Paid In' => 'Paid In', 'Other Party Info' => 'Other Party Info');
        $SheetDataKey = array();
        foreach ($allDataInSheet as $dataInSheet) {
          foreach ($dataInSheet as $key => $value) {
            if (in_array(trim($value), $createArray)) {
                        //$value = preg_replace('/\s+/', '', $value);
              $SheetDataKey[trim($value)] = $key;
            } else {
            }
          }
        }
        
        $data = array_diff_key($makeArray, $SheetDataKey);  
        
        if (empty($data)) {
          $flag = 1;
        }

        if ($flag == 1) {
          for ($i = 8; $i <= $arrayCount; $i++) {
            $addresses = array();
            $TransID = $objPHPExcel->getActiveSheet()->getCell('A'.$i)->getValue();
            $TransTime = $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getValue();
            $TransAmount = $objPHPExcel->getActiveSheet()->getCell('F'.$i)->getValue();
            $BusinessShortCode = $objPHPExcel->getActiveSheet()->getCell('B3')->getValue();
            $person =  explode("-", $objPHPExcel->getActiveSheet()->getCell('K'.$i)->getValue());
            $MSISDN = $person[0];
            $ContactName = $person[1];
            $Balance = $objPHPExcel->getActiveSheet()->getCell('F'.$i)->getValue();
			
			
			$year = substr($TransTime,6,4);
			  $month = substr($TransTime,3,2);
			  $day = substr($TransTime,0,2); 
			  
			  $hour = substr($TransTime,11,2);
			  $min = substr($TransTime,14,2);
			  $sec = substr($TransTime,17,2);
			  
			  $mdate = $year ."-". $month ."-". $day ." ". $hour .":".$min .":". $sec;
			  
			  
            if(($TransAmount > 0) && (strlen($MSISDN) > 7)){
              $dataexist = $this->import->confirmifexist($TransID);
              if($dataexist->num_rows()<1){
				  //echo $TransID.' '.$mdate.' '.$TransAmount.' '.$BusinessShortCode.' '.$MSISDN.' '.$ContactName.' '.$Balance.'<br>';
				//$fetchData[] = array('TransID' => $TransID, 'TransTime' => $TransTime, 'TransAmount' => $TransAmount, 'BusinessShortCode' => $BusinessShortCode, 'MSISDN' => $MSISDN, 'ContactName' => $ContactName, 'Balance' => $Balance, 'flag' => '1',  'reconciledate' => date('Y-m-d H:i:s'),  'reconciledBy' => $this->session->userdata('userid'));
			 
			  $db = $this->load->database("ARI_CONFIRM", true);
			  $db->query("insert into ARI_CONFIRM.OMPS (\"TransID\",\"TransTime\",\"TransAmount\",\"BusinessShortCode\",\"MSISDN\",\"ContactName\",\"Balance\",\"flag\",\"RECONSTATUS\")  
				values('". $TransID ."','". $mdate ."',". $TransAmount .",'". $BusinessShortCode ."','". $MSISDN ."','". $ContactName ."',". $TransAmount .",1,1)");
			
			  }
            }
          }   
          
		  /*
          $data['insertinfo'] = $fetchData;
          $this->import->setBatchImport($fetchData);
          $this->import->importData();*/
          
    //unlink the uploaded file
          unlink($inputFileName);
          
       //Delete the used otp per user
          $data=array('user_id'=>$userid);
          $result = $this->import->rowdelete($data);
         // redirect(APP_BASE ."reconciled");
          
        } else {
          $this->session->set_flashdata('tempdata','Sorry Please import correct file.');
          redirect(APP_BASE ."reconciliation");
        }
      }
      else{
        $this->session->set_flashdata('tempdata','Sorry please provide a correct OTP.');
        redirect(APP_BASE ."reconciliation");
      }
    }
  }
}
}
?>