<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Payments extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('moneyin');
		$this->load->model('audit');
		$this->load->model('credentials');
		$this->load->model('auditmodel');
	}

	public function registerUrl($token, $code, $ip, $theid, $registerval)
	{
		$condition = array('id' => $theid);
		$addedby = $this->session->userdata('userid');
		
		$curl_post_data = array(
			'ShortCode' => $code,
			'ResponseType' => 'Completed',
			'ConfirmationURL' => base_url() . 'malipoapi/confirmation',
			'ValidationURL' => base_url() . 'malipoapi/validation_url'
		);
		$data_string = json_encode($curl_post_data);
		$url = SAFCOM .'/mpesa/c2b/v1/registerurl';
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:Bearer ' . $token)); //setting a custom header
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        $curl_response = curl_exec($curl);
        
        if (curl_error($curl)) {
        	$output = curl_error($curl);
        } else {
        	$this->auditmodel->insert("Mpesa Url registered ", 1, "Mpesa - Interface", $curl_response);
        	$alldata = substr($curl_response, strpos( $curl_response,"{"),strlen($curl_response)-1);
        	if (strpos($curl_response, "HTTP/1.1 200 OK") >= 0) {
                    //$json = $data[8];
        		$object = json_decode($alldata);
        		$m = "";
        		foreach ($object as $item => $value) {
        			$m .= "<b>" . $item . " : </b>\t\t " . $value . "<br>";
        		}
        		$output = $m;
        	} else
        	$output = $alldata;
        	
        	$billdata = array('registered' => 1);

        	$auditdata = array('item_id' => $theid, 'edited_by' => $addedby, 'status' => 'success', 'description' => 'ShortCode registered', 'ip_address' => $ip, 'headers' => json_encode($this->input->request_headers()));

        	$result = $this->credentials->update($billdata, $condition, $auditdata);
        	if($result > 0){
        		$tempdatadata = "The shortcode urls have been registered successfully.<br>";
        	}else{
        		$tempdatadata = "Sorry, status change error, but the registration is successful.<br>";
        	}
        	
        }
        curl_close($curl);
        
        $this->session->set_flashdata('tempdata',  $tempdatadata.''.$output);
        redirect('dashboard/pay_bill');
        //echo $output;
    }


    public function doit($activity = "register")
    {
    	$addedby = $this->session->userdata('userid');
    	$ip = $_SERVER['REMOTE_ADDR'];

    	$code = $this->uri->segment(3);
    	$code = base64_decode(strtr($code, '._-', '+/='));

    	$key = $this->uri->segment(4);
    	$key = base64_decode(strtr($key, '._-', '+/='));

    	$sec = $this->uri->segment(5);
    	$sec = base64_decode(strtr($sec, '._-', '+/='));

    	$theid = $this->uri->segment(6);
    	$theid = base64_decode(strtr($theid, '._-', '+/='));

    	$registerval = $this->uri->segment(7);
    	$registerval = base64_decode(strtr($registerval, '._-', '+/='));

    	$condition = array('id' => $theid);
		
    	$url = SAFCOM .'/oauth/v1/generate?grant_type=client_credentials';
    	$curl = curl_init();
    	curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Basic ' . base64_encode($key.':'.$sec))); //setting a custom header
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
		
        curl_close($curl);
        //$data  = json_decode( $curl_response);
        //$data  = explode("\r\n", $curl_response);
        $alldata = substr($curl_response, strpos( $curl_response,"{"),strlen($curl_response)-1);
        if (strpos($curl_response, "HTTP/1.1 200 OK") >= 0) {
        	$object = json_decode($alldata);
			$resp = $this->registerUrl($object->access_token, $code, $ip, $theid, $registerval);
			/*
				echo $object->access_token;
        	if ($activity == "register"){
				//echo $object;
        		//$resp = $this->registerUrl($object->access_token);
        	}else{
				$resp ="no action";
			}*/
				//	$resp = $this->simulatePayment($object->access_token);
        	$this->session->set_flashdata('tempdata', $resp);
        } else{
			$this->session->set_flashdata('tempdata', $curl_response);
		}
        
        //redirect(APP_BASE . "simulate");
    }
}