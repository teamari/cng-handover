<?php

require APPPATH . 'libraries/AfricasTalkingGateway.php';

class Login extends CI_Controller
{
    public $smsServer;

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('loginaudits');
        $this->load->model('user');
        $this->load->model('smsmodel');
    }

    //This confirms the existence of the email in the database
    public function checklogin()
    {
        $this->form_validation->set_rules('username', 'Email', 'trim|required|xss_clean|valid_email');

        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('tempdata', "Sorry there is an error on your data. Please fix the issues addressed and try again.<br>". validation_errors());
            redirect('admin/');
        } else {

            //Pick email from the input
            $email = $this->input->post('username');

            $data = array('email' => $email);

            $resultuser = $this->user->confirmifexist($data);

            if ($resultuser->num_rows() > 0) {
                $row = $resultuser->row();

                $fullname_value = $row->full_name;
                $email_value = $row->email;
                $activated_value = $row->activated;

                $this->session->set_userdata('namesession', $fullname_value);
                $this->session->set_userdata('emailsession', $email_value);
                $this->session->set_userdata('useridvalue', $row->user_id);
                $this->session->set_userdata('phonevalue', $row->phone);

                if ($activated_value == 0) {
                    redirect('admin/entercode');
                } elseif ($activated_value == 1) {
                    redirect('admin/pass');
                }
            } else {
                $this->session->set_flashdata('tempdata', "Sorry Invalid Email provided.<br>". validation_errors());
                redirect('admin/');
            }
        }
    }


    //Check the provided password
    public function checkpass()
    {
        $user_id = $this->session->userdata('useridvalue');
        if (!empty($user_id) && ($user_id != null)) {
            $this->form_validation->set_rules('pass', 'Password', 'trim|required|xss_clean');

            if ($this->form_validation->run() == false) {
                $this->session->set_flashdata('tempdata', "Sorry there is an error on your data. Please fix the issues addressed and try again.<br>". validation_errors());
                redirect('admin/');
            } else {
                $password = $this->input->post('pass');

                $cpassword = $password . $this->config->item('encryption_key');
                $password = hash('whirlpool', $cpassword);


                $user_id = $this->session->userdata('useridvalue');

                $data = array('user_id' => $user_id, 'password' => $password);

                $result = $this->user->confirmifexist($data);

                if ($result->num_rows() > 0) {
                    $row = $result->row();
                    $enabled_value = $row->enabled;
                    $delete_value = $row->deleted;
                    $ip = $_SERVER['REMOTE_ADDR'];

                    if ($enabled_value == 0) {
                        $this->session->set_flashdata('tempdata', "Sorry Account not enabled. Kindly contact the system admin to enable your account.<br>". validation_errors());
                        redirect('admin/');
                    } else {
                        if ($delete_value == 1) {
                            $this->session->set_flashdata('tempdata', "Sorry Account is deleted. Kindly contact the system admin to undelete your account.<br>". validation_errors());
                            redirect('admin/');
                        } else {
                            $data = array('last_login' => date('Y-m-d H:i:s'), 'login_ip' => $ip);
                            $condition = array('user_id' => $user_id);

                            $result2 = $this->user->update($data, $condition);
                            $audit_Desc = '';
                            if ($result2 > 0) {
                                $audit_Desc = 'Login successful.';
                                $usertyperole = $row->role;
                                $this->session->set_userdata('userid', $row->user_id);
                                $this->session->set_userdata('usertype', $row->role);
                                $this->session->set_userdata('fullname', $row->full_name);
                                $this->session->set_userdata('photo', $row->photo);

                                $this->session->set_flashdata('tempdata', "Login successful.<br>". validation_errors());
                                $this->loginaudits->insert($user_id, 'Login successful.');

                                redirect('dashboard/dashboard');
                            } else {
                                $this->session->set_flashdata('tempdata', "Sorry There was an problem loggin you in.<br>". validation_errors());
                                $this->loginaudits->insert($user_id, 'Error login in.');
                                redirect('admin/');
                            }
                        }
                    }
                } else {
                    $this->session->set_flashdata('tempdata', "Sorry You have provided an invalid password.<br>". validation_errors());
                    redirect('admin/');
                }
            }
        } else {
            $this->session->set_flashdata('tempdata', "Sorry Kindly enter your email address.<br>");
            redirect('admin/');
        }
    }

    //Confirm if the given registration code exists in th database
    public function confirm()
    {
        $user_id = $this->session->userdata('useridvalue');
        if (!empty($user_id) && ($user_id != null)) {
            $this->form_validation->set_rules('activationcode', 'Activation Code', 'trim|required|xss_clean');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|min_length[6]');

            if ($this->form_validation->run() == false) {
                $this->session->set_flashdata('tempdata', "Sorry there is an error on your data. Please fix the issues addressed and try again.<br>". validation_errors());
                redirect('admin/entercode');
            } else {
                $userid = $this->session->userdata('useridvalue');
                $confirmationcode = $this->input->post('activationcode');
                $password_value = $this->input->post('password');

                $data = array('code' => $confirmationcode, 'user_id' => $userid);
                $result = $this->smsmodel->getcode($data);

                if ($result->num_rows() > 0) {
                    $cpassword = $password_value . $this->config->item('encryption_key');
                    $password_value = hash('whirlpool', $cpassword);


                    $data = array('activated' => 1, 'password' => $password_value);
                    $condition = array('user_id' => $userid);

                    $result = $this->user->update($data, $condition);

                    if ($result > 0) {
                        $data = array('code' => $confirmationcode, 'user_id' => $userid);
                        $result = $this->smsmodel->rowdelete($data);
                        $this->session->set_flashdata('tempdata', 'Account activation successful, login to proceed.');
                        redirect('admin/');
                    } else {
                        $this->session->set_flashdata('tempdata', "Sorry Contact the admin for help.<br>". validation_errors());
                        redirect('admin/entercode');
                    }
                } else {
                    $this->session->set_flashdata('tempdata', "Sorry You have provided a wrong activation code.<br>". validation_errors());
                    redirect('admin/entercode');
                }
            }
        } else {
            $this->session->set_flashdata('tempdata', "Sorry You nedd to enter your email address to proceed.<br>". validation_errors());
            redirect('admin/');
        }
    }


    //Resend the code incase the user loses the earlier code
    //Its different from the ealier one though

    public function resend()
    {
        $emailsession = $this->session->userdata('emailsession');

        if (!empty($emailsession) && $emailsession != null) {
            $phone_value = $this->session->userdata('phonevalue');
            $result = $this->session->userdata('useridvalue');

            $data = array('user_id' => $result);
            $resultdelete = $this->smsmodel->rowdelete($data);

            $generated_code = $this->generatecode();
            $generated_code_text = 'Reset Password Code: ' . $generated_code;


            $this->sendcode($phone_value, $generated_code_text);

            $data = array('code' => $generated_code, 'user_id' => $result);
            $this->smsmodel->insert($data);

            $this->session->set_flashdata('tempdata', "Code has been resend. Kindly login to proceed.<br>". validation_errors());

            redirect('admin/');
        } else {
            $this->session->set_flashdata('tempdata', "Sorry Kindly provide your email.<br>". validation_errors());
            redirect('admin/');
        }
    }

    public function forgetpass()
    {
        $email_value = $this->session->userdata('emailsession');
        $phone_value = $this->session->userdata('phonevalue');
        $result = $this->session->userdata('useridvalue');

        if (!empty($email_value) && $email_value != null) {
            $generated_code = $this->generatecode();
            $generated_code_text = 'Reset Password Code: ' . $generated_code;

            $this->sendcode($phone_value, $generated_code_text);

            $data = array('code' => $generated_code, 'user_id' => $result);
            $this->smsmodel->insert($data);

            $data = array('activated' => 0);
            $condition = array('user_id' => $result);

            $result = $this->user->update($data, $condition);

            if ($result > 0) {
                $this->load->view("adminwebsite/header");
                $this->load->view('adminwebsite/resetcode');
                $this->load->view("adminwebsite/footer");
            } else {
                $this->session->set_flashdata('tempdata', "Sorry there was a problem, please try again.<br>". validation_errors());
                redirect('admin/');
            }
        } else {
            $this->session->set_flashdata('tempdata', "Sorry You need to provide your email.<br>". validation_errors());
            redirect('admin/');
        }
    }


    //Forget password functionality
    public function confirmreset()
    {
        $user_id = $this->session->userdata('useridvalue');
        if (!empty($user_id) && ($user_id != null)) {
            $this->form_validation->set_rules('activationcode', 'Activation Code', 'trim|required|xss_clean');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|min_length[6]');

            if ($this->form_validation->run() == false) {
                $this->session->set_flashdata('sesserror', validation_errors());
                redirect('main/enterresetcode');
            } else {
                $userid = $this->session->userdata('useridvalue');
                $confirmationcode = $this->input->post('activationcode');
                $password_value = $this->input->post('password');

                $data = array('code' => $confirmationcode, 'user_id' => $userid);
                $result = $this->smsmodel->getcode($data);

                if ($result->num_rows() > 0) {
                    $cpassword = $password_value . $this->config->item('encryption_key');
                    $password_value = hash('whirlpool', $cpassword);

                    $data = array('activated' => 1, 'password' => $password_value);
                    $condition = array('user_id' => $userid);

                    $result = $this->user->update($data, $condition);

                    if ($result > 0) {
                        $data = array('code' => $confirmationcode, 'user_id' => $userid);
                        $result = $this->smsmodel->rowdelete($data);
                        $this->session->set_flashdata('sesssuccess', 'Reset password successful, login to proceed.');
                        redirect('admin/');
                    } else {
                        $this->session->set_flashdata('sesserror', 'Ooops! Something went wrong, kindly re-enter the reset code sent to you. ');
                        redirect('main/enterresetcode');
                    }
                } else {
                    $this->session->set_flashdata('sesserror', 'Wrong reset code');
                    redirect('main/enterresetcode');
                }
            }
        } else {
            $this->session->set_flashdata('sesserror', 'Kindly enter your phone number');
            redirect('admin/');
        }
    }


    public function sendcode($phone, $message)
    {
        $username = 'daguindd'; // use 'sandbox' for development in the test environment
        $apiKey = '266b72f9303a7fd8fae0eff285ab2adf0e0254442ab1db2c6880d564648fa9d3'; // use your sandbox app API key for development in the test environment
        $AT = new AfricasTalkingGateway($username, $apiKey);

        // Get one of the services
        $sms = $AT->sendMessage($phone, $message, 'ARI');
    }

    public function endsession()
    {
        $this->session->sess_destroy();
        $this->session->set_flashdata('tempdata', "Sorry your previous session has expired. Please log in to continue.");
        redirect('admin/');
    }

    public function generatecode()
    {
        $word = '';
        $pick = '0123456789';
        $length = 4;
        while (strlen($word) < $length) {
            $word .= substr($pick, mt_rand(0, 100000) % (strlen($pick)), 1);
        }

        return $word;
    }
}
