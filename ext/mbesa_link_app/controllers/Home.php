<?php
class Home extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('captcha');
		$this->load->model('captchamodel');
	}
	
	public function index(){
		$this->load->view("website/header");
		$this->load->view('website/index');
		$this->load->view("website/footer");
	}

	public function reset_password(){
		$this->load->view("website/header");
		$this->load->view('website/reset');
		$this->load->view("website/footer");
	}
	
	public function forgot_password(){
		$this->load->view("website/header");
		$this->load->view('website/forgot');
		$this->load->view("website/footer");
	}

	function getCaptcha(){
		$vals = array(
		   'img_path'	=>  dirname(dirname(dirname(__FILE__))).'/captcha/',
		   'img_url'	=>  base_url() .'captcha/',
		);
	   $cap = $this->captcha->create_captcha($vals);
		$this->captchamodel->insert($cap['word'],$cap['time'],"login");
	   header("Content-type: image/png");
	   echo file_get_contents(dirname(dirname(dirname(__FILE__))).'/captcha/'.$cap['filename']);
   }
   
}
?>