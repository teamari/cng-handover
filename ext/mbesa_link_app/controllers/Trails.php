<?php
class Trails extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $userid = $this->session->userdata('userid');
        if (empty($userid) || ($userid == null)) {
            $this->session->set_flashdata('tempdata', "Sorry Please log in to continue.");
            redirect('admin/');
        }

        $this->load->library('Datatables');
    }

    public function countusers()
    {
        $query = 'select count(*) as countusers from tblUser';

        $data = array();
        foreach ($this->db->query($query)->result_array() as $row) {
            $k=array();
            foreach ($row as $item=>$value) {
                array_push($k, $value);
            }
            array_push($data, $k);
        }
        echo json_encode(array("aaData"=>$data));
    }

    public function counttills()
    {
      $query = 'select count(*) as countusers from tblCredentials where type = 1';

        $data = array();
        foreach ($this->db->query($query)->result_array() as $row) {
            $k=array();
            foreach ($row as $item=>$value) {
                array_push($k, $value);
            }
            array_push($data, $k);
        }
        echo json_encode(array("aaData"=>$data));
    }

    public function countbills()
    {
        $query = 'select count(*) as countusers from tblCredentials where type = 0';

        $data = array();
        foreach ($this->db->query($query)->result_array() as $row) {
            $k=array();
            foreach ($row as $item=>$value) {
                array_push($k, $value);
            }
            array_push($data, $k);
        }
        echo json_encode(array("aaData"=>$data));
    }

    //Datatables for the admin users
    public function getusers()
    {
        $this->datatables->select("full_name, email, phone, case when activated=1 then 'Active' else 'Inactive' end, case when enabled=1 then 'Enable' else 'Disabled' end")
        ->from('tblUser')
        ->join('tblLogin', 'tblLogin.user_id = tblUser.id', 'inner');
        echo $this->datatables->generate();
    }

//DataTable for the paybill numbers
    public function getbills()
    {
        $this->datatables->select("shortCode, branch, case when deleted=1 then 'Yes' else 'No' end, case when enabled=1 then 'Yes' else 'No' end")
        ->from('tblCredentials')
        ->where('tblCredentials.type=0');
        echo $this->datatables->generate();
    }

    //DataTable for the till numbers
    public function gettills()
    {
        $this->datatables->select("shortCode, branch, case when deleted=1 then 'Yes' else 'No' end, case when enabled=1 then 'Yes' else 'No' end")
        ->from('tblCredentials')
        ->where('tblCredentials.type=1');
        echo $this->datatables->generate();
    }

        //DataTable for the reg trail numbers
    public function getregtrail()
    {
        $this->datatables->select("full_name, phone_registered, tblRegistrationLog.registration_date,status, description, ip_address, headers")
        ->from('tblRegistrationLog')
        ->join('tblUser', 'tblUser.id = tblRegistrationLog.registered_by_id', 'inner');;
        echo $this->datatables->generate();
    }
}
