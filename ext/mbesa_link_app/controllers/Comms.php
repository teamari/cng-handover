<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Comms extends CI_Controller {

	public function __construct()
	{
		parent::__construct(); 
		$this->load->model('msgmodel');
		$this->load->library('email');
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html'; 
		$config['newline'] ='\r\n';
		$config['clrf'] ='\r\n';
		$config['protocol'] = 'smtp';
		
		$config['smtp_host'] = SMTP_HOST;
		$config['smtp_port'] = 4050;
		$config['smtp_user'] = SMTP_USER;
		$config['smtp_pass'] = SMTP_PASS;

        $this->email->initialize($config);
	}

    public function index(){
        show_404();
	}

	function comm($user,$code){
		set_time_limit(0);
		$user = base64_decode(urldecode($user));
		$code = base64_decode(urldecode($code));
		$msgs = $this->msgmodel->get(array('code'=>$code));
		foreach($msgs->result_array() as $msg){
			if(strtolower($msg['type']) == strtolower("email")){
				$this->msgmodel->update(array('deliverydescription'=> 'Sent'),array('code'=>$code));
				$this->email->to($msg['destination']);
				$this->email->subject($msg['subject']);
				$this->email->from(MAIL_FROM);
				$this->email->message(  $msg['message'] );
				$this->email->send();
			}
		}
	}
}
