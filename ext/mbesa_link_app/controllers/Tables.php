<?php
class Tables extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('Datatables');
		$this->load->model('tokenmodel');
		$this->load->model('dailyreport','export');
		$this->load->model('user');
		$this->load->model('credentials');
		
    //Load the emailing settings
		$this->load->library('email');
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html'; 
		$config['newline'] ='\r\n';
		$config['clrf'] ='\r\n';
		$config['protocol'] = 'smtp';
		
		$config['smtp_host'] = SMTP_HOST;
		$config['smtp_port'] = SMTP_PORT;
		$config['smtp_user'] = SMTP_USER;
		$config['smtp_pass'] = SMTP_PASS;

		$this->email->initialize($config);
		
		$this->load->library('excel');

	}

	function enforce(){
		$session = $this->session->userdata('username');
		$token = $this->session->userdata('token');
		if(empty($token)){
			show_404();
			return false;
		} else {
			$tokens = $this->tokenmodel->get(array('token'=>$token,'isvalid'=>1,'username'=>$this->session->userdata('username')));
			if($tokens->num_rows()>0){
				$mtoken = $tokens->row();
				$now = strtotime(date('Y-m-d H:i:s'));
				$lastactivity = strtotime($mtoken->lastactive);
				$interval =  round(abs($now - $lastactivity) / 60,0);

				if($interval < TIMEOUT){
					return true; 
				}
			} else{
				show_404();
				return false; 
			}
		}
	}

	function leta_audittrail()
	{
		$this->enforce();
		$from = strtotime($this->input->get('from'));
		$to = strtotime($this->input->get('to'));
		$sfrom = date('Y-m-d',$from);
		$sto = date('Y-m-d',$to);
		$this->datatables->select("case when users.UserCode is null then audit.UserCode else concat(firstname,' ',secondname) end,
			activity,activitydate,ipaddress,case when queryStatus=1 then 'OK' else 'FAILED' end")
		->from('audit')
		->join('users', 'audit.UserCode = users.UserCode and users.recordstate=0', 'left outer')
		->where('activitydate >=', $sfrom .' 00:00:00')
		->where('activitydate <=', $sto  .' 23:59:59');
		echo $this->datatables->generate();
	}

	function fetchusers()
	{
		$this->enforce();
		$this->datatables->select("tid,UserCode, firstname,email, phone,usertype,creationdate,case when UserEnabled=1 then 'Active' else 'Disabled' end")
		->from('users')
		->where('usertype <>','Supa')
		->where('recordstate',0);
		echo $this->datatables->generate();
	}

	function fetchreportusers()
	{
		$this->enforce();
		$this->datatables->select("id,name, email,case when active=1 then 'Active' else 'Disabled' end")
		->from('tblreportemails');
		echo $this->datatables->generate();
	}

	function leta_customers_trans(){
		$this->enforce();

		$from = strtotime($this->input->get('from'));
		$to = strtotime($this->input->get('to'));
		$sfrom = date('Y-m-d',$from);
		$sto = date('Y-m-d',$to);
		$selected = $this->session->userdata('selectdcode');
		
		if($selected  == 664988){
			$db =  $this->load->database('ARI_CUMMINS_LIVE',true);
		}else if($selected  == 562691){
			$db =  $this->load->database('ARI_CONF_NIIT',true);
		}else{
			$db =  $this->load->database('ARI_CONFIRM',true);
		}
		$query = 'select max("ContactName") as ContactName,"MSISDN",sum("TransAmount") as TransAmount, sum("Balance") as Balance,"MSISDN" as pno';
		
		if($selected  == 664988){
			$query .=' from ARI_CUMMINS_LIVE.OMPS where';
		}else if($selected  == 562691){
			$query .=' from ARI_CONF_NIIT.OMPS where';
		}else{
			$query .=' from ARI_CONFIRM.OMPS where';
		}
		
		$query .=' "TransTime" >=\''. $sfrom .' 00:00:00\' and';

		if($selected != null)
		{
			if($selected  == 664988){
				$query .=' ARI_CUMMINS_LIVE.OMPS."BusinessShortCode"='. $selected .' and "TransTime" <=\''. $sto  .' 23:59:59\'  group by "MSISDN" ';
			}else if($selected  == 562691){
				$query .=' ARI_CONF_NIIT.OMPS."BusinessShortCode"='. $selected .' and "TransTime" <=\''. $sto  .' 23:59:59\'  group by "MSISDN" ';
			}else{
				$query .=' ARI_CONFIRM.OMPS."BusinessShortCode"='. $selected .' and "TransTime" <=\''. $sto  .' 23:59:59\'  group by "MSISDN" ';
			}
		}else{
			$query .=' "TransTime" <=\''. $sto  .' 23:59:59\'  group by "MSISDN"';
		}   
		
		$m = $db->query($query);

		$data = array();
		foreach($db->query($query)->result_array() as $row){
			$k=array();
			foreach($row as $item=>$value){
				array_push($k,$value);
			}
			array_push($data,$k);
		}
		echo json_encode(array("sEcho"=>0,"iTotalRecords"=>count($data),"iTotalDisplayRecords"=>count($data),"aaData"=>$data));
	}


  /**
   * Array ( [0] => Array ( [ID] => 1 [ObjType] => 24 [TransID] => Mx001 
   * [SapDocNum] => 110055698 [BPCode] => C-ASS-CG0008 [BPName] => CAR & GENERAL - MOMBASA 
   * [DocDate] => 2019-03-22 [DocTotal] => 1000 [TransfSum] => 1000 
   * [MPESA_Amount] => 1000 [DocStatus] => N ) ) 
   */
  function leta_trans(){
  	$this->enforce();

  	$from = strtotime($this->input->get('from'));
  	$to = strtotime($this->input->get('to'));
  	$sfrom = date('Y-m-d',$from);
  	$sto = date('Y-m-d',$to);

  	$selected = $this->session->userdata('selectdcode');
  	if($selected  == 664988){
  		$query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CUMMINS_LIVE.OMPS."TransID","SapDocNum","BPName","DocDate",
  		"DocTotal", CASE WHEN "DocStatus" like \'N\' then \'Ok\' else case when "DocStatus" is null then \'Pending Allocation\' else \'Failed\' end end as statuss';
  		$query .= ' from ARI_CUMMINS_LIVE.OMPS';
  		$query .= ' left outer join ARI_CUMMINS_LIVE.MPS1 on ARI_CUMMINS_LIVE.OMPS."TransID" = ARI_CUMMINS_LIVE.MPS1."TransID"';
  		$query .= ' where "TransTime" >=\''. $sfrom .' 00:00:00\' and  "TransTime" <=\''. $sto  .' 23:59:59\'';
  	}else if($selected  == 562691){
  		$query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CONF_NIIT.OMPS."TransID","SapDocNum","BPName","DocDate",
  		"DocTotal", CASE WHEN "DocStatus" like \'N\' then \'Ok\' else case when "DocStatus" is null then \'Pending Allocation\' else \'Failed\' end end as statuss';
  		$query .= ' from ARI_CONF_NIIT.OMPS';
  		$query .= ' left outer join ARI_CONF_NIIT.MPS1 on ARI_CONF_NIIT.OMPS."TransID" = ARI_CONF_NIIT.MPS1."TransID"';
  		$query .= ' where "TransTime" >=\''. $sfrom .' 00:00:00\' and  "TransTime" <=\''. $sto  .' 23:59:59\'';
  	}else{
  		$query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CONFIRM.OMPS."TransID","SapDocNum","BPName","DocDate",
  		"DocTotal", CASE WHEN "DocStatus" like \'N\' then \'Ok\' else case when "DocStatus" is null then \'Pending Allocation\' else \'Failed\' end end as statuss';
  		$query .= ' from ARI_CONFIRM.OMPS';
  		$query .= ' left outer join ARI_CONFIRM.MPS1 on ARI_CONFIRM.OMPS."TransID" = ARI_CONFIRM.MPS1."TransID"';
  		$query .= ' where "TransTime" >=\''. $sfrom .' 00:00:00\' and  "TransTime" <=\''. $sto  .' 23:59:59\'';
  	}
  	
  	if($selected != null)
  	{
  		if($selected  == 664988){
  			$query .=' and ARI_CUMMINS_LIVE.OMPS."BusinessShortCode"='. $selected .'';
  		}else if($selected  == 562691){
  			$query .=' and ARI_CONF_NIIT.OMPS."BusinessShortCode"='. $selected .'';
  		}else{
  			$query .=' and ARI_CONFIRM.OMPS."BusinessShortCode"='. $selected .'';
  		}
  	}
  	if($selected  == 664988){
  		$db =  $this->load->database('ARI_CUMMINS_LIVE',true);
  	}else if($selected  == 562691){
  		$db =  $this->load->database('ARI_CONF_NIIT',true);
  	}else{
  		$db =  $this->load->database('ARI_CONFIRM',true);
  	}

  	$m = $db->query($query);

  	$data = array();
  	foreach($db->query($query)->result_array() as $row){
  		$k=array();
  		foreach($row as $item=>$value){
  			array_push($k,$value);
  		}
  		array_push($data,$k);
  	}
  	echo json_encode(array("sEcho"=>0,"iTotalRecords"=>count($data),"iTotalDisplayRecords"=>count($data),"aaData"=>$data));
  }

  function leta_trans_today(){
  	$this->enforce();

  	$from = strtotime(date('Y-m-d'));
  	$to = strtotime(date('Y-m-d'));
  	$sfrom = date('Y-m-d',$from);
  	$sto = date('Y-m-d',$to);

  	$selected = $this->session->userdata('selectdcode');
  	if($selected  == 664988){
  		$query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CUMMINS_LIVE.OMPS."TransID","SapDocNum","BPName","DocDate",
  		"DocTotal", CASE WHEN "DocStatus" like \'N\' then \'Ok\' else case when "DocStatus" is null then \'Pending Allocation\' else \'Failed\' end end as statuss';
  		$query .= ' from ARI_CUMMINS_LIVE.OMPS';
  		$query .= ' left outer join ARI_CUMMINS_LIVE.MPS1 on ARI_CUMMINS_LIVE.OMPS."TransID" = ARI_CUMMINS_LIVE.MPS1."TransID"';
  		$query .= ' where "TransTime" >=\''. $sfrom .' 00:00:00\' and  "TransTime" <=\''. $sto  .' 23:59:59\'';
  	}else if($selected  == 562691){
  		$query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CONF_NIIT.OMPS."TransID","SapDocNum","BPName","DocDate",
  		"DocTotal", CASE WHEN "DocStatus" like \'N\' then \'Ok\' else case when "DocStatus" is null then \'Pending Allocation\' else \'Failed\' end end as statuss';
  		$query .= ' from ARI_CONF_NIIT.OMPS';
  		$query .= ' left outer join ARI_CONF_NIIT.MPS1 on ARI_CONF_NIIT.OMPS."TransID" = ARI_CONF_NIIT.MPS1."TransID"';
  		$query .= ' where "TransTime" >=\''. $sfrom .' 00:00:00\' and  "TransTime" <=\''. $sto  .' 23:59:59\'';
  	}else{
  		$query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CONFIRM.OMPS."TransID","SapDocNum","BPName","DocDate",
  		"DocTotal", CASE WHEN "DocStatus" like \'N\' then \'Ok\' else case when "DocStatus" is null then \'Pending Allocation\' else \'Failed\' end end as statuss';
  		$query .= ' from ARI_CONFIRM.OMPS';
  		$query .= ' left outer join ARI_CONFIRM.MPS1 on ARI_CONFIRM.OMPS."TransID" = ARI_CONFIRM.MPS1."TransID"';
  		$query .= ' where "TransTime" >=\''. $sfrom .' 00:00:00\' and  "TransTime" <=\''. $sto  .' 23:59:59\'';
  	}

  	if($selected != null)
  	{
  		if($selected  == 664988){
  			$query .=' and ARI_CUMMINS_LIVE.OMPS."BusinessShortCode"='. $selected .'';
  		}else if($selected  == 562691){
  			$query .=' and ARI_CONF_NIIT.OMPS."BusinessShortCode"='. $selected .'';
  		}else{
  			$query .=' and ARI_CONFIRM.OMPS."BusinessShortCode"='. $selected .'';
  		}
  	}
  	
  	if($selected  == 664988){
  		$db =  $this->load->database('ARI_CUMMINS_LIVE',true);
  	}else if($selected  == 562691){
  		$db =  $this->load->database('ARI_CONF_NIIT',true);
  	}else{
  		$db =  $this->load->database('ARI_CONFIRM',true);
  	}
  	$m = $db->query($query);

  	$data = array();
  	foreach($db->query($query)->result_array() as $row){
  		$k=array();
  		foreach($row as $item=>$value){
  			array_push($k,$value);
  		}
  		array_push($data,$k);
  	}
  	echo json_encode(array("sEcho"=>0,"iTotalRecords"=>count($data),"iTotalDisplayRecords"=>count($data),"aaData"=>$data));
  }

  function leta_trans_status(){
  	$this->enforce();

  	$from = strtotime($this->input->get('from'));
  	$to = strtotime($this->input->get('to'));
  	$sfrom = date('Y-m-d',$from);
  	$sto = date('Y-m-d',$to);
  	$status = $this->input->get('status');
  	$selected = $this->session->userdata('selectdcode');
  	
  	if($selected  == 664988){
  		$query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CUMMINS_LIVE.OMPS."TransID","SapDocNum","BPName","DocDate",
  		"DocTotal", CASE WHEN "DocStatus" like \'N\' then \'Ok\' else case when "DocStatus" is null then \'Pending Allocation\' else \'Failed\' end end as statuss';
  		$query .= ' from ARI_CUMMINS_LIVE.OMPS';
  		$query .= ' left outer join ARI_CUMMINS_LIVE.MPS1 on ARI_CUMMINS_LIVE.OMPS."TransID" = ARI_CUMMINS_LIVE.MPS1."TransID"';
  		$query .= ' where "TransTime" >=\''. $sfrom .' 00:00:00\' and  "TransTime" <=\''. $sto  .' 23:59:59\'';
  		if(!empty($status)){
  			$query .= ' and  "DocStatus" = \''. $status .'\'';
  		}
  		else{
  			$query .= ' and  "DocStatus" is null';
  		}
  	}else if($selected  == 562691){
  		$query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CONF_NIIT.OMPS."TransID","SapDocNum","BPName","DocDate",
  		"DocTotal", CASE WHEN "DocStatus" like \'N\' then \'Ok\' else case when "DocStatus" is null then \'Pending Allocation\' else \'Failed\' end end as statuss';
  		$query .= ' from ARI_CONF_NIIT.OMPS';
  		$query .= ' left outer join ARI_CONF_NIIT.MPS1 on ARI_CONF_NIIT.OMPS."TransID" = ARI_CONF_NIIT.MPS1."TransID"';
  		$query .= ' where "TransTime" >=\''. $sfrom .' 00:00:00\' and  "TransTime" <=\''. $sto  .' 23:59:59\'';
  		if(!empty($status)){
  			$query .= ' and  "DocStatus" = \''. $status .'\'';
  		}
  		else{
  			$query .= ' and  "DocStatus" is null';
  		}
  	}else{
  		$query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CONFIRM.OMPS."TransID","SapDocNum","BPName","DocDate",
  		"DocTotal", CASE WHEN "DocStatus" like \'N\' then \'Ok\' else case when "DocStatus" is null then \'Pending Allocation\' else \'Failed\' end end as statuss';
  		$query .= ' from ARI_CONFIRM.OMPS';
  		$query .= ' left outer join ARI_CONFIRM.MPS1 on ARI_CONFIRM.OMPS."TransID" = ARI_CONFIRM.MPS1."TransID"';
  		$query .= ' where "TransTime" >=\''. $sfrom .' 00:00:00\' and  "TransTime" <=\''. $sto  .' 23:59:59\'';
  		if(!empty($status)){
  			$query .= ' and  "DocStatus" = \''. $status .'\'';
  		}
  		else{
  			$query .= ' and  "DocStatus" is null';
  		}
  	}

  	if($selected != null)
  	{
  		if($selected  == 664988){
  			$query .=' and ARI_CUMMINS_LIVE.OMPS."BusinessShortCode"='. $selected .'';
  		}else if($selected  == 562691){
  			$query .=' and ARI_CONF_NIIT.OMPS."BusinessShortCode"='. $selected .'';
  		}else{
  			$query .=' and ARI_CONFIRM.OMPS."BusinessShortCode"='. $selected .'';
  		}
  	}

  	if($selected  == 664988){
  		$db =  $this->load->database('ARI_CUMMINS_LIVE',true);
  	}if($selected  == 562691){
  		$db =  $this->load->database('ARI_CONF_NIIT',true);
  	}else{
  		$db =  $this->load->database('ARI_CONFIRM',true);
  	}
  	$m = $db->query($query);

  	$data = array();
  	foreach($db->query($query)->result_array() as $row){
  		$k=array();
  		foreach($row as $item=>$value){
  			array_push($k,$value);
  		}
  		array_push($data,$k);
  	}
  	echo json_encode(array("sEcho"=>0,"iTotalRecords"=>count($data),"iTotalDisplayRecords"=>count($data),"aaData"=>$data));

  }

  function leta_todays_pending_allocations(){
  	$this->enforce();
  	
  	$from = strtotime(date('Y-m-d'));
  	$to = strtotime(date('Y-m-d'));
  	$sfrom = date('Y-m-d',$from);
  	$sto = date('Y-m-d',$to);
  	
  	$status = $this->input->get('status');
  	$selected = $this->session->userdata('selectdcode');

  	if($selected  == 664988){
  		$query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CUMMINS_LIVE.OMPS."TransID","SapDocNum","BPName","DocDate",
  		"DocTotal", CASE WHEN "DocStatus" like \'N\' then \'Ok\' else case when "DocStatus" is null then \'Pending Allocation\' else \'Failed\' end end as statuss';
  		$query .= ' from ARI_CUMMINS_LIVE.OMPS';
  		$query .= ' left outer join ARI_CUMMINS_LIVE.MPS1 on ARI_CUMMINS_LIVE.OMPS."TransID" = ARI_CUMMINS_LIVE.MPS1."TransID"';
  		
  		$query .= ' where "TransTime" >=\''. $sfrom .' 00:00:00\' and  "TransTime" <=\''. $sto  .' 23:59:59\'';
  		if(!empty($status)){
  			$query .= ' and  "DocStatus" = \''. $status .'\'';
  		}
  		else{
  			$query .= ' and  "DocStatus" is null';
  		}
  	}else if($selected  == 562691){
  		$query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CONF_NIIT.OMPS."TransID","SapDocNum","BPName","DocDate",
  		"DocTotal", CASE WHEN "DocStatus" like \'N\' then \'Ok\' else case when "DocStatus" is null then \'Pending Allocation\' else \'Failed\' end end as statuss';
  		$query .= ' from ARI_CONF_NIIT.OMPS';
  		$query .= ' left outer join ARI_CONF_NIIT.MPS1 on ARI_CONF_NIIT.OMPS."TransID" = ARI_CONF_NIIT.MPS1."TransID"';
  		
  		$query .= ' where "TransTime" >=\''. $sfrom .' 00:00:00\' and  "TransTime" <=\''. $sto  .' 23:59:59\'';
  		if(!empty($status)){
  			$query .= ' and  "DocStatus" = \''. $status .'\'';
  		}
  		else{
  			$query .= ' and  "DocStatus" is null';
  		}
  	}else{
  		$query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CONFIRM.OMPS."TransID","SapDocNum","BPName","DocDate",
  		"DocTotal", CASE WHEN "DocStatus" like \'N\' then \'Ok\' else case when "DocStatus" is null then \'Pending Allocation\' else \'Failed\' end end as statuss';
  		$query .= ' from ARI_CONFIRM.OMPS';
  		$query .= ' left outer join ARI_CONFIRM.MPS1 on ARI_CONFIRM.OMPS."TransID" = ARI_CONFIRM.MPS1."TransID"';
  		
  		$query .= ' where "TransTime" >=\''. $sfrom .' 00:00:00\' and  "TransTime" <=\''. $sto  .' 23:59:59\'';
  		if(!empty($status)){
  			$query .= ' and  "DocStatus" = \''. $status .'\'';
  		}
  		else{
  			$query .= ' and  "DocStatus" is null';
  		}
  	}

  	if($selected != null)
  	{
  		if($selected  == 664988){
  			$query .=' and ARI_CUMMINS_LIVE.OMPS."BusinessShortCode"='. $selected .'';
  		}else{
  			$query .=' and ARI_CONFIRM.OMPS."BusinessShortCode"='. $selected .'';
  		}
  	}

  	if($selected  == 664988){
  		$db =  $this->load->database('ARI_CUMMINS_LIVE',true);
  	}else if($selected  == 562691){
  		$db =  $this->load->database('ARI_CONF_NIIT',true);
  	}else{
  		$db =  $this->load->database('ARI_CONFIRM',true);
  	}
  	$m = $db->query($query);

  	$data = array();
  	foreach($db->query($query)->result_array() as $row){
  		$k=array();
  		foreach($row as $item=>$value){
  			array_push($k,$value);
  		}
  		array_push($data,$k);
  	}
  	echo json_encode(array("sEcho"=>0,"iTotalRecords"=>count($data),"iTotalDisplayRecords"=>count($data),"aaData"=>$data));

  }

  function leta_reconciled(){
  	$this->enforce();

  	$from = strtotime($this->input->get('from'));
  	$to = strtotime($this->input->get('to'));
  	$sfrom = date('Y-m-d',$from);
  	$sto = date('Y-m-d',$to);
  	$status = '1';
  	$selected = $this->session->userdata('selectdcode');

  	if($selected  == 664988){
  		$query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CUMMINS_LIVE.OMPS."TransID","SapDocNum","BPName","DocDate",
  		"DocTotal", CASE WHEN "DocStatus" like \'N\' then \'Ok\' else case when "DocStatus" is null then \'Pending Allocation\' else \'Failed\' end end';
  		$query .= ' from ARI_CUMMINS_LIVE.OMPS';
  		$query .= ' left outer join ARI_CUMMINS_LIVE.MPS1 on ARI_CUMMINS_LIVE.OMPS."TransID" = ARI_CUMMINS_LIVE.MPS1."TransID"';
  		$query .=' where TO_VARCHAR(ARI_CUMMINS_LIVE.OMPS."TransTime", \'YYYY-MM-DD\')>=\''. $sfrom .'   00:00:00\' and TO_VARCHAR(ARI_CUMMINS_LIVE.OMPS."TransTime", \'YYYY-MM-DD\')<=\''. $sto .'  23:59:59\' and "RECONSTATUS" =\'1\'';
  		$query .= ' and "RECONSTATUS" = \''. $status .'\'';
  	}else if($selected  == 562691){
  		$query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CONF_NIIT.OMPS."TransID","SapDocNum","BPName","DocDate",
  		"DocTotal", CASE WHEN "DocStatus" like \'N\' then \'Ok\' else case when "DocStatus" is null then \'Pending Allocation\' else \'Failed\' end end';
  		$query .= ' from ARI_CONF_NIIT.OMPS';
  		$query .= ' left outer join ARI_CONF_NIIT.MPS1 on ARI_CONF_NIIT.OMPS."TransID" = ARI_CONF_NIIT.MPS1."TransID"';
  		$query .=' where TO_VARCHAR(ARI_CONF_NIIT.OMPS."TransTime", \'YYYY-MM-DD\')>=\''. $sfrom .'   00:00:00\' and TO_VARCHAR(ARI_CONF_NIIT.OMPS."TransTime", \'YYYY-MM-DD\')<=\''. $sto .'  23:59:59\' and "RECONSTATUS" =\'1\'';
  		$query .= ' and "RECONSTATUS" = \''. $status .'\'';
  	}else{
  		$query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CONFIRM.OMPS."TransID","SapDocNum","BPName","DocDate",
  		"DocTotal", CASE WHEN "DocStatus" like \'N\' then \'Ok\' else case when "DocStatus" is null then \'Pending Allocation\' else \'Failed\' end end';
  		$query .= ' from ARI_CONFIRM.OMPS';
  		$query .= ' left outer join ARI_CONFIRM.MPS1 on ARI_CONFIRM.OMPS."TransID" = ARI_CONFIRM.MPS1."TransID"';
  		$query .=' where TO_VARCHAR(ARI_CONFIRM.OMPS."TransTime", \'YYYY-MM-DD\')>=\''. $sfrom .'   00:00:00\' and TO_VARCHAR(ARI_CONFIRM.OMPS."TransTime", \'YYYY-MM-DD\')<=\''. $sto .'  23:59:59\' and "RECONSTATUS" =\'1\'';
  		$query .= ' and "RECONSTATUS" = \''. $status .'\'';
  	}
  	
  	if($selected != null)
  	{
  		if($selected  == 664988){
  			$query .=' and ARI_CUMMINS_LIVE.OMPS."BusinessShortCode"='. $selected .'';
  		}else if($selected  == 562691){
  			$query .=' and ARI_CONF_NIIT.OMPS."BusinessShortCode"='. $selected .'';
  		}else{
  			$query .=' and ARI_CONFIRM.OMPS."BusinessShortCode"='. $selected .'';
  		}
  	}

  	if($selected  == 664988){
  		$db =  $this->load->database('ARI_CUMMINS_LIVE',true);
  	}else if($selected  == 562691){
  		$db =  $this->load->database('ARI_CONF_NIIT',true);
  	}else{
  		$db =  $this->load->database('ARI_CONFIRM',true);
  	}
  	$m = $db->query($query);

  	$data = array();
  	foreach($db->query($query)->result_array() as $row){
  		$k=array();
  		foreach($row as $item=>$value){
  			array_push($k,$value);
  		}
  		array_push($data,$k);
  	}
  	echo json_encode(array("sEcho"=>0,"iTotalRecords"=>count($data),"iTotalDisplayRecords"=>count($data),"aaData"=>$data));

  }

  /**
   * Array ( [0] => Array ( [ID] => 1 [TransID] => Mx001 [TransTime] => 2019-03-22:1503 
   * [TransAmount] => 5000 [BusinessShortCode] => txx0 [MSISDN] => 0734400966 
   * [ContactName] => test1 [Balance] => 4000 [flag] => 1 ) 
   */

  function leta_customer_history(){
  	$this->enforce();

  	$from = strtotime($this->input->get('from'));
  	$to = strtotime($this->input->get('to'));
  	$customer = $this->input->get('id');

  	$sfrom = date('Y-m-d',$from);
  	$sto = date('Y-m-d',$to);
  	$selected = $this->session->userdata('selectdcode');
  	if($selected  == 664988){
  		$query ='select ARI_CUMMINS_LIVE.OMPS."ContactName",ARI_CUMMINS_LIVE.OMPS."TransTime",ARI_CUMMINS_LIVE.OMPS."TransAmount",ARI_CUMMINS_LIVE.OMPS."TransID","SapDocNum"';
  		$query .=" from ARI_CUMMINS_LIVE.OMPS";
  		$query .=' left outer join ARI_CUMMINS_LIVE.MPS1 on ARI_CUMMINS_LIVE.OMPS."TransID" = ARI_CUMMINS_LIVE.MPS1."TransID"';
  		$query .=' where "MSISDN" like \''. $customer .'\' and "TransTime" >=\''. $sfrom .' 00:00:00\' and ';
  		$query .=' "TransTime" <=\''. $sto  .' 23:59:59\'';
  	}else if($selected  == 562691){
  		$query ='select ARI_CONF_NIIT.OMPS."ContactName",ARI_CONF_NIIT.OMPS."TransTime",ARI_CONF_NIIT.OMPS."TransAmount",ARI_CONF_NIIT.OMPS."TransID","SapDocNum"';
  		$query .=" from ARI_CONF_NIIT.OMPS";
  		$query .=' left outer join ARI_CONF_NIIT.MPS1 on ARI_CONF_NIIT.OMPS."TransID" = ARI_CONF_NIIT.MPS1."TransID"';
  		$query .=' where "MSISDN" like \''. $customer .'\' and "TransTime" >=\''. $sfrom .' 00:00:00\' and ';
  		$query .=' "TransTime" <=\''. $sto  .' 23:59:59\'';
  	}else{
  		$query ='select ARI_CONFIRM.OMPS."ContactName",ARI_CONFIRM.OMPS."TransTime",ARI_CONFIRM.OMPS."TransAmount",ARI_CONFIRM.OMPS."TransID","SapDocNum"';
  		$query .=" from ARI_CONFIRM.OMPS";
  		$query .=' left outer join ARI_CONFIRM.MPS1 on ARI_CONFIRM.OMPS."TransID" = ARI_CONFIRM.MPS1."TransID"';
  		$query .=' where "MSISDN" like \''. $customer .'\' and "TransTime" >=\''. $sfrom .' 00:00:00\' and ';
  		$query .=' "TransTime" <=\''. $sto  .' 23:59:59\'';
  	}

  	if($selected != null)
  	{
  		if($selected  == 664988){
  			$query .=' and ARI_CUMMINS_LIVE.OMPS."BusinessShortCode"='. $selected .'';
  		}else if($selected  == 562691){
  			$query .=' and ARI_CONF_NIIT.OMPS."BusinessShortCode"='. $selected .'';
  		}else{
  			$query .=' and ARI_CONFIRM.OMPS."BusinessShortCode"='. $selected .'';
  		}
  	}

  	if($selected  == 664988){
  		$db =  $this->load->database('ARI_CUMMINS_LIVE',true);
  	}else if($selected  == 562691){
  		$db =  $this->load->database('ARI_CONF_NIIT',true);
  	}else{
  		$db =  $this->load->database('ARI_CONFIRM',true);
  	}
  	$m = $db->query($query);

  	$data = array();
  	foreach($db->query($query)->result_array() as $row){
  		$k=array();
  		foreach($row as $item=>$value){
  			array_push($k,$value);
  		}
  		array_push($data,$k);
  	}
  	echo json_encode(array("sEcho"=>0,"iTotalRecords"=>count($data),"iTotalDisplayRecords"=>count($data),"aaData"=>$data));
  }


  function topStats(){
  	$this->enforce();
   
  	$selected = $this->session->userdata('selectdcode');
  	
  	if($selected  == 664988){
  		$query ='select count(*) as count, sum(ARI_CUMMINS_LIVE.OMPS."TransAmount"), sum(case when ARI_CUMMINS_LIVE.MPS1."TransID" is null then 0 else ARI_CUMMINS_LIVE.MPS1."DocTotal"  end )';
  		$query .=' from ARI_CUMMINS_LIVE.OMPS';
  		$query .=' left outer join ARI_CUMMINS_LIVE.MPS1 on ARI_CUMMINS_LIVE.OMPS."TransID" = ARI_CUMMINS_LIVE.MPS1."TransID"';
  	}else if($selected  == 562691){
  		$query ='select count(*) as count, sum(ARI_CONF_NIIT.OMPS."TransAmount"), sum(case when ARI_CONF_NIIT.MPS1."TransID" is null then 0 else ARI_CONF_NIIT.MPS1."DocTotal"  end )';
  		$query .=' from ARI_CONF_NIIT.OMPS';
  		$query .=' left outer join ARI_CONF_NIIT.MPS1 on ARI_CONF_NIIT.OMPS."TransID" = ARI_CONF_NIIT.MPS1."TransID"';
  	}else{
  		$query ='select count(*) as count, sum(ARI_CONFIRM.OMPS."TransAmount"), sum(case when ARI_CONFIRM.MPS1."TransID" is null then 0 else ARI_CONFIRM.MPS1."DocTotal"  end )';
  		$query .=' from ARI_CONFIRM.OMPS';
  		$query .=' left outer join ARI_CONFIRM.MPS1 on ARI_CONFIRM.OMPS."TransID" = ARI_CONFIRM.MPS1."TransID"';
  	}
   
  	if($selected != null)
  	{
  		if($selected  == 664988){
  			$query .=' where TO_VARCHAR(ARI_CUMMINS_LIVE.OMPS."TransTime", \'YYYY-MM-DD\')=\''. date('Y-m-d') .'\' and ARI_CUMMINS_LIVE.OMPS."BusinessShortCode"='. $selected .' group by TO_VARCHAR(ARI_CUMMINS_LIVE.OMPS."TransTime", \'YYYY-MM-DD\')';
  		}else if($selected  == 562691){
  			$query .=' where TO_VARCHAR(ARI_CONF_NIIT.OMPS."TransTime", \'YYYY-MM-DD\')=\''. date('Y-m-d') .'\' and ARI_CONF_NIIT.OMPS."BusinessShortCode"='. $selected .' group by TO_VARCHAR(ARI_CONF_NIIT.OMPS."TransTime", \'YYYY-MM-DD\')';
  		}else{
  			$query .=' where TO_VARCHAR(ARI_CONFIRM.OMPS."TransTime", \'YYYY-MM-DD\')=\''. date('Y-m-d') .'\' and ARI_CONFIRM.OMPS."BusinessShortCode"='. $selected .' group by TO_VARCHAR(ARI_CONFIRM.OMPS."TransTime", \'YYYY-MM-DD\')';
  		}
  	}else{
  		if($selected  == 664988){
  			$query .=' where TO_VARCHAR(ARI_CUMMINS_LIVE.OMPS."TransTime", \'YYYY-MM-DD\')=\''. date('Y-m-d') .'\' group by TO_VARCHAR(ARI_CUMMINS_LIVE.OMPS."TransTime", \'YYYY-MM-DD\') ';
  		}else if($selected  == 562691){
  			$query .=' where TO_VARCHAR(ARI_CONF_NIIT.OMPS."TransTime", \'YYYY-MM-DD\')=\''. date('Y-m-d') .'\' group by TO_VARCHAR(ARI_CONF_NIIT.OMPS."TransTime", \'YYYY-MM-DD\') ';
  		}else{
  			$query .=' where TO_VARCHAR(ARI_CONFIRM.OMPS."TransTime", \'YYYY-MM-DD\')=\''. date('Y-m-d') .'\' group by TO_VARCHAR(ARI_CONFIRM.OMPS."TransTime", \'YYYY-MM-DD\') ';
  		}
  	}

  	if($selected  == 664988){
  		$db =  $this->load->database('ARI_CUMMINS_LIVE',true);
  	}else if($selected  == 562691){
  		$db =  $this->load->database('ARI_CONF_NIIT',true);
  	}else{
  		$db =  $this->load->database('ARI_CONFIRM',true);
  	}
  	$m = $db->query($query);

  	$data = array();
  	foreach($db->query($query)->result_array() as $row){
  		$k=array();
  		foreach($row as $item=>$value){
  			array_push($k,$value);
  		}
  		array_push($data,$k);
  	}
  	echo json_encode(array("sEcho"=>0,"iTotalRecords"=>count($data),"iTotalDisplayRecords"=>count($data),"aaData"=>$data));
  }


  function monthlyStats(){
  	$this->enforce();
  	$selected = $this->session->userdata('selectdcode');
  	if($selected  == 664988){
  		$query ='select LEFT(MONTHNAME(ARI_CUMMINS_LIVE.OMPS."TransTime"),3) as month, sum(case when ARI_CUMMINS_LIVE.MPS1."TransID" is null then 0 else ARI_CUMMINS_LIVE.MPS1."DocTotal"  end ) as total';
  		$query .=' from ARI_CUMMINS_LIVE.OMPS';
  		$query .=' left outer join ARI_CUMMINS_LIVE.MPS1 on ARI_CUMMINS_LIVE.OMPS."TransID" = ARI_CUMMINS_LIVE.MPS1."TransID"';
  	}if($selected  == 562691){
  		$query ='select LEFT(MONTHNAME(ARI_CONF_NIIT.OMPS."TransTime"),3) as month, sum(case when ARI_CONF_NIIT.MPS1."TransID" is null then 0 else ARI_CONF_NIIT.MPS1."DocTotal"  end ) as total';
  		$query .=' from ARI_CONF_NIIT.OMPS';
  		$query .=' left outer join ARI_CONF_NIIT.MPS1 on ARI_CONF_NIIT.OMPS."TransID" = ARI_CONF_NIIT.MPS1."TransID"';
  	}else{
  		$query ='select LEFT(MONTHNAME(ARI_CONFIRM.OMPS."TransTime"),3) as month, sum(case when ARI_CONFIRM.MPS1."TransID" is null then 0 else ARI_CONFIRM.MPS1."DocTotal"  end ) as total';
  		$query .=' from ARI_CONFIRM.OMPS';
  		$query .=' left outer join ARI_CONFIRM.MPS1 on ARI_CONFIRM.OMPS."TransID" = ARI_CONFIRM.MPS1."TransID"';
  	}
  	
  	if($selected != null)
  	{
  		if($selected  == 664988){
  			$query .=' where TO_VARCHAR(ARI_CUMMINS_LIVE.OMPS."TransTime", \'YYYY\')=\''. date('Y') .'\'  and ARI_CUMMINS_LIVE.OMPS."BusinessShortCode"='. $selected .' group by LEFT(MONTHNAME(ARI_CUMMINS_LIVE.OMPS."TransTime"),3)';
  		}else if($selected  == 562691){
  			$query .=' where TO_VARCHAR(ARI_CONF_NIIT.OMPS."TransTime", \'YYYY\')=\''. date('Y') .'\'  and ARI_CONF_NIIT.OMPS."BusinessShortCode"='. $selected .' group by LEFT(MONTHNAME(ARI_CONF_NIIT.OMPS."TransTime"),3)';
  		}else{
  			$query .=' where TO_VARCHAR(ARI_CONFIRM.OMPS."TransTime", \'YYYY\')=\''. date('Y') .'\'  and ARI_CONFIRM.OMPS."BusinessShortCode"='. $selected .' group by LEFT(MONTHNAME(ARI_CONFIRM.OMPS."TransTime"),3)';
  		}
  	}else{
  		if($selected  == 664988){
  			$query .=' where TO_VARCHAR(ARI_CUMMINS_LIVE.OMPS."TransTime", \'YYYY\')=\''. date('Y') .'\' group by LEFT(MONTHNAME(ARI_CUMMINS_LIVE.OMPS."TransTime"),3)';
  		}else if($selected  == 562691){
  			$query .=' where TO_VARCHAR(ARI_CONF_NIIT.OMPS."TransTime", \'YYYY\')=\''. date('Y') .'\' group by LEFT(MONTHNAME(ARI_CONF_NIIT.OMPS."TransTime"),3)';
  		}else{
  			$query .=' where TO_VARCHAR(ARI_CONFIRM.OMPS."TransTime", \'YYYY\')=\''. date('Y') .'\' group by LEFT(MONTHNAME(ARI_CONFIRM.OMPS."TransTime"),3)';
  		}
  	}

  	if($selected  == 664988){
  		$db =  $this->load->database('ARI_CUMMINS_LIVE',true);
  	}else if($selected  == 562691){
  		$db =  $this->load->database('ARI_CONF_NIIT',true);
  	}else{
  		$db =  $this->load->database('ARI_CONFIRM',true);
  	}
  	$m = $db->query($query);

  	$data = array();
  	foreach ($db->query($query)->result_array() as $row) {
  		$data[] = $row;
  	}
  	echo json_encode($data);

  }

  function monthlyStatsallocated(){
  	$this->enforce();
  	$selected = $this->session->userdata('selectdcode');
  	$sdate = date('Y-m-d', strtotime('today - 30 days'));
  	$status = $this->input->get('status');
  	if($selected  == 664988){
  		$query ='select DAYOFMONTH(ARI_CUMMINS_LIVE.OMPS."TransTime") as month, sum(case when ARI_CUMMINS_LIVE.MPS1."TransID" is null then 0 else ARI_CUMMINS_LIVE.MPS1."DocTotal"  end ) as total';
  		$query .=' from ARI_CUMMINS_LIVE.OMPS';
  		$query .=' left outer join ARI_CUMMINS_LIVE.MPS1 on ARI_CUMMINS_LIVE.OMPS."TransID" = ARI_CUMMINS_LIVE.MPS1."TransID"';
  	}else if($selected  == 664988){
  		$query ='select DAYOFMONTH(ARI_CONF_NIIT.OMPS."TransTime") as month, sum(case when ARI_CONF_NIIT.MPS1."TransID" is null then 0 else ARI_CONF_NIIT.MPS1."DocTotal"  end ) as total';
  		$query .=' from ARI_CONF_NIIT.OMPS';
  		$query .=' left outer join ARI_CONF_NIIT.MPS1 on ARI_CONF_NIIT.OMPS."TransID" = ARI_CONF_NIIT.MPS1."TransID"';
  	}else{
  		$query ='select DAYOFMONTH(ARI_CONFIRM.OMPS."TransTime") as month, sum(case when ARI_CONFIRM.MPS1."TransID" is null then 0 else ARI_CONFIRM.MPS1."DocTotal"  end ) as total';
  		$query .=' from ARI_CONFIRM.OMPS';
  		$query .=' left outer join ARI_CONFIRM.MPS1 on ARI_CONFIRM.OMPS."TransID" = ARI_CONFIRM.MPS1."TransID"';
  	}
  	

  	if($selected != null)
  	{
  		if($selected  == 664988){
  			if(!empty($status)){
  				$query .=' where ARI_CUMMINS_LIVE.OMPS."TransTime">=\''. $sdate .'\' and  "DocStatus" = \''. $status .'\'  and ARI_CUMMINS_LIVE.OMPS."BusinessShortCode"='. $selected .' group by DAYOFMONTH(ARI_CUMMINS_LIVE.OMPS."TransTime") order by DAYOFMONTH(ARI_CUMMINS_LIVE.OMPS."TransTime")';
  			}else{
  				$query .=' where ARI_CUMMINS_LIVE.OMPS."TransTime">=\''. $sdate .'\'  and ARI_CUMMINS_LIVE.OMPS."BusinessShortCode"='. $selected .' group by DAYOFMONTH(ARI_CUMMINS_LIVE.OMPS."TransTime") order by DAYOFMONTH(ARI_CUMMINS_LIVE.OMPS."TransTime")';
  			}
  		}else if($selected  == 664988){
  			if(!empty($status)){
  				$query .=' where ARI_CONF_NIIT.OMPS."TransTime">=\''. $sdate .'\' and  "DocStatus" = \''. $status .'\'  and ARI_CONF_NIIT.OMPS."BusinessShortCode"='. $selected .' group by DAYOFMONTH(ARI_CONF_NIIT.OMPS."TransTime") order by DAYOFMONTH(ARI_CONF_NIIT.OMPS."TransTime")';
  			}else{
  				$query .=' where ARI_CONF_NIIT.OMPS."TransTime">=\''. $sdate .'\'  and ARI_CONF_NIIT.OMPS."BusinessShortCode"='. $selected .' group by DAYOFMONTH(ARI_CONF_NIIT.OMPS."TransTime") order by DAYOFMONTH(ARI_CONF_NIIT.OMPS."TransTime")';
  			}
  		}else{
  			if(!empty($status)){
  				$query .=' where ARI_CONFIRM.OMPS."TransTime">=\''. $sdate .'\' and  "DocStatus" = \''. $status .'\'  and ARI_CONFIRM.OMPS."BusinessShortCode"='. $selected .' group by DAYOFMONTH(ARI_CONFIRM.OMPS."TransTime") order by DAYOFMONTH(ARI_CONFIRM.OMPS."TransTime")';
  			}else{
  				$query .=' where ARI_CONFIRM.OMPS."TransTime">=\''. $sdate .'\'  and ARI_CONFIRM.OMPS."BusinessShortCode"='. $selected .' group by DAYOFMONTH(ARI_CONFIRM.OMPS."TransTime") order by DAYOFMONTH(ARI_CONFIRM.OMPS."TransTime")';
  			}
  		}
  	}else{
  		if($selected  == 664988){
  			if(!empty($status)){
  				$query .=' where ARI_CUMMINS_LIVE.OMPS."TransTime">=\''. $sdate .'\' and  "DocStatus" = \''. $status .'\' group by DAYOFMONTH(ARI_CUMMINS_LIVE.OMPS."TransTime") order by DAYOFMONTH(ARI_CUMMINS_LIVE.OMPS."TransTime")';
  			}else{
  				$query .=' where ARI_CUMMINS_LIVE.OMPS."TransTime">=\''. $sdate .'\' group by DAYOFMONTH(ARI_CUMMINS_LIVE.OMPS."TransTime") order by DAYOFMONTH(ARI_CUMMINS_LIVE.OMPS."TransTime")';
  			}
  		}else if($selected  == 562691){
  			if(!empty($status)){
  				$query .=' where ARI_CONF_NIIT.OMPS."TransTime">=\''. $sdate .'\' and  "DocStatus" = \''. $status .'\' group by DAYOFMONTH(ARI_CONF_NIIT.OMPS."TransTime") order by DAYOFMONTH(ARI_CONF_NIIT.OMPS."TransTime")';
  			}else{
  				$query .=' where ARI_CONF_NIIT.OMPS."TransTime">=\''. $sdate .'\' group by DAYOFMONTH(ARI_CONF_NIIT.OMPS."TransTime") order by DAYOFMONTH(ARI_CONF_NIIT.OMPS."TransTime")';
  			}
  		}else{
  			if(!empty($status)){
  				$query .=' where ARI_CONFIRM.OMPS."TransTime">=\''. $sdate .'\' and  "DocStatus" = \''. $status .'\' group by DAYOFMONTH(ARI_CONFIRM.OMPS."TransTime") order by DAYOFMONTH(ARI_CONFIRM.OMPS."TransTime")';
  			}else{
  				$query .=' where ARI_CONFIRM.OMPS."TransTime">=\''. $sdate .'\' group by DAYOFMONTH(ARI_CONFIRM.OMPS."TransTime") order by DAYOFMONTH(ARI_CONFIRM.OMPS."TransTime")';
  			}
  		}
  	}

  	if($selected  == 664988){
  		$db =  $this->load->database('ARI_CUMMINS_LIVE',true);
  	}else if($selected  == 562691){
  		$db =  $this->load->database('ARI_CONF_NIIT',true);
  	}else{
  		$db =  $this->load->database('ARI_CONFIRM',true);
  	}
  	$m = $db->query($query);

  	$data = array();
  	foreach ($db->query($query)->result_array() as $row) {
  		$data[] = $row;
  	}
  	echo json_encode($data);

  }

  function monthlyStatsall(){
  	$this->enforce();
  	$status = $this->input->get('status');
  	$selected = $this->session->userdata('selectdcode');
  	
  	if($selected  == 664988){
  		$query ='select LEFT(MONTHNAME(ARI_CUMMINS_LIVE.OMPS."TransTime"),3) as month, sum(case when "DocStatus" is null then 0 else ARI_CUMMINS_LIVE.MPS1."DocTotal"  end ) as allocated, sum(case when "DocStatus" is null then ARI_CUMMINS_LIVE.MPS1."DocTotal" else 0  end ) as pending';
  		$query .=' from ARI_CUMMINS_LIVE.OMPS';
  		$query .=' left outer join ARI_CUMMINS_LIVE.MPS1 on ARI_CUMMINS_LIVE.OMPS."TransID" = ARI_CUMMINS_LIVE.MPS1."TransID"';
  	}else if($selected  == 562691){
  		$query ='select LEFT(MONTHNAME(ARI_CONF_NIIT.OMPS."TransTime"),3) as month, sum(case when "DocStatus" is null then 0 else ARI_CONF_NIIT.MPS1."DocTotal"  end ) as allocated, sum(case when "DocStatus" is null then ARI_CONF_NIIT.MPS1."DocTotal" else 0  end ) as pending';
  		$query .=' from ARI_CONF_NIIT.OMPS';
  		$query .=' left outer join ARI_CONF_NIIT.MPS1 on ARI_CONF_NIIT.OMPS."TransID" = ARI_CONF_NIIT.MPS1."TransID"';
  	}else{
  		$query ='select LEFT(MONTHNAME(ARI_CONFIRM.OMPS."TransTime"),3) as month, sum(case when "DocStatus" is null then 0 else ARI_CONFIRM.MPS1."DocTotal"  end ) as allocated, sum(case when "DocStatus" is null then ARI_CONFIRM.MPS1."DocTotal" else 0  end ) as pending';
  		$query .=' from ARI_CONFIRM.OMPS';
  		$query .=' left outer join ARI_CONFIRM.MPS1 on ARI_CONFIRM.OMPS."TransID" = ARI_CONFIRM.MPS1."TransID"';
  	}
  	
  	if($selected != null)
  	{
  		if($selected  == 664988){
  			if(!empty($status)){
  				$query .=' where ARI_CUMMINS_LIVE.OMPS."TransTime">=\''. date('Y') .'\' and  "DocStatus" = \''. $status .'\' and ARI_CUMMINS_LIVE.OMPS."BusinessShortCode"='. $selected .' group by LEFT(MONTHNAME(ARI_CUMMINS_LIVE.OMPS."TransTime"),3) order by month';
  			}else{
  				$query .=' where ARI_CUMMINS_LIVE.OMPS."TransTime">=\''. date('Y') .'\' and ARI_CUMMINS_LIVE.OMPS."BusinessShortCode"='. $selected .' group by LEFT(MONTHNAME(ARI_CUMMINS_LIVE.OMPS."TransTime"),3) order by month';
  			}
  		}else if($selected  == 562691){
  			if(!empty($status)){
  				$query .=' where ARI_CONF_NIIT.OMPS."TransTime">=\''. date('Y') .'\' and  "DocStatus" = \''. $status .'\' and ARI_CONF_NIIT.OMPS."BusinessShortCode"='. $selected .' group by LEFT(MONTHNAME(ARI_CONF_NIIT.OMPS."TransTime"),3) order by month';
  			}else{
  				$query .=' where ARI_CONF_NIIT.OMPS."TransTime">=\''. date('Y') .'\' and ARI_CONF_NIIT.OMPS."BusinessShortCode"='. $selected .' group by LEFT(MONTHNAME(ARI_CONF_NIIT.OMPS."TransTime"),3) order by month';
  			}
  		}else{
  			if(!empty($status)){
  				$query .=' where ARI_CONFIRM.OMPS."TransTime">=\''. date('Y') .'\' and  "DocStatus" = \''. $status .'\' and ARI_CONFIRM.OMPS."BusinessShortCode"='. $selected .' group by LEFT(MONTHNAME(ARI_CONFIRM.OMPS."TransTime"),3) order by month';
  			}else{
  				$query .=' where ARI_CONFIRM.OMPS."TransTime">=\''. date('Y') .'\' and ARI_CONFIRM.OMPS."BusinessShortCode"='. $selected .' group by LEFT(MONTHNAME(ARI_CONFIRM.OMPS."TransTime"),3) order by month';
  			}
  		}
  	}else{
  		if($selected  == 664988){
  			if(!empty($status)){
  				$query .=' where ARI_CUMMINS_LIVE.OMPS."TransTime">=\''. date('Y') .'\' and  "DocStatus" = \''. $status .'\' group by LEFT(MONTHNAME(ARI_CUMMINS_LIVE.OMPS."TransTime"),3) order by month';
  			}else{
  				$query .=' where ARI_CUMMINS_LIVE.OMPS."TransTime">=\''. date('Y') .'\' group by LEFT(MONTHNAME(ARI_CUMMINS_LIVE.OMPS."TransTime"),3) order by month';
  			}
  		}else if($selected  == 562691){
  			if(!empty($status)){
  				$query .=' where ARI_CONF_NIIT.OMPS."TransTime">=\''. date('Y') .'\' and  "DocStatus" = \''. $status .'\' group by LEFT(MONTHNAME(ARI_CONF_NIIT.OMPS."TransTime"),3) order by month';
  			}else{
  				$query .=' where ARI_CONF_NIIT.OMPS."TransTime">=\''. date('Y') .'\' group by LEFT(MONTHNAME(ARI_CONF_NIIT.OMPS."TransTime"),3) order by month';
  			}
  		}else{
  			if(!empty($status)){
  				$query .=' where ARI_CONFIRM.OMPS."TransTime">=\''. date('Y') .'\' and  "DocStatus" = \''. $status .'\' group by LEFT(MONTHNAME(ARI_CONFIRM.OMPS."TransTime"),3) order by month';
  			}else{
  				$query .=' where ARI_CONFIRM.OMPS."TransTime">=\''. date('Y') .'\' group by LEFT(MONTHNAME(ARI_CONFIRM.OMPS."TransTime"),3) order by month';
  			}
  		}
  	}

  	if($selected  == 664988){
  		$db =  $this->load->database('ARI_CUMMINS_LIVE',true);
  	}else if($selected  == 562691){
  		$db =  $this->load->database('ARI_CONF_NIIT',true);
  	}else{
  		$db =  $this->load->database('ARI_CONFIRM',true);
  	}
  	$m = $db->query($query);

  	$data = array();
  	foreach ($db->query($query)->result_array() as $row) {
  		$data[] = $row;
  	}
  	echo json_encode($data);
  }


//Get data for the pending transactions
  function pendingStats(){
  	$this->enforce();
  	$selected = $this->session->userdata('selectdcode');
  	
  	if($selected  == 664988){
  		$query ='select count(*) as count, sum(ARI_CUMMINS_LIVE.OMPS."TransAmount"), sum(case when ARI_CUMMINS_LIVE.MPS1."TransID" is null then 0 else ARI_CUMMINS_LIVE.MPS1."DocTotal"  end )';
  		$query .=' from ARI_CUMMINS_LIVE.OMPS';
  		$query .=' left outer join ARI_CUMMINS_LIVE.MPS1 on ARI_CUMMINS_LIVE.OMPS."TransID" = ARI_CUMMINS_LIVE.MPS1."TransID"';
  	}else if($selected  == 562691){
  		$query ='select count(*) as count, sum(ARI_CONF_NIIT.OMPS."TransAmount"), sum(case when ARI_CONF_NIIT.MPS1."TransID" is null then 0 else ARI_CONF_NIIT.MPS1."DocTotal"  end )';
  		$query .=' from ARI_CONF_NIIT.OMPS';
  		$query .=' left outer join ARI_CONF_NIIT.MPS1 on ARI_CONF_NIIT.OMPS."TransID" = ARI_CONF_NIIT.MPS1."TransID"';
  	}else{
  		$query ='select count(*) as count, sum(ARI_CONFIRM.OMPS."TransAmount"), sum(case when ARI_CONFIRM.MPS1."TransID" is null then 0 else ARI_CONFIRM.MPS1."DocTotal"  end )';
  		$query .=' from ARI_CONFIRM.OMPS';
  		$query .=' left outer join ARI_CONFIRM.MPS1 on ARI_CONFIRM.OMPS."TransID" = ARI_CONFIRM.MPS1."TransID"';
  	}
  	
  	if($selected != null)
  	{
  		if($selected  == 664988){
  			$query .=' where ARI_CUMMINS_LIVE.OMPS."TransTime">=\''. date('Y-m-d') .'\' and ARI_CUMMINS_LIVE.OMPS."BusinessShortCode"='. $selected .' and  "DocStatus" is null group by TO_VARCHAR(ARI_CUMMINS_LIVE.OMPS."TransTime", \'YYYY-MM-DD\') ';
  		}else if($selected  == 562691){
  			$query .=' where ARI_CONF_NIIT.OMPS."TransTime">=\''. date('Y-m-d') .'\' and ARI_CONF_NIIT.OMPS."BusinessShortCode"='. $selected .' and  "DocStatus" is null group by TO_VARCHAR(ARI_CONF_NIIT.OMPS."TransTime", \'YYYY-MM-DD\') ';
  		}else{
  			$query .=' where ARI_CONFIRM.OMPS."TransTime">=\''. date('Y-m-d') .'\' and ARI_CONFIRM.OMPS."BusinessShortCode"='. $selected .' and  "DocStatus" is null group by TO_VARCHAR(ARI_CONFIRM.OMPS."TransTime", \'YYYY-MM-DD\') ';
  		}
  	}else{
  		if($selected  == 664988){
  			$query .=' where ARI_CUMMINS_LIVE.OMPS."TransTime">=\''. date('Y-m-d') .'\' and  "DocStatus" is null group by TO_VARCHAR(ARI_CUMMINS_LIVE.OMPS."TransTime", \'YYYY-MM-DD\') ';
  		}else if($selected  == 562691){
  			$query .=' where ARI_CONF_NIIT.OMPS."TransTime">=\''. date('Y-m-d') .'\' and  "DocStatus" is null group by TO_VARCHAR(ARI_CONF_NIIT.OMPS."TransTime", \'YYYY-MM-DD\') ';
  		}else{
  			$query .=' where ARI_CONFIRM.OMPS."TransTime">=\''. date('Y-m-d') .'\' and  "DocStatus" is null group by TO_VARCHAR(ARI_CONFIRM.OMPS."TransTime", \'YYYY-MM-DD\') ';
  		}
  	}

  	if($selected  == 664988){
  		$db =  $this->load->database('ARI_CUMMINS_LIVE',true);
  	}else if($selected  == 562691){
  		$db =  $this->load->database('ARI_CONF_NIIT',true);
  	}else{
  		$db =  $this->load->database('ARI_CONFIRM',true);
  	}
  	$m = $db->query($query);

  	$data = array();
  	foreach($db->query($query)->result_array() as $row){
  		$k=array();
  		foreach($row as $item=>$value){
  			array_push($k,$value);
  		}
  		array_push($data,$k);
  	}
  	echo json_encode(array("sEcho"=>0,"iTotalRecords"=>count($data),"iTotalDisplayRecords"=>count($data),"aaData"=>$data));

  }

  function listchart(){
  	$this->enforce();

  	$sfrom = date('Y-m-d', strtotime(("-4 week")));
  	$sto = date('Y-m-d');
  	$selected = $this->session->userdata('selectdcode');

  	if($selected  == 664988){
  		$query ='select TO_VARCHAR(ARI_CUMMINS_LIVE.OMPS."TransTime", \'YYYY-MM-DD\') as mdate,ARI_CUMMINS_LIVE.MPS1."BPName",count(*) as count, sum(ARI_CUMMINS_LIVE.OMPS."TransAmount")';
  		$query .=' from ARI_CUMMINS_LIVE.OMPS';
  		$query .=' left outer join ARI_CUMMINS_LIVE.MPS1 on ARI_CUMMINS_LIVE.OMPS."TransID" = ARI_CUMMINS_LIVE.MPS1."TransID"';
  		$query .=' where ARI_CUMMINS_LIVE.OMPS."TransTime" >=\''. $sfrom .' 00:00:00\' and  ARI_CUMMINS_LIVE.OMPS."TransTime" <=\''. $sto  .' 23:59:59\'';
  	}if($selected  == 562691){
  		$query ='select TO_VARCHAR(ARI_CONF_NIIT.OMPS."TransTime", \'YYYY-MM-DD\') as mdate,ARI_CONF_NIIT.MPS1."BPName",count(*) as count, sum(ARI_CONF_NIIT.OMPS."TransAmount")';
  		$query .=' from ARI_CONF_NIIT.OMPS';
  		$query .=' left outer join ARI_CONF_NIIT.MPS1 on ARI_CONF_NIIT.OMPS."TransID" = ARI_CONF_NIIT.MPS1."TransID"';
  		$query .=' where ARI_CONF_NIIT.OMPS."TransTime" >=\''. $sfrom .' 00:00:00\' and  ARI_CONF_NIIT.OMPS."TransTime" <=\''. $sto  .' 23:59:59\'';
  	}else{
  		$query ='select TO_VARCHAR(ARI_CONFIRM.OMPS."TransTime", \'YYYY-MM-DD\') as mdate,ARI_CONFIRM.MPS1."BPName",count(*) as count, sum(ARI_CONFIRM.OMPS."TransAmount")';
  		$query .=' from ARI_CONFIRM.OMPS';
  		$query .=' left outer join ARI_CONFIRM.MPS1 on ARI_CONFIRM.OMPS."TransID" = ARI_CONFIRM.MPS1."TransID"';
  		$query .=' where ARI_CONFIRM.OMPS."TransTime" >=\''. $sfrom .' 00:00:00\' and  ARI_CONFIRM.OMPS."TransTime" <=\''. $sto  .' 23:59:59\'';
  	}
	
  	if($selected != null)
  	{
  		if($selected  == 664988){
  			$query .=' and ARI_CUMMINS_LIVE.OMPS."BusinessShortCode"='. $selected .'';
  		}else if($selected  == 562691){
  			$query .=' and ARI_CONF_NIIT.OMPS."BusinessShortCode"='. $selected .'';
  		}else{
  			$query .=' and ARI_CONFIRM.OMPS."BusinessShortCode"='. $selected .'';
  		}
  	}
  	if($selected  == 664988){
  		$query .=' group by TO_VARCHAR(ARI_CUMMINS_LIVE.OMPS."TransTime", \'YYYY-MM-DD\') ,ARI_CUMMINS_LIVE.MPS1."BPName"';
  	}else if($selected  == 562691){
  		$query .=' group by TO_VARCHAR(ARI_CONF_NIIT.OMPS."TransTime", \'YYYY-MM-DD\') ,ARI_CONF_NIIT.MPS1."BPName"';
  	}else{
  		$query .=' group by TO_VARCHAR(ARI_CONFIRM.OMPS."TransTime", \'YYYY-MM-DD\') ,ARI_CONFIRM.MPS1."BPName"';
  	}

  	if($selected  == 664988){
  		$db =  $this->load->database('ARI_CUMMINS_LIVE',true);
  	}else if($selected  == 562691){
  		$db =  $this->load->database('ARI_CONF_NIIT',true);
  	}else{
  		$db =  $this->load->database('ARI_CONFIRM',true);
  	}
  	$m = $db->query($query);

  	$data = array();
  	foreach($db->query($query)->result_array() as $row){
  		$k=array();
  		foreach($row as $item=>$value){
  			array_push($k,$value);
  		}
  		array_push($data,$k);
  	}
  	echo json_encode(array("sEcho"=>0,"iTotalRecords"=>count($data),"iTotalDisplayRecords"=>count($data),"aaData"=>$data));

  }

//Get the details that has not been sent sms confirmation
//for the utilised amount
  function checkunsend($condition = '')
  {
// $this->enforce();
  	$selected = $this->session->userdata('selectdcode');
  	$query ='select *, ARI_CONFIRM.MPS1."ID" as ID';
  	$query .=' from ARI_CONFIRM.MPS1';
  	$query .= ' inner join ARI_CONFIRM.OMPS on ARI_CONFIRM.MPS1."TransID" = ARI_CONFIRM.OMPS."TransID"';
  	$query .= ' where ARI_CONFIRM.MPS1."MessageStatus"=0 order by ARI_CONFIRM.MPS1."ID"';

  	if($selected  == 664988){
  		$db =  $this->load->database('ARI_CUMMINS_LIVE',true);
  	}else if($selected  == 562691){
  		$db =  $this->load->database('ARI_CONF_NIIT',true);
  	}else{
  		$db =  $this->load->database('ARI_CONFIRM',true);
  	}
  	$m = $db->query($query);

  	foreach ($m->result() as $row) {
  		
  		$name = explode(" ", $row->ContactName);
  		$name = $name[0];
  		
    //Get the total balance remaining in account
  		$querybalance ='select *, ARI_CONFIRM.OMPS."Balance"';
  		$querybalance .=' from ARI_CONFIRM.OMPS';
  		$querybalance .= ' where ARI_CONFIRM.OMPS."MSISDN"=254717269895';
  		
  		$thebalance = 0;

  		
  		$dbb =  $this->load->database('ARI_CONFIRM',true);
  		$mm = $dbb->query($querybalance);

  		foreach ($mm->result() as $rows) {
  			$thebalance +=  $rows->Balance;
  		}
  		$curl_post_data = array(
  			'apiKey' => '971d33a3080c4335bc8135bc13c3f2cb',
  			'shortCode' => 'Car_General',
  			'message' => 'Thank you for shopping with us. KES. '. $row->DocTotal.' has been allocated against Doc: '. $row->SapDocNum.'. Your Current account balance is KES '. $thebalance.'. For any queries call our toll free number  0800724600.  Car and General.',
  			'recipient' => $row->MSISDN,
  			'callbackURL' => 'https://774d6fd7.ngrok.io',
  			'enqueue' => 0
  		);
  		$data_string = json_encode($curl_post_data);
  		$url = 'https://api.vaspro.co.ke/v3/BulkSMS/api/create';
  		$curl = curl_init();
  		curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json')); //setting a custom header
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);


    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
    	echo "cURL Error #:" . $err;
    } else {
    	$query ='update ARI_CONFIRM.MPS1 set ARI_CONFIRM.MPS1."MessageStatus"=1 where ARI_CONFIRM.MPS1."ID"='. $row->ID;

    	$db =  $this->load->database('ARI_CONFIRM',true);
    	$m = $db->query($query);

    	if($m == 1){
    		echo $response;
    	}else{
    		echo 'Error';
    	}
    }
  }
}


public function test1(){
	$dateToTest = "2020-02-29";
	$stime =  date('Y-m-d',strtotime($dateToTest));
	$empInfo = $this->export->mytest($stime);
	$counter = 1;
	foreach ($empInfo as $element) {
		//echo $counter.' '.$element['ID'].' '.$element['ContactName'].' '.$element['MSISDN'].' '.$element['TransAmount'].' '.$element['TransTime'].'<br>';
		echo $counter.' '.$element['ID'].' '.$element['ContactName'].' '.$element['TransID'].' '.$element['SapDocNum'].' '.$element['BPCode'].' '.$element['BPName'].' '.$element['DocDate'].' '.$element['DocTotal'].' '.$element['TransfSum'].' '.$element['MPESA_Amount'].' '.$element['DocStatus'].'<br>';
		$counter+=1;
	}
}

public function test2(){
	$dateToTest = "2020-06-27";
	$stime =  date('Y-m-d',strtotime($dateToTest));
	$empInfo = $this->export->fetchunallocatedcumulativecumminstest($stime);
	$counter = 1;
	foreach ($empInfo as $element) {
		echo $counter.' '.$element['ObjType'].' '.$element['TransID'].' '.$element['MSISDN'].' '.$element['TransAmount'].' '.date('Y-m-d',strtotime($element['TransTime'])).'<br>';
		//echo $counter.' '.$element['ObjType'].' '.$element['TransID'].' '.$element['SapDocNum'].' '.$element['BPCode'].' '.$element['BPName'].' '.$element['DocDate'].' '.$element['DocTotal'].' '.$element['TransfSum'].' '.$element['MPESA_Amount'].' '.$element['DocStatus'].'<br>';
		$counter+=1;
	}
}
public function test41(){
	$query ='update ARI_CUMMINS_LIVE.OMPS set ARI_CUMMINS_LIVE.OMPS."Balance"=0 where ARI_CUMMINS_LIVE.OMPS."TransID"=\'OHC7KE6UAH\'';

    	$db =  $this->load->database('ARI_CUMMINS_LIVE',true);
    	$m = $db->query($query);

    	if($m == 1){
    		echo $response;
    	}else{
    		echo 'Error';
    	}
}
public function test40(){
	$dateToTest = "2020-06-27";
	$stime =  date('Y-m-d',strtotime($dateToTest));
	$empInfo = $this->export->fetthemps1();
	$counter = 1;
	foreach ($empInfo as $element) {
		echo $counter.' '.$element['ID'].' '.$element['TransID'].' '.$element['TransID'].' '.'<br>';
		//echo $counter.' '.$element['ObjType'].' '.$element['TransID'].' '.$element['SapDocNum'].' '.$element['BPCode'].' '.$element['BPName'].' '.$element['DocDate'].' '.$element['DocTotal'].' '.$element['TransfSum'].' '.$element['MPESA_Amount'].' '.$element['DocStatus'].'<br>';
		$counter+=1;
	}
}

public function test3(){
	$mdate = date('Y-m-d');
	
	$TransAmount =  29349;
	$obt = 24;
	$sabdoc = 50014365;
	$db = $this->load->database("ARI_CUMMINS_LIVE", true);
	//$db->query('delete from  ARI_CUMMINS_LIVE.MPS1 where "ID" = 260');
	$tes  = $db->query("insert into ARI_CUMMINS_LIVE.MPS1 (\"ObjType\",\"TransID\",\"DocDate\",\"MPESA_Amount\",\"DocTotal\",\"SapDocNum\",\"BPCode\",\"BPName\",\"DocStatus\",\"TransfSum\") 
					values(". $obt .",'OHC7KE6UAH','". $mdate ."',". $TransAmount .",". $TransAmount .",". $sabdoc .",'CC00015','CASH SALES - CUMMINS','N',0)");
	echo $tes;
}

public function test4(){
$time =  date('Y-m-d');
  		$query ='select *  from ARI_CUMMINS_LIVE.MPS1 where "TransID" =\'OGU66DWR9O\'';
		
		$db =  $this->load->database('ARI_CUMMINS_LIVE',true);
    $m = $db->query($query);

echo $m->num_rows();
/*
    //return $db->query($query)->result_array();
	$counter = 1;
	foreach ($db->query($query)->result_array() as $element) {
//		echo $counter.' '.$element['ID'].' '.$element['TransID'].' '.$element['MSISDN'].' '.$element['TransAmount'].'<br>';
		echo $counter.' '.$element['TransID'].'<br>';
		$counter+=1;
	}*/
	
}


public function docummins(){
	//Do for cummins
	$totaltransactions = 0;
	$totalamount = 0;
	$mergedtransactions = 0;
	$unmergedtransactions = 0;
	
	$totaltransactionsprevious = 0;
	$totalamountprevious = 0;
	$mergedtransactionsprevious = 0;
	$unmergedtransactionsprevious = 0;
	
	$totaltransactionsthismonth = 0;
	$totalamountthismonth = 0;
	$mergedtransactionsthismonth = 0;
	$unmergedtransactionsthismonth = 0;
	
	$totaltransactionspreviousmonth = 0;
	$totalamountpreviousmonth = 0;
	$mergedtransactionspreviousmonth = 0;
	$unmergedtransactionspreviousmonth = 0;
	
	
        //Dates
	$reportdate = date('Y-m-d', strtotime(("-1 day")));
	$prevousdate = date('Y-m-d', strtotime(("-2 day")));
	
    // create file name
	$fileNamec = 'Daily data summary (Cummins)-'.$reportdate.'.xlsx';  
    // load excel library
	$stime =  date('Y-m-d', strtotime(("-1 day")));
	$this->load->library('excel');
	$empInfo = $this->export->fetchallocatedcummins($stime);
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(1);
	$objPHPExcel->getActiveSheet()->setTitle("Allocated Transactions");
        // set Header
	$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Customer');
	$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Phone Number');
	$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Paid KES');
	$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'On');
	$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Balance KES'); 
	$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Mpesa Code');
	$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'SAP Doc');
	$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Branch');
	$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'SAP Doc Date');
	$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'SAP Doc Total KSH');
	
        // set Row
	$rowCount = 2;
	$transactionsallocated = 0;
	foreach ($empInfo as $element) {
		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['ContactName']);
		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['MSISDN']);
		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['TransAmount']);
		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['TransTime']);
		$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['Balance']);
		$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['TransID']);
		$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['SapDocNum']);
		$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['BPName']);
		$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['DocDate']);
		$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['DocTotal']);
		$rowCount++;
		$transactionsallocated++;
		$totalamount += $element['TransAmount'];
		$mergedtransactions += $element['TransAmount'];
	}

	for($col = 'A'; $col !== 'K'; $col++) {
		$objPHPExcel->getActiveSheet()
		->getColumnDimension($col)
		->setAutoSize(true);
	}
	$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050'); 
/*
	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter->save($fileNamec);*/

        // create file name unallocated
	$fileNameunallocatedc = 'Pending Transactions data (Cummins)-'.$reportdate.'.xlsx';  
        // load excel library
	$this->load->library('excel');
	$stime =  date('Y-m-d', strtotime(("-1 day")));
	$empInfo = $this->export->fetchunallocatedcummins($stime);
	//$objPHPExcel = new PHPExcel();
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(2);
	$objPHPExcel->getActiveSheet()->setTitle("Pending Transactions");
        // set Header
	$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Customer');
	$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Phone Number');
	$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Paid KES');
	$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'On');
	$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Balance KES'); 
	$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Mpesa Code');

        // set Row
	$rowCount = 2;
	$transactionsunallocated = 0;
	foreach ($empInfo as $element) {
		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['ContactName']);
		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['MSISDN']);
		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['TransAmount']);
		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['TransTime']);
		$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['Balance']);
		$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['TransID']);
		$rowCount++;
		$transactionsunallocated++;
		$totalamount += $element['TransAmount'];
		$unmergedtransactions += $element['TransAmount'];
	}

	for($col = 'A'; $col !== 'G'; $col++) {
		$objPHPExcel->getActiveSheet()
		->getColumnDimension($col)
		->setAutoSize(true);
	}
		$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050'); 

	/*
	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter->save($fileNameunallocatedc);*/

        //Get the list of the cumulative pending
        // create file name unallocated cumulative
	$fileNameunallocatedcumulativec = 'Pending Cumulative Transactions  (Cummins) data as at '.$reportdate.'.xlsx';  
        // load excel library
	$this->load->library('excel');
	$stime =  date('Y-m-d', strtotime(("-1 day")));
	$empInfo = $this->export->fetchunallocatedcumulativecummins($stime);
	//$objPHPExcel = new PHPExcel();
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(3);
	$objPHPExcel->getActiveSheet()->setTitle("Pending Cummulative");
        // set Header
	$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Customer');
	$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Phone Number');
	$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Paid KES');
	$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'On');
	$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Balance KES'); 
	$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Mpesa Code');
	$objPHPExcel->getActiveSheet()->SetCellValue('G1', '1 – 30 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('H1', '31 – 60 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('I1', '61 - 90 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Over 90 Days');

        // set Row
	$rowCount = 2;
	foreach ($empInfo as $element) {
		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['ContactName']);
		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['MSISDN']);
		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['TransAmount']);
		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['TransTime']);
		$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['Balance']);
		$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['TransID']);
		$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['thethirty']);
		$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['thesixty']);
		$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['theninety']);
		$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['theabove']);
		$rowCount++;
	}

	for($col = 'A'; $col !== 'K'; $col++) {
		$objPHPExcel->getActiveSheet()
		->getColumnDimension($col)
		->setAutoSize(true);
	}
		$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050'); 

	/*
	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter->save($fileNameunallocatedcumulativec);*/
        //End the list of cumulative pending
		
		

	$totaltransactions = number_format($transactionsunallocated + $transactionsallocated);
	$totalamountunformatted = $totalamount;
	$totalamount = number_format($totalamount);
	$mergedtransactionsunformatted = $mergedtransactions;
	$mergedtransactions = number_format($mergedtransactions);
	$unmergedtransactionsunformatted = $unmergedtransactions;
	$unmergedtransactions = number_format($unmergedtransactions);



     //Get the daily b/f details
    $stime = date('Y-m-d', strtotime(("-1 day")));
	
	$previousdaysallocated = $this->export->fetchallocatedpreviousdayscummins($stime);
	$previousdaysunallocated = $this->export->fetchunallocatedprevioudayscummins($stime);

     //Get the totals for the previous days
	 $transactionsallocatedprevious = 0;
	foreach ($previousdaysallocated as $row) {
		$transactionsallocatedprevious +=  $row[0];
		$totalamountprevious += $row[1];
		$mergedtransactionsprevious +=$row[1];
	}

$transactionsunallocatedprevious = 0;
	foreach ($previousdaysunallocated as $row) {
		$transactionsunallocatedprevious += $row[0];
		$totalamountprevious += $row[1];
		$unmergedtransactionsprevious += $row[1];
	}

	$totaltransactionsprevious = number_format($transactionsunallocatedprevious + $transactionsallocatedprevious - $transactionsunallocated - $transactionsallocated);
	$totalamountpreviousunformatted =  $totalamountprevious;
	$totalamountprevious =  number_format($totalamountprevious);
	$mergedtransactionspreviousunformatted = $mergedtransactionsprevious;
	$mergedtransactionsprevious = number_format($mergedtransactionsprevious);
	$unmergedtransactionspreviousunformatted = $unmergedtransactionsprevious;
	$unmergedtransactionsprevious = number_format($unmergedtransactionsprevious);


    //Create filename for the movement summary for the day's data 
	$fileNamemovementc = 'Movement Summary data  (Cummins)-'.$reportdate.'.xlsx';

        // load excel library
	$this->load->library('excel');
	//$objPHPExcel = new PHPExcel();
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle("Movement Summary");
	
	

        //Create filename for the movement summary for the month's data 
	$objPHPExcel->getActiveSheet()->SetCellValue('A13', 'Mpesa Monthly Movement Summary');
	$objPHPExcel->getActiveSheet()->SetCellValue('A14', 'Month');
	$objPHPExcel->getActiveSheet()->SetCellValue('B14', date('F-Y'));
	$objPHPExcel->getActiveSheet()->SetCellValue('A15', 'Particulars');
	$objPHPExcel->getActiveSheet()->SetCellValue('B15', 'Transactions');
	$objPHPExcel->getActiveSheet()->SetCellValue('C15', 'Total');
	$objPHPExcel->getActiveSheet()->SetCellValue('D15', 'Allocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('E15', 'Un Allocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('B16', 'Nos');
	$objPHPExcel->getActiveSheet()->SetCellValue('C16', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('D16', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('E16', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('A17', 'Month Balance b/f');
	$objPHPExcel->getActiveSheet()->SetCellValue('A18', 'This month');
	$objPHPExcel->getActiveSheet()->SetCellValue('A19', 'Monthly c/f');

     //Populate the data for the monthly section of the movement 
     //This month data 
	$stime =  date('Y-m-d',strtotime("first day of this month"));
	$thismonthallocated = $this->export->fetchallocatedthismonthcummins($stime);
	$thismonthunallocated = $this->export->fetchunallocatedthismonthcummins($stime);

     //Previous month
	$stime =  date('Y-m-d',strtotime("last day of previous month"));
	$previousmonthallocated = $this->export->fetchallocatedpreviousmonthcummins($stime);
	$previousmonthunallocated = $this->export->fetchunallocatedpreviousmonthcummins($stime);

     //Get the totals for the previous month
	foreach ($previousmonthallocated as $row) {
		$transactionsallocatedpreviousmonth +=  $row[0];
		$totalamountpreviousmonth += $row[2];
		$mergedtransactionspreviousmonth +=$row[2];
	}

	foreach ($previousmonthunallocated as $row) {
		$transactionsunallocatedpreviousmonth+= $row[0];
		$totalamountpreviousmonth += $row[1];
		$unmergedtransactionspreviousmonth += $row[1];
	}


     //Get the allocated totals for this month
	foreach ($thismonthallocated as $row) {
		$transactionsallocatedthismonth++;
		$totalamountthismonth += $row['DocTotal'];
		$mergedtransactionsthismonth += $row['DocTotal'];
	}


  //Get the unallocated totals for this month
	foreach ($thismonthunallocated as $row) {
		$transactionsunallocatedthismonth++;
		$totalamountthismonth += $row['TransAmount'];
		$unmergedtransactionsthismonth += $row['TransAmount'];
	}

    //Sums for this month 
	$totaltransactionsthismonthunformatted = $transactionsunallocatedthismonth + $transactionsallocatedthismonth;
	$totaltransactionsthismonth = number_format($transactionsunallocatedthismonth + $transactionsallocatedthismonth);
	$totalamountthismonthunformatted = $totalamountthismonth;
	$totalamountthismonth = number_format($totalamountthismonth);
	$mergedtransactionsthismonthunformatted = $mergedtransactionsthismonth;
	$mergedtransactionsthismonth = number_format($mergedtransactionsthismonth);
	$unmergedtransactionsthismonthunformatted = $unmergedtransactionsthismonth;
	$unmergedtransactionsthismonth = number_format($unmergedtransactionsthismonth);

      //Sums for previous months
	$totaltransactionspreviousmonthunformatted = $transactionsunallocatedpreviousmonth + $transactionsallocatedpreviousmonth;
	$totaltransactionspreviousmonth = number_format($transactionsunallocatedpreviousmonth + $transactionsallocatedpreviousmonth);
	$totalamountpreviousmonthunformatted =  $totalamountpreviousmonth;
	$totalamountpreviousmonth =  number_format($totalamountpreviousmonth);
	$mergedtransactionspreviousmonthunformatted = $mergedtransactionspreviousmonth;
	$mergedtransactionspreviousmonth = number_format($mergedtransactionspreviousmonth);
	$unmergedtransactionspreviousmonthunformatted = $unmergedtransactionspreviousmonth;
	$unmergedtransactionspreviousmonth = number_format($unmergedtransactionspreviousmonth);


        //last months' transactions
	$objPHPExcel->getActiveSheet()->SetCellValue('B17', $totaltransactionspreviousmonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('C17', $totalamountpreviousmonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('D17', $mergedtransactionspreviousmonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('E17', $unmergedtransactionspreviousmonth);

    //this month's transactions
	$objPHPExcel->getActiveSheet()->SetCellValue('B18', $totaltransactionsthismonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('C18', $totalamountthismonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('D18', $mergedtransactionsthismonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('E18', $unmergedtransactionsthismonth);

      //summary
	$objPHPExcel->getActiveSheet()->SetCellValue('B19', (number_format($totaltransactionspreviousmonthunformatted + $totaltransactionsthismonthunformatted)));
	$objPHPExcel->getActiveSheet()->SetCellValue('C19', (number_format($totalamountpreviousmonthunformatted +  $totalamountthismonthunformatted)));
	$objPHPExcel->getActiveSheet()->SetCellValue('D19', (number_format($mergedtransactionspreviousmonthunformatted + $mergedtransactionsthismonthunformatted)));
	$objPHPExcel->getActiveSheet()->SetCellValue('E19', (number_format($unmergedtransactionspreviousmonthunformatted + $unmergedtransactionsthismonthunformatted)));

     //Do some alignment
	$objPHPExcel->getActiveSheet()->getStyle('B16:E19')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
     // $objPHPExcel->getActiveSheet()->getStyle('B5')->getFill()->getStartColor()->setRGB('92d050');
	$objPHPExcel->getActiveSheet()->getStyle('B5:E5')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050'); 
	$objPHPExcel->getActiveSheet()->getStyle('B15:E15')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050'); 


//the first bit
 // set Header
	$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'C&G Group');
	$objPHPExcel->getActiveSheet()->SetCellValue('A2', 'Div/Co');
	$objPHPExcel->getActiveSheet()->SetCellValue('B2', 'Cummins');
	$objPHPExcel->getActiveSheet()->SetCellValue('A3', 'Mpesa Daily Movement Summary');
	$objPHPExcel->getActiveSheet()->SetCellValue('A4', 'Date');
	$objPHPExcel->getActiveSheet()->SetCellValue('B4', $reportdate);
	$objPHPExcel->getActiveSheet()->SetCellValue('A5', 'Particulars');
	$objPHPExcel->getActiveSheet()->SetCellValue('B5', 'Transactions');
	$objPHPExcel->getActiveSheet()->SetCellValue('C5', 'Total');
	$objPHPExcel->getActiveSheet()->SetCellValue('D5', 'Allocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('E5', 'Un Allocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('B6', 'Nos');
	$objPHPExcel->getActiveSheet()->SetCellValue('C6', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('D6', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('E6', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('A7', 'Balance b/f');
	$objPHPExcel->getActiveSheet()->SetCellValue('A8', 'Transactions Today');
	$objPHPExcel->getActiveSheet()->SetCellValue('A9', 'Daily c/f');

        //yesterday's transactions
	$objPHPExcel->getActiveSheet()->SetCellValue('B7', $totaltransactionsprevious);
	$objPHPExcel->getActiveSheet()->SetCellValue('C7', number_format($totalamountpreviousmonthunformatted +  $totalamountthismonthunformatted - $totalamountunformatted));
	$objPHPExcel->getActiveSheet()->SetCellValue('D7', (number_format($mergedtransactionspreviousmonthunformatted + $mergedtransactionsthismonthunformatted - $mergedtransactionsunformatted)));
	$objPHPExcel->getActiveSheet()->SetCellValue('E7', number_format($unmergedtransactionspreviousmonthunformatted + $unmergedtransactionsthismonthunformatted - $unmergedtransactionsunformatted));

    //Today's transactions
	$objPHPExcel->getActiveSheet()->SetCellValue('B8', $totaltransactions);
	$objPHPExcel->getActiveSheet()->SetCellValue('C8', $totalamount);
	$objPHPExcel->getActiveSheet()->SetCellValue('D8', $mergedtransactions);
	$objPHPExcel->getActiveSheet()->SetCellValue('E8', $unmergedtransactions);

	$objPHPExcel->getActiveSheet()->SetCellValue('B9', number_format($transactionsunallocatedprevious + $transactionsallocatedprevious + $transactionsunallocated + $transactionsallocated - $transactionsunallocated - $transactionsallocated));
	$objPHPExcel->getActiveSheet()->SetCellValue('C9', number_format($totalamountpreviousmonthunformatted +  $totalamountthismonthunformatted - $totalamountunformatted +  $totalamountunformatted));
	$objPHPExcel->getActiveSheet()->SetCellValue('D9', number_format($mergedtransactionspreviousmonthunformatted + $mergedtransactionsthismonthunformatted - $mergedtransactionsunformatted + $mergedtransactionsunformatted));
	$objPHPExcel->getActiveSheet()->SetCellValue('E9', number_format($unmergedtransactionspreviousmonthunformatted + $unmergedtransactionsthismonthunformatted - $unmergedtransactionsunformatted + $unmergedtransactionsunformatted));


$unmergedtransactionsaging30 = 0;
	$stime =  date('Y-m-d', strtotime(("-31 day")));
	$etime =  date('Y-m-d', strtotime(("-1 day")));
	$empInfo = $this->export->fetchunallocatedcumminsthirty($stime, $etime);
	
	foreach ($empInfo as $element) {
		$unmergedtransactionsaging30 += $element['TransAmount'];
	}
	
	$unmergedtransactionsaging60 = 0;
	$stime =  date('Y-m-d', strtotime(("-61 day")));
	$etime =  date('Y-m-d', strtotime(("-32 day")));
	$empInfo = $this->export->fetchunallocatedcumminsthirty($stime, $etime);
	
	foreach ($empInfo as $element) {
		$unmergedtransactionsaging60 += $element['TransAmount'];
	}
	
	$unmergedtransactionsaging90 = 0;
	$stime =  date('Y-m-d', strtotime(("-91 day")));
	$etime =  date('Y-m-d', strtotime(("-62 day")));
	$empInfo = $this->export->fetchunallocatedcumminsthirty($stime, $etime);
	
	foreach ($empInfo as $element) {
		$unmergedtransactionsaging90 += $element['TransAmount'];
	}
	
	$unmergedtransactionsaging120 = number_format(($unmergedtransactionspreviousmonthunformatted + $unmergedtransactionsthismonthunformatted) - ($unmergedtransactionsaging30 + $unmergedtransactionsaging60 + $unmergedtransactionsaging90));
	$unmergedtransactionsaging30 = number_format($unmergedtransactionsaging30);
	$unmergedtransactionsaging60 = number_format($unmergedtransactionsaging60);
	$unmergedtransactionsaging90 = number_format($unmergedtransactionsaging90);
	
	
	$objPHPExcel->getActiveSheet()->SetCellValue('A23', 'Aging Report – unallocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('A24', 'Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('B24', '1 – 30 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('C24', '31 – 60 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('D24', '61 - 90 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('E24', 'Over 90 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('F24', 'Total Due');
	$objPHPExcel->getActiveSheet()->SetCellValue('B25', $unmergedtransactionsaging30);
	$objPHPExcel->getActiveSheet()->SetCellValue('C25', $unmergedtransactionsaging60);
	$objPHPExcel->getActiveSheet()->SetCellValue('D25', $unmergedtransactionsaging90);
	$objPHPExcel->getActiveSheet()->SetCellValue('E25', $unmergedtransactionsaging120);
	$objPHPExcel->getActiveSheet()->SetCellValue('F25', number_format($unmergedtransactionspreviousmonthunformatted + $unmergedtransactionsthismonthunformatted));
     //Do some alignment
     //Do some alignment
	$objPHPExcel->getActiveSheet()->getStyle('B6:E9')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	//end first bit 
	$objPHPExcel->getActiveSheet()->getStyle('B25:F25')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	$objPHPExcel->getActiveSheet()->getStyle('B24:F24')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050'); 

	
	for($col = 'A'; $col !== 'G'; $col++) {
		$objPHPExcel->getActiveSheet()
		->getColumnDimension($col)
		->setAutoSize(true);
	}

	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter->save($fileNamec);


  //$myemails = array('vlangat@ari.co.ke, viclan29@gmail.com');
//$myemails = array('vlangat@ari.co.ke, joel@cargen.com, gilbert.mutai@cargen.com, titus.murage@cargen.com, nkinyagia@ari.co.ke');
	$msg = '<span style="font-family: arial;">Dear Team,  <br> <br>';
	$msg .= 'Transactions summary. Date: '.$reportdate.' <br> <br>';
	$msg .= 'Total transactions: '.$totaltransactions.'<br>';
	$msg .= 'Total amount: KES '.$totalamount.'<br>';
	$msg .= 'Amount Allocated: KES '.$mergedtransactions.'<br>';
	$msg .= 'Amount un-allocated: Kes '.$unmergedtransactions.'<br> <br>';
	$msg .= 'Attached see system generated daily reports.<br><br>Kind Regards, <br>M-Link <br> Pay Bill Number - 664988 - Cummins</span>';

	$data = array('tblreportemails.active' => 1, 'tblcredentials.shortCode' => 664988);

	$myemails = $this->user->getreportusers($data);
	
	foreach ($myemails as $address)
	{
		$this->email->clear();
		$this->email->to($address['email']);
		$this->email->subject('M-Link Daily Transactions Summary Report 664988 -  Cummins');
		$this->email->from(MAIL_FROM);
		$this->email->message( $msg );
		$this->email->attach($fileNamec);
		$this->email->send();
	}


        //Delete the created files from the harddisk after sent to the users
	unlink($fileNamec);
}

public function test() {
	/*$protocol = is_https() ? "https://" : "http://";
	$host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : "";
	echo $protocol.'<br>';
	echo $host.'<br>';
	
	if((stristr($host, '196.201.') !== FALSE)){
		echo 'Its correct';
	}else{
		echo 'Not correct';
	}*/
	$arr = array(889384, 889388, 889388, 889391, 889394, 889393, 889385, 889387, 889386, 180514, 583050, 583030);
	foreach ($arr as &$value) {
    echo $value.'<br>';
}
}

public function docng($shortcode) {
	
	$condition = array('shortCode' => $shortcode);

        $thecodes = $this->credentials->get($condition);

        if ($thecodes->num_rows() > 0) {
            $row = $thecodes->row();
            $shortcode = $row->shortCode;
			$branch = $row->branch;
			 $branches = $row->branch;
			
			
			//Get initiaal values
	$totaltransactions = 0;
	$totalamount = 0;
	$mergedtransactions = 0;
	$unmergedtransactions = 0;
	
	$totaltransactionsprevious = 0;
	$totalamountprevious = 0;
	$mergedtransactionsprevious = 0;
	$unmergedtransactionsprevious = 0;
	
	$totaltransactionsthismonth = 0;
	$totalamountthismonth = 0;
	$mergedtransactionsthismonth = 0;
	$unmergedtransactionsthismonth = 0;
	
	$totaltransactionspreviousmonth = 0;
	$totalamountpreviousmonth = 0;
	$mergedtransactionspreviousmonth = 0;
	$unmergedtransactionspreviousmonth = 0;
	
	
        //Dates
	$reportdate = date('Y-m-d', strtotime(("-1 day")));
	$prevousdate = date('Y-m-d', strtotime(("-2 day")));
	
    // create file name
	$fileName = 'Daily data summary '.$branch.'-'.$reportdate.'.xlsx';  
    // load excel library
	
	$stime =  date('Y-m-d', strtotime(("-1 day")));
	$this->load->library('excel');
	$empInfo = $this->export->fetchallocated($stime, $shortcode);
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(1);
	$objPHPExcel->getActiveSheet()->setTitle("Allocated Transactions");
        // set Header
	$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Customer');
	$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Phone Number');
	$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Paid KES');
	$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'On');
	$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Balance KES'); 
	$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Mpesa Code');
	$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'SAP Doc');
	$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Branch');
	$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'SAP Doc Date');
	$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'SAP Doc Total KSH');
	
        // set Row
	$rowCount = 2;
	$transactionsallocated = 0;
	foreach ($empInfo as $element) {
		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['ContactName']);
		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['MSISDN']);
		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['TransAmount']);
		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['TransTime']);
		$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['Balance']);
		$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['TransID']);
		$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['SapDocNum']);
		$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['BPName']);
		$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['DocDate']);
		$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['DocTotal']);
		$rowCount++;
		$transactionsallocated++;
		$totalamount += $element['TransAmount'];
		$mergedtransactions += $element['TransAmount'];		
	}
	

	$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050'); 

	for($col = 'A'; $col !== 'K'; $col++) {
		$objPHPExcel->getActiveSheet()
		->getColumnDimension($col)
		->setAutoSize(true);
	}
	

        // create file name unallocated
	$fileNameunallocated = 'Pending Transactions data '.$branch.'-'.$reportdate.'.xlsx';  
        // load excel library
	$this->load->library('excel');
	$stime =  date('Y-m-d', strtotime(("-1 day")));
	$empInfo = $this->export->fetchunallocated($stime, $shortcode);
	//	$objPHPExcel = new PHPExcel();
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(2);
	$objPHPExcel->getActiveSheet()->setTitle("Pending Transactions");
        // set Header
	$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Customer');
	$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Phone Number');
	$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Paid KES');
	$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'On');
	$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Balance KES'); 
	$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Mpesa Code');

        // set Row
	$rowCount = 2;
	$transactionsunallocated = 0;
	foreach ($empInfo as $element) {
		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['ContactName']);
		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['MSISDN']);
		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['TransAmount']);
		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['TransTime']);
		$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['Balance']);
		$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['TransID']);
		$rowCount++;
		$transactionsunallocated++;
		$totalamount += $element['TransAmount'];
		$unmergedtransactions += $element['TransAmount'];
	}


	$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050'); 

	for($col = 'A'; $col !== 'G'; $col++) {
		$objPHPExcel->getActiveSheet()
		->getColumnDimension($col)
		->setAutoSize(true);
	}
	

        //Get the list of the cumulative pending
        // create file name unallocated cumulative
	$fileNameunallocatedcumulative = 'Pending Cumulative Transactions data ('.$branch.') as at '.$reportdate.'.xlsx';  
        // load excel library
	$this->load->library('excel');
	$stime =  date('Y-m-d', strtotime(("-1 day")));
	$empInfo = $this->export->fetchunallocatedcumulative($stime, $shortcode);
	
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(3);
	$objPHPExcel->getActiveSheet()->setTitle("Pending Cummulative");
        // set Header
	$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Customer');
	$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Phone Number');
	$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Paid KES');
	$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'On');
	$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Balance KES'); 
	$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Mpesa Code');
	$objPHPExcel->getActiveSheet()->SetCellValue('G1', '1 – 30 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('H1', '31 – 60 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('I1', '61 - 90 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Over 90 Days');

        // set Row
	$rowCount = 2;
	foreach ($empInfo as $element) {
		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['ContactName']);
		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['MSISDN']);
		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['TransAmount']);
		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['TransTime']);
		$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['Balance']);
		$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['TransID']);
		$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['thethirty']);
		$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['thesixty']);
		$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['theninety']);
		$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['theabove']);
		$rowCount++;
	}
	
	$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050'); 

	for($col = 'A'; $col !== 'K'; $col++) {
		$objPHPExcel->getActiveSheet()
		->getColumnDimension($col)
		->setAutoSize(true);
	}
        //End the list of cumulative pending
		
	$totaltransactions = number_format($transactionsunallocated + $transactionsallocated);
	$totalamountunformatted = $totalamount;
	$totalamount = number_format($totalamount);
	$mergedtransactionsunformatted = $mergedtransactions;
	$unmergedtransactionsunformatted = $unmergedtransactions;


     //Get the daily b/f details
    $stime = date('Y-m-d', strtotime(("-2 day")));
	
	$previousdaysallocated = $this->export->fetchallocatedpreviousdays($stime, $shortcode);
	$previousdaysunallocated = $this->export->fetchunallocatedprevioudays($stime, $shortcode);

     //Get the totals for the previous days
	 $transactionsallocatedprevious = 0;
	foreach ($previousdaysallocated as $row) {
		$transactionsallocatedprevious +=  $row[0];
		$totalamountprevious += $row[1];
		$mergedtransactionsprevious +=$row[1];
	}
	
$transactionsunallocatedprevious = 0;
	foreach ($previousdaysunallocated as $row) {
		$transactionsunallocatedprevious += $row[0];
		$totalamountprevious += $row[1];
		$unmergedtransactionsprevious += $row[1];
	}



	$totaltransactionsprevious = number_format($transactionsunallocatedprevious + $transactionsallocatedprevious);
	$totalamountpreviousunformatted =  $totalamountprevious;
	$totalamountprevious =  number_format($totalamountprevious);
	$mergedtransactionspreviousunformatted = $mergedtransactionsprevious;
	$unmergedtransactionspreviousunformatted = $unmergedtransactionsprevious;


    //Create filename for the movement summary for the day's data 
	$fileNamemovement = 'Movement Summary data ('.$branch.')-'.$reportdate.'.xlsx';

        // load excel library
	$this->load->library('excel');
	
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle("Movement Summary");
	
	

        //Create filename for the movement summary for the month's data 
	$objPHPExcel->getActiveSheet()->SetCellValue('A13', 'Mpesa Monthly Movement Summary');
	$objPHPExcel->getActiveSheet()->SetCellValue('A14', 'Month');
	$objPHPExcel->getActiveSheet()->SetCellValue('B14', date('F-Y'));
	$objPHPExcel->getActiveSheet()->SetCellValue('A15', 'Particulars');
	$objPHPExcel->getActiveSheet()->SetCellValue('B15', 'Transactions');
	$objPHPExcel->getActiveSheet()->SetCellValue('C15', 'Total');
	$objPHPExcel->getActiveSheet()->SetCellValue('D15', 'Allocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('E15', 'Un Allocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('B16', 'Nos');
	$objPHPExcel->getActiveSheet()->SetCellValue('C16', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('D16', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('E16', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('A17', 'Month Balance b/f');
	$objPHPExcel->getActiveSheet()->SetCellValue('A18', 'This month');
	$objPHPExcel->getActiveSheet()->SetCellValue('A19', 'Monthly c/f');

     //Populate the data for the monthly section of the movement 
     //This month data 
	$stime =  date('Y-m-d',strtotime("first day of this month"));
	$thismonthallocated = $this->export->fetchallocatedthismonth($stime, $shortcode);
	$thismonthunallocated = $this->export->fetchunallocatedthismonth($stime, $shortcode);

     //Previous month
	$stime =  date('Y-m-d',strtotime("last day of previous month"));
	$previousmonthallocated = $this->export->fetchallocatedpreviousmonth($stime, $shortcode);
	$previousmonthunallocated = $this->export->fetchunallocatedpreviousmonth($stime, $shortcode);

     //Get the totals for the previous month
	foreach ($previousmonthallocated as $row) {
		$transactionsallocatedpreviousmonth +=  $row[0];
		$totalamountpreviousmonth += $row[2];
		$mergedtransactionspreviousmonth +=$row[2];
	}

	foreach ($previousmonthunallocated as $row) {
		$transactionsunallocatedpreviousmonth+= $row[0];
		$totalamountpreviousmonth += $row[1];
		$unmergedtransactionspreviousmonth += $row[1];
	}


     //Get the allocated totals for this month
	 $transactionsallocatedthismonth = 0;
	foreach ($thismonthallocated as $row) {
		$transactionsallocatedthismonth++;
		$totalamountthismonth += $row['DocTotal'];
		$mergedtransactionsthismonth += $row['DocTotal'];
	}

  //Get the unallocated totals for this month
  $transactionsunallocatedthismonth = 0;
	foreach ($thismonthunallocated as $row) {
		$transactionsunallocatedthismonth++;
		$totalamountthismonth += $row['TransAmount'];
		$unmergedtransactionsthismonth += $row['TransAmount'];
	}
	

    //Sums for this month 
	$totaltransactionsthismonthunformatted = $transactionsunallocatedthismonth + $transactionsallocatedthismonth;
	$totaltransactionsthismonth = number_format($transactionsunallocatedthismonth + $transactionsallocatedthismonth);
	$totalamountthismonthunformatted = $totalamountthismonth;
	$totalamountthismonth = number_format($totalamountthismonth);
	$mergedtransactionsthismonthunformatted = $mergedtransactionsthismonth;
	$mergedtransactionsthismonth = number_format($mergedtransactionsthismonth);
	$unmergedtransactionsthismonthunformatted = $unmergedtransactionsthismonth;
	$unmergedtransactionsthismonth = number_format($unmergedtransactionsthismonth);

      //Sums for previous months
	$totaltransactionspreviousmonthunformatted = $transactionsunallocatedpreviousmonth + $transactionsallocatedpreviousmonth;
	$totaltransactionspreviousmonth = number_format($transactionsunallocatedpreviousmonth + $transactionsallocatedpreviousmonth);
	$totalamountpreviousmonthunformatted =  $totalamountpreviousmonth;
	$totalamountpreviousmonth =  number_format($totalamountpreviousmonth);
	$mergedtransactionspreviousmonthunformatted = $mergedtransactionspreviousmonth;
	$mergedtransactionspreviousmonth = number_format($mergedtransactionspreviousmonth);
	$unmergedtransactionspreviousmonthunformatted = $unmergedtransactionspreviousmonth;
	$unmergedtransactionspreviousmonth = number_format($unmergedtransactionspreviousmonth);


        //last months' transactions
	$objPHPExcel->getActiveSheet()->SetCellValue('B17', number_format($transactionsallocatedpreviousmonth + $transactionsunallocatedpreviousmonth));
	$objPHPExcel->getActiveSheet()->SetCellValue('C17', $totalamountpreviousmonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('D17', $mergedtransactionspreviousmonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('E17', $unmergedtransactionspreviousmonth);

    //this month's transactions
	$objPHPExcel->getActiveSheet()->SetCellValue('B18', number_format($transactionsallocatedthismonth + $transactionsunallocatedthismonth));
	$objPHPExcel->getActiveSheet()->SetCellValue('C18', $totalamountthismonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('D18', $mergedtransactionsthismonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('E18', $unmergedtransactionsthismonth);

      //summary
	$objPHPExcel->getActiveSheet()->SetCellValue('B19', (number_format($transactionsallocatedpreviousmonth + $transactionsunallocatedpreviousmonth + $transactionsallocatedthismonth + $transactionsunallocatedthismonth)));
	$objPHPExcel->getActiveSheet()->SetCellValue('C19', (number_format($totalamountpreviousmonthunformatted +  $totalamountthismonthunformatted)));
	$objPHPExcel->getActiveSheet()->SetCellValue('D19', (number_format($mergedtransactionspreviousmonthunformatted + $mergedtransactionsthismonthunformatted)));
	$objPHPExcel->getActiveSheet()->SetCellValue('E19', (number_format($unmergedtransactionspreviousmonthunformatted + $unmergedtransactionsthismonthunformatted)));

     //Do some alignment
	$objPHPExcel->getActiveSheet()->getStyle('B16:E19')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
     // $objPHPExcel->getActiveSheet()->getStyle('B5')->getFill()->getStartColor()->setRGB('92d050');
	$objPHPExcel->getActiveSheet()->getStyle('B5:E5')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050'); 
	$objPHPExcel->getActiveSheet()->getStyle('B15:E15')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050'); 


//the first bit
 // set Header
	$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'C&G Group');
	$objPHPExcel->getActiveSheet()->SetCellValue('A2', 'Div/Co');
	$objPHPExcel->getActiveSheet()->SetCellValue('B2', $branch);
	$objPHPExcel->getActiveSheet()->SetCellValue('A3', 'Mpesa Daily Movement Summary');
	$objPHPExcel->getActiveSheet()->SetCellValue('A4', 'Date');
	$objPHPExcel->getActiveSheet()->SetCellValue('B4', $reportdate);
	$objPHPExcel->getActiveSheet()->SetCellValue('A5', 'Particulars');
	$objPHPExcel->getActiveSheet()->SetCellValue('B5', 'Transactions');
	$objPHPExcel->getActiveSheet()->SetCellValue('C5', 'Total');
	$objPHPExcel->getActiveSheet()->SetCellValue('D5', 'Allocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('E5', 'Un Allocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('B6', 'Nos');
	$objPHPExcel->getActiveSheet()->SetCellValue('C6', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('D6', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('E6', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('A7', 'Balance b/f');
	$objPHPExcel->getActiveSheet()->SetCellValue('A8', 'Transactions Today');
	$objPHPExcel->getActiveSheet()->SetCellValue('A9', 'Daily c/f');

       //yesterday's transactions
	$objPHPExcel->getActiveSheet()->SetCellValue('B7', $totaltransactionsprevious);
	$objPHPExcel->getActiveSheet()->SetCellValue('C7', number_format($unmergedtransactionsprevious + $mergedtransactionsprevious));
	$objPHPExcel->getActiveSheet()->SetCellValue('D7', (number_format($mergedtransactionsprevious))); //mergedtransactionsunformatted
	$objPHPExcel->getActiveSheet()->SetCellValue('E7', number_format($unmergedtransactionsprevious));

    //Today's transactions
	$objPHPExcel->getActiveSheet()->SetCellValue('B8', $totaltransactions);
	$objPHPExcel->getActiveSheet()->SetCellValue('C8', number_format( $mergedtransactions + $unmergedtransactions ));
	$objPHPExcel->getActiveSheet()->SetCellValue('D8', number_format($mergedtransactions));
	$objPHPExcel->getActiveSheet()->SetCellValue('E8', number_format($unmergedtransactions));

	$objPHPExcel->getActiveSheet()->SetCellValue('B9', number_format($transactionsunallocatedprevious + $transactionsallocatedprevious + $transactionsunallocated + $transactionsallocated));
	$objPHPExcel->getActiveSheet()->SetCellValue('C9', number_format($unmergedtransactionsprevious + $mergedtransactionsprevious  +  $mergedtransactions + $unmergedtransactions));
	$objPHPExcel->getActiveSheet()->SetCellValue('D9', number_format($mergedtransactionsprevious + $mergedtransactions));
	$objPHPExcel->getActiveSheet()->SetCellValue('E9', number_format($unmergedtransactionsprevious + $unmergedtransactions));


$unmergedtransactionsaging30 = 0;
	$stime =  date('Y-m-d', strtotime(("-31 day")));
	$etime =  date('Y-m-d', strtotime(("-1 day")));
	$empInfo = $this->export->fetchunallocatedthirty($stime, $etime, $shortcode);
	
	foreach ($empInfo as $element) {
		$unmergedtransactionsaging30 += $element['TransAmount'];
	}
	
	$unmergedtransactionsaging60 = 0;
	$stime =  date('Y-m-d', strtotime(("-61 day")));
	$etime =  date('Y-m-d', strtotime(("-32 day")));
	$empInfo = $this->export->fetchunallocatedthirty($stime, $etime, $shortcode);
	
	foreach ($empInfo as $element) {
		$unmergedtransactionsaging60 += $element['TransAmount'];
	}
	
	$unmergedtransactionsaging90 = 0;
	$stime =  date('Y-m-d', strtotime(("-91 day")));
	$etime =  date('Y-m-d', strtotime(("-62 day")));
	$empInfo = $this->export->fetchunallocatedthirty($stime, $etime, $shortcode);
	
	foreach ($empInfo as $element) {
		$unmergedtransactionsaging90 += $element['TransAmount'];
	}
	
	$unmergedtransactionsaging120 = number_format(($unmergedtransactionspreviousmonthunformatted + $unmergedtransactionsthismonthunformatted) - ($unmergedtransactionsaging30 + $unmergedtransactionsaging60 + $unmergedtransactionsaging90));
	$unmergedtransactionsaging30 = number_format($unmergedtransactionsaging30);
	$unmergedtransactionsaging60 = number_format($unmergedtransactionsaging60);
	$unmergedtransactionsaging90 = number_format($unmergedtransactionsaging90);
	
	$objPHPExcel->getActiveSheet()->SetCellValue('A23', 'Aging Report – unallocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('A24', 'Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('B24', '1 – 30 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('C24', '31 – 60 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('D24', '61 - 90 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('E24', 'Over 90 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('F24', 'Total Due');
	$objPHPExcel->getActiveSheet()->SetCellValue('B25', $unmergedtransactionsaging30);
	$objPHPExcel->getActiveSheet()->SetCellValue('C25', $unmergedtransactionsaging60);
	$objPHPExcel->getActiveSheet()->SetCellValue('D25', $unmergedtransactionsaging90);
	$objPHPExcel->getActiveSheet()->SetCellValue('E25', $unmergedtransactionsaging120);
	$objPHPExcel->getActiveSheet()->SetCellValue('F25', number_format($unmergedtransactionspreviousmonthunformatted + $unmergedtransactionsthismonthunformatted));

     //Do some alignment
	$objPHPExcel->getActiveSheet()->getStyle('B6:E9')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	//end first bit 
	$objPHPExcel->getActiveSheet()->getStyle('B25:F25')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	$objPHPExcel->getActiveSheet()->getStyle('B24:F24')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050'); 
	$totaltransactionsfinal = $totaltransactions;
	$totalamountfinal = $totalamount;
	$mergedtransactionsfinal = $mergedtransactions;
	$unmergedtransactionsfinal = $unmergedtransactions;
	if($shortcode == "554554"){
		$arr = array(889384, 889388, 889389, 889391, 889394, 889393, 889385, 889387, 889386, 180514, 583050, 583030);
		
		$counter1 = 28; $counter2 = 29; $counter3 = 30; $counter4 = 31; $counter5 = 32; $counter6 = 33; $counter7 = 34; $counter8 = 35; $counter9 = 36; $counter13 = 40; $counter14 = 41; $counter15 = 42; $counter16 = 43; $counter17 = 44; $counter18 = 45; $counter19 = 46; $counter23 = 50; $counter24 = 51; $counter25 = 52;
		
		foreach ($arr as &$value) {
	$condition = array('shortCode' => $value);

        $thecodess = $this->credentials->get($condition);

        if ($thecodess->num_rows() > 0) {
            $row = $thecodess->row();
			$branch = $row->branch;
		//Get initiaal values
	$totaltransactions = 0;
	$totalamount = 0;
	$mergedtransactions = 0;
	$unmergedtransactions = 0;
	
	$totaltransactionsprevious = 0;
	$totalamountprevious = 0;
	$mergedtransactionsprevious = 0;
	$unmergedtransactionsprevious = 0;
	
	$totaltransactionsthismonth = 0;
	$totalamountthismonth = 0;
	$mergedtransactionsthismonth = 0;
	$unmergedtransactionsthismonth = 0;
	
	$totaltransactionspreviousmonth = 0;
	$totalamountpreviousmonth = 0;
	$mergedtransactionspreviousmonth = 0;
	$unmergedtransactionspreviousmonth = 0;
	
	
	$stime =  date('Y-m-d', strtotime(("-1 day")));
	$empInfo = $this->export->fetchallocated($stime, $value);
	
        // set Row
	$rowCount = 2;
	$transactionsallocated = 0;
	foreach ($empInfo as $element) {
		$rowCount++;
		$transactionsallocated++;
		$totalamount += $element['TransAmount'];
		$mergedtransactions += $element['TransAmount'];		
	}
	

	$stime =  date('Y-m-d', strtotime(("-1 day")));
	$empInfo = $this->export->fetchunallocated($stime, $value);
	$transactionsunallocated = 0;
	foreach ($empInfo as $element) {
		$transactionsunallocated++;
		$totalamount += $element['TransAmount'];
		$unmergedtransactions += $element['TransAmount'];
	}
	

	$stime =  date('Y-m-d', strtotime(("-1 day")));
	$empInfo = $this->export->fetchunallocatedcumulative($stime, $value);
		
	$totaltransactions = number_format($transactionsunallocated + $transactionsallocated);
	$totalamountunformatted = $totalamount;
	$totalamount = number_format($totalamount);
	$mergedtransactionsunformatted = $mergedtransactions;
	$unmergedtransactionsunformatted = $unmergedtransactions;


     //Get the daily b/f details
    $stime = date('Y-m-d', strtotime(("-2 day")));
	
	$previousdaysallocated = $this->export->fetchallocatedpreviousdays($stime, $value);
	$previousdaysunallocated = $this->export->fetchunallocatedprevioudays($stime, $value);

     //Get the totals for the previous days
	 $transactionsallocatedprevious = 0;
	foreach ($previousdaysallocated as $row) {
		$transactionsallocatedprevious +=  $row[0];
		$totalamountprevious += $row[1];
		$mergedtransactionsprevious +=$row[1];
	}
	
$transactionsunallocatedprevious = 0;
	foreach ($previousdaysunallocated as $row) {
		$transactionsunallocatedprevious += $row[0];
		$totalamountprevious += $row[1];
		$unmergedtransactionsprevious += $row[1];
	}



	$totaltransactionsprevious = number_format($transactionsunallocatedprevious + $transactionsallocatedprevious);
	$totalamountpreviousunformatted =  $totalamountprevious;
	$totalamountprevious =  number_format($totalamountprevious);
	$mergedtransactionspreviousunformatted = $mergedtransactionsprevious;
	$unmergedtransactionspreviousunformatted = $unmergedtransactionsprevious;

		
		    //Create filename for the movement summary for the month's data
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$counter13, 'Mpesa Monthly Movement Summary');
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$counter14, 'Month');
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$counter14, date('F-Y'));
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$counter15, 'Particulars');
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$counter15, 'Transactions');
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$counter15, 'Total');
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$counter15, 'Allocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$counter15, 'Un Allocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$counter16, 'Nos');
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$counter16, 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$counter16, 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$counter16, 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$counter17, 'Month Balance b/f');
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$counter18, 'This month');
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$counter19, 'Monthly c/f');

     //Populate the data for the monthly section of the movement 
     //This month data 
	$stime =  date('Y-m-d',strtotime("first day of this month"));
	$thismonthallocated = $this->export->fetchallocatedthismonth($stime, $value);
	$thismonthunallocated = $this->export->fetchunallocatedthismonth($stime, $value);

     //Previous month
	$stime =  date('Y-m-d',strtotime("last day of previous month"));
	$previousmonthallocated = $this->export->fetchallocatedpreviousmonth($stime, $value);
	$previousmonthunallocated = $this->export->fetchunallocatedpreviousmonth($stime, $value);

     //Get the totals for the previous month
	foreach ($previousmonthallocated as $row) {
		$transactionsallocatedpreviousmonth +=  $row[0];
		$totalamountpreviousmonth += $row[2];
		$mergedtransactionspreviousmonth +=$row[2];
	}

	foreach ($previousmonthunallocated as $row) {
		$transactionsunallocatedpreviousmonth+= $row[0];
		$totalamountpreviousmonth += $row[1];
		$unmergedtransactionspreviousmonth += $row[1];
	}


     //Get the allocated totals for this month
	 $transactionsallocatedthismonth = 0;
	foreach ($thismonthallocated as $row) {
		$transactionsallocatedthismonth++;
		$totalamountthismonth += $row['DocTotal'];
		$mergedtransactionsthismonth += $row['DocTotal'];
	}

  //Get the unallocated totals for this month
  $transactionsunallocatedthismonth = 0;
	foreach ($thismonthunallocated as $row) {
		$transactionsunallocatedthismonth++;
		$totalamountthismonth += $row['TransAmount'];
		$unmergedtransactionsthismonth += $row['TransAmount'];
	}
	

    //Sums for this month 
	$totaltransactionsthismonthunformatted = $transactionsunallocatedthismonth + $transactionsallocatedthismonth;
	$totaltransactionsthismonth = number_format($transactionsunallocatedthismonth + $transactionsallocatedthismonth);
	$totalamountthismonthunformatted = $totalamountthismonth;
	$totalamountthismonth = number_format($totalamountthismonth);
	$mergedtransactionsthismonthunformatted = $mergedtransactionsthismonth;
	$mergedtransactionsthismonth = number_format($mergedtransactionsthismonth);
	$unmergedtransactionsthismonthunformatted = $unmergedtransactionsthismonth;
	$unmergedtransactionsthismonth = number_format($unmergedtransactionsthismonth);

      //Sums for previous months
	$totaltransactionspreviousmonthunformatted = $transactionsunallocatedpreviousmonth + $transactionsallocatedpreviousmonth;
	$totaltransactionspreviousmonth = number_format($transactionsunallocatedpreviousmonth + $transactionsallocatedpreviousmonth);
	$totalamountpreviousmonthunformatted =  $totalamountpreviousmonth;
	$totalamountpreviousmonth =  number_format($totalamountpreviousmonth);
	$mergedtransactionspreviousmonthunformatted = $mergedtransactionspreviousmonth;
	$mergedtransactionspreviousmonth = number_format($mergedtransactionspreviousmonth);
	$unmergedtransactionspreviousmonthunformatted = $unmergedtransactionspreviousmonth;
	$unmergedtransactionspreviousmonth = number_format($unmergedtransactionspreviousmonth);


        //last months' transactions
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$counter17, number_format($transactionsallocatedpreviousmonth + $transactionsunallocatedpreviousmonth));
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$counter17, $totalamountpreviousmonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$counter17, $mergedtransactionspreviousmonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$counter17, $unmergedtransactionspreviousmonth);

    //this month's transactions
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$counter18, number_format($transactionsallocatedthismonth + $transactionsunallocatedthismonth));
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$counter18, $totalamountthismonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$counter18, $mergedtransactionsthismonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$counter18, $unmergedtransactionsthismonth);

      //summary
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$counter19, (number_format($transactionsallocatedpreviousmonth + $transactionsunallocatedpreviousmonth + $transactionsallocatedthismonth + $transactionsunallocatedthismonth)));
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$counter19, (number_format($totalamountpreviousmonthunformatted +  $totalamountthismonthunformatted)));
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$counter19, (number_format($mergedtransactionspreviousmonthunformatted + $mergedtransactionsthismonthunformatted)));
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$counter19, (number_format($unmergedtransactionspreviousmonthunformatted + $unmergedtransactionsthismonthunformatted)));

     //Do some alignment
	$objPHPExcel->getActiveSheet()->getStyle('B'.$counter16.':E'.$counter19)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
     // $objPHPExcel->getActiveSheet()->getStyle('B5')->getFill()->getStartColor()->setRGB('92d050');
	$objPHPExcel->getActiveSheet()->getStyle('B'.$counter5.':E'.$counter5)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050'); 
	$objPHPExcel->getActiveSheet()->getStyle('B'.$counter15.':E'.$counter15)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050'); 


//the first bit
 // set Header
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$counter1, 'C&G Group');
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$counter2, 'Div/Co');
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$counter2, $branch);
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$counter3, 'Mpesa Daily Movement Summary');
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$counter4, 'Date');
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$counter4, $reportdate);
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$counter5, 'Particulars');
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$counter5, 'Transactions');
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$counter5, 'Total');
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$counter5, 'Allocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$counter5, 'Un Allocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$counter6, 'Nos');
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$counter6, 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$counter6, 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$counter6, 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$counter7, 'Balance b/f');
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$counter8, 'Transactions Today');
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$counter9, 'Daily c/f');;

       //yesterday's transactions
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$counter7, $totaltransactionsprevious);
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$counter7, number_format($unmergedtransactionsprevious + $mergedtransactionsprevious));
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$counter7, (number_format($mergedtransactionsprevious))); //mergedtransactionsunformatted
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$counter7, number_format($unmergedtransactionsprevious));

    //Today's transactions
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$counter8, $totaltransactions);
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$counter8, number_format( $mergedtransactions + $unmergedtransactions ));
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$counter8, number_format($mergedtransactions));
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$counter8, number_format($unmergedtransactions));

	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$counter9, number_format($transactionsunallocatedprevious + $transactionsallocatedprevious + $transactionsunallocated + $transactionsallocated));
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$counter9, number_format($unmergedtransactionsprevious + $mergedtransactionsprevious  +  $mergedtransactions + $unmergedtransactions));
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$counter9, number_format($mergedtransactionsprevious + $mergedtransactions));
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$counter9, number_format($unmergedtransactionsprevious + $unmergedtransactions));


$unmergedtransactionsaging30 = 0;
	$stime =  date('Y-m-d', strtotime(("-31 day")));
	$etime =  date('Y-m-d', strtotime(("-1 day")));
	$empInfo = $this->export->fetchunallocatedthirty($stime, $etime, $value);
	
	foreach ($empInfo as $element) {
		$unmergedtransactionsaging30 += $element['TransAmount'];
	}
	
	$unmergedtransactionsaging60 = 0;
	$stime =  date('Y-m-d', strtotime(("-61 day")));
	$etime =  date('Y-m-d', strtotime(("-32 day")));
	$empInfo = $this->export->fetchunallocatedthirty($stime, $etime, $value);
	
	foreach ($empInfo as $element) {
		$unmergedtransactionsaging60 += $element['TransAmount'];
	}
	
	$unmergedtransactionsaging90 = 0;
	$stime =  date('Y-m-d', strtotime(("-91 day")));
	$etime =  date('Y-m-d', strtotime(("-62 day")));
	$empInfo = $this->export->fetchunallocatedthirty($stime, $etime, $value);
	
	foreach ($empInfo as $element) {
		$unmergedtransactionsaging90 += $element['TransAmount'];
	}
	
	$unmergedtransactionsaging120 = number_format(($unmergedtransactionspreviousmonthunformatted + $unmergedtransactionsthismonthunformatted) - ($unmergedtransactionsaging30 + $unmergedtransactionsaging60 + $unmergedtransactionsaging90));
	$unmergedtransactionsaging30 = number_format($unmergedtransactionsaging30);
	$unmergedtransactionsaging60 = number_format($unmergedtransactionsaging60);
	$unmergedtransactionsaging90 = number_format($unmergedtransactionsaging90);
	
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$counter23, 'Aging Report – unallocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$counter24, 'Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$counter24, '1 – 30 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$counter24, '31 – 60 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$counter24, '61 - 90 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$counter24, 'Over 90 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$counter24, 'Total Due');
	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$counter25, $unmergedtransactionsaging30);
	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$counter25, $unmergedtransactionsaging60);
	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$counter25, $unmergedtransactionsaging90);
	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$counter25, $unmergedtransactionsaging120);
	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$counter25, number_format($unmergedtransactionspreviousmonthunformatted + $unmergedtransactionsthismonthunformatted));

     //Do some alignment
	$objPHPExcel->getActiveSheet()->getStyle('B'.$counter6.':E'.$counter9)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	//end first bit 
	$objPHPExcel->getActiveSheet()->getStyle('B'.$counter25.':F'.$counter25)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	$objPHPExcel->getActiveSheet()->getStyle('B'.$counter24.':F'.$counter24)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050'); 
	}
	$counter1 = $counter1 + 27; $counter2 = $counter2 + 27; $counter3 = $counter3 + 27; $counter4 = $counter4 + 27; $counter5 = $counter5 + 27; $counter6 = $counter6 + 27; $counter7 = $counter7 + 27; $counter8 = $counter8 + 27; $counter9 = $counter9 + 27; $counter13 = $counter13 + 27; $counter14 = $counter14 + 27; $counter15 = $counter15 + 27; $counter16 = $counter16 + 27; $counter17 = $counter17 + 27; $counter18 = $counter18 + 27; $counter19 = $counter19 + 27; $counter23 = $counter23 + 27; $counter24 = $counter24 + 27; $counter25 = $counter25 + 27;
	}
	
	#For the niit summary in the nairobi 
	$totaltransactions = 0;
	$totalamount = 0;
	$mergedtransactions = 0;
	$unmergedtransactions = 0;
	
	$totaltransactionsprevious = 0;
	$totalamountprevious = 0;
	$mergedtransactionsprevious = 0;
	$unmergedtransactionsprevious = 0;
	
	$totaltransactionsthismonth = 0;
	$totalamountthismonth = 0;
	$mergedtransactionsthismonth = 0;
	$unmergedtransactionsthismonth = 0;
	
	$totaltransactionspreviousmonth = 0;
	$totalamountpreviousmonth = 0;
	$mergedtransactionspreviousmonth = 0;
	$unmergedtransactionspreviousmonth = 0;
	
	
        //Dates
	$reportdate = date('Y-m-d', strtotime(("-1 day")));
	$prevousdate = date('Y-m-d', strtotime(("-2 day")));
	
	$this->load->library('excel');
	$empInfo = $this->export->fetchallocatedniit($stime);
	
	$transactionsallocated = 0;
	foreach ($empInfo as $element) {
		$transactionsallocated++;
		$totalamount += $element['TransAmount'];
		$mergedtransactions += $element['TransAmount'];
	}

	for($col = 'A'; $col !== 'K'; $col++) {
		$objPHPExcel->getActiveSheet()
		->getColumnDimension($col)
		->setAutoSize(true);
	}


	$stime =  date('Y-m-d', strtotime(("-1 day")));
	$empInfo = $this->export->fetchunallocatedniit($stime);


	$transactionsunallocated = 0;
	foreach ($empInfo as $element) {
		$transactionsunallocated++;
		$totalamount += $element['TransAmount'];
		$unmergedtransactions += $element['TransAmount'];
	}


	$stime =  date('Y-m-d', strtotime(("-1 day")));
	$empInfo = $this->export->fetchunallocatedcumulativeniit($stime);
		

	$totaltransactions = number_format($transactionsunallocated + $transactionsallocated);
	$totalamountunformatted = $totalamount;
	$totalamount = number_format($totalamount);
	$mergedtransactionsunformatted = $mergedtransactions;
	$mergedtransactions = number_format($mergedtransactions);
	$unmergedtransactionsunformatted = $unmergedtransactions;
	$unmergedtransactions = number_format($unmergedtransactions);



     //Get the daily b/f details
    $stime = date('Y-m-d', strtotime(("-1 day")));
	
	$previousdaysallocated = $this->export->fetchallocatedpreviousdaysniit($stime);
	$previousdaysunallocated = $this->export->fetchunallocatedprevioudaysniit($stime);

     //Get the totals for the previous days
	 $transactionsallocatedprevious = 0;
	foreach ($previousdaysallocated as $row) {
		$transactionsallocatedprevious +=  $row[0];
		$totalamountprevious += $row[1];
		$mergedtransactionsprevious +=$row[1];
	}

$transactionsunallocatedprevious = 0;
	foreach ($previousdaysunallocated as $row) {
		$transactionsunallocatedprevious += $row[0];
		$totalamountprevious += $row[1];
		$unmergedtransactionsprevious += $row[1];
	}

	$totaltransactionsprevious = number_format($transactionsunallocatedprevious + $transactionsallocatedprevious - $transactionsunallocated - $transactionsallocated);
	$totalamountpreviousunformatted =  $totalamountprevious;
	$totalamountprevious =  number_format($totalamountprevious);
	$mergedtransactionspreviousunformatted = $mergedtransactionsprevious;
	$mergedtransactionsprevious = number_format($mergedtransactionsprevious);
	$unmergedtransactionspreviousunformatted = $unmergedtransactionsprevious;
	$unmergedtransactionsprevious = number_format($unmergedtransactionsprevious);
	

        //Create filename for the movement summary for the month's data 
	$objPHPExcel->getActiveSheet()->SetCellValue('A364', 'Mpesa Monthly Movement Summary');
	$objPHPExcel->getActiveSheet()->SetCellValue('A365', 'Month');
	$objPHPExcel->getActiveSheet()->SetCellValue('B365', date('F-Y'));
	$objPHPExcel->getActiveSheet()->SetCellValue('A366', 'Particulars');
	$objPHPExcel->getActiveSheet()->SetCellValue('B366', 'Transactions');
	$objPHPExcel->getActiveSheet()->SetCellValue('C366', 'Total');
	$objPHPExcel->getActiveSheet()->SetCellValue('D366', 'Allocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('E366', 'Un Allocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('B367', 'Nos');
	$objPHPExcel->getActiveSheet()->SetCellValue('C367', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('D367', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('E367', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('A368', 'Month Balance b/f');
	$objPHPExcel->getActiveSheet()->SetCellValue('A369', 'This month');
	$objPHPExcel->getActiveSheet()->SetCellValue('A370', 'Monthly c/f');

     //Populate the data for the monthly section of the movement 
     //This month data 
	$stime =  date('Y-m-d',strtotime("first day of this month"));
	$thismonthallocated = $this->export->fetchallocatedthismonthniit($stime);
	$thismonthunallocated = $this->export->fetchunallocatedthismonthniit($stime);

     //Previous month
	$stime =  date('Y-m-d',strtotime("last day of previous month"));
	$previousmonthallocated = $this->export->fetchallocatedpreviousmonthniit($stime);
	$previousmonthunallocated = $this->export->fetchunallocatedpreviousmonthniit($stime);

     //Get the totals for the previous month
	foreach ($previousmonthallocated as $row) {
		$transactionsallocatedpreviousmonth +=  $row[0];
		$totalamountpreviousmonth += $row[2];
		$mergedtransactionspreviousmonth +=$row[2];
	}

	foreach ($previousmonthunallocated as $row) {
		$transactionsunallocatedpreviousmonth+= $row[0];
		$totalamountpreviousmonth += $row[1];
		$unmergedtransactionspreviousmonth += $row[1];
	}


     //Get the allocated totals for this month
	foreach ($thismonthallocated as $row) {
		$transactionsallocatedthismonth++;
		$totalamountthismonth += $row['DocTotal'];
		$mergedtransactionsthismonth += $row['DocTotal'];
	}


  //Get the unallocated totals for this month
	foreach ($thismonthunallocated as $row) {
		$transactionsunallocatedthismonth++;
		$totalamountthismonth += $row['TransAmount'];
		$unmergedtransactionsthismonth += $row['TransAmount'];
	}

    //Sums for this month 
	$totaltransactionsthismonthunformatted = $transactionsunallocatedthismonth + $transactionsallocatedthismonth;
	$totaltransactionsthismonth = number_format($transactionsunallocatedthismonth + $transactionsallocatedthismonth);
	$totalamountthismonthunformatted = $totalamountthismonth;
	$totalamountthismonth = number_format($totalamountthismonth);
	$mergedtransactionsthismonthunformatted = $mergedtransactionsthismonth;
	$mergedtransactionsthismonth = number_format($mergedtransactionsthismonth);
	$unmergedtransactionsthismonthunformatted = $unmergedtransactionsthismonth;
	$unmergedtransactionsthismonth = number_format($unmergedtransactionsthismonth);

      //Sums for previous months
	$totaltransactionspreviousmonthunformatted = $transactionsunallocatedpreviousmonth + $transactionsallocatedpreviousmonth;
	$totaltransactionspreviousmonth = number_format($transactionsunallocatedpreviousmonth + $transactionsallocatedpreviousmonth);
	$totalamountpreviousmonthunformatted =  $totalamountpreviousmonth;
	$totalamountpreviousmonth =  number_format($totalamountpreviousmonth);
	$mergedtransactionspreviousmonthunformatted = $mergedtransactionspreviousmonth;
	$mergedtransactionspreviousmonth = number_format($mergedtransactionspreviousmonth);
	$unmergedtransactionspreviousmonthunformatted = $unmergedtransactionspreviousmonth;
	$unmergedtransactionspreviousmonth = number_format($unmergedtransactionspreviousmonth);


        //last months' transactions
	$objPHPExcel->getActiveSheet()->SetCellValue('B368', $totaltransactionspreviousmonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('C368', $totalamountpreviousmonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('D368', $mergedtransactionspreviousmonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('E368', $unmergedtransactionspreviousmonth);

    //this month's transactions
	$objPHPExcel->getActiveSheet()->SetCellValue('B369', $totaltransactionsthismonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('C369', $totalamountthismonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('D369', $mergedtransactionsthismonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('E369', $unmergedtransactionsthismonth);

      //summary
	$objPHPExcel->getActiveSheet()->SetCellValue('B370', (number_format($totaltransactionspreviousmonthunformatted + $totaltransactionsthismonthunformatted)));
	$objPHPExcel->getActiveSheet()->SetCellValue('C370', (number_format($totalamountpreviousmonthunformatted +  $totalamountthismonthunformatted)));
	$objPHPExcel->getActiveSheet()->SetCellValue('D370', (number_format($mergedtransactionspreviousmonthunformatted + $mergedtransactionsthismonthunformatted)));
	$objPHPExcel->getActiveSheet()->SetCellValue('E370', (number_format($unmergedtransactionspreviousmonthunformatted + $unmergedtransactionsthismonthunformatted)));

     //Do some alignment
	$objPHPExcel->getActiveSheet()->getStyle('B368:E370')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
     // $objPHPExcel->getActiveSheet()->getStyle('B5')->getFill()->getStartColor()->setRGB('92d050');
	$objPHPExcel->getActiveSheet()->getStyle('B366:E366')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050'); 
	$objPHPExcel->getActiveSheet()->getStyle('B366:E366')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050'); 


//the first bit
 // set Header
	$objPHPExcel->getActiveSheet()->SetCellValue('A352', 'C&G Group');
	$objPHPExcel->getActiveSheet()->SetCellValue('A353', 'Div/Co');
	$objPHPExcel->getActiveSheet()->SetCellValue('B353', 'NIIT');
	$objPHPExcel->getActiveSheet()->SetCellValue('A354', 'Mpesa Daily Movement Summary');
	$objPHPExcel->getActiveSheet()->SetCellValue('A355', 'Date');
	$objPHPExcel->getActiveSheet()->SetCellValue('B355', $reportdate);
	$objPHPExcel->getActiveSheet()->SetCellValue('A356', 'Particulars');
	$objPHPExcel->getActiveSheet()->SetCellValue('B356', 'Transactions');
	$objPHPExcel->getActiveSheet()->SetCellValue('C356', 'Total');
	$objPHPExcel->getActiveSheet()->SetCellValue('D356', 'Allocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('E356', 'Un Allocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('B357', 'Nos');
	$objPHPExcel->getActiveSheet()->SetCellValue('C357', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('D357', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('E357', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('A358', 'Balance b/f');
	$objPHPExcel->getActiveSheet()->SetCellValue('A359', 'Transactions Today');
	$objPHPExcel->getActiveSheet()->SetCellValue('A360', 'Daily c/f');

        //yesterday's transactions
	$objPHPExcel->getActiveSheet()->SetCellValue('B358', $totaltransactionsprevious);
	$objPHPExcel->getActiveSheet()->SetCellValue('C358', number_format($totalamountpreviousmonthunformatted +  $totalamountthismonthunformatted - $totalamountunformatted));
	$objPHPExcel->getActiveSheet()->SetCellValue('D358', (number_format($mergedtransactionspreviousmonthunformatted + $mergedtransactionsthismonthunformatted - $mergedtransactionsunformatted)));
	$objPHPExcel->getActiveSheet()->SetCellValue('E358', number_format($unmergedtransactionspreviousmonthunformatted + $unmergedtransactionsthismonthunformatted - $unmergedtransactionsunformatted));

    //Today's transactions
	$objPHPExcel->getActiveSheet()->SetCellValue('B359', $totaltransactions);
	$objPHPExcel->getActiveSheet()->SetCellValue('C359', $totalamount);
	$objPHPExcel->getActiveSheet()->SetCellValue('D359', $mergedtransactions);
	$objPHPExcel->getActiveSheet()->SetCellValue('E359', $unmergedtransactions);

	$objPHPExcel->getActiveSheet()->SetCellValue('B360', number_format($transactionsunallocatedprevious + $transactionsallocatedprevious + $transactionsunallocated + $transactionsallocated - $transactionsunallocated - $transactionsallocated));
	$objPHPExcel->getActiveSheet()->SetCellValue('C360', number_format($totalamountpreviousmonthunformatted +  $totalamountthismonthunformatted - $totalamountunformatted +  $totalamountunformatted));
	$objPHPExcel->getActiveSheet()->SetCellValue('D360', number_format($mergedtransactionspreviousmonthunformatted + $mergedtransactionsthismonthunformatted - $mergedtransactionsunformatted + $mergedtransactionsunformatted));
	$objPHPExcel->getActiveSheet()->SetCellValue('E360', number_format($unmergedtransactionspreviousmonthunformatted + $unmergedtransactionsthismonthunformatted - $unmergedtransactionsunformatted + $unmergedtransactionsunformatted));


$unmergedtransactionsaging30 = 0;
	$stime =  date('Y-m-d', strtotime(("-31 day")));
	$etime =  date('Y-m-d', strtotime(("-1 day")));
	$empInfo = $this->export->fetchunallocatedniitthirty($stime, $etime);
	
	foreach ($empInfo as $element) {
		$unmergedtransactionsaging30 += $element['TransAmount'];
	}
	
	$unmergedtransactionsaging60 = 0;
	$stime =  date('Y-m-d', strtotime(("-61 day")));
	$etime =  date('Y-m-d', strtotime(("-32 day")));
	$empInfo = $this->export->fetchunallocatedniitthirty($stime, $etime);
	
	foreach ($empInfo as $element) {
		$unmergedtransactionsaging60 += $element['TransAmount'];
	}
	
	$unmergedtransactionsaging90 = 0;
	$stime =  date('Y-m-d', strtotime(("-91 day")));
	$etime =  date('Y-m-d', strtotime(("-62 day")));
	$empInfo = $this->export->fetchunallocatedniitthirty($stime, $etime);
	
	foreach ($empInfo as $element) {
		$unmergedtransactionsaging90 += $element['TransAmount'];
	}
	
	$unmergedtransactionsaging120 = number_format(($unmergedtransactionspreviousmonthunformatted + $unmergedtransactionsthismonthunformatted) - ($unmergedtransactionsaging30 + $unmergedtransactionsaging60 + $unmergedtransactionsaging90));
	$unmergedtransactionsaging30 = number_format($unmergedtransactionsaging30);
	$unmergedtransactionsaging60 = number_format($unmergedtransactionsaging60);
	$unmergedtransactionsaging90 = number_format($unmergedtransactionsaging90);
	
	
	$objPHPExcel->getActiveSheet()->SetCellValue('A374', 'Aging Report – unallocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('A375', 'Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('B375', '1 – 30 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('C375', '31 – 60 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('D375', '61 - 90 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('E375', 'Over 90 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('F375', 'Total Due');
	$objPHPExcel->getActiveSheet()->SetCellValue('B376', $unmergedtransactionsaging30);
	$objPHPExcel->getActiveSheet()->SetCellValue('C376', $unmergedtransactionsaging60);
	$objPHPExcel->getActiveSheet()->SetCellValue('D376', $unmergedtransactionsaging90);
	$objPHPExcel->getActiveSheet()->SetCellValue('E376', $unmergedtransactionsaging120);
	$objPHPExcel->getActiveSheet()->SetCellValue('F376', number_format($unmergedtransactionspreviousmonthunformatted + $unmergedtransactionsthismonthunformatted));
     //Do some alignment
     //Do some alignment
	$objPHPExcel->getActiveSheet()->getStyle('B357:E360')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	//end first bit 
	$objPHPExcel->getActiveSheet()->getStyle('B376:F376')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	$objPHPExcel->getActiveSheet()->getStyle('B375:F375')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050'); 


#Do cummins in teh nairobi summary
$totaltransactions = 0;
	$totalamount = 0;
	$mergedtransactions = 0;
	$unmergedtransactions = 0;
	
	$totaltransactionsprevious = 0;
	$totalamountprevious = 0;
	$mergedtransactionsprevious = 0;
	$unmergedtransactionsprevious = 0;
	
	$totaltransactionsthismonth = 0;
	$totalamountthismonth = 0;
	$mergedtransactionsthismonth = 0;
	$unmergedtransactionsthismonth = 0;
	
	$totaltransactionspreviousmonth = 0;
	$totalamountpreviousmonth = 0;
	$mergedtransactionspreviousmonth = 0;
	$unmergedtransactionspreviousmonth = 0;
	
	
        //Dates
	$reportdate = date('Y-m-d', strtotime(("-1 day")));
	$prevousdate = date('Y-m-d', strtotime(("-2 day")));
	

	$stime =  date('Y-m-d', strtotime(("-1 day")));
	$this->load->library('excel');
	$empInfo = $this->export->fetchallocatedcummins($stime);

        // set Row
	$rowCount = 2;
	$transactionsallocated = 0;
	foreach ($empInfo as $element) {
		$rowCount++;
		$transactionsallocated++;
		$totalamount += $element['TransAmount'];
		$mergedtransactions += $element['TransAmount'];
	}

	$stime =  date('Y-m-d', strtotime(("-1 day")));
	$empInfo = $this->export->fetchunallocatedcummins($stime);

        // set Row
	$rowCount = 2;
	$transactionsunallocated = 0;
	foreach ($empInfo as $element) {
		$rowCount++;
		$transactionsunallocated++;
		$totalamount += $element['TransAmount'];
		$unmergedtransactions += $element['TransAmount'];
	}


	$stime =  date('Y-m-d', strtotime(("-1 day")));
	$empInfo = $this->export->fetchunallocatedcumulativecummins($stime);
		

	$totaltransactions = number_format($transactionsunallocated + $transactionsallocated);
	$totalamountunformatted = $totalamount;
	$totalamount = number_format($totalamount);
	$mergedtransactionsunformatted = $mergedtransactions;
	$mergedtransactions = number_format($mergedtransactions);
	$unmergedtransactionsunformatted = $unmergedtransactions;
	$unmergedtransactions = number_format($unmergedtransactions);



     //Get the daily b/f details
    $stime = date('Y-m-d', strtotime(("-1 day")));
	
	$previousdaysallocated = $this->export->fetchallocatedpreviousdayscummins($stime);
	$previousdaysunallocated = $this->export->fetchunallocatedprevioudayscummins($stime);

     //Get the totals for the previous days
	 $transactionsallocatedprevious = 0;
	foreach ($previousdaysallocated as $row) {
		$transactionsallocatedprevious +=  $row[0];
		$totalamountprevious += $row[1];
		$mergedtransactionsprevious +=$row[1];
	}

$transactionsunallocatedprevious = 0;
	foreach ($previousdaysunallocated as $row) {
		$transactionsunallocatedprevious += $row[0];
		$totalamountprevious += $row[1];
		$unmergedtransactionsprevious += $row[1];
	}

	$totaltransactionsprevious = number_format($transactionsunallocatedprevious + $transactionsallocatedprevious - $transactionsunallocated - $transactionsallocated);
	$totalamountpreviousunformatted =  $totalamountprevious;
	$totalamountprevious =  number_format($totalamountprevious);
	$mergedtransactionspreviousunformatted = $mergedtransactionsprevious;
	$mergedtransactionsprevious = number_format($mergedtransactionsprevious);
	$unmergedtransactionspreviousunformatted = $unmergedtransactionsprevious;
	$unmergedtransactionsprevious = number_format($unmergedtransactionsprevious);



        // load excel library
	$this->load->library('excel');
	//$objPHPExcel = new PHPExcel();
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(0);

        //Create filename for the movement summary for the month's data 
	$objPHPExcel->getActiveSheet()->SetCellValue('A391', 'Mpesa Monthly Movement Summary');
	$objPHPExcel->getActiveSheet()->SetCellValue('A392', 'Month');
	$objPHPExcel->getActiveSheet()->SetCellValue('B392', date('F-Y'));
	$objPHPExcel->getActiveSheet()->SetCellValue('A393', 'Particulars');
	$objPHPExcel->getActiveSheet()->SetCellValue('B393', 'Transactions');
	$objPHPExcel->getActiveSheet()->SetCellValue('C393', 'Total');
	$objPHPExcel->getActiveSheet()->SetCellValue('D393', 'Allocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('E393', 'Un Allocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('B394', 'Nos');
	$objPHPExcel->getActiveSheet()->SetCellValue('C394', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('D394', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('E394', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('A395', 'Month Balance b/f');
	$objPHPExcel->getActiveSheet()->SetCellValue('A396', 'This month');
	$objPHPExcel->getActiveSheet()->SetCellValue('A397', 'Monthly c/f');

     //Populate the data for the monthly section of the movement 
     //This month data 
	$stime =  date('Y-m-d',strtotime("first day of this month"));
	$thismonthallocated = $this->export->fetchallocatedthismonthcummins($stime);
	$thismonthunallocated = $this->export->fetchunallocatedthismonthcummins($stime);

     //Previous month
	$stime =  date('Y-m-d',strtotime("last day of previous month"));
	$previousmonthallocated = $this->export->fetchallocatedpreviousmonthcummins($stime);
	$previousmonthunallocated = $this->export->fetchunallocatedpreviousmonthcummins($stime);

     //Get the totals for the previous month
	foreach ($previousmonthallocated as $row) {
		$transactionsallocatedpreviousmonth +=  $row[0];
		$totalamountpreviousmonth += $row[2];
		$mergedtransactionspreviousmonth +=$row[2];
	}

	foreach ($previousmonthunallocated as $row) {
		$transactionsunallocatedpreviousmonth+= $row[0];
		$totalamountpreviousmonth += $row[1];
		$unmergedtransactionspreviousmonth += $row[1];
	}


     //Get the allocated totals for this month
	foreach ($thismonthallocated as $row) {
		$transactionsallocatedthismonth++;
		$totalamountthismonth += $row['DocTotal'];
		$mergedtransactionsthismonth += $row['DocTotal'];
	}


  //Get the unallocated totals for this month
	foreach ($thismonthunallocated as $row) {
		$transactionsunallocatedthismonth++;
		$totalamountthismonth += $row['TransAmount'];
		$unmergedtransactionsthismonth += $row['TransAmount'];
	}

    //Sums for this month 
	$totaltransactionsthismonthunformatted = $transactionsunallocatedthismonth + $transactionsallocatedthismonth;
	$totaltransactionsthismonth = number_format($transactionsunallocatedthismonth + $transactionsallocatedthismonth);
	$totalamountthismonthunformatted = $totalamountthismonth;
	$totalamountthismonth = number_format($totalamountthismonth);
	$mergedtransactionsthismonthunformatted = $mergedtransactionsthismonth;
	$mergedtransactionsthismonth = number_format($mergedtransactionsthismonth);
	$unmergedtransactionsthismonthunformatted = $unmergedtransactionsthismonth;
	$unmergedtransactionsthismonth = number_format($unmergedtransactionsthismonth);

      //Sums for previous months
	$totaltransactionspreviousmonthunformatted = $transactionsunallocatedpreviousmonth + $transactionsallocatedpreviousmonth;
	$totaltransactionspreviousmonth = number_format($transactionsunallocatedpreviousmonth + $transactionsallocatedpreviousmonth);
	$totalamountpreviousmonthunformatted =  $totalamountpreviousmonth;
	$totalamountpreviousmonth =  number_format($totalamountpreviousmonth);
	$mergedtransactionspreviousmonthunformatted = $mergedtransactionspreviousmonth;
	$mergedtransactionspreviousmonth = number_format($mergedtransactionspreviousmonth);
	$unmergedtransactionspreviousmonthunformatted = $unmergedtransactionspreviousmonth;
	$unmergedtransactionspreviousmonth = number_format($unmergedtransactionspreviousmonth);


        //last months' transactions
	$objPHPExcel->getActiveSheet()->SetCellValue('B395', $totaltransactionspreviousmonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('C395', $totalamountpreviousmonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('D395', $mergedtransactionspreviousmonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('E395', $unmergedtransactionspreviousmonth);

    //this month's transactions
	$objPHPExcel->getActiveSheet()->SetCellValue('B396', $totaltransactionsthismonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('C396', $totalamountthismonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('D396', $mergedtransactionsthismonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('E396', $unmergedtransactionsthismonth);

      //summary
	$objPHPExcel->getActiveSheet()->SetCellValue('B397', (number_format($totaltransactionspreviousmonthunformatted + $totaltransactionsthismonthunformatted)));
	$objPHPExcel->getActiveSheet()->SetCellValue('C397', (number_format($totalamountpreviousmonthunformatted +  $totalamountthismonthunformatted)));
	$objPHPExcel->getActiveSheet()->SetCellValue('D397', (number_format($mergedtransactionspreviousmonthunformatted + $mergedtransactionsthismonthunformatted)));
	$objPHPExcel->getActiveSheet()->SetCellValue('E397', (number_format($unmergedtransactionspreviousmonthunformatted + $unmergedtransactionsthismonthunformatted)));

     //Do some alignment
	$objPHPExcel->getActiveSheet()->getStyle('B394:E397')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	$objPHPExcel->getActiveSheet()->getStyle('B393:E393')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050'); 
	$objPHPExcel->getActiveSheet()->getStyle('B383:E383')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050'); 


//the first bit
 // set Header
	$objPHPExcel->getActiveSheet()->SetCellValue('A379', 'C&G Group');
	$objPHPExcel->getActiveSheet()->SetCellValue('A380', 'Div/Co');
	$objPHPExcel->getActiveSheet()->SetCellValue('B380', 'Cummins');
	$objPHPExcel->getActiveSheet()->SetCellValue('A381', 'Mpesa Daily Movement Summary');
	$objPHPExcel->getActiveSheet()->SetCellValue('A382', 'Date');
	$objPHPExcel->getActiveSheet()->SetCellValue('B382', $reportdate);
	$objPHPExcel->getActiveSheet()->SetCellValue('A383', 'Particulars');
	$objPHPExcel->getActiveSheet()->SetCellValue('B383', 'Transactions');
	$objPHPExcel->getActiveSheet()->SetCellValue('C383', 'Total');
	$objPHPExcel->getActiveSheet()->SetCellValue('D383', 'Allocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('E383', 'Un Allocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('B384', 'Nos');
	$objPHPExcel->getActiveSheet()->SetCellValue('C384', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('D384', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('E384', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('A385', 'Balance b/f');
	$objPHPExcel->getActiveSheet()->SetCellValue('A386', 'Transactions Today');
	$objPHPExcel->getActiveSheet()->SetCellValue('A387', 'Daily c/f');

        //yesterday's transactions
	$objPHPExcel->getActiveSheet()->SetCellValue('B385', $totaltransactionsprevious);
	$objPHPExcel->getActiveSheet()->SetCellValue('C385', number_format($totalamountpreviousmonthunformatted +  $totalamountthismonthunformatted - $totalamountunformatted));
	$objPHPExcel->getActiveSheet()->SetCellValue('D385', (number_format($mergedtransactionspreviousmonthunformatted + $mergedtransactionsthismonthunformatted - $mergedtransactionsunformatted)));
	$objPHPExcel->getActiveSheet()->SetCellValue('E385', number_format($unmergedtransactionspreviousmonthunformatted + $unmergedtransactionsthismonthunformatted - $unmergedtransactionsunformatted));

    //Today's transactions
	$objPHPExcel->getActiveSheet()->SetCellValue('B386', $totaltransactions);
	$objPHPExcel->getActiveSheet()->SetCellValue('C386', $totalamount);
	$objPHPExcel->getActiveSheet()->SetCellValue('D386', $mergedtransactions);
	$objPHPExcel->getActiveSheet()->SetCellValue('E386', $unmergedtransactions);

	$objPHPExcel->getActiveSheet()->SetCellValue('B387', number_format($transactionsunallocatedprevious + $transactionsallocatedprevious + $transactionsunallocated + $transactionsallocated - $transactionsunallocated - $transactionsallocated));
	$objPHPExcel->getActiveSheet()->SetCellValue('C387', number_format($totalamountpreviousmonthunformatted +  $totalamountthismonthunformatted - $totalamountunformatted +  $totalamountunformatted));
	$objPHPExcel->getActiveSheet()->SetCellValue('D387', number_format($mergedtransactionspreviousmonthunformatted + $mergedtransactionsthismonthunformatted - $mergedtransactionsunformatted + $mergedtransactionsunformatted));
	$objPHPExcel->getActiveSheet()->SetCellValue('E387', number_format($unmergedtransactionspreviousmonthunformatted + $unmergedtransactionsthismonthunformatted - $unmergedtransactionsunformatted + $unmergedtransactionsunformatted));


$unmergedtransactionsaging30 = 0;
	$stime =  date('Y-m-d', strtotime(("-31 day")));
	$etime =  date('Y-m-d', strtotime(("-1 day")));
	$empInfo = $this->export->fetchunallocatedcumminsthirty($stime, $etime);
	
	foreach ($empInfo as $element) {
		$unmergedtransactionsaging30 += $element['TransAmount'];
	}
	
	$unmergedtransactionsaging60 = 0;
	$stime =  date('Y-m-d', strtotime(("-61 day")));
	$etime =  date('Y-m-d', strtotime(("-32 day")));
	$empInfo = $this->export->fetchunallocatedcumminsthirty($stime, $etime);
	
	foreach ($empInfo as $element) {
		$unmergedtransactionsaging60 += $element['TransAmount'];
	}
	
	$unmergedtransactionsaging90 = 0;
	$stime =  date('Y-m-d', strtotime(("-91 day")));
	$etime =  date('Y-m-d', strtotime(("-62 day")));
	$empInfo = $this->export->fetchunallocatedcumminsthirty($stime, $etime);
	
	foreach ($empInfo as $element) {
		$unmergedtransactionsaging90 += $element['TransAmount'];
	}
	
	$unmergedtransactionsaging120 = number_format(($unmergedtransactionspreviousmonthunformatted + $unmergedtransactionsthismonthunformatted) - ($unmergedtransactionsaging30 + $unmergedtransactionsaging60 + $unmergedtransactionsaging90));
	$unmergedtransactionsaging30 = number_format($unmergedtransactionsaging30);
	$unmergedtransactionsaging60 = number_format($unmergedtransactionsaging60);
	$unmergedtransactionsaging90 = number_format($unmergedtransactionsaging90);
	
	
	$objPHPExcel->getActiveSheet()->SetCellValue('A401', 'Aging Report – unallocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('A402', 'Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('B402', '1 – 30 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('C402', '31 – 60 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('D402', '61 - 90 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('E402', 'Over 90 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('F402', 'Total Due');
	$objPHPExcel->getActiveSheet()->SetCellValue('B403', $unmergedtransactionsaging30);
	$objPHPExcel->getActiveSheet()->SetCellValue('C403', $unmergedtransactionsaging60);
	$objPHPExcel->getActiveSheet()->SetCellValue('D403', $unmergedtransactionsaging90);
	$objPHPExcel->getActiveSheet()->SetCellValue('E403', $unmergedtransactionsaging120);
	$objPHPExcel->getActiveSheet()->SetCellValue('F403', number_format($unmergedtransactionspreviousmonthunformatted + $unmergedtransactionsthismonthunformatted));
     //Do some alignment
	$objPHPExcel->getActiveSheet()->getStyle('B384:E387')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	//end first bit 
	$objPHPExcel->getActiveSheet()->getStyle('B403:F403')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	$objPHPExcel->getActiveSheet()->getStyle('B402:F402')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050');
	}
	
	for($col = 'A'; $col !== 'G'; $col++) {
		$objPHPExcel->getActiveSheet()
		->getColumnDimension($col)
		->setAutoSize(true);
	}

	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter->save($fileName);


  //$myemails = array('vlangat@ari.co.ke, viclan29@gmail.com');
//$myemails = array('vlangat@ari.co.ke, joel@cargen.com, gilbert.mutai@cargen.com, titus.murage@cargen.com, nkinyagia@ari.co.ke');
	$msg = '<span style="font-family: arial;">Dear Team,  <br> <br>';
	$msg .= 'Transactions summary. Date: '.$reportdate.' <br> <br>';
	$msg .= 'Total transactions: '.$totaltransactionsfinal.'<br>';
	$msg .= 'Total amount: KES '.$totalamountfinal.'<br>';
	$msg .= 'Amount Allocated: KES '.$mergedtransactionsfinal.'<br>';
	$msg .= 'Amount un-allocated: Kes '.$unmergedtransactionsfinal.'<br> <br>';
	$msg .= 'Attached see system generated daily reports.<br><br>Kind Regards, <br>M-Link <br> Pay Bill Number - '.$shortcode.' -'.$branch.'</span>';


	$data = array('tblreportemails.active' => 1, 'tblcredentials.shortCode' => $shortcode);

	$myemails = $this->user->getreportusers($data);
	
	foreach ($myemails as $address)
	{
		$this->email->clear();
		$this->email->to($address['email']);
		$this->email->subject('M-Link Daily Transactions Summary Report '.$shortcode.' - '. $branches);
		$this->email->from(MAIL_FROM);
		$this->email->message( $msg );
		$this->email->attach($fileName);
		$this->email->send();
		echo 'sent';
	}
	
	 //Delete the created files from the harddisk after sent to the users
	unlink($fileName);
        }else{
		echo 'Not there';
		}
}

public function doniit(){
	//Do for cummins
	$totaltransactions = 0;
	$totalamount = 0;
	$mergedtransactions = 0;
	$unmergedtransactions = 0;
	
	$totaltransactionsprevious = 0;
	$totalamountprevious = 0;
	$mergedtransactionsprevious = 0;
	$unmergedtransactionsprevious = 0;
	
	$totaltransactionsthismonth = 0;
	$totalamountthismonth = 0;
	$mergedtransactionsthismonth = 0;
	$unmergedtransactionsthismonth = 0;
	
	$totaltransactionspreviousmonth = 0;
	$totalamountpreviousmonth = 0;
	$mergedtransactionspreviousmonth = 0;
	$unmergedtransactionspreviousmonth = 0;
	
	
        //Dates
	$reportdate = date('Y-m-d', strtotime(("-1 day")));
	$prevousdate = date('Y-m-d', strtotime(("-2 day")));
	
    // create file name
	$fileNamec = 'Daily data summary (Niit)-'.$reportdate.'.xlsx';  
    // load excel library
	$stime =  date('Y-m-d', strtotime(("-1 day")));
	$this->load->library('excel');
	$empInfo = $this->export->fetchallocatedniit($stime);
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(1);
	$objPHPExcel->getActiveSheet()->setTitle("Allocated Transactions");
        // set Header
	$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Customer');
	$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Phone Number');
	$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Paid KES');
	$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'On');
	$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Balance KES'); 
	$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Mpesa Code');
	$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'SAP Doc');
	$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Branch');
	$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'SAP Doc Date');
	$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'SAP Doc Total KSH');
	
        // set Row
	$rowCount = 2;
	$transactionsallocated = 0;
	foreach ($empInfo as $element) {
		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['ContactName']);
		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['MSISDN']);
		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['TransAmount']);
		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['TransTime']);
		$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['Balance']);
		$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['TransID']);
		$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['SapDocNum']);
		$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['BPName']);
		$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['DocDate']);
		$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['DocTotal']);
		$rowCount++;
		$transactionsallocated++;
		$totalamount += $element['TransAmount'];
		$mergedtransactions += $element['TransAmount'];
	}

	for($col = 'A'; $col !== 'K'; $col++) {
		$objPHPExcel->getActiveSheet()
		->getColumnDimension($col)
		->setAutoSize(true);
	}
	$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050'); 
/*
	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter->save($fileNamec);*/

        // create file name unallocated
	$fileNameunallocatedc = 'Pending Transactions data (NIIT)-'.$reportdate.'.xlsx';  
        // load excel library
	$this->load->library('excel');
	$stime =  date('Y-m-d', strtotime(("-1 day")));
	$empInfo = $this->export->fetchunallocatedniit($stime);
	//$objPHPExcel = new PHPExcel();
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(2);
	$objPHPExcel->getActiveSheet()->setTitle("Pending Transactions");
        // set Header
	$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Customer');
	$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Phone Number');
	$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Paid KES');
	$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'On');
	$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Balance KES'); 
	$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Mpesa Code');

        // set Row
	$rowCount = 2;
	$transactionsunallocated = 0;
	foreach ($empInfo as $element) {
		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['ContactName']);
		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['MSISDN']);
		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['TransAmount']);
		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['TransTime']);
		$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['Balance']);
		$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['TransID']);
		$rowCount++;
		$transactionsunallocated++;
		$totalamount += $element['TransAmount'];
		$unmergedtransactions += $element['TransAmount'];
	}

	for($col = 'A'; $col !== 'G'; $col++) {
		$objPHPExcel->getActiveSheet()
		->getColumnDimension($col)
		->setAutoSize(true);
	}
		$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050'); 

	/*
	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter->save($fileNameunallocatedc);*/

        //Get the list of the cumulative pending
        // create file name unallocated cumulative
	$fileNameunallocatedcumulativec = 'Pending Cumulative Transactions  (NIIT) data as at '.$reportdate.'.xlsx';  
        // load excel library
	$this->load->library('excel');
	$stime =  date('Y-m-d', strtotime(("-1 day")));
	$empInfo = $this->export->fetchunallocatedcumulativeniit($stime);
	//$objPHPExcel = new PHPExcel();
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(3);
	$objPHPExcel->getActiveSheet()->setTitle("Pending Cummulative");
        // set Header
	$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Customer');
	$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Phone Number');
	$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Paid KES');
	$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'On');
	$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Balance KES'); 
	$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Mpesa Code');
	$objPHPExcel->getActiveSheet()->SetCellValue('G1', '1 – 30 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('H1', '31 – 60 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('I1', '61 - 90 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Over 90 Days');

        // set Row
	$rowCount = 2;
	foreach ($empInfo as $element) {
		$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['ContactName']);
		$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['MSISDN']);
		$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['TransAmount']);
		$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['TransTime']);
		$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['Balance']);
		$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['TransID']);
		$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['thethirty']);
		$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['thesixty']);
		$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['theninety']);
		$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['theabove']);
		$rowCount++;
	}

	for($col = 'A'; $col !== 'K'; $col++) {
		$objPHPExcel->getActiveSheet()
		->getColumnDimension($col)
		->setAutoSize(true);
	}
		$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050'); 

	/*
	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter->save($fileNameunallocatedcumulativec);*/
        //End the list of cumulative pending
		
		

	$totaltransactions = number_format($transactionsunallocated + $transactionsallocated);
	$totalamountunformatted = $totalamount;
	$totalamount = number_format($totalamount);
	$mergedtransactionsunformatted = $mergedtransactions;
	$mergedtransactions = number_format($mergedtransactions);
	$unmergedtransactionsunformatted = $unmergedtransactions;
	$unmergedtransactions = number_format($unmergedtransactions);



     //Get the daily b/f details
    $stime = date('Y-m-d', strtotime(("-1 day")));
	
	$previousdaysallocated = $this->export->fetchallocatedpreviousdaysniit($stime);
	$previousdaysunallocated = $this->export->fetchunallocatedprevioudaysniit($stime);

     //Get the totals for the previous days
	 $transactionsallocatedprevious = 0;
	foreach ($previousdaysallocated as $row) {
		$transactionsallocatedprevious +=  $row[0];
		$totalamountprevious += $row[1];
		$mergedtransactionsprevious +=$row[1];
	}

$transactionsunallocatedprevious = 0;
	foreach ($previousdaysunallocated as $row) {
		$transactionsunallocatedprevious += $row[0];
		$totalamountprevious += $row[1];
		$unmergedtransactionsprevious += $row[1];
	}

	$totaltransactionsprevious = number_format($transactionsunallocatedprevious + $transactionsallocatedprevious - $transactionsunallocated - $transactionsallocated);
	$totalamountpreviousunformatted =  $totalamountprevious;
	$totalamountprevious =  number_format($totalamountprevious);
	$mergedtransactionspreviousunformatted = $mergedtransactionsprevious;
	$mergedtransactionsprevious = number_format($mergedtransactionsprevious);
	$unmergedtransactionspreviousunformatted = $unmergedtransactionsprevious;
	$unmergedtransactionsprevious = number_format($unmergedtransactionsprevious);


    //Create filename for the movement summary for the day's data 
	$fileNamemovementc = 'Movement Summary data  (NIIT)-'.$reportdate.'.xlsx';

        // load excel library
	$this->load->library('excel');
	//$objPHPExcel = new PHPExcel();
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle("Movement Summary");
	
	

        //Create filename for the movement summary for the month's data 
	$objPHPExcel->getActiveSheet()->SetCellValue('A13', 'Mpesa Monthly Movement Summary');
	$objPHPExcel->getActiveSheet()->SetCellValue('A14', 'Month');
	$objPHPExcel->getActiveSheet()->SetCellValue('B14', date('F-Y'));
	$objPHPExcel->getActiveSheet()->SetCellValue('A15', 'Particulars');
	$objPHPExcel->getActiveSheet()->SetCellValue('B15', 'Transactions');
	$objPHPExcel->getActiveSheet()->SetCellValue('C15', 'Total');
	$objPHPExcel->getActiveSheet()->SetCellValue('D15', 'Allocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('E15', 'Un Allocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('B16', 'Nos');
	$objPHPExcel->getActiveSheet()->SetCellValue('C16', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('D16', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('E16', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('A17', 'Month Balance b/f');
	$objPHPExcel->getActiveSheet()->SetCellValue('A18', 'This month');
	$objPHPExcel->getActiveSheet()->SetCellValue('A19', 'Monthly c/f');

     //Populate the data for the monthly section of the movement 
     //This month data 
	$stime =  date('Y-m-d',strtotime("first day of this month"));
	$thismonthallocated = $this->export->fetchallocatedthismonthniit($stime);
	$thismonthunallocated = $this->export->fetchunallocatedthismonthniit($stime);

     //Previous month
	$stime =  date('Y-m-d',strtotime("last day of previous month"));
	$previousmonthallocated = $this->export->fetchallocatedpreviousmonthniit($stime);
	$previousmonthunallocated = $this->export->fetchunallocatedpreviousmonthniit($stime);

     //Get the totals for the previous month
	foreach ($previousmonthallocated as $row) {
		$transactionsallocatedpreviousmonth +=  $row[0];
		$totalamountpreviousmonth += $row[2];
		$mergedtransactionspreviousmonth +=$row[2];
	}

	foreach ($previousmonthunallocated as $row) {
		$transactionsunallocatedpreviousmonth+= $row[0];
		$totalamountpreviousmonth += $row[1];
		$unmergedtransactionspreviousmonth += $row[1];
	}


     //Get the allocated totals for this month
	foreach ($thismonthallocated as $row) {
		$transactionsallocatedthismonth++;
		$totalamountthismonth += $row['DocTotal'];
		$mergedtransactionsthismonth += $row['DocTotal'];
	}


  //Get the unallocated totals for this month
	foreach ($thismonthunallocated as $row) {
		$transactionsunallocatedthismonth++;
		$totalamountthismonth += $row['TransAmount'];
		$unmergedtransactionsthismonth += $row['TransAmount'];
	}

    //Sums for this month 
	$totaltransactionsthismonthunformatted = $transactionsunallocatedthismonth + $transactionsallocatedthismonth;
	$totaltransactionsthismonth = number_format($transactionsunallocatedthismonth + $transactionsallocatedthismonth);
	$totalamountthismonthunformatted = $totalamountthismonth;
	$totalamountthismonth = number_format($totalamountthismonth);
	$mergedtransactionsthismonthunformatted = $mergedtransactionsthismonth;
	$mergedtransactionsthismonth = number_format($mergedtransactionsthismonth);
	$unmergedtransactionsthismonthunformatted = $unmergedtransactionsthismonth;
	$unmergedtransactionsthismonth = number_format($unmergedtransactionsthismonth);

      //Sums for previous months
	$totaltransactionspreviousmonthunformatted = $transactionsunallocatedpreviousmonth + $transactionsallocatedpreviousmonth;
	$totaltransactionspreviousmonth = number_format($transactionsunallocatedpreviousmonth + $transactionsallocatedpreviousmonth);
	$totalamountpreviousmonthunformatted =  $totalamountpreviousmonth;
	$totalamountpreviousmonth =  number_format($totalamountpreviousmonth);
	$mergedtransactionspreviousmonthunformatted = $mergedtransactionspreviousmonth;
	$mergedtransactionspreviousmonth = number_format($mergedtransactionspreviousmonth);
	$unmergedtransactionspreviousmonthunformatted = $unmergedtransactionspreviousmonth;
	$unmergedtransactionspreviousmonth = number_format($unmergedtransactionspreviousmonth);


        //last months' transactions
	$objPHPExcel->getActiveSheet()->SetCellValue('B17', $totaltransactionspreviousmonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('C17', $totalamountpreviousmonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('D17', $mergedtransactionspreviousmonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('E17', $unmergedtransactionspreviousmonth);

    //this month's transactions
	$objPHPExcel->getActiveSheet()->SetCellValue('B18', $totaltransactionsthismonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('C18', $totalamountthismonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('D18', $mergedtransactionsthismonth);
	$objPHPExcel->getActiveSheet()->SetCellValue('E18', $unmergedtransactionsthismonth);

      //summary
	$objPHPExcel->getActiveSheet()->SetCellValue('B19', (number_format($totaltransactionspreviousmonthunformatted + $totaltransactionsthismonthunformatted)));
	$objPHPExcel->getActiveSheet()->SetCellValue('C19', (number_format($totalamountpreviousmonthunformatted +  $totalamountthismonthunformatted)));
	$objPHPExcel->getActiveSheet()->SetCellValue('D19', (number_format($mergedtransactionspreviousmonthunformatted + $mergedtransactionsthismonthunformatted)));
	$objPHPExcel->getActiveSheet()->SetCellValue('E19', (number_format($unmergedtransactionspreviousmonthunformatted + $unmergedtransactionsthismonthunformatted)));

     //Do some alignment
	$objPHPExcel->getActiveSheet()->getStyle('B16:E19')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
     // $objPHPExcel->getActiveSheet()->getStyle('B5')->getFill()->getStartColor()->setRGB('92d050');
	$objPHPExcel->getActiveSheet()->getStyle('B5:E5')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050'); 
	$objPHPExcel->getActiveSheet()->getStyle('B15:E15')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050'); 


//the first bit
 // set Header
	$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'C&G Group');
	$objPHPExcel->getActiveSheet()->SetCellValue('A2', 'Div/Co');
	$objPHPExcel->getActiveSheet()->SetCellValue('B2', 'NIIT');
	$objPHPExcel->getActiveSheet()->SetCellValue('A3', 'Mpesa Daily Movement Summary');
	$objPHPExcel->getActiveSheet()->SetCellValue('A4', 'Date');
	$objPHPExcel->getActiveSheet()->SetCellValue('B4', $reportdate);
	$objPHPExcel->getActiveSheet()->SetCellValue('A5', 'Particulars');
	$objPHPExcel->getActiveSheet()->SetCellValue('B5', 'Transactions');
	$objPHPExcel->getActiveSheet()->SetCellValue('C5', 'Total');
	$objPHPExcel->getActiveSheet()->SetCellValue('D5', 'Allocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('E5', 'Un Allocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('B6', 'Nos');
	$objPHPExcel->getActiveSheet()->SetCellValue('C6', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('D6', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('E6', 'Ksh');
	$objPHPExcel->getActiveSheet()->SetCellValue('A7', 'Balance b/f');
	$objPHPExcel->getActiveSheet()->SetCellValue('A8', 'Transactions Today');
	$objPHPExcel->getActiveSheet()->SetCellValue('A9', 'Daily c/f');

        //yesterday's transactions
	$objPHPExcel->getActiveSheet()->SetCellValue('B7', $totaltransactionsprevious);
	$objPHPExcel->getActiveSheet()->SetCellValue('C7', number_format($totalamountpreviousmonthunformatted +  $totalamountthismonthunformatted - $totalamountunformatted));
	$objPHPExcel->getActiveSheet()->SetCellValue('D7', (number_format($mergedtransactionspreviousmonthunformatted + $mergedtransactionsthismonthunformatted - $mergedtransactionsunformatted)));
	$objPHPExcel->getActiveSheet()->SetCellValue('E7', number_format($unmergedtransactionspreviousmonthunformatted + $unmergedtransactionsthismonthunformatted - $unmergedtransactionsunformatted));

    //Today's transactions
	$objPHPExcel->getActiveSheet()->SetCellValue('B8', $totaltransactions);
	$objPHPExcel->getActiveSheet()->SetCellValue('C8', $totalamount);
	$objPHPExcel->getActiveSheet()->SetCellValue('D8', $mergedtransactions);
	$objPHPExcel->getActiveSheet()->SetCellValue('E8', $unmergedtransactions);

	$objPHPExcel->getActiveSheet()->SetCellValue('B9', number_format($transactionsunallocatedprevious + $transactionsallocatedprevious + $transactionsunallocated + $transactionsallocated - $transactionsunallocated - $transactionsallocated));
	$objPHPExcel->getActiveSheet()->SetCellValue('C9', number_format($totalamountpreviousmonthunformatted +  $totalamountthismonthunformatted - $totalamountunformatted +  $totalamountunformatted));
	$objPHPExcel->getActiveSheet()->SetCellValue('D9', number_format($mergedtransactionspreviousmonthunformatted + $mergedtransactionsthismonthunformatted - $mergedtransactionsunformatted + $mergedtransactionsunformatted));
	$objPHPExcel->getActiveSheet()->SetCellValue('E9', number_format($unmergedtransactionspreviousmonthunformatted + $unmergedtransactionsthismonthunformatted - $unmergedtransactionsunformatted + $unmergedtransactionsunformatted));


$unmergedtransactionsaging30 = 0;
	$stime =  date('Y-m-d', strtotime(("-31 day")));
	$etime =  date('Y-m-d', strtotime(("-1 day")));
	$empInfo = $this->export->fetchunallocatedniitthirty($stime, $etime);
	
	foreach ($empInfo as $element) {
		$unmergedtransactionsaging30 += $element['TransAmount'];
	}
	
	$unmergedtransactionsaging60 = 0;
	$stime =  date('Y-m-d', strtotime(("-61 day")));
	$etime =  date('Y-m-d', strtotime(("-32 day")));
	$empInfo = $this->export->fetchunallocatedniitthirty($stime, $etime);
	
	foreach ($empInfo as $element) {
		$unmergedtransactionsaging60 += $element['TransAmount'];
	}
	
	$unmergedtransactionsaging90 = 0;
	$stime =  date('Y-m-d', strtotime(("-91 day")));
	$etime =  date('Y-m-d', strtotime(("-62 day")));
	$empInfo = $this->export->fetchunallocatedniitthirty($stime, $etime);
	
	foreach ($empInfo as $element) {
		$unmergedtransactionsaging90 += $element['TransAmount'];
	}
	
	$unmergedtransactionsaging120 = number_format(($unmergedtransactionspreviousmonthunformatted + $unmergedtransactionsthismonthunformatted) - ($unmergedtransactionsaging30 + $unmergedtransactionsaging60 + $unmergedtransactionsaging90));
	$unmergedtransactionsaging30 = number_format($unmergedtransactionsaging30);
	$unmergedtransactionsaging60 = number_format($unmergedtransactionsaging60);
	$unmergedtransactionsaging90 = number_format($unmergedtransactionsaging90);
	
	
	$objPHPExcel->getActiveSheet()->SetCellValue('A23', 'Aging Report – unallocated');
	$objPHPExcel->getActiveSheet()->SetCellValue('A24', 'Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('B24', '1 – 30 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('C24', '31 – 60 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('D24', '61 - 90 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('E24', 'Over 90 Days');
	$objPHPExcel->getActiveSheet()->SetCellValue('F24', 'Total Due');
	$objPHPExcel->getActiveSheet()->SetCellValue('B25', $unmergedtransactionsaging30);
	$objPHPExcel->getActiveSheet()->SetCellValue('C25', $unmergedtransactionsaging60);
	$objPHPExcel->getActiveSheet()->SetCellValue('D25', $unmergedtransactionsaging90);
	$objPHPExcel->getActiveSheet()->SetCellValue('E25', $unmergedtransactionsaging120);
	$objPHPExcel->getActiveSheet()->SetCellValue('F25', number_format($unmergedtransactionspreviousmonthunformatted + $unmergedtransactionsthismonthunformatted));
     //Do some alignment
     //Do some alignment
	$objPHPExcel->getActiveSheet()->getStyle('B6:E9')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	//end first bit 
	$objPHPExcel->getActiveSheet()->getStyle('B25:F25')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	$objPHPExcel->getActiveSheet()->getStyle('B24:F24')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('92d050'); 

	
	for($col = 'A'; $col !== 'G'; $col++) {
		$objPHPExcel->getActiveSheet()
		->getColumnDimension($col)
		->setAutoSize(true);
	}

	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter->save($fileNamec);


  //$myemails = array('vlangat@ari.co.ke, viclan29@gmail.com');
//$myemails = array('vlangat@ari.co.ke, joel@cargen.com, gilbert.mutai@cargen.com, titus.murage@cargen.com, nkinyagia@ari.co.ke');
	$msg = '<span style="font-family: arial;">Dear Team,  <br> <br>';
	$msg .= 'Transactions summary. Date: '.$reportdate.' <br> <br>';
	$msg .= 'Total transactions: '.$totaltransactions.'<br>';
	$msg .= 'Total amount: KES '.$totalamount.'<br>';
	$msg .= 'Amount Allocated: KES '.$mergedtransactions.'<br>';
	$msg .= 'Amount un-allocated: Kes '.$unmergedtransactions.'<br> <br>';
	$msg .= 'Attached see system generated daily reports.<br><br>Kind Regards, <br>M-Link <br> Pay Bill Number - 562691 - NIIT</span>';

	$data = array('tblreportemails.active' => 1, 'tblcredentials.shortCode' => 562691);

	$myemails = $this->user->getreportusers($data);
	
	foreach ($myemails as $address)
	{
		$this->email->clear();
		$this->email->to($address['email']);
		$this->email->subject('M-Link Daily Transactions Summary Report 562691 -  NIIT');
		$this->email->from(MAIL_FROM);
		$this->email->message( $msg );
		$this->email->attach($fileNamec);
		$this->email->send();
	}


        //Delete the created files from the harddisk after sent to the users
	unlink($fileNamec);
}
}