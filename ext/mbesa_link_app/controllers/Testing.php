<?php
class Testing extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('pesa_model');
		$this->load->model('auditmodel');
		$this->load->model('dailyreport','export');
		
		//Load the emailing settings
		$this->load->library('email');
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html'; 
		$config['newline'] ='\r\n';
		$config['clrf'] ='\r\n';
		$config['protocol'] = 'smtp';
		
		$config['smtp_host'] = SMTP_HOST;
		$config['smtp_port'] = SMTP_PORT;
		$config['smtp_user'] = SMTP_USER;
		$config['smtp_pass'] = SMTP_PASS;

		$this->email->initialize($config);
		
		$this->load->library('excel');
		
	}
	
	function checkvalues()
    {
		$status = 'N';
		$stime =  date('Y-m-d',strtotime("last day of previous month"));
		
	
	$query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CUMMINS_LIVE.OMPS."TransID"';
	//$query ='select count(*) as count, sum(ARI_CUMMINS_LIVE.OMPS."TransAmount") as received, sum(case when ARI_CUMMINS_LIVE.MPS1."TransID" is null then 0 else ARI_CUMMINS_LIVE.MPS1."DocTotal"  end ) as doct';
	 $query .=' from ARI_CUMMINS_LIVE.OMPS';
    $query .=' left outer join ARI_CUMMINS_LIVE.MPS1 on ARI_CUMMINS_LIVE.OMPS."TransID" = ARI_CUMMINS_LIVE.MPS1."TransID"';
    $query .= ' where "TransTime" <=\''. $stime  .' 23:59:59\'';
    $query .= ' and  "DocStatus" is null';

    
    $db =  $this->load->database('ARI_CUMMINS_LIVE',true);
    $m = $db->query($query);
	
      $empInfo =  $db->query($query)->result_array();
	  foreach ($empInfo as $element) {
	  echo $element['ContactName'].' '.$element['MSISDN'].' '.$element['TransAmount'].' '.$element['TransTime'].'<br>';
	  }
}
	
	
	public function createdailyreport() {
		//Date
		$reportdate = date('Y-m-d', strtotime(("-1 day")));
    // create file name
        $fileName = 'Allocated Transactions data-'.$reportdate.'.xlsx';  
    // load excel library
        $this->load->library('excel');
        $empInfo = $this->export->fetchallocated();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Customer');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Phone Number');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Paid KES');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'On');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Balance KES'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Mpesa Code');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'SAP Doc');
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Branch');
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'SAP Doc Date');
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'SAP Doc Total KSH');
		
        // set Row
        $rowCount = 2;
        foreach ($empInfo as $element) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['ContactName']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['MSISDN']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['TransAmount']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['TransTime']);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['Balance']);
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['TransID']);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['SapDocNum']);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['BPName']);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['DocDate']);
            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['DocTotal']);
            $rowCount++;
        }
		
		
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($fileName);
		
		// create file name unallocated
        $fileNameunallocated = 'Pending Transactions data-'.$reportdate.'.xlsx';  
		// load excel library
        $this->load->library('excel');
        $empInfo = $this->export->fetchunallocated();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Customer');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Phone Number');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Paid KES');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'On');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Balance KES'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Mpesa Code');
		
        // set Row
        $rowCount = 2;
        foreach ($empInfo as $element) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['ContactName']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['MSISDN']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['TransAmount']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['TransTime']);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['Balance']);
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['TransID']);
            $rowCount++;
        }
		
		
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($fileNameunallocated);
		
		$msg = 'Hi.<br> <br> Attached see system generated daily report for '.$reportdate.'.<br>Thank you. <br><br>Regards, <br> M-Link.';
		$this->email->to('vlangat@ari.co.ke');
		$this->email->subject('Daily Reports');
		$this->email->from(MAIL_FROM);
		$this->email->message( $msg );
		$this->email->attach($fileName);
		$this->email->attach($fileNameunallocated);
		$this->email->send();
		
		echo $this->email->print_debugger();
    }
}