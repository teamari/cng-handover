<?php
class Sim extends CI_Controller
{

	var $OrgAccountBalance;
	var $TransactionType;
	var $TransID;
	var $TransTime;
	var $BusinessShortCode;
	var $BillRefNumber;
	var $ThirdPartyTransID;
	var $MSISDN;
	var $TransAmount;
	var $FirstName;
	var $MiddleName;
	var $InvoiceNumber;
	var $LastName;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('pesa_model');
		$this->load->model('auditmodel');
	}

	public function confirmation()
	{
		$this->TransactionType = "Pay Bill";
		$this->TransID = uniqid();
		$this->TransTime = date('YmdHis');
		$this->BusinessShortCode = "";
		$this->BillRefNumber = uniqid();
		$this->ThirdPartyTransacID = "Simulate";
		$this->MSISDN = $this->input->post('phone');
		$this->TransAmount = $this->input->post('amount');
		$this->FirstName = $this->input->post('fname');
		$this->MiddleName = $this->input->post('mname');
		$this->InvoiceNumber = "CNG";
		$this->OrgAccountBalance = 1000;
		$this->LastName = $this->input->post('lname');

		$year = substr($this->TransTime, 0, 4);
		$month = substr($this->TransTime, 4, 2);
		$day = substr($this->TransTime, 6, 2);
		$hour = substr($this->TransTime, 8, 2);
		$min = substr($this->TransTime, 10, 2);
		$sec = substr($this->TransTime, 12, 2);
		$mdate = $year . "-" . $month . "-" . $day . " " . $hour . ":" . $min . ":" . $sec;

		$added = $this->pesa_model->insert($this);
		if ($added > 0) {
			$db = $this->load->database("ARI_CONFIRM", true);
			$db->query("insert into ARI_CONFIRM.OMPS (\"TransID\",\"TransTime\",\"TransAmount\",\"BusinessShortCode\",\"MSISDN\",\"ContactName\",\"Balance\",\"flag\") 
			values('". $this->TransID ."','". $mdate ."',". $this->TransAmount .",'". $this->BusinessShortCode ."','".$this->MSISDN ."','". $this->FirstName .' '.$this->MiddleName ."',". $this->TransAmount .", 1)");
		}

		$this->session->set_flashdata('tempdata', 'Simulation done');
		redirect(APP_BASE . "simulate");
	}
}
