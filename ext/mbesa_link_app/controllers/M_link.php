<?php
class M_link extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('tokenmodel');
        $this->load->model('usersmodel');
        $this->load->model('commonmodel');
        $this->load->model('notificationsmodel');
    }

    public function platform($page = "dashboard")
    {
        $this->index($page);
    }
	
	
    public function index($page = "dashboard")
    {
        $token = $this->session->userdata('token');
        if (empty($token))
            $this->timeout();
        else {
            $tokens = $this->tokenmodel->get(array('token' => $token, 'isvalid' => 1, 'username' => $this->session->userdata('username')));

            if ($tokens->num_rows() > 0) {
                $mtoken = $tokens->row();

                $now = strtotime(date('Y-m-d H:i:s'));
                $lastactivity = strtotime($mtoken->lastactive);
                $interval =  round(abs($now - $lastactivity) / 60, 0);

                if ($interval < TIMEOUT) {
                    $username = $mtoken->username;
                    $users = $this->usersmodel->get(array('usercode' => $username, 'recordstate' => 0));

                    if ($users->num_rows() > 0) {
                        $user = $users->row();
                        $role = $user->Usertype;
                        $BusinessID = $user->BusinessID;

                        $menus = array();
                        if ($role == "Admin")
                            $menus = $this->commonmodel->get_menu(array('link' => $page, 'adminallowed' => 1));
                        else if($role  == "Normal")
                            $menus = $this->commonmodel->get_menu(array('link' => $page, 'normalallowed' => 1));
						else
							$menus = $this->commonmodel->get_menu(array('link' => $page, 'reconcilerallowed' => 1));

                        $this->load->view('shared/header.php', array('role' => $role));

                        if ($menus->num_rows() > 0 && file_exists('mbesa_link_app/views/mlink/' . $page . '.php')) {
                            $this->load->view('mlink/' . $page);
                        } else
                            $this->load->view('mlink/sorry');
                        $this->load->view('shared/footer.php');

                        $this->tokenmodel->update(array('lastactive' => date('Y-m-d H:i:s')), array('token' => $token, 'isvalid' => 1));
                    } else
                        $this->timeout();
                } else
                    $this->timeout();
            } else
                $this->timeout();
        }
    }

    private function timeout()
    {
        redirect("/users/timeout");
    }

    function checksesssion()
    {
        $token = $this->session->userdata('token');
        if (empty($token)) {
            echo json_encode(array('state' =>  'timeout'));
        } else {
            $tokens = $this->tokenmodel->get(array('token' => $token, 'username' => $this->session->userdata('username')));
            if ($tokens->num_rows() > 0) {
                $mtoken = $tokens->row();
                $now = strtotime(date('Y-m-d H:i:s'));
                $lastactivity = strtotime($mtoken->lastactive);
                $interval =  round(abs($now - $lastactivity) / 60, 0);
                if ($interval < TIMEOUT)
                    echo json_encode(array('state' => 'ok'));
                else
                    echo json_encode(array('state' =>  'timeout'));
            } else {
                echo json_encode(array('state' =>  'timeout'));
            }
        }
    }

    function notifications()
    {
        $notes = $this->notificationsmodel->get(array('username' => $this->session->userdata('username'), 'isread' => 0));
        if ($notes->num_rows() > 0) {
            $note = $notes->row();
            $this->notificationsmodel->update(array('isread' => 1), array('id' => $note->id));
            echo json_encode(array('result' => 'ok', 'message' => $note));
        } else
            echo json_encode(array('result' => 'fail'));
    }

    function listchart(){
        $sfrom = date('Y-m-d', strtotime(("-2 week")));
        $sto = date('Y-m-d');
		$db = $this->load->database("ARI_CONFIRM",true);
		$result = $db->query("select date(ARI_CONFIRM.OMPS.TransTime) as mdate,ARI_CONFIRM.MPS1.BPName, sum(ARI_CONFIRM.OMPS.TransAmount) as TransAmount
        from ARI_CONFIRM.MPS1 
        left outer join ARI_CONFIRM.OMPS on ARI_CONFIRM.OMPS.TransID = ARI_CONFIRM.MPS1.Transid 
        where TransTime >='". $sfrom ." 00:00:00' and TransTime <='". $sto  ." 23:59:59' 
        group by date(ARI_CONFIRM.OMPS.TransTime),ARI_CONFIRM.MPS1.BPName");
        echo json_encode($result->result_array());
	}



}
