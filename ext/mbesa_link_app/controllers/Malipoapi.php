<?php
class Malipoapi extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('pesa_model');
		$this->load->model('auditmodel');
		
		//Load the emailing settings
		$this->load->library('email');
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html'; 
		$config['newline'] ='\r\n';
		$config['clrf'] ='\r\n';
		$config['protocol'] = 'smtp';
		
		$config['smtp_host'] = SMTP_HOST;
		$config['smtp_port'] = SMTP_PORT;
		$config['smtp_user'] = SMTP_USER;
		$config['smtp_pass'] = SMTP_PASS;

		$this->email->initialize($config);
	}
	
	public function confirmation()
	{
		$postData = file_get_contents('php://input');
		$object = json_decode($postData);
  
		$protocol = is_https() ? "https://" : "http://";
		$host = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "";
		
		if((stristr($host, '196.201.') !== FALSE)){
			$transCode = $object->TransID;
			$codes = $this->pesa_model->get(array('TransID'=>$transCode));
	  
			if($codes->num_rows()<1 ){
				$added = $this->pesa_model->insert($object);
		
				$tcode = $object->TransID;
				$data = array(
				  'InvoiceNumber' => $object->InvoiceNumber,
				  'OrgAccountBalance' => $object->OrgAccountBalance,
				  'ThirdPartyTransacID' => $object->ThirdPartyTransID
				);

				$where = array(
				  'MSISDN' => $object->MSISDN,
				  'TransID' => $object->TransID
				);

		
				$this->auditmodel->insert("Payment confirmation done " . $tcode, $this->db->affected_rows(), "Mpesa - Interface", $postData);
				$this->pesa_model->update($data, $where);
				
				if($added>0 && ($object->TransactionType =="Pay Bill" || $object->TransactionType =="Customer Merchant Payment")){
				  $obj = $object;
				  $year = substr($obj->TransTime,0,4);
				  $month = substr($obj->TransTime,4,2);
				  $day = substr($obj->TransTime,6,2); 
				  $hour = substr($obj->TransTime,8,2);
				  $min = substr($obj->TransTime,10,2);
				  $sec = substr($obj->TransTime,12,2);
				  $mdate = $year ."-". $month ."-". $day ." ". $hour .":".$min .":". $sec;
				  
				  if($obj->BusinessShortCode == 664988){
					  $db = $this->load->database("ARI_CUMMINS_LIVE", true);
					  $db->query("insert into ARI_CUMMINS_LIVE.OMPS (\"TransID\",\"TransTime\",\"TransAmount\",\"BusinessShortCode\",\"MSISDN\",\"ContactName\",\"Balance\",\"flag\") 
					values('". $obj->TransID ."','". $mdate ."',". $obj->TransAmount .",'". $obj->BusinessShortCode ."','". $obj->MSISDN ."','". str_replace("'","''", $obj->FirstName) .' '. str_replace("'","''", $obj->MiddleName) ."',". $obj->TransAmount .",1)");
				  }else if($obj->BusinessShortCode == 562691){
					  $db = $this->load->database("ARI_CONF_NIIT", true);
					  $db->query("insert into ARI_CONF_NIIT.OMPS (\"TransID\",\"TransTime\",\"TransAmount\",\"BusinessShortCode\",\"MSISDN\",\"ContactName\",\"Balance\",\"flag\") 
					values('". $obj->TransID ."','". $mdate ."',". $obj->TransAmount .",'". $obj->BusinessShortCode ."','". $obj->MSISDN ."','". str_replace("'","''", $obj->FirstName) .' '. str_replace("'","''", $obj->MiddleName) ."',". $obj->TransAmount .",1)");
				  }else{
					  $db = $this->load->database("ARI_CONFIRM", true);
					  $db->query("insert into ARI_CONFIRM.OMPS (\"TransID\",\"TransTime\",\"TransAmount\",\"BusinessShortCode\",\"MSISDN\",\"ContactName\",\"Balance\",\"flag\") 
					values('". $obj->TransID ."','". $mdate ."',". $obj->TransAmount .",'". $obj->BusinessShortCode ."','". $obj->MSISDN ."','". str_replace("'","''", $obj->FirstName) .' '. str_replace("'","''", $obj->MiddleName) ."',". $obj->TransAmount .",1)");
				  }
				 }
		
				if ($added > 0) {
					echo '{"ResultCode": 0, "ResultDesc": "", "ThirdPartyTransID": "1234567890"}';
				}
				else
					echo '{"ResultCode": 1, "ResultDesc": "", "ThirdPartyTransID": "0000"}';
			}else{
				$msg = '<p>Transaction of Phone number '. $object->MSISDN.' '.$object->TransID.' with the amount '.$object->TransAmount.' for the customer '.$object->FirstName.' already  has a another entry.';
				$this->email->to('info@ari.co.ke');
				$this->email->subject('Double Entry');
				$this->email->from(MAIL_FROM);
				$this->email->message( $msg );
				$this->email->send();
			}
		}
	}
	
	public function validation_url()
	{
		$postData = file_get_contents('php://input');
		$obj = json_decode($postData);
		header("Content-Type:application/json");
		$added = $this->pesa_model->insert($obj);
		if ($added > 0) {
			echo '{"ResultCode": 0, "ResultDesc": "", "ThirdPartyTransID": "1234567890"}';
		} else
			echo '{"ResultCode": 1, "ResultDesc": "", "ThirdPartyTransID": "0000"}';

		$this->auditmodel->insert("Trasaction validated, Request submitted to safaricom.", $added, "Mpesa - Interface", $postData);
	}

	public function timeout()
	{
		$postData = file_get_contents('php://input');
		header("Content-Type:application/json");
		echo '{"ResultCode": 0, "ResultDesc": "The service was accepted successfully", "ThirdPartyTransID": "0000"}';
		$this->auditmodel->insert("Trasaction failed. Timeout experienced.", 1, "Mpesa - Interface", $postData);
	}

	public function balance()
	{
		$postData = file_get_contents('php://input');
		header("Content-Type:application/json");
		echo '{"ResultCode": 0, "ResultDesc": "The service was accepted successfully", "ThirdPartyTransID": "0000"}';
	}
/*

	public function simulatePayment($token)
	{
		$amount = $this->input->post('amount');
		$curl_post_data = array(
			//Fill in the request parameters with valid values
			'ShortCode' =>'600000',
			'CommandID' =>  'CustomerPayBillOnline',
			'Amount' =>$amount,
			'Msisdn' => '254708374149',
			'BillRefNumber' => 'Tet34587'
		);

		$data_string = json_encode($curl_post_data);
		$url = SAFCOM .'/mpesa/c2b/v1/simulate';

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:Bearer ' . $token)); //setting custom header
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_HEADER, true);

		$curl_response = curl_exec($curl);
		$this->auditmodel->insert("Mpesa response", 1, "Mpesa - Interface", $curl_response);
		$alldata = substr($curl_response, strpos( $curl_response,"{"),strlen($curl_response)-1);
		if (strpos($curl_response, "HTTP/1.1 200 OK") >= 0) {
				$object = json_decode($alldata);
				$m = "";
				foreach ($object as $item => $value) {
					$m .= "<b>" . $item . " : </b>\t\t " . $value . "<br>";
				}
				$output = $m;
			} else
				$output = $alldata;
		
		return $output;
	}

*/
	public function registerUrl($token)
	{
		$curl_post_data = array(
			'ShortCode' => SHORTCODE,
			'ResponseType' => 'Completed',
			'ConfirmationURL' => base_url() . 'malipoapi/confirmation',
			'ValidationURL' => base_url() . 'malipoapi/validation_url'
		);
		$data_string = json_encode($curl_post_data);
		$url = SAFCOM .'/mpesa/c2b/v1/registerurl';
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:Bearer ' . $token)); //setting a custom header
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
		$curl_response = curl_exec($curl);
		
		if (curl_error($curl)) {
			$output = curl_error($curl);
		} else {
			$this->auditmodel->insert("Mpesa Url registered ", 1, "Mpesa - Interface", $curl_response);
			$alldata = substr($curl_response, strpos( $curl_response,"{"),strlen($curl_response)-1);
				if (strpos($curl_response, "HTTP/1.1 200 OK") >= 0) {
					//$json = $data[8];
					$object = json_decode($alldata);
					$m = "";
					foreach ($object as $item => $value) {
						$m .= "<b>" . $item . " : </b>\t\t " . $value . "<br>";
					}
					$output = $m;
				} else
					$output = $alldata;
		
		}
		curl_close($curl);
		
		return $output;
	}


	public function index($activity = "register")
	{
		$url = SAFCOM .'/oauth/v1/generate?grant_type=client_credentials';
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Basic ' . base64_encode(API_KEY . ':' . API_SECRET))); //setting a custom header
		curl_setopt($curl, CURLOPT_HEADER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$curl_response = curl_exec($curl);
		
		curl_close($curl);
		//$data  = json_decode( $curl_response);
		//$data  = explode("\r\n", $curl_response);
		$alldata = substr($curl_response, strpos( $curl_response,"{"),strlen($curl_response)-1);
		if (strpos($curl_response, "HTTP/1.1 200 OK") >= 0) {
				$object = json_decode($alldata);
				if ($activity == "register"){
					$resp = $this->registerUrl($object->access_token);
				}else
					$resp ="no action";
				//	$resp = $this->simulatePayment($object->access_token);
				$this->session->set_flashdata('tempdata', $resp);
			} else
				$this->session->set_flashdata('tempdata', $curl_response);
		
		redirect(APP_BASE . "simulate");
	}
}
