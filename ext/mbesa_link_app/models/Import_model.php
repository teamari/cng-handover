<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');

class Import_model extends CI_Model {

  private $_batchImport;

  public function setBatchImport($batchImport) {
    $this->_batchImport = $batchImport;
  }

    // save data
  public function importData() {
    $data = $this->_batchImport;
    $this->db->insert_batch('omps', $data);
  }
    // get inserted list
  public function employeeList() {
    $this->db->select(array('e.TransID', 'e.TransTime', 'e.TransAmount', 'e.BusinessShortCode', 'e.MSISDN', 'e.ContactName', 'e.Balance', 'e.flag'));
    $this->db->from('omps as e');
    $query = $this->db->get();
    return $query->result_array();
  }
  
  //Get the user details meeting the condition
  public function confirmifexist($condition = '')
  {
    $query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance","TransID","BusinessShortCode" from ARI_CONFIRM.OMPS';
    if (!empty($condition)) {
      $query .=' where "TransID" = \''. $condition .'\'';
    }
	
    $db =  $this->load->database('ARI_CONFIRM',true);

    return $db->query($query);
  }

  //Insert the generated code
  public function insertcode($data)
  {
    return $this->db->insert('tblResetCode', $data);
  }

   // get code
  public function getcode($data)
  {
    $this->db->where($data);

    return $this->db->get('tblResetCode');
  }

    //Delete the authenticated confirmation code
  public function rowdelete($data)
  {
    $this->db->where($data);
    $this->db->delete('tblResetCode');
  }

}

?>