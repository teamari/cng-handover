<?php

class Smsmodel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    //Insert code to database
    public function insert($data)
    {
        return $this->db->insert('tblResetCode', $data);
    }

    // get code
    public function getcode($data)
    {
        $this->db->where($data);

        return $this->db->get('tblResetCode');
    }

    //Delete the authenticated confirmation code
    public function rowdelete($data)
    {
        $this->db->where($data);
        $this->db->delete('tblResetCode');
    }
}
