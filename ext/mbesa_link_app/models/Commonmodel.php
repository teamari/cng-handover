<?php
class Commonmodel extends CI_Model{

  function __construct()
  {
    parent::__construct();   
  }

  function get_menu($condition="",$orderby="")
  {
    if(!empty($orderby))
      $this->db->order_by($orderby,'asc');

    if(!empty($condition))
     $this->db->where($condition);
   return $this->db->get('menus');
 }
 
 function get_branches($condition="",$orderby="")
 {
  $this->db->select('*');
  $this->db->from('tblcredentials');
  $this->db->join('tblportalgrants', 'tblportalgrants.creadential_id=tblcredentials.id', 'inner');
  if (!empty($condition)) {
    $this->db->where($condition);
  }
  if(!empty($orderby)){
    $this->db->order_by($orderby,'asc');
  }

  return $this->db->get();
}

function get_county($condition="")
{
  if(!empty($condition))
   $this->db->where($condition);
 return $this->db->get('county');
}

function get_subcounty($condition="")
{
  if(!empty($condition))
   $this->db->where($condition);
 return $this->db->get('sub_county');
}
}