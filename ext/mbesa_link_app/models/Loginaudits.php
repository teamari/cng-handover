<?php

class Loginaudits extends CI_Model
{
    public $user_id = '';
    public $logindate;
    public $description;
    public $ip_address = '';
    public $headers = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function insert($by, $activity)
    {
        $this->user_id  = $by;
        $this->logindate = date('Y-m-d H:i:s');
        $this->description = $activity;
        $this->ip_address = $_SERVER['REMOTE_ADDR'];
        $this->headers = json_encode($this->input->request_headers());

        return $this->db->insert('tblAdminLoginLog', $this);
    }
}
