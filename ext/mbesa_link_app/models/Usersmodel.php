<?php
class Usersmodel extends CI_Model{
  var $recordstate="";
  var $CreationDate;
  var $CreationUser="";
  var $ApproveDate ="";
  var $ApproveComment ="";
  var $DeleteDate;
  var $DeleteUser ="";
  var $DeleteReason="";
  var $UserCode ="";
  var $Surname ="";
  var $Firstname ="";
  var $Secondname ="";
  var $email ="";
  var $phone ="";
  var $LastLoginTime;
  var $UserEnabled ="";
  var $Password ="";
  var $BusinessID ="";
  var $UserType="";
  
    var $region="";  //territory
    var $route=""; 

    var $CUST_CD="";

    function __construct()
    {
      parent::__construct();   
    }
    
    
    function get($condition="")
    {
      $this->db->order_by('CreationDate','desc');

      if(!empty($condition))
       $this->db->where($condition);
     return $this->db->get('users');
   }

   function update($data,$condition){
    return $this->db->update('users',$data,$condition);
  }

  function insertObj($obj){
    return $this->db->insert('users',$obj);
  }

 
  
  function insert($generatedPassword = "",$BusinessID,$usercode="")
  {
    if(!empty($usercode))
      $username = $usercode;
    else
      $username=  $this->input->post('email');

    if(!empty($generatedPassword))
      $mpassword = $generatedPassword;
    else
      $mpassword = $this->input->post('password');
    
    $upassword = password_hash($mpassword,PASSWORD_BCRYPT);
    
    $username = trim($username);
    $username = strip_tags($username);
    
    $this->recordstate = 0;
    $this->CreationUser= $this->session->userdata('username');
    $this->CreationDate = date('Y-m-d H:i:s');
    $this->ApproveComment="New System User";
    $this->ApproveDate= date('Y-m-d H:i:s');
    $this->Firstname = $this->input->post('firstname');
    $this->Secondname =$this->input->post('secondname');
    $this->Surname = $this->input->post('surname');
    $this->email = $this->input->post('email');
    $this->Password = $upassword;
    $this->phone = $this->input->post('phoneno');
    $this->UserType = $this->input->post('usertype');
    $this->UserCode = $username;
    $this->UserEnabled = 1;
    $this->BusinessID = $BusinessID;
    $this->LastLoginTime = date('Y-m-d H:i:s');

    $this->region = $this->input->post('territory');
    $this->route = $this->input->post('route');
    $this->CUST_CD = $this->input->post('custcode');
    
    return $this->db->insert('users',$this);
  }

//Get the user list
  public function getuserslist($condition = '')
  {
   $this->db->select('*');
   $this->db->from('users');
   if (!empty($condition)) {
     $this->db->where($condition);
   }
   
   return $this->db->get()->result_array();
 }
 
 //get the users for the reports assigning
 public function getuserslistreports($condition = '')
  {
   $this->db->select('*');
   $this->db->from('tblreportemails');
   if (!empty($condition)) {
     $this->db->where($condition);
   }
   
   return $this->db->get()->result_array();
 }
 
 //Get the list of the paybills to assign access to users
 public function getcredentialslist($condition = '')
 {
   $this->db->select('*');
   $this->db->from('tblcredentials');
   if (!empty($condition)) {
     $this->db->where($condition);
   }
   $this->db->order_by('branch');
   
   return $this->db->get()->result_array();
 }
}