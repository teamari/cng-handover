<?php
class msgmodel extends CI_Model {
    
	function __construct()
    {
        parent::__construct();
	}

	function get($condition="")
    {
        if(!empty($condition))
            $this->db->where($condition);
        return $this->db->get('outbox');
	}
	
	function update($data,$condition){
        return $this->db->update('outbox',$data,$condition);
    }
	
    function insert_comm($destination, $type, $message, $subject="" , $too="",$addedby="")
    {
		
		if(empty($addedby))
		$addedby = $this->session->userdata('username');
		$code = date('YmdHisu');
		$comms = array(
			'destination' => $destination,
			'name' => $too,
			'subject' => $subject,
			'type' => $type,
			'addedon' => date('Y-m-d H:i:s'),
			'addedby' => $addedby,
			'code' =>$code
		); 

		if(strtolower($type) == strtolower("email")){
			$comms['deliverydescription'] = 'Sent';
			$comms['message']  =  trim($this->emailcampain($too,$message,$addedby));
		} else 
			$comms['message']  = trim($message);
			
		$this->db->insert('outbox',$comms);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, base_url() ."comms/comm/". urlencode(base64_encode($addedby)) ."/". urlencode(base64_encode($code)) );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		$apiresult = curl_exec($ch);
		curl_close($ch);
		return 1;
	}
	
     function emailcampain($to,$message,$addedby=""){
		return '<html>
		<head>
			 <style>.button{ background-color:#0000ff; color: #fff !important; font-size: 14px; border-radius: 5px ;
				cursor:pointer; padding: 10px; text-decoration: none}
			body{font-family:sans-serif;font-size:12px; background: #f6f6f6}
			.body{background-color:#fff; border: solid 1px #dcdcdc; border-top-left-radius: 5px; 
				   border-top-right-radius: 5px; width: 90%; line-height: 28px; margin: 0 auto;}
			.footer{width: 90%; line-height: 28px;margin-left: 1px; margin: 0 auto; background: #dcdcdc; border-bottom-left-radius: 5px; 
				   border-bottom-right-radius: 5px;  border: solid 1px #dcdcdc; padding: 10px;}
			.content{ padding: 15px;}
			 </style>
		</head>
		<body>
			<div class="body">
				<div class="content">'. (empty($addedby)? 'Hi  '. @ucfirst(strtolower($to)) .',<br>' : '' ).'
				'. $message .'
				<p><b>Note:</b> Do not share your password with anyone to ensure account security and privacy.</p>
				<p><i>This is an automated email, Please do not reply.</i></p>
				Kind Regards, <br/>  
				'. APP_NAME .' 
				</div>
			</div>
			<table class="footer">	
					<tr>
						<td width="25%"><img  src="'.base_url().'_public/images/logo.png" height="80"></td>
						<td width="*" style="line-height: 28px; font-family:sans-serif;font-size:12px; ">
							&nbsp;&nbsp;&nbsp;&nbsp; &copy; '.date("Y").' Powered by ARI Ltd
						</td>
					</tr>
			</table>
		</body>
		</html>';
    }
}