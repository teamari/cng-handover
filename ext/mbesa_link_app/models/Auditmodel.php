<?php
class Auditmodel extends CI_Model {

    var $UserCode="";
    var $activity="";
    var $ActivityDate;
    var $queryStatus="";
    var $ipAddress ="";
    var $headers="";
    var $data="";

    function __construct()
    {
        parent::__construct();
    }

    function get($condition="")
    {
        $this->db->order_by('auditID','desc');
        if(!empty($condition))
           $this->db->where($condition);
        return $this->db->get('audit');
    }

    function insert($activity, $status, $by="",$mdata="")
    {
        $this->ipAddress =$_SERVER['REMOTE_ADDR'] ;
        $this->activity = $activity;
        $this->queryStatus = $status;
        $this->ActivityDate = date('Y-m-d H:i:s');
        $this->data = $mdata;
        if(!empty($by))
            $this->UserCode =$by;
        else
            $this->UserCode =$this->session->userdata('username');
        $this->headers = json_encode($this->input->request_headers());
        return $this->db->insert('audit', $this);
    }

}