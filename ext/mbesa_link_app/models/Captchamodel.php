<?php
class Captchamodel extends CI_Model {

    var $word="";
    var $captcha_time;
    var $ip_address ="";
    var $origin ="";


    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function get($condition="")
    {
        if(!empty($condition))
           $this->db->where($condition);
        return $this->db->get('captcha');
    }
	
	function update($id){
		return $this->db->update('captcha',array('deleted'=>1),array('captcha_id'=>$id));
	}

    function insert($word, $time,$origin)
    {
		$this->db->update('captcha',array('deleted'=>1),array('captcha_time <='=>strtotime("-30 min")));
        $this->ip_address = $this->input->ip_address();
        $this->word = $word;
        $this->captcha_time = $time;
        $this->origin = $origin;
        return $this->db->insert('captcha', $this);
    }

}