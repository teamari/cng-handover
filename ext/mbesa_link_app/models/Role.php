<?php
class Role extends CI_Model
{
    public $roleName="";

    public function __construct()
    {
        parent::__construct();
    }

    //Get the details from the table tblRole
    public function get($condition="")
    {
        $this->db->order_by('id', 'asc');
        if (!empty($condition)) {
            $this->db->where($condition);
        }
        return $this->db->get('tblRole');
    }

    //Insert into the tblRole
    public function insert($roleName)
    {
        $this->roleName = $roleName;

        return $this->db->insert('tblRole', $this);
    }

    //Update the values in the database
    public function update($data, $condition)
    {
        $this->db->trans_start();

        $this->db->where($condition);
        $this->db->update('tblRole', $data);

        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            return 0;
        } elseif ($this->db->trans_status() === true) {
            return 1;
        }
    }

    //delete the values from the database
    public function delete($data, $condition)
    {
        $this->db->trans_start();

        $this->db->where($condition);
        $this->db->update('tblRole', $data);

        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            return 0;
        } elseif ($this->db->trans_status() === true) {
            return 1;
        }
    }
}
