<?php
class Notificationsmodel extends CI_Model{

    var $username="";
    var $message="";

    function __construct()
    {
        parent::__construct();   
    }

   function get($condition="")
    {
        $this->db->order_by('id','desc');
        if(!empty($condition))
           $this->db->where($condition);
        return $this->db->get('notifications');
    }

    function update($data,$condition){
        return $this->db->update('notifications',$data,$condition);
    }

    function insert($username,$message)
    {
        $this->username = $username;
        $this->message = $message;
        return $this->db->insert('notifications',$this);
    }
}