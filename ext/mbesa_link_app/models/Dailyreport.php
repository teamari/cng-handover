  <?php
  class Dailyreport extends CI_Model{
    function __construct()
    {
      parent::__construct();   
    }
    
    function fetchallocated($stime, $shortcode)
    {
      $status = 'N';
      
      $query = 'select "ContactName","BusinessShortCode","MSISDN","TransAmount","TransTime","Balance",ARI_CONFIRM.OMPS."TransID","SapDocNum","BPName","DocDate",
      "DocTotal"';
      $query .= ' from ARI_CONFIRM.OMPS';
      $query .= ' left outer join ARI_CONFIRM.MPS1 on ARI_CONFIRM.OMPS."TransID" = ARI_CONFIRM.MPS1."TransID"';
      $query .= ' where "BusinessShortCode"= \''. $shortcode .'\' and "TransTime" >=\''. $stime .' 00:00:00\' and  "TransTime" <=\''. $stime  .' 23:59:59\'';
      $query .= ' and "DocStatus" = \''. $status .'\'';
      
      $db =  $this->load->database('ARI_CONFIRM',true);
      $m = $db->query($query);

      return $db->query($query)->result_array();
    }
	
	function fetchallocatedcummins($stime)
    {
      $status = 'N';
      
      $query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CUMMINS_LIVE.OMPS."TransID","SapDocNum","BPName","DocDate",
      "DocTotal"';
      $query .= ' from ARI_CUMMINS_LIVE.OMPS';
      $query .= ' left outer join ARI_CUMMINS_LIVE.MPS1 on ARI_CUMMINS_LIVE.OMPS."TransID" = ARI_CUMMINS_LIVE.MPS1."TransID"';
      $query .= ' where "TransTime" >=\''. $stime .' 00:00:00\' and  "TransTime" <=\''. $stime  .' 23:59:59\'';
      $query .= ' and  "DocStatus" = \''. $status .'\'';
      
      $db =  $this->load->database('ARI_CUMMINS_LIVE',true);
      $m = $db->query($query);

      return $db->query($query)->result_array();
    }
    
    function fetchallocatedthismonth($stime, $shortcode)
    {
      $status = 'N';
      $query = 'select "ContactName","BusinessShortCode","MSISDN","TransAmount","TransTime","Balance",ARI_CONFIRM.OMPS."TransID","SapDocNum","BPName","DocDate",
      "DocTotal"';
      $query .= ' from ARI_CONFIRM.OMPS';
      $query .= ' left outer join ARI_CONFIRM.MPS1 on ARI_CONFIRM.OMPS."TransID" = ARI_CONFIRM.MPS1."TransID"';
      $query .= ' where "BusinessShortCode"= \''. $shortcode .'\' and "TransTime" >=\''. $stime .' 00:00:00\'';

      $query .= ' and  "DocStatus" = \''. $status .'\'';
      
      $db =  $this->load->database('ARI_CONFIRM',true);
      $m = $db->query($query);

      return $db->query($query)->result_array();
    }
	
	function fetchallocatedthismonthcummins($stime)
    {
      $status = 'N';
      $query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CUMMINS_LIVE.OMPS."TransID","SapDocNum","BPName","DocDate",
      "DocTotal"';
      $query .= ' from ARI_CUMMINS_LIVE.OMPS';
      $query .= ' left outer join ARI_CUMMINS_LIVE.MPS1 on ARI_CUMMINS_LIVE.OMPS."TransID" = ARI_CUMMINS_LIVE.MPS1."TransID"';
      $query .= ' where "TransTime" >=\''. $stime .' 00:00:00\'';

      $query .= ' and  "DocStatus" = \''. $status .'\'';

    
    $db =  $this->load->database('ARI_CUMMINS_LIVE',true);
    $m = $db->query($query);

      return $db->query($query)->result_array();
    }
    
    function fetchallocatedprevious()
    {
      $status = 'N';
      
      $query ='select count(*) as count, sum(ARI_CONFIRM.OMPS."TransAmount") as received, sum(case when ARI_CONFIRM.MPS1."TransID" is null then 0 else ARI_CONFIRM.MPS1."DocTotal"  end ) as doct';
      $query .=' from ARI_CONFIRM.OMPS';
      $query .=' left outer join ARI_CONFIRM.MPS1 on ARI_CONFIRM.OMPS."TransID" = ARI_CONFIRM.MPS1."TransID"';
      $query .=' where TO_VARCHAR(ARI_CONFIRM.OMPS."TransTime", \'YYYY-MM-DD\')<\''. date('Y-m-d', strtotime("-1 day")) .'\'';
      $query .= ' and "DocStatus" = \''. $status .'\'';

      $db =  $this->load->database('ARI_CONFIRM',true);
      $m = $db->query($query);

      $data = array();
      foreach($db->query($query)->result_array() as $row){
       $k=array();
       foreach($row as $item=>$value){
        array_push($k,$value);
      }
      array_push($data,$k);
    }
    return $data;
  }

  function fetchallocatedpreviousmonth($stime, $shortcode)
  {
    $status = 'N';
    
    $query ='select count(case when ARI_CONFIRM.OMPS."BusinessShortCode" = \''. $shortcode .'\' then 1 end) as count, sum(case when ARI_CONFIRM.OMPS."BusinessShortCode" = \''. $shortcode .'\' then ARI_CONFIRM.OMPS."TransAmount" else 0 end) as received, sum(case when ARI_CONFIRM.MPS1."TransID" is null then 0 when ARI_CONFIRM.OMPS."BusinessShortCode" != \''. $shortcode .'\' then 0 else ARI_CONFIRM.MPS1."DocTotal"  end ) as doct';
    $query .=' from ARI_CONFIRM.OMPS';
    $query .=' left outer join ARI_CONFIRM.MPS1 on ARI_CONFIRM.OMPS."TransID" = ARI_CONFIRM.MPS1."TransID"';
    $query .= ' where ARI_CONFIRM.OMPS."BusinessShortCode"= \''. $shortcode .'\' and "TransTime" <=\''. $stime  .' 23:59:59\'';
    $query .= ' and "DocStatus" = \''. $status .'\'';

    $db =  $this->load->database('ARI_CONFIRM',true);
    $m = $db->query($query);

    $data = array();
    foreach($db->query($query)->result_array() as $row){
     $k=array();
     foreach($row as $item=>$value){
      array_push($k,$value);
    }
    array_push($data,$k);
  }
  return $data;
  }
  
  function fetchallocatedpreviousdays($stime, $shortcode)
  {
    $status = 'N';
    
    $query ='select count(case when ARI_CONFIRM.OMPS."BusinessShortCode" = \''. $shortcode .'\' then 1 end) as count, sum(case when ARI_CONFIRM.OMPS."BusinessShortCode" = \''. $shortcode .'\' then ARI_CONFIRM.OMPS."TransAmount" else 0 end) as received, sum(case when ARI_CONFIRM.MPS1."TransID" is null then 0 else ARI_CONFIRM.MPS1."DocTotal"  end ) as doct';
    $query .=' from ARI_CONFIRM.OMPS';
    $query .=' left outer join ARI_CONFIRM.MPS1 on ARI_CONFIRM.OMPS."TransID" = ARI_CONFIRM.MPS1."TransID"';
    $query .= ' where "TransTime" <=\''. $stime  .' 23:59:59\'';
    $query .= ' and "DocStatus" = \''. $status .'\'';

    $db =  $this->load->database('ARI_CONFIRM',true);
    $m = $db->query($query);

    $data = array();
    foreach($db->query($query)->result_array() as $row){
     $k=array();
     foreach($row as $item=>$value){
      array_push($k,$value);
    }
    array_push($data,$k);
  }
  return $data;
  }
  
  function fetchallocatedpreviousdayscummins($stime)
  {
    $status = 'N';
    
    $query ='select count(*) as count, sum(ARI_CUMMINS_LIVE.OMPS."TransAmount") as received, sum(case when ARI_CUMMINS_LIVE.MPS1."TransID" is null then 0 else ARI_CUMMINS_LIVE.MPS1."DocTotal"  end ) as doct';
    $query .=' from ARI_CUMMINS_LIVE.OMPS';
    $query .=' left outer join ARI_CUMMINS_LIVE.MPS1 on ARI_CUMMINS_LIVE.OMPS."TransID" = ARI_CUMMINS_LIVE.MPS1."TransID"';
    $query .= ' where "TransTime" <=\''. $stime  .' 23:59:59\'';
    $query .= ' and "DocStatus" = \''. $status .'\'';

    $db =  $this->load->database('ARI_CUMMINS_LIVE',true);
    $m = $db->query($query);

    $data = array();
    foreach($db->query($query)->result_array() as $row){
     $k=array();
     foreach($row as $item=>$value){
      array_push($k,$value);
    }
    array_push($data,$k);
  }
  return $data;
  }
  
  function fetchallocatedpreviousmonthcummins($stime)
  {
    $status = 'N';
    
    $query ='select count(*) as count, sum(ARI_CUMMINS_LIVE.OMPS."TransAmount") as received, sum(case when ARI_CUMMINS_LIVE.MPS1."TransID" is null then 0 else ARI_CUMMINS_LIVE.MPS1."DocTotal"  end ) as doct';
    $query .=' from ARI_CUMMINS_LIVE.OMPS';
    $query .=' left outer join ARI_CUMMINS_LIVE.MPS1 on ARI_CUMMINS_LIVE.OMPS."TransID" = ARI_CUMMINS_LIVE.MPS1."TransID"';
    $query .= ' where "TransTime" <=\''. $stime  .' 23:59:59\'';
    $query .= ' and "DocStatus" = \''. $status .'\'';

    
    $db =  $this->load->database('ARI_CUMMINS_LIVE',true);
    $m = $db->query($query);

    $data = array();
    foreach($db->query($query)->result_array() as $row){
     $k=array();
     foreach($row as $item=>$value){
      array_push($k,$value);
    }
    array_push($data,$k);
  }
  return $data;
  }
	
	
  function fetchunallocated($stime, $shortcode)
  {
    $query = 'select "ContactName", "BusinessShortCode", "MSISDN","TransAmount","TransTime","Balance",ARI_CONFIRM.OMPS."TransID"';
    $query .= ' from ARI_CONFIRM.OMPS';
    $query .= ' left outer join ARI_CONFIRM.MPS1 on ARI_CONFIRM.OMPS."TransID" = ARI_CONFIRM.MPS1."TransID"';
    $query .= ' where "BusinessShortCode"= \''. $shortcode .'\' and "TransTime" >=\''. $stime .' 00:00:00\' and  "TransTime" <=\''. $stime  .' 23:59:59\'';
    $query .= ' and  "DocStatus" is null order by "TransTime"';
    
    $db =  $this->load->database('ARI_CONFIRM',true);
    $m = $db->query($query);

    return $db->query($query)->result_array();
  }
  
  function fetchunallocatedthirty($stime, $etime, $shortcode)
  {
    $query = 'select "ContactName","BusinessShortCode","MSISDN","TransAmount","TransTime","Balance",ARI_CONFIRM.OMPS."TransID"';
    $query .= ' from ARI_CONFIRM.OMPS';
    $query .= ' left outer join ARI_CONFIRM.MPS1 on ARI_CONFIRM.OMPS."TransID" = ARI_CONFIRM.MPS1."TransID"';
    $query .= ' where "BusinessShortCode"= \''. $shortcode .'\' and "TransTime" >=\''. $stime .' 00:00:00\' and  "TransTime" <=\''. $etime  .' 23:59:59\'';
    $query .= ' and  "DocStatus" is null';
    
    $db =  $this->load->database('ARI_CONFIRM',true);
    $m = $db->query($query);

    return $db->query($query)->result_array();
  }
  
  function fetchunallocatedcummins($stime)
  {
    $query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CUMMINS_LIVE.OMPS."TransID"';
    $query .= ' from ARI_CUMMINS_LIVE.OMPS';
    $query .= ' left outer join ARI_CUMMINS_LIVE.MPS1 on ARI_CUMMINS_LIVE.OMPS."TransID" = ARI_CUMMINS_LIVE.MPS1."TransID"';
    $query .= ' where "TransTime" >=\''. $stime .' 00:00:00\' and  "TransTime" <=\''. $stime  .' 23:59:59\'';
    $query .= ' and  "DocStatus" is null';
    
    $db =  $this->load->database('ARI_CUMMINS_LIVE',true);
    $m = $db->query($query);

    return $db->query($query)->result_array();
  }
  
  function fetchunallocatedcumminsthirty($stime, $etime)
  {
    $query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CUMMINS_LIVE.OMPS."TransID"';
    $query .= ' from ARI_CUMMINS_LIVE.OMPS';
    $query .= ' left outer join ARI_CUMMINS_LIVE.MPS1 on ARI_CUMMINS_LIVE.OMPS."TransID" = ARI_CUMMINS_LIVE.MPS1."TransID"';
    $query .= ' where "TransTime" >=\''. $stime .' 00:00:00\' and  "TransTime" <=\''. $etime  .' 23:59:59\'';
    $query .= ' and  "DocStatus" is null';
    
    $db =  $this->load->database('ARI_CUMMINS_LIVE',true);
    $m = $db->query($query);

    return $db->query($query)->result_array();
  }

  function fetchunallocatedcumulative($stime, $shortcode)
  {
	  $s30time =  date('Y-m-d', strtotime(("-31 day")));
$e30time =  date('Y-m-d', strtotime(("-1 day")));

$s60time =  date('Y-m-d', strtotime(("-61 day")));
$e60time =  date('Y-m-d', strtotime(("-32 day")));

$s90time =  date('Y-m-d', strtotime(("-91 day")));
$e90time =  date('Y-m-d', strtotime(("-62 day")));

$above90s =  date('Y-m-d', strtotime("2019-01-06"));
$above90 =  date('Y-m-d', strtotime(("-92 day")));


    $query = 'select "ContactName", "BusinessShortCode", "MSISDN","TransAmount","TransTime","Balance",ARI_CONFIRM.OMPS."TransID",';
	$query .= ' case when "TransTime" <=\''. $e30time  .' 23:59:59\' and "TransTime" >=\''. $s30time  .' 00:00:00\' then "TransAmount" end as "thethirty",';
	$query .= ' case when "TransTime" <=\''. $e60time  .' 23:59:59\' and "TransTime" >=\''. $s60time  .' 00:00:00\' then "TransAmount" end as "thesixty",';
	$query .= ' case when "TransTime" <=\''. $e90time  .' 23:59:59\' and "TransTime" >=\''. $s90time  .' 00:00:00\' then "TransAmount" end as "theninety",';
	$query .= ' case when "TransTime" <=\''. $above90  .' 23:59:59\' and "TransTime" >=\''. $above90s  .' 00:00:00\' then "TransAmount" end as "theabove"';
    $query .= ' from ARI_CONFIRM.OMPS';
    $query .= ' left outer join ARI_CONFIRM.MPS1 on ARI_CONFIRM.OMPS."TransID" = ARI_CONFIRM.MPS1."TransID"';
    $query .= ' where "BusinessShortCode"= \''. $shortcode .'\' and "TransTime" <=\''. $stime  .' 23:59:59\'';
    $query .= ' and  "DocStatus" is null order by "TransTime" desc';
    
    $db =  $this->load->database('ARI_CONFIRM',true);
    $m = $db->query($query);

    return $db->query($query)->result_array();
  }
  function mytest($stime){
	  $status = 'OCU54TOOEP';
    $query = 'select ARI_CUMMINS_LIVE.OMPS."ID", "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CUMMINS_LIVE.OMPS."TransID"';
	$query .= ' from ARI_CUMMINS_LIVE.OMPS';
	    $query .= ' left outer join ARI_CUMMINS_LIVE.MPS1 on ARI_CUMMINS_LIVE.OMPS."TransID" = ARI_CUMMINS_LIVE.MPS1."TransID"';
    $query .= ' where ARI_CUMMINS_LIVE.OMPS."TransID" = \''. $status .'\'';
    
    $db =  $this->load->database('ARI_CUMMINS_LIVE',true);
    $m = $db->query($query);

    return $db->query($query)->result_array();
  }
  
  
  function fetchunallocatedcumulativecumminstest($stime)
  {
	  $s30time =  date('Y-m-d', strtotime(("-31 day")));
$e30time =  date('Y-m-d', strtotime(("-1 day")));

$s60time =  date('Y-m-d', strtotime(("-61 day")));
$e60time =  date('Y-m-d', strtotime(("-32 day")));

$s90time =  date('Y-m-d', strtotime(("-91 day")));
$e90time =  date('Y-m-d', strtotime(("-62 day")));

$above90s =  date('Y-m-d', strtotime("2019-01-06"));
$above90 =  date('Y-m-d', strtotime(("-92 day")));


    $query = 'select OMPS."ID", "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CUMMINS_TEST.OMPS."TransID",';
    $query .= ' case when "TransTime" <=\''. $e30time  .' 23:59:59\' and "TransTime" >=\''. $s30time  .' 00:00:00\' then "TransAmount" end as "thethirty",';
	$query .= ' case when "TransTime" <=\''. $e60time  .' 23:59:59\' and "TransTime" >=\''. $s60time  .' 00:00:00\' then "TransAmount" end as "thesixty",';
	$query .= ' case when "TransTime" <=\''. $e90time  .' 23:59:59\' and "TransTime" >=\''. $s90time  .' 00:00:00\' then "TransAmount" end as "theninety",';
	$query .= ' case when "TransTime" <=\''. $above90  .' 23:59:59\' and "TransTime" >=\''. $above90s  .' 00:00:00\' then "TransAmount" end as "theabove"';
	
	$query .= ' from ARI_CUMMINS_TEST.OMPS';
    $query .= ' left outer join ARI_CUMMINS_TEST.MPS1 on ARI_CUMMINS_TEST.OMPS."TransID" = ARI_CUMMINS_TEST.MPS1."TransID"';
    $query .= ' where "TransTime" <=\''. $stime  .' 23:59:59\'';
    $query .= ' and  "DocStatus" is null order by "TransTime" desc';
    
    $db =  $this->load->database('ARI_CUMMINS_TEST',true);
    $m = $db->query($query);

    return $db->query($query)->result_array();
  }
  
  function fetthemps1(){
  $query = 'select *';
   
	$query .= ' from ARI_CUMMINS_LIVE.MPS1 order by ID desc';
    $db =  $this->load->database('ARI_CUMMINS_LIVE',true);
    $m = $db->query($query);

    return $db->query($query)->result_array();
  }
  
  function fetchunallocatedcumulativecummins($stime)
  {
	  $s30time =  date('Y-m-d', strtotime(("-31 day")));
$e30time =  date('Y-m-d', strtotime(("-1 day")));

$s60time =  date('Y-m-d', strtotime(("-61 day")));
$e60time =  date('Y-m-d', strtotime(("-32 day")));

$s90time =  date('Y-m-d', strtotime(("-91 day")));
$e90time =  date('Y-m-d', strtotime(("-62 day")));

$above90s =  date('Y-m-d', strtotime("2019-01-06"));
$above90 =  date('Y-m-d', strtotime(("-92 day")));


    $query = 'select OMPS."ID", "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CUMMINS_LIVE.OMPS."TransID",';
    $query .= ' case when "TransTime" <=\''. $e30time  .' 23:59:59\' and "TransTime" >=\''. $s30time  .' 00:00:00\' then "TransAmount" end as "thethirty",';
	$query .= ' case when "TransTime" <=\''. $e60time  .' 23:59:59\' and "TransTime" >=\''. $s60time  .' 00:00:00\' then "TransAmount" end as "thesixty",';
	$query .= ' case when "TransTime" <=\''. $e90time  .' 23:59:59\' and "TransTime" >=\''. $s90time  .' 00:00:00\' then "TransAmount" end as "theninety",';
	$query .= ' case when "TransTime" <=\''. $above90  .' 23:59:59\' and "TransTime" >=\''. $above90s  .' 00:00:00\' then "TransAmount" end as "theabove"';
	
	$query .= ' from ARI_CUMMINS_LIVE.OMPS';
    $query .= ' left outer join ARI_CUMMINS_LIVE.MPS1 on ARI_CUMMINS_LIVE.OMPS."TransID" = ARI_CUMMINS_LIVE.MPS1."TransID"';
    $query .= ' where "TransTime" <=\''. $stime  .' 23:59:59\'';
    $query .= ' and  "DocStatus" is null order by "TransTime" desc';
    
    $db =  $this->load->database('ARI_CUMMINS_LIVE',true);
    $m = $db->query($query);

    return $db->query($query)->result_array();
  }

  function fetchunallocatedthismonth($stime, $shortcode)
  {
    $query = 'select "ContactName", "BusinessShortCode", "MSISDN","TransAmount","TransTime","Balance",ARI_CONFIRM.OMPS."TransID"';
    $query .= ' from ARI_CONFIRM.OMPS';
    $query .= ' left outer join ARI_CONFIRM.MPS1 on ARI_CONFIRM.OMPS."TransID" = ARI_CONFIRM.MPS1."TransID"';
    $query .= ' where "BusinessShortCode"= \''. $shortcode .'\' and "TransTime" >=\''. $stime .' 00:00:00\'';

    $query .= ' and  "DocStatus" is null';
    
    $db =  $this->load->database('ARI_CONFIRM',true);
    $m = $db->query($query);

    return $db->query($query)->result_array();
  }
  
  function fetchunallocatedthismonthcummins($stime)
  {
   $query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CUMMINS_LIVE.OMPS."TransID"';
    $query .= ' from ARI_CUMMINS_LIVE.OMPS';
    $query .= ' left outer join ARI_CUMMINS_LIVE.MPS1 on ARI_CUMMINS_LIVE.OMPS."TransID" = ARI_CUMMINS_LIVE.MPS1."TransID"';
    $query .= ' where "TransTime" >=\''. $stime .' 00:00:00\'';

    $query .= ' and  "DocStatus" is null';
    
    $db =  $this->load->database('ARI_CUMMINS_LIVE',true);
    $m = $db->query($query);

    return $db->query($query)->result_array();
  }

  function fetchunallocatedprevious()
  {
    $query ='select count(*) as count, sum(ARI_CONFIRM.OMPS."TransAmount") as received, sum(case when ARI_CONFIRM.MPS1."TransID" is null then 0 else ARI_CONFIRM.MPS1."DocTotal"  end ) as doct';
    $query .=' from ARI_CONFIRM.OMPS';
    $query .=' left outer join ARI_CONFIRM.MPS1 on ARI_CONFIRM.OMPS."TransID" = ARI_CONFIRM.MPS1."TransID"';
    $query .=' where TO_VARCHAR(ARI_CONFIRM.OMPS."TransTime", \'YYYY-MM-DD\')<\''. date('Y-m-d', strtotime("-1 day")) .'\'';
    $query .= ' and  "DocStatus" is null';

    $db =  $this->load->database('ARI_CONFIRM',true);
    $m = $db->query($query);

    $data = array();
    foreach($db->query($query)->result_array() as $row){
     $k=array();
     foreach($row as $item=>$value){
      array_push($k,$value);
    }
    array_push($data,$k);
  }
  return $data;
  }

  function fetchunallocatedpreviousmonth($stime, $shortcode)
  {
    $stime =  date('Y-m-d',strtotime("last day of previous month"));
    $query ='select count(case when ARI_CONFIRM.OMPS."BusinessShortCode" = \''. $shortcode .'\' then 1 end) as count, sum(case when ARI_CONFIRM.OMPS."BusinessShortCode" = \''. $shortcode .'\' then ARI_CONFIRM.OMPS."TransAmount" else 0 end) as received, sum(case when ARI_CONFIRM.MPS1."TransID" is null then 0 when ARI_CONFIRM.OMPS."BusinessShortCode" != \''. $shortcode .'\' then 0 else ARI_CONFIRM.MPS1."DocTotal"  end ) as doct';
    $query .=' from ARI_CONFIRM.OMPS';
    $query .=' left outer join ARI_CONFIRM.MPS1 on ARI_CONFIRM.OMPS."TransID" = ARI_CONFIRM.MPS1."TransID"';
    $query .= ' where ARI_CONFIRM.OMPS."BusinessShortCode"= \''. $shortcode .'\' and "TransTime" <=\''. $stime  .' 23:59:59\'';
    $query .= ' and  "DocStatus" is null';

    $db =  $this->load->database('ARI_CONFIRM',true);
    $m = $db->query($query);

    $data = array();
    foreach($db->query($query)->result_array() as $row){
     $k=array();
     foreach($row as $item=>$value){
      array_push($k,$value);
    }
    array_push($data,$k);
  }
  return $data;
  }
  
  
   function fetchunallocatedprevioudays($stime, $shortcode)
  {
	  
    $query ='select count(case when ARI_CONFIRM.OMPS."BusinessShortCode" = \''. $shortcode .'\' then 1 end), sum(case when ARI_CONFIRM.OMPS."BusinessShortCode" = \''. $shortcode .'\' then ARI_CONFIRM.OMPS."TransAmount" else 0 end) as received, sum(case when ARI_CONFIRM.MPS1."TransID" is null then 0 when ARI_CONFIRM.OMPS."BusinessShortCode" != \''. $shortcode .'\' then 0 else ARI_CONFIRM.MPS1."DocTotal"  end ) as doct';
    $query .=' from ARI_CONFIRM.OMPS';
    $query .=' left outer join ARI_CONFIRM.MPS1 on ARI_CONFIRM.OMPS."TransID" = ARI_CONFIRM.MPS1."TransID"';
    $query .= ' where ARI_CONFIRM.OMPS."BusinessShortCode"= \''. $shortcode .'\' and "TransTime" <=\''. $stime  .' 23:59:59\'';
    $query .= ' and  "DocStatus" is null';

    $db =  $this->load->database('ARI_CONFIRM',true);
    $m = $db->query($query);

    $data = array();
    foreach($db->query($query)->result_array() as $row){
     $k=array();
     foreach($row as $item=>$value){
      array_push($k,$value);
    }
    array_push($data,$k);
  }
  return $data;
  }
  
  function fetchunallocatedprevioudayscummins($stime)
  {
    $stime =  date('Y-m-d', strtotime(("-1 day")));
    $query ='select count(*) as count, sum(ARI_CUMMINS_LIVE.OMPS."TransAmount") as received, sum(case when ARI_CUMMINS_LIVE.MPS1."TransID" is null then 0 else ARI_CUMMINS_LIVE.MPS1."DocTotal"  end ) as doct';
    $query .=' from ARI_CUMMINS_LIVE.OMPS';
    $query .=' left outer join ARI_CUMMINS_LIVE.MPS1 on ARI_CUMMINS_LIVE.OMPS."TransID" = ARI_CUMMINS_LIVE.MPS1."TransID"';
    $query .= ' where "TransTime" <=\''. $stime  .' 23:59:59\'';
    $query .= ' and  "DocStatus" is null';

    $db =  $this->load->database('ARI_CUMMINS_LIVE',true);
    $m = $db->query($query);

    $data = array();
    foreach($db->query($query)->result_array() as $row){
     $k=array();
     foreach($row as $item=>$value){
      array_push($k,$value);
    }
    array_push($data,$k);
  }
  return $data;
  }
  
  function fetchunallocatedpreviousmonthcummins($stime)
  {
    $stime =  date('Y-m-d',strtotime("last day of previous month"));
    $query ='select count(*) as count, sum(ARI_CUMMINS_LIVE.OMPS."TransAmount") as received, sum(case when ARI_CUMMINS_LIVE.MPS1."TransID" is null then 0 else ARI_CUMMINS_LIVE.MPS1."DocTotal"  end ) as doct';
     $query .=' from ARI_CUMMINS_LIVE.OMPS';
    $query .=' left outer join ARI_CUMMINS_LIVE.MPS1 on ARI_CUMMINS_LIVE.OMPS."TransID" = ARI_CUMMINS_LIVE.MPS1."TransID"';
    $query .= ' where "TransTime" <=\''. $stime  .' 23:59:59\'';
    $query .= ' and  "DocStatus" is null';

    $db =  $this->load->database('ARI_CUMMINS_LIVE',true);
    $m = $db->query($query);

    $data = array();
    foreach($db->query($query)->result_array() as $row){
     $k=array();
     foreach($row as $item=>$value){
      array_push($k,$value);
    }
    array_push($data,$k);
  }
  return $data;
  }



  function fettest(){
   $query ='select LEFT(MONTHNAME(ARI_CONFIRM.OMPS."TransTime"),3) as month, sum(case when ARI_CONFIRM.MPS1."TransID" is null then 0 else ARI_CONFIRM.MPS1."DocTotal"  end ) as total';
   $query .=' from ARI_CONFIRM.OMPS';
   $query .=' left outer join ARI_CONFIRM.MPS1 on ARI_CONFIRM.OMPS."TransID" = ARI_CONFIRM.MPS1."TransID"';
   $query .=' where TO_VARCHAR(ARI_CONFIRM.OMPS."TransTime", \'YYYY\')=\''. date('Y') .'\'';
   
   
   $db =  $this->load->database('ARI_CONFIRM',true);
   $m = $db->query($query);
   
   return $db->query($query)->result_array();
  }

  function fetchsql()
  {
    
    $query = 'select id, TransactionType, TransID, TransTime, TransAmount, BusinessShortCode, BillRefNumber, InvoiceNumber, OrgAccountBalance, ThirdPartyTransacID, MSISDN, FirstName, MiddleName, LastName, flag';
    $query .= ' from pesa_confirmation';
    
    $db =  $this->load->database('default',true);
    $m = $db->query($query);

    return $db->query($query)->result_array();
  }

  function fetchallpesa()
  {
    
    $query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CONFIRM.OMPS."TransID"';
    $query .= ' from ARI_CONFIRM.OMPS';
    
    $db =  $this->load->database('ARI_CONFIRM',true);
    $m = $db->query($query);

    return $db->query($query)->result_array();
  }
  
  function fetchallocatedniit($stime)
    {
      $status = 'N';
      
      $query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CONF_NIIT.OMPS."TransID","SapDocNum","BPName","DocDate",
      "DocTotal"';
      $query .= ' from ARI_CONF_NIIT.OMPS';
      $query .= ' left outer join ARI_CONF_NIIT.MPS1 on ARI_CONF_NIIT.OMPS."TransID" = ARI_CONF_NIIT.MPS1."TransID"';
      $query .= ' where "TransTime" >=\''. $stime .' 00:00:00\' and  "TransTime" <=\''. $stime  .' 23:59:59\'';
      $query .= ' and  "DocStatus" = \''. $status .'\'';
      
      $db =  $this->load->database('ARI_CONF_NIIT',true);
      $m = $db->query($query);

      return $db->query($query)->result_array();
    }
	
	function fetchunallocatedniit($stime)
  {
    $query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CONF_NIIT.OMPS."TransID"';
    $query .= ' from ARI_CONF_NIIT.OMPS';
    $query .= ' left outer join ARI_CONF_NIIT.MPS1 on ARI_CONF_NIIT.OMPS."TransID" = ARI_CONF_NIIT.MPS1."TransID"';
    $query .= ' where "TransTime" >=\''. $stime .' 00:00:00\' and  "TransTime" <=\''. $stime  .' 23:59:59\'';
    $query .= ' and  "DocStatus" is null';
    
    $db =  $this->load->database('ARI_CONF_NIIT',true);
    $m = $db->query($query);

    return $db->query($query)->result_array();
  }
  
  function fetchunallocatedcumulativeniit($stime)
  {
	  $s30time =  date('Y-m-d', strtotime(("-31 day")));
$e30time =  date('Y-m-d', strtotime(("-1 day")));

$s60time =  date('Y-m-d', strtotime(("-61 day")));
$e60time =  date('Y-m-d', strtotime(("-32 day")));

$s90time =  date('Y-m-d', strtotime(("-91 day")));
$e90time =  date('Y-m-d', strtotime(("-62 day")));

$above90s =  date('Y-m-d', strtotime("2019-01-06"));
$above90 =  date('Y-m-d', strtotime(("-92 day")));


    $query = 'select OMPS."ID", "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CONF_NIIT.OMPS."TransID",';
    $query .= ' case when "TransTime" <=\''. $e30time  .' 23:59:59\' and "TransTime" >=\''. $s30time  .' 00:00:00\' then "TransAmount" end as "thethirty",';
	$query .= ' case when "TransTime" <=\''. $e60time  .' 23:59:59\' and "TransTime" >=\''. $s60time  .' 00:00:00\' then "TransAmount" end as "thesixty",';
	$query .= ' case when "TransTime" <=\''. $e90time  .' 23:59:59\' and "TransTime" >=\''. $s90time  .' 00:00:00\' then "TransAmount" end as "theninety",';
	$query .= ' case when "TransTime" <=\''. $above90  .' 23:59:59\' and "TransTime" >=\''. $above90s  .' 00:00:00\' then "TransAmount" end as "theabove"';
	
	$query .= ' from ARI_CONF_NIIT.OMPS';
    $query .= ' left outer join ARI_CONF_NIIT.MPS1 on ARI_CONF_NIIT.OMPS."TransID" = ARI_CONF_NIIT.MPS1."TransID"';
    $query .= ' where "TransTime" <=\''. $stime  .' 23:59:59\'';
    $query .= ' and  "DocStatus" is null order by "TransTime" desc';
    
    $db =  $this->load->database('ARI_CONF_NIIT',true);
    $m = $db->query($query);

    return $db->query($query)->result_array();
  }
  
  function fetchallocatedpreviousdaysniit($stime)
  {
    $status = 'N';
    
    $query ='select count(*) as count, sum(ARI_CONF_NIIT.OMPS."TransAmount") as received, sum(case when ARI_CONF_NIIT.MPS1."TransID" is null then 0 else ARI_CONF_NIIT.MPS1."DocTotal"  end ) as doct';
    $query .=' from ARI_CONF_NIIT.OMPS';
    $query .=' left outer join ARI_CONF_NIIT.MPS1 on ARI_CONF_NIIT.OMPS."TransID" = ARI_CONF_NIIT.MPS1."TransID"';
    $query .= ' where "TransTime" <=\''. $stime  .' 23:59:59\'';
    $query .= ' and "DocStatus" = \''. $status .'\'';

    $db =  $this->load->database('ARI_CONF_NIIT',true);
    $m = $db->query($query);

    $data = array();
    foreach($db->query($query)->result_array() as $row){
     $k=array();
     foreach($row as $item=>$value){
      array_push($k,$value);
    }
    array_push($data,$k);
  }
  return $data;
  }
  
  function fetchunallocatedprevioudaysniit($stime)
  {
    $stime =  date('Y-m-d', strtotime(("-1 day")));
    $query ='select count(*) as count, sum(ARI_CONF_NIIT.OMPS."TransAmount") as received, sum(case when ARI_CONF_NIIT.MPS1."TransID" is null then 0 else ARI_CONF_NIIT.MPS1."DocTotal"  end ) as doct';
    $query .=' from ARI_CONF_NIIT.OMPS';
    $query .=' left outer join ARI_CONF_NIIT.MPS1 on ARI_CONF_NIIT.OMPS."TransID" = ARI_CONF_NIIT.MPS1."TransID"';
    $query .= ' where "TransTime" <=\''. $stime  .' 23:59:59\'';
    $query .= ' and  "DocStatus" is null';

    $db =  $this->load->database('ARI_CONF_NIIT',true);
    $m = $db->query($query);

    $data = array();
    foreach($db->query($query)->result_array() as $row){
     $k=array();
     foreach($row as $item=>$value){
      array_push($k,$value);
    }
    array_push($data,$k);
  }
  return $data;
  }
  
  function fetchallocatedthismonthniit($stime)
    {
      $status = 'N';
      $query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CONF_NIIT.OMPS."TransID","SapDocNum","BPName","DocDate",
      "DocTotal"';
      $query .= ' from ARI_CONF_NIIT.OMPS';
      $query .= ' left outer join ARI_CONF_NIIT.MPS1 on ARI_CONF_NIIT.OMPS."TransID" = ARI_CONF_NIIT.MPS1."TransID"';
      $query .= ' where "TransTime" >=\''. $stime .' 00:00:00\'';

      $query .= ' and  "DocStatus" = \''. $status .'\'';

    
    $db =  $this->load->database('ARI_CONF_NIIT',true);
    $m = $db->query($query);

      return $db->query($query)->result_array();
    }
	
	function fetchunallocatedthismonthniit($stime)
  {
   $query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CONF_NIIT.OMPS."TransID"';
    $query .= ' from ARI_CONF_NIIT.OMPS';
    $query .= ' left outer join ARI_CONF_NIIT.MPS1 on ARI_CONF_NIIT.OMPS."TransID" = ARI_CONF_NIIT.MPS1."TransID"';
    $query .= ' where "TransTime" >=\''. $stime .' 00:00:00\'';

    $query .= ' and  "DocStatus" is null';
    
    $db =  $this->load->database('ARI_CONF_NIIT',true);
    $m = $db->query($query);

    return $db->query($query)->result_array();
  }
  
  function fetchallocatedpreviousmonthniit($stime)
  {
    $status = 'N';
    
    $query ='select count(*) as count, sum(ARI_CONF_NIIT.OMPS."TransAmount") as received, sum(case when ARI_CONF_NIIT.MPS1."TransID" is null then 0 else ARI_CONF_NIIT.MPS1."DocTotal"  end ) as doct';
    $query .=' from ARI_CONF_NIIT.OMPS';
    $query .=' left outer join ARI_CONF_NIIT.MPS1 on ARI_CONF_NIIT.OMPS."TransID" = ARI_CONF_NIIT.MPS1."TransID"';
    $query .= ' where "TransTime" <=\''. $stime  .' 23:59:59\'';
    $query .= ' and "DocStatus" = \''. $status .'\'';

    
    $db =  $this->load->database('ARI_CONF_NIIT',true);
    $m = $db->query($query);

    $data = array();
    foreach($db->query($query)->result_array() as $row){
     $k=array();
     foreach($row as $item=>$value){
      array_push($k,$value);
    }
    array_push($data,$k);
  }
  return $data;
  }
  
  function fetchunallocatedpreviousmonthniit($stime)
  {
    $stime =  date('Y-m-d',strtotime("last day of previous month"));
    $query ='select count(*) as count, sum(ARI_CONF_NIIT.OMPS."TransAmount") as received, sum(case when ARI_CONF_NIIT.MPS1."TransID" is null then 0 else ARI_CONF_NIIT.MPS1."DocTotal"  end ) as doct';
     $query .=' from ARI_CONF_NIIT.OMPS';
    $query .=' left outer join ARI_CONF_NIIT.MPS1 on ARI_CONF_NIIT.OMPS."TransID" = ARI_CONF_NIIT.MPS1."TransID"';
    $query .= ' where "TransTime" <=\''. $stime  .' 23:59:59\'';
    $query .= ' and  "DocStatus" is null';

    $db =  $this->load->database('ARI_CONF_NIIT',true);
    $m = $db->query($query);

    $data = array();
    foreach($db->query($query)->result_array() as $row){
     $k=array();
     foreach($row as $item=>$value){
      array_push($k,$value);
    }
    array_push($data,$k);
  }
  return $data;
  }
  
  function fetchunallocatedniitthirty($stime, $etime)
  {
    $query = 'select "ContactName" ,"MSISDN","TransAmount","TransTime","Balance",ARI_CONF_NIIT.OMPS."TransID"';
    $query .= ' from ARI_CONF_NIIT.OMPS';
    $query .= ' left outer join ARI_CONF_NIIT.MPS1 on ARI_CONF_NIIT.OMPS."TransID" = ARI_CONF_NIIT.MPS1."TransID"';
    $query .= ' where "TransTime" >=\''. $stime .' 00:00:00\' and  "TransTime" <=\''. $etime  .' 23:59:59\'';
    $query .= ' and  "DocStatus" is null';
    
    $db =  $this->load->database('ARI_CONF_NIIT',true);
    $m = $db->query($query);

    return $db->query($query)->result_array();
  }
  }