<?php
class Tills extends CI_Model
{
    public $shortCode="";
    public $apiKey="";
    public $branch="";
    public $apiSecret="";
    public $enabled="";
    public $deleted ="";
    public $added_by ="";

    public function __construct()
    {
        parent::__construct();
    }

    //Get the details from the table tblTills
    public function get($condition="")
    {
        $this->db->order_by('id', 'asc');
        if (!empty($condition)) {
            $this->db->where($condition);
        }
        return $this->db->get('tblTills');
    }

    //Get the cred details for listing at the card
    public function getlist($condition = '')
    {
        $this->db->select('*, tblTills.id as credid');
        $this->db->from('tblTills');
        $this->db->join('tblUser', 'tblUser.id=tblTills.added_by','inner');
        if (!empty($condition)) {
            $this->db->where($condition);
        }

        return $this->db->get()->result();
    }

    //Insert into the tblTills
    public function insert($data)
    {
        return $this->db->insert('tblTills', $data);
    }

    //Update the values in the database
    public function update($data, $condition, $auditdata='')
    {
        $this->db->trans_start();

        $this->db->where($condition);
        $this->db->update('tblTills', $data);

        if (!empty($auditdata)) {
          $this->db->insert('tblEditLog', $auditdata);
      }
      $this->db->trans_complete();

      if ($this->db->trans_status() === false) {
        return 0;
    } elseif ($this->db->trans_status() === true) {
        return 1;
    }
}

    //delete the values from the database
public function delete($data, $condition)
{
    $this->db->trans_start();

    $this->db->where($condition);
    $this->db->update('tblTills', $data);

    $this->db->trans_complete();

    if ($this->db->trans_status() === false) {
        return 0;
    } elseif ($this->db->trans_status() === true) {
        return 1;
    }
}
}
