<?php
class Tokenmodel extends CI_Model {

    var $token="";
    var $userid="";
    var $username="";
    var $createdon;
	var $lastactive;
	var $isvalid=0;

    function __construct()
    {
        parent::__construct();
    }

    function get($condition="")
    {
        $this->db->order_by('id','desc');
        if(!empty($condition))
           $this->db->where($condition);
        return $this->db->get('tokens');
    }

    function update($data,$condition)
    {
        return $this->db->update('tokens',$data,$condition);
    }

    function insert($token,$username)
    {
        $this->token = $token;
        $this->userid = base64_encode($username);
        $this->username = $username;
        $this->createdon = date("Y-m-d H:i:s");
		$this->lastactive = date("Y-m-d H:i:s");
        $this->isvalid = 1;
        return $this->db->insert('tokens', $this);
    }

}