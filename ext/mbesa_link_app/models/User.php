<?php

class User extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

    //Get the user details meeting the condition
	public function confirmifexist($condition = '')
	{
		$this->db->select('*');
		$this->db->from('tblUser');
		$this->db->join('tblLogin', 'tblLogin.user_id=tblUser.id');
		if (!empty($condition)) {
			$this->db->where($condition);
		}
		return $this->db->get();
	}

    //Get the user details(system) for listing at the card
	public function getuserlist($condition = '')
	{
		$this->db->select('*, tblUser.id as userid');
		$this->db->from('tblUser');
		$this->db->join('tblLogin', 'tblLogin.user_id=tblUser.id');
		if (!empty($condition)) {
			$this->db->where($condition);
		}
		return $this->db->get()->result();
	}

    //Update the login table for logs
	public function update($data, $condition)
	{
		$this->db->trans_start();

		$this->db->where($condition);
		$this->db->update('tblLogin', $data);

		$this->db->trans_complete();

		if ($this->db->trans_status() === false) {
			return 0;
		} elseif ($this->db->trans_status() === true) {
			return 1;
		}
	}

    //Update the tblUser table for logs
	public function updateuser($data, $condition, $auditdata = '')
	{
		$this->db->trans_start();

		$this->db->where($condition);
		$this->db->update('tblUser', $data);

		if (!empty($auditdata)) {
			$this->db->insert('tblEditLog', $auditdata);
		}

		$this->db->trans_complete();

		if ($this->db->trans_status() === false) {
			return 0;
		} elseif ($this->db->trans_status() === true) {
			return 1;
		}
	}

    //Insert user into the system
	public function insert($userdata, $audit)
	{
        //starting transaction
		$this->db->trans_begin();

		$this->db->insert('tblUser', $userdata);
		$insert_id = $this->db->insert_id();

		$post_data = array('user_id' => $insert_id);
		$usable_insert = $insert_id;
		$this->db->insert('tblLogin', $post_data);

		$this->db->insert('tblRegistrationLog', $audit);

        //Transaction completed
		$this->db->trans_complete();

		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			return 0;
		} elseif ($this->db->trans_status() === true) {
			$this->db->trans_commit();
			return $usable_insert;
		}
	}
	
	public function updatereportusers($data,$condition){
		return $this->db->update('tblreportemails',$data,$condition);
	}
	
  //Get the report user details
	public function confirmifexistreportusers($condition = '')
	{
		$this->db->select('*');
		$this->db->from('tblreportemails');
		if (!empty($condition)) {
			$this->db->where($condition);
		}
		return $this->db->get();
	}
	
   //Insert report user into the system
	public function insertreportusers($userdata)
	{
        //starting transaction
		$this->db->trans_begin();

		$insert = $this->db->insert('tblreportemails', $userdata);
        //$insert = $this->db->insert_id();

        //Transaction completed
		$this->db->trans_complete();

		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			return 0;
		} elseif ($this->db->trans_status() === true) {
			$this->db->trans_commit();
			return $insert;
		}
	}
	
  //Get the emails to send the reports
	public function getreportusers($condition = '')
	{
		$this->db->select('*, tblreportemails.id as reportemailid, tblReportGrant.id as reportgrantid, tblcredentials.id as credentialid');
		$this->db->from('tblReportGrant');
		$this->db->join('tblreportemails', 'tblreportemails.id=tblReportGrant.user_id','inner');
		$this->db->join('tblcredentials', 'tblcredentials.id=tblReportGrant.creadential_id','inner');
		if (!empty($condition)) {
			$this->db->where($condition);
		}

		return $this->db->get()->result_array();
	}
	
  //Insert user into the system
	public function insertgrants($data)
	{
	  //starting transaction
		$this->db->trans_begin();

		$this->db->insert('tblportalgrants', $data);

        //Transaction completed
		$this->db->trans_complete();

		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			return 0;
		} elseif ($this->db->trans_status() === true) {
			$this->db->trans_commit();
			return 1;
		}
	}
	
	public function getgrants($condition="")
	{
		$this->db->select('*');
		$this->db->from('tblportalgrants');
		if (!empty($condition)) {
			$this->db->where($condition);
		}
		return $this->db->get();
	}
	
  //get the details about the report granted users
	public function getreportreceipients($condition="")
	{
		$this->db->select('*');
		$this->db->from('tblReportGrant');
		if (!empty($condition)) {
			$this->db->where($condition);
		}
		return $this->db->get();
	}
	
  //the report receipients for listing
	public function getreceipientslisting($condition="")
	{
		$this->db->select('*, tblreportemails.id as reportemailid, tblReportGrant.id as reportgrantid, tblcredentials.id as credentialid');
		$this->db->from('tblReportGrant');
		$this->db->join('tblreportemails', 'tblreportemails.id=tblReportGrant.user_id','inner');
		$this->db->join('tblcredentials', 'tblcredentials.id=tblReportGrant.creadential_id','inner');
		if (!empty($condition)) {
			$this->db->where($condition);
		}
		$this->db->order_by('branch');
		return $this->db->get()->result();
	}
	
	
	//List the portals accessed by users
	public function getportalslisting($condition="")
	{
		$this->db->select('*, users.tid as usertableid, tblportalgrants.id as portalgrantid, tblcredentials.id as credentialid');
		$this->db->from('tblportalgrants');
		$this->db->join('users', 'users.tid=tblportalgrants.user_id','inner');
		$this->db->join('tblcredentials', 'tblcredentials.id=tblportalgrants.creadential_id','inner');
		if (!empty($condition)) {
			$this->db->where($condition);
		}
		$this->db->order_by('branch');
		return $this->db->get()->result();
	}
	
	
  //assign the users reports
	public function insertreportreceipients($data)
	{
	  //starting transaction
		$this->db->trans_begin();

		$this->db->insert('tblReportGrant', $data);

        //Transaction completed
		$this->db->trans_complete();

		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback();
			return 0;
		} elseif ($this->db->trans_status() === true) {
			$this->db->trans_commit();
			return 1;
		}
	}
	
  //delete the email receipients
	public function deletereportreceipients($data)
	{
		$this->db->delete('tblReportGrant', $data);
		return $this->db->affected_rows();
	}
	
	//delete the user from accessing the portal
	public function deleteportalaccess($data)
	{
		$this->db->delete('tblportalgrants', $data);
		return $this->db->affected_rows();
	}
}
