<?php
class Pesa_model extends CI_Model {
	
	var $TransactionType="";
	var $TransID="";
	var $TransTime;
	var $TransAmount;
	var $BusinessShortCode="";
	var $BillRefNumber="";
	var $InvoiceNumber="";
	var $OrgAccountBalance;
	var $ThirdPartyTransacID="";
	var $MSISDN="";
	var $FirstName="";
	var $MiddleName="";
	var $LastName="";
	var $flag=0;
 
	public function get($condition="")
	{ 
		if(!empty($condition))
			$this->db->where($condition);
		return $this->db->get("pesa_confirmation");
	}
	
	public function update($data,$where){
		return $this->db->update("pesa_confirmation",$data,$where);
	}
 
	public function insert($obj)
	{
		$bal = $obj->OrgAccountBalance;
		$year = substr($obj->TransTime,0,4);
		$month = substr($obj->TransTime,4,2);
		$day = substr($obj->TransTime,6,2); 
		$hour = substr($obj->TransTime,8,2);
		$min = substr($obj->TransTime,10,2);
		$sec = substr($obj->TransTime,12,2);
		$mdate = $year ."-". $month ."-". $day ." ". $hour .":".$min .":". $sec;
		$this->TransactionType=$obj->TransactionType;
		$this->TransID=$obj->TransID;
		$this->TransTime=$mdate;
		$this->BusinessShortCode=$obj->BusinessShortCode;
		$this->BillRefNumber=$obj->BillRefNumber;
		$this->ThirdPartyTransacID=$obj->ThirdPartyTransID;
		$this->MSISDN=$obj->MSISDN;
		$this->TransAmount=$obj->TransAmount;
		$this->FirstName=$obj->FirstName;
		$this->MiddleName=$obj->MiddleName;
		$this->InvoiceNumber=$obj->InvoiceNumber;
		$this->OrgAccountBalance= empty($bal)?0:$bal;
		$this->LastName=$obj->LastName;
		$this->flag=0;
		return $this->db->insert("pesa_confirmation",$this);
	}
}