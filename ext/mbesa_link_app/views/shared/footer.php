 </div><!-- /.content-wrapper -->
      <footer class="main-footer  no-print">
           &copy; <?=date('Y');?> All Rights Reserved,&nbsp;&nbsp;&nbsp;&nbsp;Powered by ARI Ltd 
      </footer>

    </div> 

    <script language="javascript">
        function mysession() {
            $.ajax({
                   type: "get",
                   url: '/m_link/checksesssion',
                   success: function (data) {
                       var d = JSON.parse(data);
                       if(String(d.state) =="timeout")
                            window.location.href="<?=base_url();?>users/timeout";
                   },
                   error: function (data) {
                   }
            });
            var timeout = setTimeout(mysession, 60000);
        }

        function mynotifications() {
            $.ajax({
                type: "get",
                url: '/m_link/notifications',
                success: function (data) {
                    
                    var json = JSON.parse(data);
                    if(json.result=="ok"){
                        var mdata = json.message;
                        $("#mtext").html(" "+ mdata.message +' <button type="button" id="mclose" onclick="dismiss()" class="mclose pull-right" data-dismiss="alert" aria-label="Close" style="background: #000; border: none; margin-right: 5px">X</button>');
                        $("#myPopup").removeClass("hide");
                        $("#myPopup").addClass("show");
                    }
                }
            });
            var timeout = setTimeout(mynotifications, 15000);
        }

        function dismiss(){
            $("#myPopup").removeClass("show");
            $("#myPopup").addClass("hide");
        }
		
	

        $(document).ready(function(){
            mysession();
            mynotifications();
            $('.sidebar-menu li').removeClass('active');
            $('.<?=$this->uri->segment(3);?>').addClass('active');

            $(".sidebar").slimScroll({destroy: true}).height("auto");
            //Add slimscroll
            $(".sidebar").slimscroll({
                height: ($(window).height() - $(".main-header").height()) + "px",
                color: "#ff0000",
                alwaysVisible: false,
                size: "20px"
            });
        });

    </script>
    
    <script src="/_public/js/app.min.js" type="text/javascript"></script>
	<script src="/_public/js/moment.js" type="text/javascript"></script>
    <script src="/_public/js/datepicker.js" type="text/javascript"></script>

    <script type="text/javascript" src="/_public/js/Chart.bundle.min.js"></script>
    <script type="text/javascript" src="/_public/js/utils.js" ></script>
    <script type="text/javascript" src="/_public/js/select.js" ></script>
    <script type="text/javascript" src="/_public/js/jquery.easy-autocomplete.min.js"></script>
    <script type="text/javascript" src="/_public/js/jquery.slimscroll.min.js"></script>

    <script type="text/javascript" src="/_public/js/datatables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="/_public/js/datatables/media/js/dataTables.semanticui.min.js"></script>
    <script type="text/javascript" src="/_public/js/datatables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="/_public/js/datatables/extensions/FixedHeader/js/dataTables.fixedHeader.min.js"></script>
    <script type="text/javascript" src="/_public/js/datatables/extensions/Select/js/dataTables.select.min.js"></script>

    <script type="text/javascript" src="/_public/js/datatables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="/_public/js/datatables/extensions/Buttons/js/buttons.semanticui.min.js"></script>

    <script type="text/javascript" src="/_public/js/datatables/extensions/Buttons/js/jszip.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script> 
    <script type="text/javascript" src="/_public/js/datatables/extensions/Buttons/js/vfs_fonts.js"></script>

    <script type="text/javascript" src="/_public/js/datatables/extensions/Buttons/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="/_public/js/datatables/extensions/Buttons/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="/_public/js/datatables/extensions/Buttons/js/buttons.colVis.min.js"></script>
    <script type="text/javascript" src="/_public/js/datatables/media/js/semanticui.js"></script>
</body>
</html>