<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?=APP_NAME;?> : M-pesa Integration</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="" />
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    
    <link href="/_public/images/logo.png" rel="shortcut icon">
    <link href="/_public/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
    <link href="/_public/css/font-awesome.css" rel="stylesheet">
    <link href="/_public/css/datepicker3.css" rel="stylesheet" type="text/css" />
    <link href="/_public/css/select.css" rel="stylesheet" type="text/css" /> 
    
    <link href="/_public/css/main.css" rel="stylesheet" type="text/css" />
    <link href="/_public/css/skins/skin.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="/_public/js/datatables/media/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="/_public/js/datatables/media/css/dataTables.semanticui.min.css">
    <link rel="stylesheet" type="text/css" href="/_public/js/datatables/media/css/semantic.min.css">
    <link rel="stylesheet" type="text/css" href="/_public/js/datatables/extensions/Responsive/css/responsive.dataTables.min.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/_public/js/datatables/extensions/FixedHeader/css/fixedHeader.semanticui.min.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/_public/js/datatables/extensions/Select/css/select.semanticui.min.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/_public/js/datatables/extensions/Buttons/css/buttons.semanticui.min.css" media="screen">

    <script src="/_public/js/jquery.min.js"></script>
    <script src="/_public/js/bootstrap.min.js" type="text/javascript"></script>
    
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="promotions-dark sidebar-mini fixed layout-boxed">
    <div class="wrapper">

      <header class="main-header  no-print">
        <a href="#" class="logo">
          <span class="logo-mini"><img src="/_public/images/logo.png" style="height:30px !important">&nbsp;&nbsp;</span>
          <span class="logo-lg"><img src="/_public/images/logo.png" style="height:55px !important" ></span>
      </a>

      <nav class="navbar fixed-top navbar-static-top no-print" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
      </a>
      <?php 
            /*
            $this->session->set_userdata('selectdid', $id);
            $this->session->set_userdata('selectdcode', $shortcode);*/
            
            if($this->session->userdata('selectdbranch') != null)
            {
                $selected = $this->session->userdata('selectdbranch');
            }else
            {
			   $theuser = $this->session->userdata('userid');
               $result = $this->commonmodel->get_branches(array('tblportalgrants.user_id'=>$theuser), 'branch');
               if($result->num_rows()>0){
                  $row=$result->row();
                  $selected = $row->branch;
                  
                  $id = $row->id;
                  $shortcode = $row->shortCode;
                  $branch = $row->branch;
                  
                  $this->session->set_userdata('selectdid', $id);
                  $this->session->set_userdata('selectdcode', $shortcode);
                  $this->session->set_userdata('selectdbranch', $branch);
              }
          }
          ?>
          <span class="hidden-xs" style="color:white; position: absolute; left: 50%; top: 30%; transform: translatex(-50%);"><h4><?=$selected;?></h4></span>
          
          <div class="row pull-right" style="margin-right: 10px">
              <?php $mname = $this->session->userdata('names');?>
              <div class="col-md-12 navbar-custom-menu">

                <ul class="nav navbar-nav">
                  <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      <span class="hidden-xs"><h4><?=ucfirst(substr($mname,0,1));?></h4></span>
                  </a>
                  <ul class="dropdown-menu">
                      <!-- User image -->
                      <li class="user-header">
                        <p>
                          <?=$this->session->userdata('names');?> :  <?=$this->session->userdata('role');?>
                      </p>
                  </li>
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?=APP_BASE;?>/changepassword" class="btn btn-primary btn-flat">Change Password</a>
                  </div>
                  <div class="pull-right">
                      <a href="/users/terminateaccess" class="btn btn-danger btn-flat">Sign out</a>
                  </div>
              </li>
          </ul>
      </li>

  </ul>
</div>
</div>

<div class="row pull-right" style="margin-right: 10px; right:0;">
  <?php $mname = $this->session->userdata('names');?>
  <div class="col-md-12 navbar-custom-menu">

    <ul class="nav navbar-nav">
      <li class="dropdown user user-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <span class="hidden-xs"><h4><i class="fa fa-ellipsis-v"></i></h4></span>
      </a>
      
      <ul class="dropdown-menu"  style="right: 0; left: auto;">
        <?php
        $menus = array();
		$theuser = $this->session->userdata('userid');
        $menus = $this->commonmodel->get_branches(array('tblportalgrants.user_id'=>$theuser), 'branch');
              
       // $menus = $this->commonmodel->get_branches('', 'branch');
        
        foreach($menus->result_array() as $subitem){
            $cred_id = strtr(base64_encode($subitem['id']), '+/=', '._-');
            $shortcode = strtr(base64_encode($subitem['shortCode']), '+/=', '._-');
            $branch = strtr(base64_encode($subitem['branch']), '+/=', '._-');
            ?>
            <li><a href="<?php echo site_url('details/currentPayBill/'.$cred_id.'/'.$shortcode.'/'.$branch); ?>"><?=$subitem['branch'];?></a></li>
            <?php
        }
        ?>
    </ul>
</li>

</ul>
</div>
</div>



</nav>
</header>
<aside class="main-sidebar">
    <section class="sidebar">
       <?php
       $class="";
       $seg = $this->uri->segment(3);
       ?> 

       <ul class="sidebar-menu">
        <?php
        $menus = array();
        if($role == "Admin")
          $menus = $this->commonmodel->get_menu(array('parentid'=>0,'adminallowed'=>1), 'menuorder');
      else if($role  == "Normal")
          $menus = $this->commonmodel->get_menu(array('parentid'=>0,'normalallowed'=>1), 'menuorder');
      else
          $menus = $this->commonmodel->get_menu(array('parentid'=>0,'reconcilerallowed'=>1), 'menuorder');
      
      foreach($menus->result_array() as $item){
          if($item['candisplay']==1){
              if($item['haschildren']==0){
                if($item['class']=="header") { ?>
                    <li class="header"><center><?=$item['menu'];?></center></li>
                <?php } else { ?>
                  <li class="<?=$item['class'];?>"><a href="<?=strpos($item['link'],'://')>0? $item['link'] :  APP_BASE . $item['link'] ;?>" ><i class="<?=$item['icon'];?>"></i> <span><?=$item['menu'];?></span> </a></li>
                  <?php
              } 
          } else {
            if($role == "Admin")
                $submenus = $this->commonmodel->get_menu(array('parentid'=>$item['id'],'adminallowed'=>1), 'menuorder');
            else if($role  == "Normal")
                $submenus = $this->commonmodel->get_menu(array('parentid'=>$item['id'],'normalallowed'=>1), 'menuorder');
            else
                $submenus = $this->commonmodel->get_menu(array('parentid'=>$item['id'],'reconcilerallowed'=>1), 'menuorder');
            
            ?>
            <li class="treeview">
                <a href="#"><i class="<?=$item['icon'];?>"></i> <span><?=$item['menu'];?></span><i class="fa fa-angle-down pull-right"></i></a>
                <ul class="treeview-menu">
                  <?php
                  foreach($submenus->result_array() as $subitem){
                      ?>
                      <li class="<?=$subitem['class'];?>"><a href="<?=strpos($subitem['link'],'://')>0? $subitem['link'] :  APP_BASE  . $subitem['link'] ;?>" ><i class="<?=$subitem['icon'];?>"></i> <span><?=$subitem['menu'];?></span> </a></li>
                      <?php
                  }
                  ?>
              </ul>
          </li>
          <?php
      }
  }
}
?>
<li><a href="/users/terminateaccess"> <i class="fa fa-lock"></i> <span>Log Out</span> </a></li>
</ul>
</section>
</aside>

<div class="content-wrapper no-print" style=" padding-left: 10px; padding-right: 10px; min-height: 600px">
  
  

  <?php
  $tempdata = $this->session->flashdata('tempdata');
  $alert="success";
  if(strpos(strtoupper($tempdata),"SORRY")>-1 ||  strpos(strtoupper($tempdata),"ERROR")>-1)
    $alert="danger";
if(isset($tempdata) && !empty($tempdata)){
    echo "<br>";
    ?>
    <div class="alert alert-<?=$alert;?> alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <b class="text-center"> <?=$tempdata; ?></b>
  </div>
<?php   }    ?>

<div class="popup" style="z-index: 5000;">
    <span class="popuptext" id="myPopup"><b id="mtext"></b> </span>
</div>
