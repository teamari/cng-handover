<!--  Begin Page Content -->
<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Registration Log</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
      <table class="ui celled table stripe" width="100%" id="datatable">
          <thead>
              <th>Registered By</th>
              <th>User Code</th>
              <th>Date</th>
              <th>Status</th>
              <th>Description</th>
              <th>IP Address</th>
              <th>Headers</th>
          </table>
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid -->

    <script type="text/javascript">
        var chart1;
        function noo(val){
           try{
             return parseFloat(val).toLocaleString();
         }catch(e){
             return val;
         }
     }
     function intVal(i) {
        return typeof i === 'string' ?
        i.replace(/[\$,]/g, '') * 1 :
        typeof i === 'number' ?
        i : 0;
    }

    $(document).ready(function() {


          var table = $('#datatable').DataTable({
              destroy: true,
              responsive: false,
              "ajax": {
                  url: '<?php echo base_url('trails/getregtrail') ?>',
                  type: 'get',
                  data: {}
              },
              "order": [
              [0, "asc"]
              ],
              select: {
                  style: 'single'
              },
              language: {
                  searchPlaceholder: "Search records.."
              },
              scrollY: "800px",
              scrollX: true,
              scrollCollapse: true,
              paging: true,
              fixedColumns: true,
              lengthChange: true,
              buttons: ['excelHtml5', 'pdfHtml5', 'colvis']
      });

          var buttons = new $.fn.dataTable.Buttons(table, {
              buttons: ['excelHtml5', 'pdfHtml5', 'colvis']
          });
          buttons.container().appendTo($('div.right.aligned.eight.column:eq(0)', table.table().container()));
    });
</script>
