<div class="table-responsive">
    <table class="table table-hover tablesorter">
        <thead>
			<tr>
                <th class="header">TransID</th>
                <th class="header">TransTime</th>                           
                <th class="header">TransAmount</th>                      
                <th class="header">BusinessShortCode</th>
                <th class="header">MSISDN</th>                         
                <th class="header">ContactName</th>                      
                <th class="header">Balance</th>
                <th class="header">flag</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (isset($insertinfo) && !empty($insertinfo)) {
                foreach ($insertinfo as $key => $element) {
                    ?>
                    <tr>
                        <td><?php echo $element['TransID']; ?></td>   
                        <td><?php echo $element['TransTime']; ?></td> 
                        <td><?php echo $element['TransAmount']; ?></td>                       
                        <td><?php echo $element['BusinessShortCode']; ?></td>
                        <td><?php echo $element['MSISDN']; ?></td>
                        <td><?php echo $element['ContactName']; ?></td>                       
                        <td><?php echo $element['Balance']; ?></td>
                        <td><?php echo $element['flag']; ?></td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="5">There is no Inserted data.</td>    
                </tr>
            <?php } ?>

        </tbody>
    </table>
</div>