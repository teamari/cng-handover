<div class="row">
<div class="col-md-6 col-md-offset-3" style="background: #fff; border-radius: 5px; color: #000; opacity: 6.5; z-index: 999;"> 
<center><img src="/_public/images/logo.png"  style="height: 120px" ></center>
	<?php
                $tempdata = $this->session->flashdata('tempdata');
                $alert="info";
                    if(strpos(strtoupper($tempdata),"SORRY")>-1 )
                        $alert="danger";
                if(isset($tempdata) && !empty($tempdata)){
          ?>
          <br>
                    <div class="alert alert-<?=$alert;?> alert-dismissible" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     <h5 class="text-center"> <?=$tempdata; ?></h5>
                    </div>
           <?php   }    ?>

	<?=form_open("/users/login"); ?>
	
		<input class="form-control" type="email" name="username"  placeholder="Username"  required="required"  />
		<br>
		<input  class="form-control" type="password" autocomplete="off" placeholder="Password" required="required" name="password" />
		<br>
		<div class="row">
			<div class="col-md-4"> 	
				<img src="/home/getCaptcha/?<?=urlencode(microtime());?>" style="border-radius: 5px" id="captcha_image" alt="Captcha" /><br><br>
			</div> 

			<div class="col-md-5">
				<a href="javascript:void(0);" class="btn btn-default btn-sm" style="text-decoration: none; margin-top: 15px" onclick="document.getElementById('captcha_image').src='/home/getCaptcha/?<?=urlencode(microtime());?>'"><h5><i class="fa fa-refresh"></i> Refresh image</h5></a>
			</div>
		</div>
		<input type="text" autocomplete="off" name="userCaptcha"  id="user_Captcha" class="form-control"   placeholder="Enter the image text shown" required="required"/>
		<br>	
		<div class="row">
			<div class="col-md-6"><button class="btn btn-danger col-md-12" style="background: #0066b2; border:none" type="submit" >Log In</button></div>
			<div class="col-md-6"><a class="btn btn-default col-md-12" style="text-decoration: none" href="/home/forgot_password"> Forgot your password?</a></div>
		</div>
	</form><br>
</div> 
</div> 

<script src="/_public/js/landing.page.skin.js" type="text/javascript"></script> 
