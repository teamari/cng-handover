<section class="register-page">
    <div class="register-box">

      <div class="col-md-6 col-md-offset-3" style="background: #fff; border-radius: 5px; color: #000; opacity: 10; z-index: 999;">
        <h3 class="text-center">Request Password Reset</h3><br>
        
        <?php
                $tempdata = $this->session->flashdata('tempdata');
                $alert="info";
                    if(strpos(strtoupper($tempdata),"SORRY")>-1 )
                        $alert="danger";
                if(isset($tempdata) && !empty($tempdata)){
          ?>
          <br>
                    <div class="alert alert-<?=$alert;?> alert-dismissible" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     <h5 class="text-center"> <?=$tempdata; ?></h5>
                    </div>
           <?php   }    ?>

        <p style="line-height: 30px">Ensure you have provide the correct details below.</p>
        <br>
        <?=form_open("/users/reset_password");?>
          <input type="email" class="form-control" required name="email" required  title="Email Format: email@domain.ext"  placeholder="Email"/>
          <br>
		  <input type="text" class="form-control" required name="reset" required  title="Enter the sent reset code"  placeholder="Reset Code"/>
          <br>
          <input type="password" id="pass" name="password" class="form-control" required pattern="^.{6,}" title="Password should be atleast 6 characters "  placeholder="Desired Password"/>
          <br>
          <input type="password" id="cpass" class="form-control" name="cpassword" required pattern="^.{6,}" title="Password should be atleast 6 characters "  placeholder="Retype password"/>
          <br>
          <label id="label" class="label label-danger"></label>
          <br><button type="submit" class="btn btn-danger" style="background: #0066b2; border: none">Reset Password</button>
          <a href="/" class="btn btn-warning pull-right" style="text-decoration: none"><i class="fa fa-arrow-left"></i>  Back to Login</a>
          <br><br>
        </form>
      </div>
    </div>
</section>

<script type="text/javascript">
    $(function(){
       $('#pass, #cpass').change(function(){
            var pass = $('#pass').val();
            var cpass = $('#cpass').val();
            if(pass!="" && cpass != ""){
                if(pass!=cpass){
                    $('#cpass').val('');
                    $('#label').html("Password and Confirmation passwords do not match<br>");
                } else
                   $('#label').html("");
            }
       });
    })
</script>

<script src="/_public/js/landing.page.skin.js" type="text/javascript"></script> 