<section class="register-page">
    <div class="register-box">

      <div class="col-md-6 col-md-offset-3" style="background: #fff; border-radius: 5px; color: #000; opacity: 10; z-index: 999;">
        <h3 class="text-center">Request Password Reset</h3><br>
        
        <?php
                $tempdata = $this->session->flashdata('tempdata');
                $alert="info";
                    if(strpos(strtoupper($tempdata),"SORRY")>-1 )
                        $alert="danger";
                if(isset($tempdata) && !empty($tempdata)){
          ?>
          <br>
                    <div class="alert alert-<?=$alert;?> alert-dismissible" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     <h5 class="text-center"> <?=$tempdata; ?></h5>
                    </div>
           <?php   }    ?>

        <p style="line-height: 30px">A reset OTP will be sent to your phone after this.</p>
        <br>
        <?=form_open("/users/forgot_password");?>
          <input type="email" class="form-control" required name="email" required  title="Email Format: email@domain.ext"  placeholder="Email"/>
          <!--<br>
		  <input type="text" class="form-control" required name="phone" required  title="Enter your phone number"  placeholder="07********"/>
          <br> -->
         
          <br><button type="submit" class="btn btn-danger" style="background: #0066b2; border: none">Request Reset</button>
          <a href="/" class="btn btn-warning pull-right" style="text-decoration: none"><i class="fa fa-arrow-left"></i>  Back to Login</a>
          <br><br>
        </form>
      </div>
    </div>
</section>

<script src="/_public/js/landing.page.skin.js" type="text/javascript"></script> 