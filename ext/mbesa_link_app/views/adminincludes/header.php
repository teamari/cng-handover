<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<title><?=APP_NAME;?> : Admin</title>
	<!-- Custom fonts for this template-->
	<link href="<?php echo base_url('assets/vendor/fontawesome-free/css/all.min.css') ?>"
	rel="stylesheet" type="text/css">
	<link
	href="<?php echo base_url('assets/css/thefont.css') ?>"
	rel="stylesheet">

	<!-- Custom styles for this template-->
	<link href="<?php echo base_url('assets/css/sb-admin-2.min.css') ?>" rel="stylesheet">

	<!-- Custom styles for this page -->
  <link href="<?php echo base_url('assets/vendor/datatables/dataTables.bootstrap4.min.css') ?>" rel="stylesheet">

	<script src="<?php echo base_url('public/js/jquery.min.js') ?>"></script>
  <script src="<?php echo base_url('public/js/bootstrap.min.js') ?>" type="text/javascript"></script>
</head>
<body id="page-top">
	<!-- Page Wrapper -->
<div id="wrapper">
