<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
	<!-- Sidebar - Brand -->
	<a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo base_url('dashboard/dashboard/') ?>">
		<div class="sidebar-brand-icon rotate-n-15">
			<!-- <i class="fas fa-laugh-wink"></i> -->
		</div>
		<div class="sidebar-brand-text mx-3">C & G</div>
	</a>
	<!-- Divider -->
	<hr class="sidebar-divider my-0">
	<!-- Nav Item - Dashboard -->
	<li class="nav-item active">
		<a class="nav-link" href="<?php echo base_url('dashboard/dashboard/') ?>">
			<i class="fas fa-fw fa-tachometer-alt"></i>
			<span>Dashboard</span></a>
	</li>
	<!-- Divider -->
	<hr class="sidebar-divider">
	<!-- Nav Item - PayBills -->
	<li class="nav-item">
		<a class="nav-link" href="<?php echo site_url('dashboard/pay_bill/') ?>">
			<i class="fas fa-fw fa-table"></i>
			<span>Short Codes</span></a>
	</li>
	<!-- Divider -->
	<hr class="sidebar-divider d-none d-md-block">
	<!-- Nav Item - Till No. -->
	<li class="nav-item">
		<a class="nav-link" href="<?php echo site_url('dashboard/users/') ?>">
			<i class="fas fa-fw fa-users"></i>
			<span>Users</span></a>
	</li>
	<hr class="sidebar-divider d-none d-md-block">

	<!-- Nav Item - Reports. -->
	<li class="nav-item">
		<a class="nav-link collapsed" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
			<i class="fas fa-fw fa-table"></i>
			<span>Reports</span>
		</a>
		<div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
			<div class="bg-white py-2 collapse-inner rounded">
				<h6 class="collapse-header">Items:</h6>
				<a class="collapse-item" href="<?php echo site_url('report/users/') ?>">Users</a>
				<a class="collapse-item" href="<?php echo site_url('report/bills/') ?>">Short Codes</a>
				<h6 class="collapse-header">Trails:</h6>
				<a class="collapse-item" href="<?php echo site_url('report/register/') ?>">Registration</a>
			</div>
		</div>
	</li>
	<li class="nav-item active">
		<a class="nav-link" href="<?php echo APP_BASE .'dashboard/' ?>">
			<i class="fas fa-fw fa-arrow-left"></i>
			<span>MLink</span></a>
	</li>


	<!-- Sidebar Toggler (Sidebar) -->
	<div class="text-center d-none d-md-inline">
		<button class="rounded-circle border-0" id="sidebarToggle"></button>
	</div>

</ul>
<!-- End of Sidebar -->