<!DOCTYPE html>
<html lang="en">
    <head>
    <meta charset="utf-8" />
    <title><?=APP_NAME;?> : M-pesa Integration</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <link href="<?php echo base_url(); ?>public/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>public/css/landing.page.skin.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>public/css/font-awesome.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>public/images/logo.png" rel="shortcut icon">

	<script src="<?php echo base_url(); ?>public/js/jquery.min.js" type="text/javascript"></script>
 	<script src="<?php echo base_url(); ?>public/js/bootstrap.min.js" type="text/javascript"></script>

</head>
<body class="login" style="background: #ebe9e8;">
	<div style="position: absolute; margin: 0;opacity:0.49; z-index: 0;padding: 0; background: #000; height: 100%; width: 100%; top:0; left: 0"></div>
	<div class="container" style="color:#fff;">
		<nav class="navbar navbar-default">
			<div class="container">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
							<a class="navbar-brand" href="#"></a>
			</div>

			<div class="collapse navbar-collapse pull-right" id="bs-navbar-collapse-1">
							<ul class="nav navbar-nav nav-right">
							</ul>
						</div>
			</div>
		</nav>
