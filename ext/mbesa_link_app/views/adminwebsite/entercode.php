<div class="row">

<div class="col-md-6 col-md-offset-3" style="background: #fff; border-radius: 5px; color: #000; opacity: 6.5; z-index: 999;">
<center><img src="<?php echo base_url(); ?>public/images/logo.png"  style="height: 120px" ></center>
	<?php
                $tempdata = $this->session->flashdata('tempdata');
                $alert="info";
                    if (strpos(strtoupper($tempdata), "SORRY")>-1) {
                        $alert="danger";
                    }
                if (isset($tempdata) && !empty($tempdata)) {
                    ?>
          <br>
                    <div class="alert alert-<?=$alert; ?> alert-dismissible" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     <h5 class="text-center"> <?=$tempdata; ?></h5>
                    </div>
           <?php
                }    ?>
								<h3>Provide activation code sent during registration</h3>

	<?=form_open("/login/confirm"); ?>

		<input class="form-control" type="text" name="activationcode"  placeholder="Actication Code"  required="required"  />
		<br>

    <input  class="form-control" type="password" autocomplete="off" placeholder="New Password" required="required" name="password" />
		<br>

    <div class="row">
			<div class="col-md-6"><button class="btn btn-danger col-md-12" style="background: #0066b2; border:none" type="submit" >Next</button></div>
			<div class="col-md-6"><a class="btn btn-default col-md-12" style="text-decoration: none" href="<?php echo base_url(); ?>login/resend"> Resend Code</a></div>
		</div>
	</form><br>
</div>
</div>

<script src="<?php echo base_url(); ?>public/js/landing.page.skin.js" type="text/javascript"></script>
