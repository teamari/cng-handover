<div class="row">

<div class="col-md-6 col-md-offset-3" style="background: #fff; border-radius: 5px; color: #000; opacity: 6.5; z-index: 999;">
<center><img src="<?php echo base_url(); ?>public/images/logo.png"  style="height: 120px" ></center>
	<?php
                $tempdata = $this->session->flashdata('tempdata');
                $alert="info";
                    if (strpos(strtoupper($tempdata), "SORRY")>-1) {
                        $alert="danger";
                    }
                if (isset($tempdata) && !empty($tempdata)) {
                    ?>
          <br>
                    <div class="alert alert-<?=$alert; ?> alert-dismissible" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     <h5 class="text-center"> <?=$tempdata; ?></h5>
                    </div>
           <?php
                }    ?>
								<h3>Enter your password</h3>

	<?=form_open("/login/checkpass"); ?>

		<input  class="form-control" type="password" autocomplete="off" placeholder="Password" required="required" name="pass" />
		<br>
		<div class="row">
			<div class="col-md-6"><button class="btn btn-danger col-md-12" style="background: #0066b2; border:none" type="submit" >Log In</button></div>
			<div class="col-md-6"><a class="btn btn-default col-md-12" style="text-decoration: none" href="<?php echo base_url(); ?>login/forgetpass"> Forgot your password?</a></div>
		</div>
	</form><br>
</div>
</div>

<script src="<?php echo base_url(); ?>public/js/landing.page.skin.js" type="text/javascript"></script>
