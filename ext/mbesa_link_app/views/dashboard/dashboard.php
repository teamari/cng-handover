<!-- Begin Page Content -->
<div class="container-fluid">
	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
	</div>
	<!-- Content Row -->
	<div class="row">
		<!-- All Users -->
		<div class="col-xl-3 col-md-6 mb-4">
			<div class="card border-left-primary shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Users
							</div>
							<div class="h5 mb-0 font-weight-bold text-gray-800">
								<span class="info-box-number" id="users">0</span>
							</div>
						</div>
						<div class="col-auto">
							<a class="dropdown-item" href="<?php echo site_url('dashboard/users/'); ?>"><i class="fas fa-eye fa-2x text-gray-300"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php
		$bills = 0;
		$tills = 1;
		
		$bills = strtr(base64_encode($bills), '+/=', '._-');
		$tills = strtr(base64_encode($tills), '+/=', '._-');
						
						?>

		<!-- Pay Bills -->
		<div class="col-xl-3 col-md-6 mb-4">
			<div class="card border-left-success shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="text-xs font-weight-bold text-success text-uppercase mb-1">Pay Bills
							</div>
							<div class="h5 mb-0 font-weight-bold text-gray-800">
								<span class="info-box-number" id="bills">0</span>
							</div>
						</div>
						<div class="col-auto">
							<a class="dropdown-item" href="<?php echo site_url('dashboard/pay_bill/'.$bills); ?>"><i class="fas fa-eye fa-2x text-gray-300"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Till Numbers -->
		<div class="col-xl-3 col-md-6 mb-4">
			<div class="card border-left-info shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="text-xs font-weight-bold text-info text-uppercase mb-1">Till Numbers</div>
							<div class="row no-gutters align-items-center">
								<div class="col-auto">
									<div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
										<span class="info-box-number" id="tills">0</span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-auto">
							<a class="dropdown-item" href="<?php echo site_url('dashboard/pay_bill/'.$tills); ?>"><i class="fas fa-eye fa-2x text-gray-300"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<!-- /.container-fluid -->

<script type="text/javascript">
	function topusers() {
		$.ajax({
			url: "<?php echo base_url('trails/countusers') ?>",
			type: "get",
			success: function(data) {
				var i = JSON.parse(data);
				var d = i.aaData;

				$("#users").html(parseFloat(d[0][0]).toLocaleString());

			}
		});

	}

	function topbills() {
		$.ajax({
			url: "<?php echo base_url('trails/countbills') ?>",
			type: "get",
			success: function(data) {
				var i = JSON.parse(data);
				var d = i.aaData;

				$("#bills").html(parseFloat(d[0][0]).toLocaleString());

			}
		});

	}

	function toptills() {
		$.ajax({
			url: "<?php echo base_url('trails/counttills') ?>",
			type: "get",
			success: function(data) {
				var i = JSON.parse(data);
				var d = i.aaData;

				$("#tills").html(parseFloat(d[0][0]).toLocaleString());

			}
		});

	}

	function statsrefresh() {
			topusers();
			topbills();
			toptills();
			// var timeout = setTimeout(mysession, 60000); -->
	}
</script>
