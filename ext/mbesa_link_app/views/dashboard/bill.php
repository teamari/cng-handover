<!-- Begin Page Content -->
<div class="container-fluid">

	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">PayBill</h1>
		<a href="#" data-toggle="modal" data-target="#paybill" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-edit"></i> Edit</a>
	</div>
	<!-- Content Row -->



	<?php
    foreach ($bills_data as $bill) {
        ?>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<!-- Basic Card Example -->
				<div class="card shadow mb-4">
					<div class="card-body">
						<h5><?php echo 'Branch<span style="float:right; color:#7a8f6d;">'.$bill->branch.'</span>'; ?></h5>
						<br><br>
						<h5><?php echo 'Short Code<span style="float:right;  color:#7a8f6d;">'.$bill->shortCode.'</span>'; ?></h5>
						<br><br>
						<h5><?php echo 'API Key<span style="float:right;  color:#7a8f6d;">'.$bill->apiKey.'</span>'; ?></h5>
						<br><br>
						<h5><?php echo 'API Secret<span style="float:right; color:#7a8f6d; ">'.$bill->apiSecret.'</span>'; ?></h5>
						<br><br>
						<h5><?php echo 'Added By<span style="float:right;  color:#7a8f6d;">'.$bill->full_name.'</span>'; ?></h5>
					</div>
				</div>

			</div>
		</div>

		<!--This is the edit pay bill  -->
		<div class="modal fade" id="paybill" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="exampleModalLabel">PayBill Details</h4>
						<button class="close" type="button" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body">

						<div style="background: #fff; border-radius: 5px; opacity: 6.5; z-index: 999;">

							<?=form_open("/details/editpaybill"); ?>
							<input class="form-control" type="number" name="shortcode"  placeholder="Short Code"  required="required" value="<?php echo (isset($bill->shortCode) && isset($bill->shortCode)) ? $bill->shortCode : '';?>" />
							<br>

							<input class="form-control" type="text" name="branchname"  placeholder="Branch"  required="required" value="<?php echo (isset($bill->branch) && isset($bill->branch)) ? $bill->branch : '';?>" />
							<br>

							<input class="form-control" type="text" name="apikey"  placeholder="API Key"  required="required"  value="<?php echo (isset($bill->apiKey) && isset($bill->apiKey)) ? $bill->apiKey : '';?>"/>
							<br>

							<input class="form-control" type="text" name="apisecret"  placeholder="API Secret"  required="required" value="<?php echo (isset($bill->apiSecret) && isset($bill->apiSecret)) ? $bill->apiSecret : '';?>" />
							<br>

							<input class="form-control" type="hidden" name="orig_bills"   required="required" value="<?php echo $orig_bill; ?>" />
							<br>
							<div class="row">
								<div class="col-md-12"><button class="btn col-md-12" style="background: #0066b2; border:none; color:white;" type="submit" >Edit</button></div>
							</div>
						</form><br>
					</div>
				</div>
			</div>
		</div>
	</div>

		<?php
    }
    ?>


</div>
<!-- /.container-fluid -->
