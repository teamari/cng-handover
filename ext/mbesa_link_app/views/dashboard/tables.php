<!--  Begin Page Content -->
<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Users</h6>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="input-daterange input-group col-md-12" id="datepicker">
                <input type="text" class="input-sm form-control" id="from" name="start" placeholder="From date" />
                <span class="input-group-addon">to</span>
                <input type="text" class="input-sm form-control" id="to" name="end" placeholder="To date" />
            </div>
        </div>
        <div class="col-md-3">
            <button class="btn btn-primary col-md-6" id="btnsearch"><i class="fa fa-search"></i></button>
        </div>
    </div>
    <div class="card-body">
      <div class="table-responsive">
      <table class="ui celled table stripe" width="100%" id="datatable">
          <thead>
              <th>Name</th>
              <th>E-mail</th>
              <th>Phone Number</th>
              <th>Activity</th>
              <th>Enable</th>
          </table>
      </div>
    </div>
  </div>
</div>
<!-- /.container-fluid -->

    <script type="text/javascript">
        var chart1;
        function noo(val){
           try{
             return parseFloat(val).toLocaleString();
         }catch(e){
             return val;
         }
     }
     function intVal(i) {
        return typeof i === 'string' ?
        i.replace(/[\$,]/g, '') * 1 :
        typeof i === 'number' ?
        i : 0;
    }

    $(document).ready(function() {
        $('.input-daterange').datepicker({});
        $('#from').val(moment().subtract(1, 'months').format('MM/D/YYYY'));
        $('#to').val(moment().endOf('month').format('MM/D/YYYY'));

        $('#btnsearch').click(function() {
            var table = $('#datatable').DataTable({
                destroy: true,
                responsive: false,
                "ajax": {
                    url: '<?php echo base_url('trails/getusers') ?>',
                    type: 'get',
                    data: {}
                },
                "order": [
                [0, "asc"]
                ],
                select: {
                    style: 'single'
                },
                language: {
                    searchPlaceholder: "Search records.."
                },
                scrollY: "800px",
                scrollX: true,
                scrollCollapse: true,
                paging: true,
                fixedColumns: true,
                lengthChange: true,
                buttons: ['excelHtml5', 'pdfHtml5', 'colvis'],
                "rowCallback": function(nRow, aData) {
                  if (aData[3] == "Inactive")
                    $('td', nRow).eq(3).html('<label class="label label-danger">' + aData[3] + '</label>');
                else if (aData[3] == "Active")
                    $('td', nRow).eq(3).html('<label class="label label-success">' + aData[3] + '</label>');
                else
                    $('td', nRow).eq(3).html('<label class="label label-info">' + aData[3] + '</label>');

            }
        });

            var buttons = new $.fn.dataTable.Buttons(table, {
                buttons: ['excelHtml5', 'pdfHtml5', 'colvis']
            });
            buttons.container().appendTo($('div.right.aligned.eight.column:eq(0)', table.table().container()));
        });

        $('#btnsearch').click();
    });
</script>
