<!-- Begin Page Content -->
<div class="container-fluid">
	<?php
	$tempdata = $this->session->flashdata('tempdata');
	$alert="info";
	if (strpos(strtoupper($tempdata), "SORRY")>-1) {
		$alert="danger";
	}
	if (isset($tempdata) && !empty($tempdata)) {
		?>
		<br>
		<div class="alert alert-<?=$alert; ?> alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h5 class="text-center"> <?=$tempdata; ?></h5>
		</div>
		<?php
	}    ?>
	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">My Profile</h1>
		<a href="#" data-toggle="modal" data-target="#paybill" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-edit"></i>&nbsp; &nbsp; Edit</a>
	</div>
	<!-- Content Row -->
	<div class="row">
		<div class="col-lg-12 mb-4">

			<!-- Illustrations -->
			<div class="card shadow mb-4">
				<div class="card-body">
					<div class="text-center">
						<?php $photo = $this->session->userdata('photo'); ?>
						<img class="img-profile rounded-circle" style="width: 10rem; height: 10rem;" src="<?php echo base_url('uploads/'.$photo) ?>" alt="profile picture">
						<br><br>
						<a href="#" data-toggle="modal" data-target="#photo" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-folder-plusfas fa-edit"></i>&nbsp; Update Photo</a>
						<br><br>

						<?php
						foreach ($users_data as $bill) {
							?>
							<h5><?php echo 'Full Name         <span style="color:#7a8f6d;">'.$bill->full_name.'</span>'; ?></h5>
							<br><br>
							<h5><?php echo 'E-mail         <span style="color:#7a8f6d;">'.$bill->email.'</span>'; ?></h5>
							<br><br>
							<h5><?php echo 'Phone         <span style="color:#7a8f6d;">'.$bill->phone.'</span>'; ?></h5>
							<br><br>
							<?php
						}
						?>

					</div>
				</div>
			</div>
		</div>
	</div>

	<!--This is the add new pay bill  -->
	<div class="modal fade" id="paybill" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="exampleModalLabel">My profile Details</h4>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">

					<div style="background: #fff; border-radius: 5px; opacity: 6.5; z-index: 999;">

						<?=form_open("/details/editmyprofile"); ?>
						<input class="form-control" type="text" name="fullname"  placeholder="Full Name"  required="required" value="<?php echo (isset($bill->full_name) && isset($bill->full_name)) ? $bill->full_name : '';?>" />
						<br>

						<input class="form-control" type="text" name="phone"  placeholder="Phone"  required="required" value="<?php echo (isset($bill->phone) && isset($bill->phone)) ? $bill->phone : '';?>" />
						<br>

						<input class="form-control" type="email" name="useremail"  placeholder="Email"  required="required"  value="<?php echo (isset($bill->email) && isset($bill->email)) ? $bill->email : '';?>" />
						<br>

						<input class="form-control" type="text" name="address"  placeholder="Address"  value="<?php echo (isset($bill->address) && isset($bill->address)) ? $bill->address : '';?>" />
						<br>

						<input class="form-control" type="text" name="zip"  placeholder="Zip"  value="<?php echo (isset($bill->zip) && isset($bill->zip)) ? $bill->zip : '';?>" />
						<br>

						<input class="form-control" type="text" name="town"  placeholder="Town"  value="<?php echo (isset($bill->town) && isset($bill->town)) ? $bill->town : '';?>" />
						<br>

						<input class="form-control" type="text" name="country"  placeholder="Country"  value="<?php echo (isset($bill->country) && isset($bill->country)) ? $bill->country : '';?>" />
						<br>

						<input class="form-control" type="hidden" name="orig_bills"   required="required" value="<?php echo $orig_bill; ?>" />
						<br>
						<div class="row">
							<div class="col-md-12"><button class="btn col-md-12" style="background: #0066b2; border:none; color:white;" type="submit" >Edit</button></div>
						</div>
					</form><br>
				</div>
			</div>
		</div>
	</div>
</div>

<!--This is the add new pay bill  -->
<div class="modal fade" id="photo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="exampleModalLabel">New Photo</h4>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">

				<div style="background: #fff; border-radius: 5px; opacity: 6.5; z-index: 999;">

					<?= form_open_multipart('/details/do_upload'); ?>
					<input type="file" name="userfile" size='20' value="New Photo" />
					<br>
					<input class="form-control" type="hidden" name="orig_bills"   required="required" value="<?php echo $orig_bill; ?>" />
					<br>
					<div class="row">
						<div class="col-md-12"><button class="btn col-md-12" style="background: #0066b2; border:none; color:white;" type="submit" >Upload</button></div>
					</div>
				</form><br>
			</div>
		</div>
	</div>
</div>
</div>

</div>
<!-- /.container-fluid -->
