<!-- Begin Page Content -->
<div class="container-fluid">
	<?php
	$tempdata = $this->session->flashdata('tempdata');
	$alert="info";
	if (strpos(strtoupper($tempdata), "SORRY")>-1) {
		$alert="danger";
	}
	if (isset($tempdata) && !empty($tempdata)) {
		?>
		<br>
		<div class="alert alert-<?=$alert; ?> alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h5 class="text-center"> <?=$tempdata; ?></h5>
		</div>
		<?php
	}    ?>
	<!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
		<h1 class="h3 mb-0 text-gray-800">Short Codes</h1>
		<a href="#" data-toggle="modal" data-target="#paybill" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-folder-plus"></i> Add New</a>
	</div>
	<!-- Content Row -->
	<div class="row">
		<!-- Pay Bills -->

		<?php
		$start = 0;
		$status_code = '';
		$user_type_string = '';

		foreach ($bills_data as $bill) {			
			$cred_id = strtr(base64_encode($bill->credid), '+/=', '._-');

			$enabled = $bill->enabled;
			$deleted = $bill->deleted;
			$type = $bill->type;
			$registered= $bill->registered;

			$code = $bill->shortCode;
			$key = $bill->apiKey;
			$sec = $bill->apiSecret;


			if($type == 0){
				$type_status = 'Paybill';
			}elseif ($type == 1) {
				$type_status = 'Till No.';
			}

			if($registered == 0){
				$egistration_status = '<span style="color:#e01e10">Unregistered</span>';
				$egistration_action = '<span style="color:#65ad11">Register</span>';
				$registerval = 1;
				$registerval = strtr(base64_encode($registerval), '+/=', '._-');
			}elseif ($registered == 1) {
				$egistration_status = '<span style="color:#65ad11">Registered</span>';
				$egistration_action = '<span style="color:#e01e10">Unregister</span>';
				$registerval = 0;
				$registerval = strtr(base64_encode($registerval), '+/=', '._-');
				
			}

			if($enabled == 0){
				$current_status = '<span style="color:#e01e10">No</span>';
			}elseif ($enabled == 1) {
				$current_status = '<span style="color:#65ad11">Yes</span>';
			}

			if($enabled == 0){
				$enabled_status = '<span style="color:#65ad11">Enable</span>';
				$enable_val = 1;
			}elseif ($enabled == 1) {
				$enabled_status = '<span style="color:#e01e10">Disable</span>';
				$enable_val = 0;
			}

			if($deleted == 0){
				$deleted_status = '<span style="color:#e01e10">Delete</span>';
				$delete_val = 1;
			}elseif ($deleted == 1) {
				$deleted_status = '<span style="color:#65ad11">Undelete</span>';
				$delete_val = 0;
			}

			$enable_val = strtr(base64_encode($enable_val), '+/=', '._-');
			$delete_val = strtr(base64_encode($delete_val), '+/=', '._-');
			$code = strtr(base64_encode($code), '+/=', '._-');
			$key = strtr(base64_encode($key), '+/=', '._-');
			$sec = strtr(base64_encode($sec), '+/=', '._-');

			?>


			<div class="col-xl-3 col-md-7 mb-5">
				<div class="card shadow mb-4">
					<!-- Card Header - Dropdown -->
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<h6 class="m-0 font-weight-bold text-primary"><?php echo $bill->branch; ?></h6>
						<div class="dropdown no-arrow">
							<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
							</a>
							<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
								<div class="dropdown-header">Action:</div>
								<a class="dropdown-item" href="<?php echo site_url('dashboard/paybill/'.$cred_id); ?>">View</a>
								<a class="dropdown-item" href="<?php echo site_url('dashboard/enable/'.$cred_id.'/'.$enable_val); ?>"><?php echo $enabled_status;?></a>
								<?php
								if($registered == 0){
									?>
									<a class="dropdown-item" href="<?php echo site_url('payments/doit/'.$code.'/'.$key.'/'.$sec.'/'.$cred_id.'/'.$registerval); ?>"><?php echo $egistration_action;?></a>
									<?php
								}
								?>
								<!--	<a class="dropdown-item" href="<?php echo site_url('dashboard/delete/'.$cred_id.'/'.$delete_val); ?>"><?php echo $deleted_status;?></a> -->
							</div>
						</div>
					</div>
					<!-- Card Body -->
					<div class="card-body">
						<div class="row no-gutters align-items-center">
							<div class="col mr-2">
								<div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo 'Shortcode <span style="float:right;">'.$bill->shortCode.'</span>'; ?></div>
								<div class="text-xs font-weight-bold text-gray-800 text-uppercase mb-1"><?php echo 'Type  <span style="float:right;">'.$type_status.'</span>'; ?></div>
								<div class="text-xs font-weight-bold text-gray-800 text-uppercase mb-1"><?php echo 'Enabled? <span style="float:right;">'.$current_status.'</span>'; ?></div>
								<div class="text-xs font-weight-bold text-gray-800 text-uppercase mb-1"><?php echo 'Registration <span style="float:right;">'.$egistration_status.'</span>'; ?></div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<?php
		}
		?>

	</div>

	<!--This is the add new pay bill  -->
	<div class="modal fade" id="paybill" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="exampleModalLabel">PayBill Details</h4>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">

					<div style="background: #fff; border-radius: 5px; opacity: 6.5; z-index: 999;">

						<?=form_open("/details/addpaybill"); ?>
						<input class="form-control" type="number" name="shortcode"  placeholder="Short Code"  required="required"  />
						<br>

						<input class="form-control" type="text" name="branchname"  placeholder="Branch"  required="required"  />
						<br>

						<select class="form-control" name="type" required>
							<option selected="true" disabled="disabled" value="">Type</option>
							<option value="0">Paybill</option>
							<option value="1">Till No</option>
						</select>
						<br>

						<input class="form-control" type="text" name="apikey"  placeholder="API Key"  required="required"  />
						<br>

						<input class="form-control" type="text" name="apisecret"  placeholder="API Secret"  required="required"  />
						<br>

						<div class="row">
							<div class="col-md-12"><button class="btn col-md-12" style="background: #0066b2; border:none; color:white;" type="submit" >Save</button></div>
						</div>
					</form><br>
				</div>
			</div>
		</div>
	</div>
</div>

</div>
<!-- /.container-fluid -->
