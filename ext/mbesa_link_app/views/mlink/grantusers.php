<style>
    .modal-dialog {
         width: 900px;
    }
</style>
<section class="content-header">
    <h1>Assign Users Portal</h1>
    
    <ol class="breadcrumb">
        <li><a href="<?=APP_BASE;?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?=$this->uri->segment(3);?></li>
    </ol>
</section>

<?php
$this->load->model('usersmodel');
$users = $this->usersmodel->getuserslist();
$credentials = $this->usersmodel->getcredentialslist();
?>


<div class="content-top-1 box box-success">
    <div class="box-body">
	<?=form_open("/users/addGrant");?>
        <div class="row">
                    <div class="col-md-12">
                       <label class="label label-danger" id="error"></label>
                       <br>
                       <label for="userid">Select User<span class="required">*</span></label>
					   <select class="custom-select custom-select-sm " id="userid" name="userid">
                                <option selected disabled >Select User</option>
                                <?php
                                    foreach ($users as $user) {
                                        echo '<option value="' . $user['tid'] . '">' . $user['UserCode'] . '</option>';
                                    }
                                ?>
                            </select>
							<span class="message" style="color:#25D84F"></span>
						<div id="show">
						</div>
						<div id="but">
							<div class="box-header">
								<div class="box-tools pull-right" style="padding-right: 30px">
									&nbsp;&nbsp;
									<button type="button" id="disableuser" class="btn btn-danger" ><i class="fa fa-ban"></i> Remove</button>
								</div>
							</div>
						</div>
						<br><br>
							
							 <label>Select Paybill to access <span class="required">*</span></label>
							 <?php
                                    foreach ($credentials as $cred) {
                                        echo '<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
										&nbsp; <input type="checkbox" name="cred[]" value="'.$cred['id'].'">&nbsp; &nbsp; &nbsp; &nbsp;'.$cred['branch'];
                                    }
                                ?>
			     	</div>
        </div>
		 <div class="modal-footer">
                <button type="submit" class="btn btn-primary ">Grant Portal</button>
			</div>
			</form>
    </div>
</div>

<script>
		$( document ).ready(function() {
			$('#but').hide();
			userchange();
		});
		
		function userchange(){
		$('#userid').change(function(){
			$("#show").empty();
			$(".message").empty();
			var id = $(this).val();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>" + "users/getportalgrantslisting",
				data: {
					userid: $(this).val()
				},
				dataType: 'json',
				success: function(data) {
					if(data.length > 0){
						$.each(data, function(index) {
							var check = $('<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <input type="checkbox" class="assigned" name="cred" value="' + data[index].portalgrantid + '">&nbsp; &nbsp; &nbsp; &nbsp;' + data[index].branch +
								'</div>');
							$('#show').append(check);
							
							$(".assigned").change(function() {
								$(".assigned").prop('checked', false);
								$(this).prop('checked', true);
								$('#but').show();
								
								$('#disableuser').click( function () {
									console.log($('.assigned:checked').val());
									$.ajax({
										url:"/users/deleteportalaccess",
										data: { id:$('.assigned:checked').val()},
										type:"post",
										success:function(data){
											$('.message').html(data);
											$(".btn").removeAttr("disabled");
										}
									});
								});
							});
						});
					}
				},
				error: function(response) {
					console.log('error loading');
				},
			});
		});
		}
		
	</script>