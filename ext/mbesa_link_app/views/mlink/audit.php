<section class="content-header">
    <h1>Audit Report</h1>
    <ol class="breadcrumb">
        <li><a href="<?=APP_BASE;?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?=$this->uri->segment(3);?></li>
    </ol>
</section>

<div class="content-top-1 box box-info">
        <div class="box-body">
                    <div class="row">
                                <div class="col-md-4">
                                    <div class="input-daterange input-group" id="datepicker">
                                        <input type="text" class="input-sm form-control" id="from" name="start" placeholder="From date" />
                                        <span class="input-group-addon">to</span>
                                        <input type="text" class="input-sm form-control" id="to" name="end"  placeholder="To date" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <button class="btn btn-primary" id="btnsearch"><i class="fa fa-search"></i></button>
                                </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12" style="padding-left: 30px">
                            <table  class="ui celled table stripe" width="100%" id="datatable">
                                <thead><th>Who?</th><th>Did What?</th><th>When?</th><th>Device Address</th> <th>Sys Status</th></thead>
                            </table>
                        </div>
                    </div>
        </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        $('.input-daterange').datepicker({});
        $('#from').val(moment().startOf('month').format('MM/D/YYYY'));
        $('#to').val(moment().endOf('month').format('MM/D/YYYY') );

        $('#btnsearch').click(function(){
            var table= $('#datatable').DataTable({
              destroy: true,
              responsive: false,
               "ajax":{
                   url:'/tables/leta_audittrail',
                   type: 'get',
                   data: {from: $('#from').val(),to: $('#to').val()}
               },
               "order": [[ 2, "desc" ]],
               select: {
                   style: 'none',
                   selector: 'td:first-child'
               },
               language: {
                    searchPlaceholder: "Search records.."
                },
                scrollY:        "800px",
                scrollX:        true,
                scrollCollapse: true,
                paging:         true,
                fixedColumns:   true,
                lengthChange: true,
                buttons: [ 'excelHtml5', 'pdfHtml5', 'colvis' ],
                "rowCallback": function( nRow, aData) {
                    
                    $('td', nRow).eq(3).html('<label class="label label-default">'+ aData[3] +'</label>');
                    $('td', nRow).eq(6).html('<label class="label label-default">'+ aData[6] +'</label>');
                    if(aData[4]=='OK')
                     $('td', nRow).eq(4).html('<label class="label label-success">OK</label>');
                    else
                    $('td', nRow).eq(4).html('<label class="label label-danger">FAIL</label>');
                }
    	});

        var buttons = new $.fn.dataTable.Buttons(table, {
          buttons: [  'excelHtml5', 'pdfHtml5', 'colvis']
        });
        buttons.container().appendTo($('div.right.aligned.eight.column:eq(0)', table.table().container()));
        });

        $('#btnsearch').click();
	});
</script>