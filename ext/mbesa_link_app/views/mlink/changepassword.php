<div class="content-top">
    <br><br>
    <div class="row">
            <div class="col-md-4 col-md-offset-3 tablecontainer">
                        <h3>Change your password</h3> <br>
                        <?=form_open("/users/reset");?>
                          <label class="label label-default">Current Password</label>
                          <div class="form-group has-feedback">
                            <input type="password" id="curpass" name="password" class="form-control" required pattern="^.{6,}" title="Password should be atleast 6 characters"  placeholder="Current Password"/>
                          </div>
                          <label class="label label-default">New Password</label>
                          <div class="form-group has-feedback">
                            <input type="password" id="pass" name="newpassword" class="form-control" required pattern="^.{6,}" title="Password should be atleast 6 characters"  placeholder="Desired Password"/>
                          </div>
                          <label class="label label-default">Retype New Password</label>
                          <div class="form-group has-feedback">
                            <input type="password" id="cpass" class="form-control" name="cpassword" required pattern="^.{6,}" title="Password should be atleast 6 characters"  placeholder="Retype desired password"/>
                          </div>
                          <div><label id="label" class="label label-danger"></label></div>

                          <br><button type="submit" class="btn btn-danger btn-block">Reset Password</button>
                        </form>
                        <br>
            </div>
    </div>
</div>

<script type="text/javascript">
    $(function(){
       $('#pass, #cpass').change(function(){
            var pass = $('#pass').val();
            var cpass = $('#cpass').val();
            if(pass!="" && cpass != ""){
                var newpass = $('#curpass').val();
                if(pass!=cpass){
                    $('#cpass').val('');
                    $('#label').html("Password and Confirmation passwords do not match");
                } else if(pass==newpass){
                    $('#pass').val('');
                    $('#label').html("New Password cannot be the same as the current password");
                } else
                   $('#label').html("");
            }
       });
    })
</script>