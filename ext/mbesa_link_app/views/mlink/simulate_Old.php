<section class="content-header">
    <h1>Simulate</h1>
    
    <ol class="breadcrumb">
        <li><a href="<?=APP_BASE;?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?=$this->uri->segment(3);?></li>
    </ol>
</section>


<div class="content-top-1 box box-info">
    <div class="box-body">
            <br>
            <div class="row">
                <div class="col-md-8">
                    Note: For this to work, ensure you machine can receive traffic from safaricom. <br>
                    i.e. reachable via a public IP <br><br>

                    For test purposes, you can use ngrok https://ngrok.com/ to expose your machine to the world to achieve test.
                    <br><br>
                    <?=form_open_multipart("/mpesa/index/simulate");?>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-money"></i> Simulate payment of ksh  1000 with Phone No. 254708374149 </button>
                    </form>
                    <br><br>
                </div>  
            </div>
            
    </div>
</div>