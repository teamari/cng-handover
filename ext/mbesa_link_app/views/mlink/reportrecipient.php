<style>
    .modal-dialog {
         width: 900px;
    }
</style>
<section class="content-header">
    <h1>Reports Recipient</h1>
    
    <ol class="breadcrumb">
        <li><a href="<?=APP_BASE;?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?=$this->uri->segment(3);?></li>
    </ol>
</section>

<div class="content-top-1 box box-info">
        <br>
        <div class="box-header">
            <div class="box-tools pull-right" style="padding-right: 30px">
            &nbsp;&nbsp;
            <a class="btn btn-success"  data-target="#usersmodal" data-toggle="modal" ><i class="fa fa-user"></i> Add  User</a>
            &nbsp;&nbsp;
            <button id="disableuser" class="btn btn-danger" ><i class="fa fa-ban"></i> Deactivate User</button>
            </div>
        </div>

        <div class="box-body">
            <br><br>
            <div class="col-md-12">
                <div id="disablelabel" class="label label-info"></div>
                <table id="usersTable" class="ui celled table stripe" cellspacing="0" width="100%">
                <thead><th></th><th>Name</th><th>Email</th><th>Status</th></thead>
                </table>
                <!--<br>
                <a class="btn btn-info col-md-2 " id="btnkey" ><i class="fa fa-key"></i> Reset Password</a>-->
            </div>
        </div>
    </div>

<div id="usersmodal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Report Recipients</h4>
			</div>

            <?=form_open("/details/addrecipientuser");?>
			<div class="modal-body">
			     <div class="row">
			     	<div class="col-md-12">
                        <label for="name">Name <span class="required">*</span></label>
						<input class="form-control" required="required" name="firstname" placeholder="Name" type="text" >
                        <br>
                        <label for="email">Email <span class="required">*</span></label>
                        <input  name="email" class="form-control email" required="required" placeholder="Email address" type="email">
                        <br>
                    </div>
			     </div>
			</div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-primary ">Save</button>
			</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
var table;

function loadTable(){
        table= $('#usersTable').DataTable({
              destroy: true,
              responsive: true,
              "ajax":{
                   url:'/tables/fetchreportusers',
                   type: 'get'
               },
               select: {
                   style: 'single',
                   selector: 'td'
               },
               columnDefs: [{
                   orderable: false,
                   className: 'select-checkbox',
                   targets: 0
               }],
               language: {
                    searchPlaceholder: "Search records.."
                },
                scrollY:        "600px",
                scrollX:        true,
                scrollCollapse: true,
                paging:         true,
                fixedColumns:   true,
                lengthChange: true,
                buttons: [ 'excelHtml5', 'pdfHtml5', 'colvis' ],
                "rowCallback": function( nRow, aData) {
                    $('td', nRow).eq(0).html('<input type="hidden" class="select" value="'+ aData[0] +'">');
                }

    	});

        var buttons = new $.fn.dataTable.Buttons(table, {
          buttons: [  'excelHtml5', 'pdfHtml5', 'colvis']
        });
        buttons.container().appendTo($('div.right.aligned.eight.column:eq(0)', table.table().container()));
    }

	$(document).ready(function() {
	    
      $('#disableuser').click( function () {
            if(table.rows('.selected').data().length >0 ){
                $(".btn").attr("disabled","disabled");
                var row =table.rows('.selected').data()[0] ;
                $('#disablelabel').html("Processing Please Wait...");
                 $.ajax({
                    url:"/details/disableThis",
                    data: { usercode: btoa(row[0]) },
                    type:"get",
                    success:function(data){
                        alert(data);
                        $('#disablelabel').html("");
                         loadTable();
                         $(".btn").removeAttr("disabled");
                    }
                 });
            }else
                alert("please select a user to continue"); 
        });
        
        
        loadTable();

	});
</script>