<section class="content-header">
    <h1>Allocated Transactions</h1>
    <ol class="breadcrumb">
        <li><a href="<?=APP_BASE;?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?= $this->uri->segment(3); ?></li>
    </ol>
</section>

<div class="content-top-1 box box-success">
    <div class="box-body">
        <div class="row">
		

            <div class="col-lg-4">
                <div class="info-box bg-orange">
                    <a style="color:white;" href="<?php echo base_url('m_link/platform/todaystransactions') ?>"><span class="info-box-icon"><i class="fa fa-users"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Today's Transactions</span>
                        <br>
                        <span class="info-box-number" id="transactions">0</span>
                    </div>
					</a>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="info-box bg-purple">
                    <a style="color:white;" href="<?php echo base_url('m_link/platform/todaystransactions') ?>"><span class="info-box-icon"><i class="fa fa-money"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Totalling (KSH)</span>
                        <br>
                        <span class="info-box-number" id="total">0</span>
                    </div>
					</a>
                </div>
            </div>
			
        </div>
    </div>
</div>
<br>

<div class="content-top-1 box box-info">
    <div class="box-body">
        <br>
        <div class="row">
            <div class="col-md-4">
                <div class="input-daterange input-group col-md-12" id="datepicker">
                    <input type="text" class="input-sm form-control" id="from" name="start" placeholder="From date" />
                    <span class="input-group-addon">to</span>
                    <input type="text" class="input-sm form-control" id="to" name="end" placeholder="To date" />
                </div>
            </div>
            <div class="col-md-3">
                <button class="btn btn-primary col-md-6" id="btnsearch"><i class="fa fa-search"></i></button>
            </div>
        </div>
        <br>
        <div class="row">

            <div class="col-md-12" style="padding-left: 30px">
                <table class="ui celled table stripe" width="100%" id="datatable">
                    <thead>
                        <th>Customer</th>
                        <th>Phone Number</th>
                        <th>Paid KSH</th>
                        <th>On</th>
                        <th>Balance KSH</th>
                        <th>Mpesa Code</th>
                        <th>SAP Doc.</th>
                        <th>Branch</th>
                        <th>SAP Doc. Date</th>
                        <th>SAP Doc. Total KSH</th>
                       <!-- <th>Status</th>-->
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var chart1;
function noo(val){
	try{
			return parseFloat(val).toLocaleString();
		}catch(e){
			return val;
		}	
	}
	
	function topStats() {
        $.ajax({
            url: "/tables/topStats",
            type: "get",
            success: function(data) {
                var i = JSON.parse(data);
                var d = i.aaData;
				
                $("#transactions").html(d[0][0]);
                $("#total").html(parseFloat(d[0][1]).toLocaleString());
               
            }
        });

    }
    function intVal(i) {
        return typeof i === 'string' ?
            i.replace(/[\$,]/g, '') * 1 :
            typeof i === 'number' ?
            i : 0;
    }

    $(document).ready(function() {
        $('.input-daterange').datepicker({});
        $('#from').val(moment().startOf('months').format('MM/D/YYYY'));
        $('#to').val(moment().endOf('month').format('MM/D/YYYY'));

        $('#btnsearch').click(function() {
            var table = $('#datatable').DataTable({
                destroy: true,
                responsive: false,
                "ajax": {
                    url: '/tables/leta_trans_status',
                    type: 'get',
                    data: {
                        from: $('#from').val(),
                        to: $('#to').val(),
                        status: 'N'
                    }
                },
                "order": [
                    [8, "desc"]
                ],
                select: {
                    style: 'single'
                },
                language: {
                    searchPlaceholder: "Search records.."
                },
                scrollY: "800px",
                scrollX: true,
                scrollCollapse: true,
                paging: true,
                fixedColumns: true,
                lengthChange: true,
                buttons: ['excelHtml5', 'pdfHtml5', 'colvis'],
                "rowCallback": function(nRow, aData) {
					$('td', nRow).eq(2).html(noo(aData[2]));
					$('td', nRow).eq(4).html(noo(aData[4]));
					if(  aData[9]!="undefined"  && aData[9]!=""  && aData[9]!=" "  && aData[9]!=null )
						$('td', nRow).eq(9).html(noo(aData[9]));
                    if (aData[10] == "Failed")
                        $('td', nRow).eq(10).html('<label class="label label-danger">' + aData[10] + '</label>');
                    else if (aData[10] == "Ok")
                        $('td', nRow).eq(10).html('<label class="label label-success">' + aData[10] + '</label>');
                    else
                        $('td', nRow).eq(10).html('<label class="label label-info">' + aData[10] + '</label>');

                }
            });

            var buttons = new $.fn.dataTable.Buttons(table, {
                buttons: ['excelHtml5', 'pdfHtml5', 'colvis']
            });
            buttons.container().appendTo($('div.right.aligned.eight.column:eq(0)', table.table().container()));
        });

        $('#btnsearch').click();
    });
	
	function statsrefresh() {
        topStats();
       // var timeout = setTimeout(statsrefresh, 10000);
    }

    $(document).ready(function() {
        statsrefresh();
    });
	
</script>