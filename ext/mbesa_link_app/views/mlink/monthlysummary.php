<head>
  <!-- Custom styles for this template-->
  <link href="/_public/js/charts/sb-admin-2.css" rel="stylesheet">

</head>

<section class="content-header">
  <h1>Monthly Summary</h1>
  <ol class="breadcrumb">
    <li><a href="<?=APP_BASE;?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?= $this->uri->segment(3); ?></li>
  </ol>
</section>

<div class="content-top-1 box box-success">
  <div class="box-body">
    <div class="row">
      

      <div class="col-xl-6 col-lg-6">

        <!-- Area Chart -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Total Income per day for the last 30 days</h6>
          </div>
          <div class="card-body">
            <div class="chart-area">
              <canvas id="myAreaChart"></canvas>
            </div>
            <hr>
            This is a summary of the total income amount this year.
          </div>
        </div>
      </div>

      <!-- Donut Chart -->
      <div class="col-xl-6 col-lg-6">
        <!-- Bar Chart -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Allocated summary per month for this year</h6>
          </div>
          <div class="card-body">
            <div class="chart-bar">
              <canvas id="myBarChart"></canvas>
            </div>
            <hr>
            This giving a summary of the <code>allocated</code> amounts.
          </div>
        </div>
        
    <!--
        <div class="card shadow mb-4">
          <!-- Card Header - Dropdown --
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"> Allocations' Summary</h6>
          </div>
          <!-- Card Body --
          <div class="card-body">
            <div class="chart-pie pt-4">
              <canvas id="myPieChart"></canvas>
            </div>
            <hr>
            A summary of the totals for <code>this year</code>.
          </div>
        </div> -->
      </div>
      
    </div>
  </div>
</div>
<br>
<script src="/_public/js/charts/jquery.easing.min.js"></script>
<!-- Page level plugins -->
<script src="/_public/js/charts/Chart.min.js"></script>

<!-- Page level custom scripts -->
<script src="/_public/js/charts/chart-area-demo.js"></script>
<script src="/_public/js/charts/chart-pie-demo.js"></script>
<script src="/_public/js/charts/chart-bar-demo.js"></script>