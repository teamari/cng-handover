<section class="content-header">
    <h1><?= ucfirst(strtolower(base64_decode(urldecode($this->uri->segment(7))))); ?> M-pesa Payments</h1>
    <ol class="breadcrumb">
        <li><a href="<?=APP_BASE;?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?= $this->uri->segment(3); ?></li>
    </ol>
</section>

<div class="content-top-1 box box-info">
    <div class="box-body"><br>
        <div class="row">

            <div class="col-md-12" style="padding-left: 30px">
                <table class="ui celled table stripe" width="100%" id="datatable">
                    <thead>
                        <th>Client Name</th>
                        <th>Date</th>
                        <th>Paid</th>
                        <th>M-pesa Code</th>
                        <th>SAP Document No.</th>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var chart1;
	function noo(val){
		try{
			return parseFloat(val).toLocaleString();
		}catch(e){
			return val;
		}
		
	}
	
    $(document).ready(function() {
        var table = $('#datatable').DataTable({
            destroy: true,
            responsive: false,
            "ajax": {
                url: '/tables/leta_customer_history',
                type: 'get',
                data: {
                    from: atob(decodeURIComponent('<?= $this->uri->segment(5); ?>')),
                    to: atob(decodeURIComponent('<?= $this->uri->segment(6); ?>')),
                    id: atob(decodeURIComponent('<?= $this->uri->segment(4); ?>'))
                }
            },
            "order": [
                [1, "desc"]
            ],
            select: {
                style: 'single'
            },
            language: {
                searchPlaceholder: "Search records.."
            },
            scrollY: "800px",
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            fixedColumns: true,
            lengthChange: true,
            buttons: ['excelHtml5', 'pdfHtml5', 'colvis'],
            "rowCallback": function(nRow, aData) {
				$('td', nRow).eq(2).html(noo(aData[2]));
			}
        });

        var buttons = new $.fn.dataTable.Buttons(table, {
            buttons: ['excelHtml5', 'pdfHtml5', 'colvis']
        });
        buttons.container().appendTo($('div.right.aligned.eight.column:eq(0)', table.table().container()));
    });
</script>