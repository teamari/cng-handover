<section class="content-header">
    <h1>Customers Payments</h1>
    <ol class="breadcrumb">
        <li><a href="<?=APP_BASE;?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?=$this->uri->segment(3);?></li>
    </ol>
</section>

<div class="content-top-1 box box-info">
        <div class="box-body">
            <br>
                    <div class="row">
                                <div class="col-md-4">
                                    <div class="input-daterange input-group col-md-12" id="datepicker">
                                        <input type="text" class="input-sm form-control" id="from" name="start" placeholder="From date" />
                                        <span class="input-group-addon">to</span>
                                        <input type="text" class="input-sm form-control" id="to" name="end"  placeholder="To date" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <button class="btn btn-primary col-md-6" id="btnsearch"><i class="fa fa-search"></i></button>
                                </div> 
                    </div>
                    <br>
                    <div class="row">
                    
                        <div class="col-md-12" style="padding-left: 30px">
                            <table  class="ui celled table stripe" width="100%" id="datatable">
                                <thead>
                                   <th>Customer Name</th>
                                   <th>Phone Number</th>
                                   <th>Paid KSH</th>
                                   <th>Balance KSH</th>
                                   <!--<th>Branch</th>-->
                                   <th></th>
                            </table>
                        </div>
                    </div>
        </div>
</div>

<script type="text/javascript">
    var chart1;

    function intVal( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
    } 
    
	function noo(val){
		try{
			return parseFloat(val).toLocaleString();
		}catch(e){
			return val;
		}
		
	}
    $(document).ready(function() {
        $('.input-daterange').datepicker({});
        $('#from').val(moment().subtract(12, 'months').format('MM/D/YYYY'));
        $('#to').val(moment().endOf('month').format('MM/D/YYYY') );
        
        $('#btnsearch').click(function(){
            var table= $('#datatable').DataTable({
              destroy: true,
              responsive: false,
               "ajax":{
                   url:'/tables/leta_customers_trans',
                   type: 'get',
                   data: {from: $('#from').val(),to: $('#to').val() ,id: '<?=$this->uri->segment(4);?>'}
               },
               "order": [[1, "desc" ]],
               select: {
                   style: 'single'
               },
               language: {
                    searchPlaceholder: "Search records.."
                },
                scrollY:        "800px",
                scrollX:        true,
                scrollCollapse: true,
                paging:         true,
                fixedColumns:   true,
                lengthChange: true,
				
                buttons: [ 'excelHtml5', 'pdfHtml5', 'colvis' ],
                "rowCallback": function( nRow, aData) { 
				  $('td', nRow).eq(2).html(noo(aData[2]));
				  $('td', nRow).eq(3).html(noo(aData[3]));
                    $('td', nRow).eq(4).html("<a href='<?=APP_BASE;?>customer_history/"+ encodeURIComponent(btoa(aData[4])) +"/"+  encodeURIComponent(btoa($('#from').val())) +"/"+ encodeURIComponent(btoa($('#to').val()))  +"/"+ encodeURIComponent(btoa(aData[0])) +"' class='btn btn-sm btn-default'> View History </a>");
                },
            });

            var buttons = new $.fn.dataTable.Buttons(table, {
            buttons: [  'excelHtml5', 'pdfHtml5', 'colvis']
            });
            buttons.container().appendTo($('div.right.aligned.eight.column:eq(0)', table.table().container()));
        });

        $('#btnsearch').click();
	});
</script>