<section class="content-header">
  <h1>Reconciliation</h1>
  <ol class="breadcrumb">
    <li><a href="<?=APP_BASE;?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active"><?= $this->uri->segment(3); ?></li>
  </ol>
</section>

<div class="content-top-1 box box-info">
  <div class="box-body">
    <div class="row">
      <div class="col-md-12">
        <h3>Select an excel file to use for reconciliation</h3>
        
        <?=form_open_multipart("/import/save");?>
        <div class="row">
          <div class="col-lg-12 col-sm-12">
            <div class="form-group">
             <input  id="userfile" name="userfile" class="form-control-file" required="required" type="file">
           </div>
           <span style="color:red;">*Please choose an Excel file(.xls or .xlxs) as Input</span>
         </div>
         
         <div class="col-lg-12 col-sm-12">
          <div class="form-group" id="genotpdiv">
           <input id="genotp" name="genotp" class="btn btn-primary" value="Generate OTP">
         </div>
       </div>
       
       <div class="col-lg-12 col-sm-12">
        <div class="form-group" id="otherFieldDiv">
          <label for="otp">OTP <span class="required">*</span></label>
          <input  id="otherField" name="otp" class="form-control" required="required" title="Enter sent otp" placeholder="xxxx" type="text">
        </div>
      </div>
      
      <div class="col-lg-12 col-sm-12"><div class="form-group text-right" id="submitDiv">
        <input id="importfile-id" name="importfile" class="btn btn-primary" value="Import" type="submit">
      </div>
    </div>
  </div>
</form>



</div>
</div>
</div>
</div>


<script>
  $( document ).ready(function() {
   $('#otherFieldDiv').hide();
   $('#genotpdiv').hide();
   $('#submitDiv').hide();
 });


  
  $("#userfile").change(function() {
    $('#genotpdiv').show();
  });
  
  $("#genotp").button().click(function(){
   $('#genotpdiv').hide();
   $('#otherFieldDiv').show();
   $('#submitDiv').show();
   $.ajax({
     url: '/import/generateotp'
   });
 }); 
  
</script>