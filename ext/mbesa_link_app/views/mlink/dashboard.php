﻿<section class="content-header">
    <h1>Dashboard</h1>
    <ol class="breadcrumb">
        <li><a href="<?=APP_BASE;?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?= $this->uri->segment(3); ?></li>
    </ol>
</section>

<div class="content-top-1 box box-success">
    <div class="box-body">
        <div class="row">
		

            <div class="col-lg-4">
                <div class="info-box bg-orange">
                    <a style="color:white;" href="<?php echo base_url('m_link/platform/todaystransactions') ?>"><span class="info-box-icon"><i class="fa fa-users"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Today's Transactions</span>
                        <br>
                        <span class="info-box-number" id="transactions">0</span>
                    </div>
					</a>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="info-box bg-purple">
                    <a style="color:white;" href="<?php echo base_url('m_link/platform/todaystransactions') ?>"><span class="info-box-icon"><i class="fa fa-money"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Totalling (KSH)</span>
                        <br>
                        <span class="info-box-number" id="total">0</span>
                    </div>
					</a>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="info-box bg-green">
                    <a style="color:white;" href="<?php echo base_url('m_link/platform/todays_allocations') ?>"><span class="info-box-icon"><i class="fa fa-money"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">SAP Allocated (KSH)</span>
                        <br>
                        <span class="info-box-number" id="allocated">0</span>
                    </div>
					</a>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<div class="content-top-1 box box-info">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
				<h3>Today's Allocations</h3>
				<table class="ui celled table stripe" width="100%" id="datatable">
                    <thead>
                        <th>Customer</th>
                        <th>Phone Number</th>
                        <th>Paid KSH</th>
                        <th>On</th>
                        <th>Balance KSH</th>
                        <th>Mpesa Code</th>
                        <th>SAP Doc.</th>
                        <th>Branch</th>
                        <th>SAP Doc. Date</th>
                        <th>SAP Doc. Total KSH</th>
                        <!--<th>Status</th>-->
                </table>
                <!--<table class="ui celled table stripe" width="100%" id="datatable">
                    <thead>
                        <th>Date</th>
                        <th>Branch</th>
                        <th>No. of Transactions</th>
                        <th>Total (KSH)</th>
                    </thead>
                </table>-->
            </div>
            <!--<div class="col-md-6">
                <br>
                <canvas id="chart" style="height: 400px !important">
            </div>-->
        </div>
    </div>
</div>


<script type="text/javascript" src="/_public/js/mburu.js"></script>
<script type="text/javascript">
function noo(val){
		try{
			return parseFloat(val).toLocaleString();
		}catch(e){
			return val;
		}
		
	}
    function topStats() {
        $.ajax({
            url: "/tables/topStats",
            type: "get",
            success: function(data) {
                var i = JSON.parse(data);
                var d = i.aaData;
				
                $("#transactions").html(d[0][0]);
                $("#total").html(parseFloat(d[0][1]).toLocaleString());
               
            }
        });

    }

    function liststatistics() {
		var table = $('#datatable').DataTable({
                destroy: true,
                responsive: false,
                "ajax": {
                    url: '/tables/leta_trans_status',
                    type: 'get',
                    data: {
                        from: "<?=date('m/d/Y');?>",
                        to:  "<?=date('m/d/Y');?>",
                        status: 'N'
                    }
                },
                "order": [
                    [3, "desc"]
                ],
                select: {
                    style: 'single'
                },
                language: {
                    searchPlaceholder: "Search records.."
                },
                scrollY: "800px",
                scrollX: true,
                scrollCollapse: true,
                paging: true,
                fixedColumns: true,
                lengthChange: true,
                buttons: ['excelHtml5', 'pdfHtml5', 'colvis'],
                "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 9 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            $("#allocated").html(total.toLocaleString()); 
        }
            });

            var buttons = new $.fn.dataTable.Buttons(table, {
                buttons: ['excelHtml5', 'pdfHtml5', 'colvis']
            });
            buttons.container().appendTo($('div.right.aligned.eight.column:eq(0)', table.table().container()));
        
        /*var table = $('#datatable').DataTable({
            destroy: true,
            responsive: false,
            "ajax": {
                url: '/tables/listchart',
                type: 'get',
            },
            "order": [
                [0, "desc"]
            ],
            select: {
                style: 'single'
            },
            language: {
                searchPlaceholder: "Search records.."
            },
            scrollY: "800px",
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            fixedColumns: true,
            lengthChange: true,
            buttons: ['excelHtml5', 'pdfHtml5', 'colvis'],
            "rowCallback": function(nRow, aData) {
				$('td', nRow).eq(3).html(noo(aData[3]));
			}
        });
        var buttons = new $.fn.dataTable.Buttons(table, {
            buttons: ['excelHtml5', 'pdfHtml5', 'colvis']
        });
        buttons.container().appendTo($('div.right.aligned.eight.column:eq(0)', table.table().container()));
    */
	}

    function statsrefresh() {
        topStats();
        liststatistics();
       // var timeout = setTimeout(statsrefresh, 10000);
    }

    $(document).ready(function() {
        statsrefresh();
    });
</script>