var Login = function() {
   	
    return {
        init: function() {
            $('.login').backstretch([
				"/_public/images/bg/1.jpg"
                ], {
                  fade: 1000,
                  duration: 8000,
                  overlay: {
                    init: true
                  }
                }
            );
        }
    };

}();

jQuery(document).ready(function() {
    Login.init();
});